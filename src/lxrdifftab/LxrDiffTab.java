/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifftab;

import java.awt.SplashScreen;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import javafx.application.Application;
import javax.swing.JOptionPane;
import lxrdifferencetable.tools.LocalProperties;
import lxrdifferencetable.tools.WorkingDirectory;

/**
 *
 * @author Stefan Canali
 * 
 * This class is created to show an initial splash screen!
 * 
 * I don't know why, but the initial splash screen does not function
 * in the expected way for FXML applications. 
 * 
 * For FXML applications, the splash screen can be invoked by the command
 * line paramenter "-splash:splash.png", but not for the jar file itself.
 * Though, we have to make a detour over a normal java application.
 */
public class LxrDiffTab {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        long MAX_2G = 2147483648L;      // 2 Giga
        long MAX_4G = 4294967296L;      // 4 Giga
        long MAX_5G = 5368709120L;      // 5 Giga
        long MAX_6G = 6442450944L;      // 6 Giga
        long MAX_8G = 8589934592L;      // 8 Giga
        long MAX_16G = 17179869184L;    // 16 Giga
        
        long maxMemory = Runtime.getRuntime().maxMemory();  // memory for JVM
        long memorySize = ((com.sun.management              // free OS memory
                .OperatingSystemMXBean) ManagementFactory
                        .getOperatingSystemMXBean())
                .getTotalPhysicalMemorySize();
        boolean isDebug = java.lang.management      // app started in Debugger?
                .ManagementFactory.getRuntimeMXBean().getInputArguments()
                .toString().indexOf("jdwp") >= 0;   
        
        // Is the virtual memory of the JVM increased by additional arguments?
        boolean xm = false;
        for (String str : args) {
            if (str.contains("Xms")) {
                xm = true;
            } else if (str.contains("Xmx")) {
                xm = true;
            }
        }
        
        // Note: 
        // The splash screen is handled in the class LxrDifferenceTable, too!
        //
        // Reason: 
        // The application is called twice, if the heap memory 
        // is too low! Though, the splash screen must be handled here and in 
        // the class LxrDifferenceTable. 
        // If the handling here is omitted, then the splash screen would not
        // be closed in the case, that the application is called via the 
        // Runtime.getruntime
        //
        SplashScreen mySplash;                   // instantiated by JVM! 
        mySplash = SplashScreen.getSplashScreen();
        if (mySplash != null) {
            System.out.println("Splash: OK");
        } else {
            System.out.println("Splash: NOK");
        }

        String virtualMemory = "";
        if (memorySize >= MAX_16G) {
            virtualMemory = " -Xms4g -Xmx8g ";
        } else if (memorySize >= MAX_8G) {
            virtualMemory = " -Xms4g -Xmx6g ";
        } else if (memorySize >= MAX_5G) {
            virtualMemory = " -Xms2g -Xmx4g ";
        } else if (memorySize >= MAX_4G) {
            virtualMemory = " -Xms1g -Xmx2g ";
        } else if (memorySize >= MAX_2G) {
            virtualMemory = " -Xms512m -Xmx1024m ";
        } else {
            // do nothing
        }
        
       boolean increaseMemoryForJVM;
        try {
            LocalProperties localProperties = new LocalProperties(null);
            increaseMemoryForJVM = localProperties.getIncreaseMemoryForJVM();
        } catch (Exception ex) {
            increaseMemoryForJVM = false;
            JOptionPane.showMessageDialog(null,"Exception = " + ex.getMessage(), "Fehler", JOptionPane.PLAIN_MESSAGE);
        }
        if (increaseMemoryForJVM) {
            if (xm) {
                // The application is started as it is.
                Application.launch(lxrdifferencetable.LxrDifferenceTable.class, args);
            } else if (virtualMemory == "") {
                // There's nothing more we can do. 
                // The application is started as it is.
                Application.launch(lxrdifferencetable.LxrDifferenceTable.class, args);
            } else if (isDebug) {
                // The application is started in the debugger. 
                Application.launch(lxrdifferencetable.LxrDifferenceTable.class, args);
            } else {
                // It's not enough heap space available. 
                // The application is restarted with more space.
                File workingDir;
                String classPath = (new File(System.getProperty("java.class.path"))).getParent();
                if (classPath == null) {
                    workingDir = new File("").getAbsoluteFile();
                } else if (classPath.contains(":")
                        || // for Linux
                        classPath.contains(";")) {       // for Windows only
                    // it runs under netbeans debugger!
                    workingDir = new File("").getAbsoluteFile();
                } else {
                    // normal call 
                    workingDir = new File(classPath);
                }

                try {
                    String command;
                    command = "java " + virtualMemory + " -jar " +
                            workingDir.getAbsolutePath() + "/LxrDiffTab.jar" + 
                            virtualMemory;
                    Process proc = Runtime.getRuntime().exec(command);
                    // read standard error and print
                    InputStream stderr = (InputStream) proc.getErrorStream();
                    InputStreamReader isr = new InputStreamReader(stderr);
                    BufferedReader br = new BufferedReader(isr);

                    if (mySplash != null) {     // check if we really had a spash screen
                        mySplash.close();       // we're done with it
                    }

                    // read and print standard output from started process (runtime).
                    String line = null ;
                    while ((line = br.readLine())!= null )
                            System.out.println(line);

                } catch (IOException ex) {
                    // An error occured.
                    // Try to start the application in a normal manner.
                    Application.launch(lxrdifferencetable.LxrDifferenceTable.class, args);
                }
            }
            
        } else {
            // The application is started as it is.
            Application.launch(lxrdifferencetable.LxrDifferenceTable.class, args);
        }
    }    
}
