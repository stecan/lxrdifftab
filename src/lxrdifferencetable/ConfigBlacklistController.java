/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import lxrdifferencetable.tools.Properties;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import static lxrdifferencetable.LxrDifferenceTable.BLACKLIST_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import lxrdifferencetable.tools.Utils;


/**
 * FXML controller class to handle the menu / window for 
 * blacklist extensions (*.gz, *.lib, *.dll, ...)
 *
 * @author Stefan Canali
 */
public class ConfigBlacklistController implements Initializable, ControlledStage {
    
    Properties props;
    Utils utils;
    
    /**
     * Constructor
     * 
     */
    public ConfigBlacklistController() {
        this.props = new Properties();
        this.utils = new Utils();
    }
    
    private StagesController myController;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    Button abortConfig;

    @FXML
    Button saveConfig;
    
    @FXML
    ListView<String> blist;     // blacklist

    @FXML
    ListView<String> wlist;     // whitelist
    
    @FXML
    TextField editTextField;    // edit field for wlist items 
                                // (default: invisible)
    @FXML
    TextField insertTextField;  // insert field for wlist items
                                // (default: invisible)

    @FXML
    private void handleAbort(ActionEvent event) {
        initialize(null, null);
        myController.hideStage(BLACKLIST_STAGE);
    }
    
    @FXML
    private void handleSave(ActionEvent event) {
        savePreferences();
        myController.hideStage(BLACKLIST_STAGE);
    }
    
    @FXML
    private void handleMenuItemEdit(ActionEvent event) {
        String selectedItem;
        selectedItem = wlist.getSelectionModel().getSelectedItem();
        editTextField.setText(selectedItem);
        editTextField.setVisible(true);
    }
    
    @FXML
    private void handleEditTextField(ActionEvent event) {
        int selectedIndex;
        String newText;
        selectedIndex = wlist.getSelectionModel().getSelectedIndex();
        newText = editTextField.getText();
        wlist.getItems().set(selectedIndex, newText);

        //
        // sort
        //
        ObservableList<String> items = wlist.getItems();    // get items
        ArrayList<String> dataArray = new ArrayList<>();
        items.stream().forEach((item) -> {                  // copy items
            dataArray.add(item);
        });
        Collections.sort(dataArray);                        // sort items
        wlist.getItems().removeAll(dataArray);              // remove items
        wlist.getItems().addAll(dataArray);                 // insert items
        
        wlist.getSelectionModel().select(newText);          // select item
        wlist.scrollTo(newText);                            // scroll to new item

        editTextField.setVisible(false);                    // hide textfield
    }
    
    @FXML
    private void handleMenuItemInsert(ActionEvent event) {
        String selectedItem;
        selectedItem = wlist.getSelectionModel().getSelectedItem();
        insertTextField.setText(selectedItem);
        insertTextField.setVisible(true);
        System.out.println("Inserted");
    }
    
    @FXML
    private void handleInsertTextField(ActionEvent event) {
        String newText;
        newText = insertTextField.getText();

        //
        // sort
        //
        ObservableList<String> items = wlist.getItems();    // get items
        ArrayList<String> dataArray = new ArrayList<>();
        items.stream().forEach((item) -> {                  // copy items
            dataArray.add(item);
        });
        dataArray.add(newText);                             // add new item
        Collections.sort(dataArray);                        // sort items
        wlist.getItems().removeAll(items);                  // remove old items
        wlist.getItems().addAll(dataArray);                 // insert new items
        
        wlist.getSelectionModel().select(newText);          // select item
        wlist.scrollTo(newText);                            // scroll to new item

        insertTextField.setVisible(false);                  // hide textfield
    }
    
    @FXML
    private void handleTextFieldAbort(KeyEvent t) {
        if (t.getCode() == KeyCode.ESCAPE) {
            editTextField.setVisible(false);                // hide "edit" textfield
            insertTextField.setVisible(false);              // hide "insert" textfield
        }
    }
    
    @FXML
    private void handleMenuItemDelete(ActionEvent event) {
        ObservableList<String> selectedItems;
        selectedItems = wlist.getSelectionModel().getSelectedItems();
        
        // copy items to dataArray
        ArrayList<String> dataArray;
        dataArray = new ArrayList<>();
        selectedItems.stream().forEach((item) -> {
            dataArray.add(item);
        });
        
        // delete selected items!
        dataArray.stream().forEach((item) -> {
            wlist.getItems().remove(item);
        });
    }
    
    @FXML
    private void select(ActionEvent event) {
        ObservableList<String> selectedItems;
        ObservableList<String> itemsBlacklist;
        ObservableList<String> itemsWhitelist;
        String[] data;
        String blacklist;
        String whitelist;
        
        //
        // handle blacklist e.g.
        //    1. get selected (new) items
        //    2. get all blacklist items
        //    3. add new items to the blacklist
        //    3. sort the new blacklist
        //    4. writes the new whitelist to the blacklist ListView
        //    5. mark the new items in the blacklist ListView
        //
        
        // get selected items from whitelist ListView
        selectedItems = wlist.getSelectionModel().getSelectedItems();
        // get all blacklist items
        itemsBlacklist = blist.getItems();
        
        // create comma separated string of blacklist items
        blacklist = utils.createCommaSeparatedString(itemsBlacklist);
        
        // add selected items to blacklist
        blacklist = utils.addToCommaSeparatedString(blacklist, selectedItems);
        
        if (blacklist.equals("")) {
            data = new String[0];
        } else {
            data = blacklist.split(",");
        }
        
        itemsBlacklist = FXCollections.observableArrayList();   // delete old one
        itemsBlacklist.addAll(Arrays.asList(data)); // add items
        blist.setItems(itemsBlacklist);             // show items
        
        // mark new items in blacklist ListView
        for (int i = 0; i < selectedItems.size(); i++) {
            blist.getSelectionModel().select(selectedItems.get(i));
        }

        //
        // handle whitelist e.g.
        //    1. get all whitelist items
        //    2. subtract selected items from whitelist
        //    3. sort the new whitelist
        //    4. write the new whitelist to the whitelist ListView
        //
        
        itemsWhitelist = wlist.getItems();
        
        // subtract selected items from whitelist and 
        // creates a comma separated string
        whitelist = "";
        boolean found;
        for (int i = 0; i < itemsWhitelist.size(); i++) {
            found = false;
            for (int j = 0; j < selectedItems.size(); j++) {
                if (itemsWhitelist.get(i).equals(selectedItems.get(j))) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (whitelist.equals("")) {
                    whitelist = itemsWhitelist.get(i);
                } else {
                    whitelist = whitelist + "," + itemsWhitelist.get(i);
                }
            } 
        }

        whitelist = utils.sortString(whitelist);        // sort whitelist
        if (whitelist.equals("")) {
            data = new String[0];
        } else {
            data = whitelist.split(",");
        }
        
        itemsWhitelist = FXCollections.observableArrayList();   // delete old one
        itemsWhitelist.addAll(Arrays.asList(data));     // add items
        wlist.setItems(itemsWhitelist);                 // show items
    }

    @FXML
    private void deselect(ActionEvent event) {
        ObservableList<String> selectedItems;
        ObservableList<String> itemsBlacklist;
        ObservableList<String> itemsWhitelist;
        String[] data;
        String blacklist;
        String whitelist;
        
        //
        // handle whitelist e.g.
        //    1. get selected (new) items
        //    2. get all whitelist items
        //    3. add new items to the whitelist
        //    3. sort the new whitelist
        //    4. write the new blacklist to the whitelist ListView
        //    5. mark the new items in the whitelist ListView
        //
        
        // get selected items from blacklist ListView
        selectedItems = blist.getSelectionModel().getSelectedItems();
        // get all whitelist items
        itemsWhitelist = wlist.getItems();
        
        // create comma separated string of whitelist items
        whitelist = utils.createCommaSeparatedString(itemsWhitelist);
        
        // add selected items to whitelist
        whitelist = utils.addToCommaSeparatedString(whitelist, selectedItems);

        if (whitelist.equals("")) {
            data = new String[0];
        } else {
            data = whitelist.split(",");
        }
        
        itemsWhitelist = FXCollections.observableArrayList();   // delete old one
        itemsWhitelist.addAll(Arrays.asList(data)); // add items
        wlist.setItems(itemsWhitelist);             // show items
        
        // mark new items in whitelist ListView
        for (int i = 0; i < selectedItems.size(); i++) {
            wlist.getSelectionModel().select(selectedItems.get(i));
        }

        //
        // handle blacklist e.g.
        //    1. get all blacklist items
        //    2. subtract selected items from blacklist
        //    3. sort the new blacklist
        //    4. write the new blacklist to the blacklist ListView
        //
        
        itemsBlacklist = blist.getItems();
        
        // subtract selected items from whitelist and 
        // creates a comma separated string
        blacklist = "";
        boolean found;
        for (int i = 0; i < itemsBlacklist.size(); i++) {
            found = false;
            for (int j = 0; j < selectedItems.size(); j++) {
                if (itemsBlacklist.get(i).equals(selectedItems.get(j))) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (blacklist.equals("")) {
                    blacklist = itemsBlacklist.get(i);
                } else {
                    blacklist = blacklist + "," + itemsBlacklist.get(i);
                }
            } 
        }

        blacklist = utils.sortString(blacklist);        // sort blacklist
        if (blacklist.equals("")) {
            data = new String[0];
        } else {
            data = blacklist.split(",");
        }
        
        itemsBlacklist = FXCollections.observableArrayList();   // delete old one
        itemsBlacklist.addAll(Arrays.asList(data));     // add items
        blist.setItems(itemsBlacklist);                 // show items
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> items;
        String blacklist;
        String whitelist;
        String[] dataWhitelist;
        String[] dataBlacklist;

        blacklist = props.getProperty(props.BLACKLIST);
        whitelist = props.getProperty(props.WHITELIST);

        if (blacklist.equals("")) {
            dataBlacklist = new String[0];
        } else {
            dataBlacklist = blacklist.split(",");
        }
        if (whitelist.equals("")) {
            dataWhitelist = new String[0];
        } else {
            dataWhitelist = whitelist.split(",");
        }

        items = FXCollections.observableArrayList ();
        items.addAll(Arrays.asList(dataBlacklist));
        blist.setItems(items);    
        blist.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        items = FXCollections.observableArrayList ();
        items.addAll(Arrays.asList(dataWhitelist));
        wlist.setItems(items);   
        wlist.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }    
    
    private void savePreferences() {
        ObservableList<String> itemsBlacklist;
        ObservableList<String> itemsWhitelist;
        String blacklist;
        String whitelist;

        itemsWhitelist = wlist.getItems();
        // create comma separated string of whitelist items
        whitelist = "";
        for (int i = 0; i < itemsWhitelist.size(); i++) {
            if (i == 0) {
                whitelist = itemsWhitelist.get(i);
            } else {
                whitelist = whitelist + "," + itemsWhitelist.get(i);
            }
        }
        props.putProperty(props.WHITELIST, whitelist);
        
        itemsBlacklist = blist.getItems();
        // create comma separated string of blacklist items
        blacklist = "";
        for (int i = 0; i < itemsBlacklist.size(); i++) {
            if (i == 0) {
                blacklist = itemsBlacklist.get(i);
            } else {
                blacklist = blacklist + "," + itemsBlacklist.get(i);
            }
        }
        props.putProperty(props.BLACKLIST, blacklist);
 
        // Set new values to text fields!
        // 
        // Note:
        // This step is very important. 
        // If a change is taken, then the text area wouldn't be refreshed 
        // exept in case of a new start!
        initialize(null, null);
        
        LxrDifferenceTableController lxrDifferenceTableController;
        FXMLLoader loader;
        
        // Reload application
        loader = myController.getLoader(MAIN_STAGE_1);
        lxrDifferenceTableController = loader.<LxrDifferenceTableController>getController();
        lxrDifferenceTableController.reloadConfig();
    }
}

