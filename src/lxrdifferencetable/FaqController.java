/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import static lxrdifferencetable.LxrDifferenceTable.FAQ_STAGE;
import lxrdifferencetable.html.Anchor;
import lxrdifferencetable.tools.Properties;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class FaqController implements Initializable, ControlledStage {

    private StagesController myController;
    private final Properties props;                     // properties
    private String lang;                                // language preference                          

    /**
     * Constructor
     */
    public FaqController() {
        this.lang = "";
        this.props = new Properties();
    }

    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }

    WebEngine webEngine;    // webEngine for htmlView
    
    @FXML
    Button close;
    
    @FXML
    WebView webView;
    
    @FXML
    private void handleClose(ActionEvent event) {
        myController.hideStage(FAQ_STAGE);
    }
    
    @FXML
    private void handleBackButton(ActionEvent event) {
        int currentIndex;
        
        currentIndex = webEngine.getHistory().getCurrentIndex();
        if (currentIndex == 0) {
            // do nothing!
        } else {
            webEngine.getHistory().go(-1);
        }
    }

    @FXML
    private void handleForwardButton(ActionEvent event) {
        int currentIndex;
        int size;
        
        currentIndex = webEngine.getHistory().getCurrentIndex();
        size = webEngine.getHistory().getEntries().size();
        if (currentIndex == size - 1) {
            // do nothing!
        } else {
            webEngine.getHistory().go(1);
        }
    }
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used
     * @param rb    not used
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ClassLoader cl;
        cl = Anchor.class.getClassLoader();
        URL resource;

        lang = props.getProperty(props.LANG);
        switch (lang) {
            case "de":
                resource = cl.getResource("lxrdifferencetable/html/faq_de.html");
                break;
            case "en":
                resource = cl.getResource("lxrdifferencetable/html/faq_en.html");
                break;
            // default
            default:
                resource = cl.getResource("lxrdifferencetable/html/faq_en.html");
                break;
        }
        webEngine = webView.getEngine();
        webEngine.load(resource.toString());
    }    
}
