/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.reports;

/**
 *
 * @author Stefan Canali
 */
import java.io.File;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import lxrdifferencetable.data.ConfigEntry;
import lxrdifferencetable.data.StatisticalData;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.tools.ConvertLxrDiffTabStrings;
import lxrdifferencetable.tools.Properties;
 
 
public class DataBeanMaker {
    
    private final int MAX_USERS = 20;
    
    private final List<TableEntry> changedFileList;
    private final StatisticalData statistics;
    private final ConfigEntry configEntry;
    private final HashMap<String, String> reviewed;
    private final HashMap<String, String> tested;
    private ZonedDateTime reviewedFrom;
    private ZonedDateTime reviewedTo;
    private ZonedDateTime testedFrom;
    private ZonedDateTime testedTo;
    private ZonedDateTime reportCreationDate;
    private final File file;
    private final boolean titlePage;
    private final boolean summaryPage;
    private String path;
    private boolean excel;
    
    public DataBeanMaker (List<TableEntry> changedFileList, 
            ConfigEntry configEntry, File file, 
            StatisticalData statistics, 
            boolean titlePage,
            boolean summaryPage) {
        this.changedFileList = changedFileList;
        this.statistics = statistics;
        this.configEntry = configEntry;
        reviewed = new HashMap<>();
        tested = new HashMap<>();
        this.file = file;
        this.summaryPage = summaryPage;
        this.titlePage = titlePage;
        this.path = "";
        this.excel = false;
    };

    public void setExcel (boolean excel) {
        this.excel = excel;
    }

    public ArrayList<DataBean> getDataBeanList() {
	ArrayList<DataBean> dataBeanList = new ArrayList<>();
 
        // locale settings
        Properties props = new Properties();
        String lang = props.getProperty(props.LANG);
        String country = props.getProperty(props.COUNTRY);
        Locale locale = new Locale(lang, country);
        ConvertLxrDiffTabStrings conv = new ConvertLxrDiffTabStrings(locale);


        reviewedFrom = ZonedDateTime.now();     // max value
        testedFrom = ZonedDateTime.now();       // max value
        reportCreationDate = ZonedDateTime.now();       // max value
        // initialize to min value
        reviewedTo = ZonedDateTime.parse("Sat, 1 Jan 2000 00:00:00 +0200", 
                DateTimeFormatter.RFC_1123_DATE_TIME.withLocale(locale));
        testedTo = ZonedDateTime.parse("Sat, 1 Jan 2000 00:00:00 +0200", 
                DateTimeFormatter.RFC_1123_DATE_TIME.withLocale(locale));
        

        // compute number of entries and long reviewed description entries
        int numberOfEntries = 0;
        int numberOfLongReviewedEntries = 0;
        for (TableEntry item : changedFileList) {
            int sizeHrefItem = item.getHref().size();
            for (int pos = 0; pos < sizeHrefItem; pos++) {
                numberOfEntries++;
                if (!emptyHtml(item.getReviewDescriptionLast(pos))) {
                    numberOfLongReviewedEntries++;
                }
                String user;
                user = item.getReviewDescAuthorLast(pos);
                if (user.isEmpty()) {
                    // do nothing
                } else {
                    reviewed.put(user, user);
                }
                user = item.getReviewResAuthorLast(pos);
                if (user.isEmpty()) {
                    // do nothing
                } else {
                    reviewed.put(user, user);
                }
                
                user = item.getTestDescAuthorLast(pos);
                if (user.isEmpty()) {
                    // do nothing
                } else {
                    tested.put(user, user);
                }
                user = item.getTestResAuthorLast(pos);
                if (user.isEmpty()) {
                    // do nothing
                } else {
                    tested.put(user, user);
                }
                
                // compute review and test period
                try {
                    ZonedDateTime date;
                    date = ZonedDateTime.parse(item.getReviewResDateLast(pos), 
                            DateTimeFormatter.RFC_1123_DATE_TIME);
                    if (date.isBefore(reviewedFrom)) {
                        reviewedFrom = date;
                        }
                } catch (Exception ex) {
                    // if entry does not exists, then continue.
                }
                try {
                    ZonedDateTime date;
                    date = ZonedDateTime.parse(item.getReviewDescDateLast(pos), 
                            DateTimeFormatter.RFC_1123_DATE_TIME);
                    if (date.isBefore(reviewedFrom)) {
                        reviewedFrom = date;
                        }
                } catch (Exception ex) {
                    // if entry does not exists, then continue.
                }
                try {
                    ZonedDateTime date;
                    date = ZonedDateTime.parse(item.getTestResDateLast(pos), 
                            DateTimeFormatter.RFC_1123_DATE_TIME);
                    if (date.isBefore(testedFrom)) {
                        testedFrom = date;
                        }
                } catch (Exception ex) {
                    // if entry does not exists, then continue.
                }
                try {
                    ZonedDateTime date;
                    date = ZonedDateTime.parse(item.getTestDescDateLast(pos), 
                            DateTimeFormatter.RFC_1123_DATE_TIME);
                    if (date.isBefore(testedFrom)) {
                        testedFrom = date;
                        }
                } catch (Exception ex) {
                    // if entry does not exists, then continue.
                }

                try {
                    ZonedDateTime date;
                    date = ZonedDateTime.parse(item.getReviewResDateLast(pos), 
                            DateTimeFormatter.RFC_1123_DATE_TIME);
                    if (date.isAfter(reviewedTo)) {
                        reviewedTo = date;
                        }
                } catch (Exception ex) {
                    // if entry does not exists, then continue.
                }
                try {
                    ZonedDateTime date;
                    date = ZonedDateTime.parse(item.getReviewDescDateLast(pos), 
                            DateTimeFormatter.RFC_1123_DATE_TIME);
                    if (date.isAfter(reviewedTo)) {
                        reviewedTo = date;
                        }
                } catch (Exception ex) {
                    // if entry does not exists, then continue.
                }
                try {
                    ZonedDateTime date;
                    date = ZonedDateTime.parse(item.getTestResDateLast(pos), 
                            DateTimeFormatter.RFC_1123_DATE_TIME);
                    if (date.isAfter(testedTo)) {
                        testedTo = date;
                        }
                } catch (Exception ex) {
                    // if entry does not exists, then continue.
                }
                try {
                    ZonedDateTime date;
                    date = ZonedDateTime.parse(item.getTestDescDateLast(pos), 
                            DateTimeFormatter.RFC_1123_DATE_TIME);
                    if (date.isAfter(testedTo)) {
                        testedTo = date;
                        }
                } catch (Exception ex) {
                    // if entry does not exists, then continue.
                }
            }
        }      
        
        String titlePart;
        if (titlePage) {
            // Pass for title part
            int lineNr = 0;
            titlePart = "Title Part";
            String summaryPart = "";
            String chapterPart = "";
            String programName = "LxrDifferenceTable, V" + props.getProperty(props.VERSION);
            String reportFileName = file.getName();
            String firstPart = "";
            String secondPart = "";
            String thirdPart = "";
            String title = configEntry.getCaptionLast();
            if (title.equals("")) {
                title = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("labelCaption");
            }
            String svnVersion = "";
            String svnDescription = "";
            String svnAuthor = "";
            String svnDate = "";
            String reviewState = ""; 
            String reviewStateAuthor = ""; 
            String reviewStateDate = "";
            String reviewDescription = "";
            String reviewedBy = "";
            {
                Collection<String> values = reviewed.values();
                ArrayList<String> users = new ArrayList<>();
                values.stream().forEach((item) -> {             // copy items
                    users.add(item);
                });
                Collections.sort(users);
                int i = 0;
                for (String user : users) {
                    i++;
                    if (i < MAX_USERS) {
                        if (user.equals("")) {
                            
                        } else {
                            reviewedBy = reviewedBy + user + "\n";
                        }
                    } else {
                        reviewedBy = reviewedBy + "...\n";
                        break;
                    }
                }
            }
            String reviewLongDescription = "";
            String idToReviewLongDescription = "";
            String idFromReviewLongDescription = "";
            String linkToReviewLongDescription = "";
            String linkFromReviewLongDescription = "";
            String reviewDescriptionAuthor = ""; 
            String reviewDescriptionDate = "";
            String testState = ""; 
            String testStateAuthor = ""; 
            String testStateDate = "";
            String testDescription = "";
            String testedBy = "";
            {
                Collection<String> values = tested.values();
                ArrayList<String> users = new ArrayList<>();
                values.stream().forEach((item) -> {             // copy items
                    users.add(item);
                });
                Collections.sort(users);
                int i = 0;
                for (String user : users) {
                    i++;
                    if (i < MAX_USERS) {
                        if (user.equals("")) {
                            
                        } else {
                            testedBy = testedBy + user + "\n";
                        }
                    } else {
                        testedBy = testedBy + "...\n";
                        break;
                    }
                }
            }
            String testLongDescription = ""; 
            String idToTestLongDescription = "";
            String idFromTestLongDescription = "";
            String linkToTestLongDescription = "";
            String linkFromTestLongDescription = "";
            String link = "";
            String lineNumber = "";
            String name = "";
            String account = System.getProperty("user.name");
            String testDescriptionAuthor = ""; 
            String testDescriptionDate = "";
            String svnLabel1 = "";
            String svnLabel2 = "";
            String reviewLabel1 = "";
            String reviewLabel2 = "";
            String testLabel1 = "";
            String testLabel2 = "";
            InputStream iconReview = null;
            InputStream iconTest = null;
            String creationDate = reportCreationDate.format(DateTimeFormatter.
                            ofLocalizedDateTime(FormatStyle.FULL).withLocale(locale));
            String reviewPeriodFrom;
            String reviewPeriodTo;
            if (reviewed.isEmpty()) {
                reviewPeriodFrom = "";
                reviewPeriodTo = "";
            } else {
                reviewPeriodFrom = reviewedFrom.format(DateTimeFormatter.
                        ofLocalizedDateTime(FormatStyle.FULL).withLocale(locale));
                reviewPeriodTo = reviewedTo.format(DateTimeFormatter.
                        ofLocalizedDateTime(FormatStyle.FULL).withLocale(locale));
            }
            String testPeriodFrom;
            String testPeriodTo;
            if (tested.isEmpty()) {
                testPeriodFrom = "";
                testPeriodTo = "";
            } else {
                testPeriodFrom = testedFrom.format(DateTimeFormatter.
                          ofLocalizedDateTime(FormatStyle.FULL).withLocale(locale));
                testPeriodTo = testedTo.format(DateTimeFormatter.
                          ofLocalizedDateTime(FormatStyle.FULL).withLocale(locale));
            }
            String chapterDirectory = "";
            
            String numberTableRows="";
            String numberFiles = "";
            String numberChangedFiles = "";
            String numberNotChangedFiles = "";
            String numberDeletedFiles = "";
            String numberNewFiles = "";
            String numberChanges = "";
            
            String passedReviewed = "";
            String failedReviewed = "";
            String commentReviewed = "";
            String inconclusiveReviewed = "";
            String openReviewed = "";
            String emptyReviewed = "";
            String passedTested = "";
            String failedTested = "";
            String commentTested = "";
            String inconclusiveTested = "";
            String openTested = "";
            String emptyTested = "";
            
            dataBeanList.add(produce(lineNr, title, 
                    programName,
                    reportFileName,
                    titlePart,
                    summaryPart,
                    chapterPart,
                    firstPart,
                    secondPart,
                    thirdPart,
                    name, account, creationDate, link, 
                    lineNumber, 
                    svnVersion, 
                    svnDescription, 
                    svnAuthor, svnDate,
                    reviewState, reviewStateAuthor, reviewStateDate, 
                    reviewDescription, 
                    reviewedBy,
                    reviewLongDescription, 
                    idToReviewLongDescription, 
                    idFromReviewLongDescription, 
                    linkToReviewLongDescription, 
                    linkFromReviewLongDescription, 
                    reviewDescriptionAuthor, 
                    reviewDescriptionDate, 
                    testState, testStateAuthor, testStateDate, 
                    testDescription, 
                    testedBy,
                    testLongDescription,
                    idToTestLongDescription, 
                    idFromTestLongDescription, 
                    linkToTestLongDescription, 
                    linkFromTestLongDescription, 
                    testDescriptionAuthor, 
                    testDescriptionDate,
                    svnLabel1, svnLabel2,
                    reviewLabel1, reviewLabel2,
                    testLabel1, testLabel2, iconReview, iconTest,
                    reviewPeriodFrom, reviewPeriodTo,
                    testPeriodFrom, testPeriodTo,
                    chapterDirectory,
                    numberTableRows,
                    numberFiles,
                    numberChangedFiles,
                    numberNotChangedFiles,
                    numberDeletedFiles,
                    numberNewFiles,
                    numberChanges,
                    passedReviewed,
                    failedReviewed,
                    commentReviewed,
                    inconclusiveReviewed,
                    openReviewed,
                    emptyReviewed,
                    passedTested,
                    failedTested,
                    commentTested,
                    inconclusiveTested,
                    openTested,
                    emptyTested));
        }
        
        String summaryPart;
        if (summaryPage) {
            // Pass for summary part
            int lineNr = 0;
            titlePart = "";
            summaryPart = "Summary Part";
            String chapterPart = "";
            String programName = "";
            String reportFileName = "";
            String firstPart = "";
            String secondPart = "";
            String thirdPart = "";
            String title = "";
            String svnVersion = "";
            String svnDescription = "";
            String svnAuthor = "";
            String svnDate = "";
            String reviewState = ""; 
            String reviewStateAuthor = ""; 
            String reviewStateDate = "";
            String reviewDescription = "";
            String reviewedBy = "";
            String reviewLongDescription = "";
            String idToReviewLongDescription = "";
            String idFromReviewLongDescription = "";
            String linkToReviewLongDescription = "";
            String linkFromReviewLongDescription = "";
            String reviewDescriptionAuthor = ""; 
            String reviewDescriptionDate = "";
            String testState = ""; 
            String testStateAuthor = ""; 
            String testStateDate = "";
            String testDescription = "";
            String testedBy = "";
            String testLongDescription = ""; 
            String idToTestLongDescription = "";
            String idFromTestLongDescription = "";
            String linkToTestLongDescription = "";
            String linkFromTestLongDescription = "";
            String link = "";
            String lineNumber = "";
            String name = "";
            String account = "";
            String testDescriptionAuthor = ""; 
            String testDescriptionDate = "";
            String svnLabel1 = "";
            String svnLabel2 = "";
            String reviewLabel1 = "";
            String reviewLabel2 = "";
            String testLabel1 = "";
            String testLabel2 = "";
            InputStream iconReview = null;
            InputStream iconTest = null;
            String creationDate = "";
            String reviewPeriodFrom;
            String reviewPeriodTo;
            reviewPeriodFrom = "";
            reviewPeriodTo = "";
            String testPeriodFrom;
            String testPeriodTo;
            testPeriodFrom = "";
            testPeriodTo = "";
            String chapterDirectory = "";
            
            String numberTableRows = "" + statistics.getNumberOfEntries();
            String numberFiles = "" + statistics.getNumberOfFiles();
            String numberChangedFiles = "" + statistics.getNumberOfChangedFiles();
            String numberNotChangedFiles = "" + statistics.getNumberOfUnmodifiedFiles();
            String numberDeletedFiles = "" + statistics.getNumberOfDeletedFiles();
            String numberNewFiles = "" + statistics.getNumberOfNewFiles();
            String numberChanges = "" + statistics.getNumberOfChanges();

            String passedReviewed = "" + statistics.getReviewPassed();
            String failedReviewed = "" + statistics.getReviewFailed();
            String commentReviewed = "" + statistics.getReviewComment();
            String inconclusiveReviewed = "" + statistics.getReviewInconclusive();
            String openReviewed = "" + statistics.getReviewOpen();
            String emptyReviewed = "" + statistics.getReviewEmpty();
            String passedTested = "" + statistics.getTestPassed();
            String failedTested = "" + statistics.getTestFailed();
            String commentTested = "" + statistics.getTestComment();
            String inconclusiveTested = "" + statistics.getTestInconclusive();
            String openTested = "" + statistics.getTestOpen();
            String emptyTested = "" + statistics.getTestEmpty();
            
            dataBeanList.add(produce(lineNr, title, 
                    programName,
                    reportFileName,
                    titlePart,
                    summaryPart,
                    chapterPart,
                    firstPart,
                    secondPart,
                    thirdPart,
                    name, account, creationDate, link, 
                    lineNumber, 
                    svnVersion, 
                    svnDescription, 
                    svnAuthor, svnDate,
                    reviewState, reviewStateAuthor, reviewStateDate, 
                    reviewDescription, 
                    reviewedBy,
                    reviewLongDescription, 
                    idToReviewLongDescription, 
                    idFromReviewLongDescription, 
                    linkToReviewLongDescription, 
                    linkFromReviewLongDescription, 
                    reviewDescriptionAuthor, 
                    reviewDescriptionDate, 
                    testState, testStateAuthor, testStateDate, 
                    testDescription, 
                    testedBy,
                    testLongDescription,
                    idToTestLongDescription, 
                    idFromTestLongDescription, 
                    linkToTestLongDescription, 
                    linkFromTestLongDescription, 
                    testDescriptionAuthor, 
                    testDescriptionDate,
                    svnLabel1, svnLabel2,
                    reviewLabel1, reviewLabel2,
                    testLabel1, testLabel2, iconReview, iconTest,
                    reviewPeriodFrom, reviewPeriodTo,
                    testPeriodFrom, testPeriodTo,
                    chapterDirectory,
                    numberTableRows,
                    numberFiles,
                    numberChangedFiles,
                    numberNotChangedFiles,
                    numberDeletedFiles,
                    numberNewFiles,
                    numberChanges,
                    passedReviewed,
                    failedReviewed,
                    commentReviewed,
                    inconclusiveReviewed,
                    openReviewed,
                    emptyReviewed,
                    passedTested,
                    failedTested,
                    commentTested,
                    inconclusiveTested,
                    openTested,
                    emptyTested));
        }

        // first pass
        int lineNr = 0;
        int longReviewDescNr = 0;
        int longTestDescNr = 0;
        for (TableEntry item : changedFileList) {
            int sizeHrefItem = item.getHref().size();
            for (int pos = 0; pos < sizeHrefItem; pos++) {
                lineNr++;
                titlePart = "";
                summaryPart = "";
                String chapterPart = "";
                String programName = "";
                String reportFileName = file.getName();
                String hrefLast = item.getHrefTextLast(pos);
                String[] split = hrefLast.split("/");
                String path1;
                if (split.length == 1) {
                    String[] split1 = hrefLast.split("\\\\");
                    String filename = split1[split1.length-1];
                    path1 = hrefLast.replace(filename, "");
                } else {
                    String filename = split[split.length-1];
                    path1 = hrefLast.replace(filename, "");
                }
                String chapterDirectory = path1;
                if (!chapterDirectory.equals(path)) {
                    chapterPart = "Chapter Part";
                    path = chapterDirectory;
                }
                String firstPart = "First Part";
                String secondPart = "";
                String thirdPart = "";
                String title = configEntry.getCaptionLast();
                if (title.equals("")) {
                    title = ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("labelCaption");
                }
                String link = item.getHrefLast(pos);
                String lineNumber = item.getLineNumberLast(pos);
                String name = item.getHrefTextLast(pos) + "#" + lineNumber;
                String account = "";
                String svnVersion = "";
                String svnDescription = "";
                String svnAuthor = "";
                String svnDate = "";
                if (pos == 0) {
                    // SVN information is printed only once
                    int i = 0;
                    boolean finished = false;
                    while (!finished) {
                        if ("".equals(item.getSvnVersion(i))) {
                            finished = true;
                        } else {
                            if (i == 0) {
                                svnVersion = item.getSvnVersion(i);
                                svnDescription = item.getSvnDesc(i);
                                svnDate = item.getSvnDate(i);
                                svnAuthor = item.getSvnAuthor(i);
                            } else {
                                if (excel) {
                                    // for table calculation reports (e.g.
                                    // excel and ods), every repo version
                                    // is written into one row.
                                    svnVersion = item.getSvnVersion(i);
                                    svnDescription = item.getSvnDesc(i);
                                    svnDate = item.getSvnDate(i);
                                    svnAuthor = item.getSvnAuthor(i);


                                    String creationDate = "";
                                    String reviewPeriodFrom = "";
                                    String reviewPeriodTo = "";
                                    String testPeriodFrom = "";
                                    String testPeriodTo = "";

                                    String numberTableRows="";
                                    String numberFiles = "";
                                    String numberChangedFiles = "";
                                    String numberNotChangedFiles = "";
                                    String numberDeletedFiles = "";
                                    String numberNewFiles = "";
                                    String numberChanges = "";

                                    String passedReviewed = "";
                                    String failedReviewed = "";
                                    String commentReviewed = "";
                                    String inconclusiveReviewed = "";
                                    String openReviewed = "";
                                    String emptyReviewed = "";
                                    String passedTested = "";
                                    String failedTested = "";
                                    String commentTested = "";
                                    String inconclusiveTested = "";
                                    String openTested = "";
                                    String emptyTested = "";

                                    String reviewState = "";
                                    String reviewStateAuthor = ""; 
                                    String reviewStateDate = "";
                                    String reviewDescription = "";
                                    String reviewedB = "";
                                    String reviewLongDescription = "";
                                    String idToReviewLongDescription = "";
                                    String idFromReviewLongDescription = "";
                                    String linkToReviewLongDescription = "";
                                    String linkFromReviewLongDescription = "";
                                    String reviewDescriptionAuthor = "";
                                    String reviewDescriptionDate = "";
                                    String testState = "";
                                    String testStateAuthor = "";
                                    String testStateDate = "";
                                    String testDescription = "";
                                    String reviewedBy = "";
                                    String testedBy = "";
                                    String testLongDescription = "";
                                    String idToTestLongDescription = "";
                                    String idFromTestLongDescription = "";
                                    String linkToTestLongDescription = "";
                                    String linkFromTestLongDescription = "";
                                    String testDescriptionAuthor = "";
                                    String testDescriptionDate = "";
                                    String svnLabel1 = "";
                                    String svnLabel2 = "";
                                    String reviewLabel1 = "";
                                    String reviewLabel2 = "";
                                    String testLabel1 = "";
                                    String testLabel2 = "";
                                    InputStream iconReview = null;
                                    InputStream iconTest = null;

                                    dataBeanList.add(produce(lineNr, title, 
                                            programName,
                                            reportFileName,
                                            titlePart,
                                            summaryPart,
                                            chapterPart,
                                            firstPart,
                                            secondPart,
                                            thirdPart,
                                            name, account, creationDate, link, 
                                            lineNumber, 
                                            svnVersion, 
                                            svnDescription, 
                                            svnAuthor, svnDate,
                                            reviewState, reviewStateAuthor, reviewStateDate, 
                                            reviewDescription, 
                                            reviewedBy,
                                            reviewLongDescription, 
                                            idToReviewLongDescription, 
                                            idFromReviewLongDescription, 
                                            linkToReviewLongDescription, 
                                            linkFromReviewLongDescription, 
                                            reviewDescriptionAuthor, 
                                            reviewDescriptionDate, 
                                            testState, testStateAuthor, testStateDate, 
                                            testDescription, 
                                            testedBy,
                                            testLongDescription, 
                                            idToTestLongDescription, 
                                            idFromTestLongDescription, 
                                            linkToTestLongDescription, 
                                            linkFromTestLongDescription, 
                                            testDescriptionAuthor, 
                                            testDescriptionDate,
                                            svnLabel1, svnLabel2,
                                            reviewLabel1, reviewLabel2,
                                            testLabel1, testLabel2, iconReview, iconTest,
                                            reviewPeriodFrom, reviewPeriodTo,
                                            testPeriodFrom, testPeriodTo,
                                            chapterDirectory,
                                            numberTableRows,
                                            numberFiles,
                                            numberChangedFiles,
                                            numberNotChangedFiles,
                                            numberDeletedFiles,
                                            numberNewFiles,
                                            numberChanges,
                                            passedReviewed,
                                            failedReviewed,
                                            commentReviewed,
                                            inconclusiveReviewed,
                                            openReviewed,
                                            emptyReviewed,
                                            passedTested,
                                            failedTested,
                                            commentTested,
                                            inconclusiveTested,
                                            openTested,
                                            emptyTested));
                                } else {
                                    svnVersion = svnVersion + "\n" + item.getSvnVersion(i);
                                    svnDescription = svnDescription + "\n" + item.getSvnDesc(i);
                                    svnDate = svnDate + "\n" + item.getSvnDate(i);
                                    svnAuthor = svnAuthor + "\n" + item.getSvnAuthor(i);
                                }
                            }
                        }
                        i++;
                    }
                } else {
                    // Empty lines are not printed.
                }
                String reviewState = item.getReviewStateLast(pos).toString();
                String reviewStateAuthor = item.getReviewResAuthorLast(pos);
                String reviewStateDate = item.getReviewResDateLast(pos);
                String reviewDescription;
                reviewDescription = conv.convertDescriptionString(
                        item.getReviewShortDescriptionLast(pos));
                String reviewedBy = "";
                String reviewLongDescription = "";
                String idToReviewLongDescription;
                String idFromReviewLongDescription = "";
                String linkFromReviewLongDescription = "";
                String linkToReviewLongDescription;
                if (emptyHtml(item.getReviewDescriptionLast(pos))) {
                    idToReviewLongDescription = "";
                    linkToReviewLongDescription = "";
                    
                } else {
                    longReviewDescNr++;
                    idToReviewLongDescription = "" + (numberOfEntries + 
                            longReviewDescNr);
                    linkToReviewLongDescription = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("report.detailedReviewDescription") + " " +
                            idToReviewLongDescription;
                }
                String reviewDescriptionAuthor = item.
                        getReviewDescAuthorLast(pos);
                String reviewDescriptionDate = item.getReviewDescDateLast(pos);
                String testState = item.getTestStateLast(pos).toString();
                String testStateAuthor = item.getTestResAuthorLast(pos);
                String testStateDate = item.getTestResDateLast(pos);
                String testDescription;
                testDescription = conv.convertDescriptionString(
                        item.getTestShortDescriptionLast(pos));
                String testedBy = "";
                String testLongDescription = "";
                String idToTestLongDescription;
                String idFromTestLongDescription = "";
                String linkFromTestLongDescription = "";
                String linkToTestLongDescription;
                if (emptyHtml(item.getTestDescriptionLast(pos))) {
                    idToTestLongDescription = "";
                    linkToTestLongDescription = "";
                    
                } else {
                    longTestDescNr++;
                    idToTestLongDescription = "" + (numberOfEntries + numberOfLongReviewedEntries +
                            longTestDescNr);
                    linkToTestLongDescription = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("report.detailedTestDescription") + " " +
                            idToTestLongDescription;
                }
                String testDescriptionAuthor = item.getTestDescAuthorLast(pos);
                String testDescriptionDate = item.getTestDescDateLast(pos);
                String svnLabel1;
                if ((svnDescription.isEmpty()) && (svnAuthor.isEmpty())) {
                    svnLabel1 = "";
                } else {
                    svnLabel1 = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("report.svn");
                }
                String svnLabel2;
                if (svnDescription.isEmpty()) {
                    svnLabel2 = "";
                } else {
                    svnLabel2 = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("report.description");
                }
                String reviewLabel1;
                if ((reviewState.isEmpty()) && (reviewStateAuthor.isEmpty()) && 
                        (reviewDescriptionAuthor.isEmpty())) {
                    reviewLabel1 = "";
                } else {
                    reviewLabel1 = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("report.review");
                }
                String reviewLabel2;
                if (reviewDescription.isEmpty()) {
                    reviewLabel2 = "";
                } else {
                    reviewLabel2 = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("report.shortDescription");
                }
                String testLabel1;
                if ((testState.isEmpty()) && (testStateAuthor.isEmpty()) && 
                        (testDescriptionAuthor.isEmpty())) {
                    testLabel1 = "";
                } else {
                    testLabel1 = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("report.test");
                }
                String testLabel2;
                if (testDescription.isEmpty()) {
                    testLabel2 = "";
                } else {
                    testLabel2 = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("report.shortDescription");
                }
                InputStream iconReview;
                switch (item.getReviewStateLast(pos)) {
                    case PASSED:
                        iconReview = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/greenCheck.png");
                        break;
                    case COMMENT:
                        iconReview = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/greenCheck.png");
                        break;
                    case NOTAPPLICABLE:
                        iconReview = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/greenCheck.png");
                        break;
                    case FAILED:
                        iconReview = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/errorCheck.png");
                        break;
                    case INCONCLUSIVE:
                        iconReview = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/highImportanceCheck.png");
                        break;
                    case OPEN:
                        iconReview = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/questionCheck.png");
                        break;
                    default:
                        iconReview = null;
                        break;
                }
                
                InputStream iconTest;
                switch (item.getTestStateLast(pos)) {
                    case PASSED:
                        iconTest = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/greenCheck.png");
                        break;
                    case COMMENT:
                        iconTest = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/greenCheck.png");
                        break;
                    case NOTAPPLICABLE:
                        iconTest = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/greenCheck.png");
                        break;
                    case FAILED:
                        iconTest = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/errorCheck.png");
                        break;
                    case INCONCLUSIVE:
                        iconTest = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/highImportanceCheck.png");
                        break;
                    case OPEN:
                        iconTest = this.getClass().getResourceAsStream(
                                "/lxrdifferencetable/icons/32/questionCheck.png");
                        break;
                    default:
                        iconTest = null;
                        break;
                }
                
                String creationDate = reportCreationDate.format(DateTimeFormatter.
                                ofLocalizedDateTime(FormatStyle.FULL).withLocale(locale));
                String reviewPeriodFrom = "";
                String reviewPeriodTo = "";
                String testPeriodFrom = "";
                String testPeriodTo = "";
    
                String numberTableRows="";
                String numberFiles = "";
                String numberChangedFiles = "";
                String numberNotChangedFiles = "";
                String numberDeletedFiles = "";
                String numberNewFiles = "";
                String numberChanges = "";
                
                String passedReviewed = "";
                String failedReviewed = "";
                String commentReviewed = "";
                String inconclusiveReviewed = "";
                String openReviewed = "";
                String emptyReviewed = "";
                String passedTested = "";
                String failedTested = "";
                String commentTested = "";
                String inconclusiveTested = "";
                String openTested = "";
                String emptyTested = "";
                
                dataBeanList.add(produce(lineNr, title, 
                        programName,
                        reportFileName,
                        titlePart,
                        summaryPart,
                        chapterPart,
                        firstPart,
                        secondPart,
                        thirdPart,
                        name, account, creationDate, link, 
                        lineNumber, 
                        svnVersion, 
                        svnDescription, 
                        svnAuthor, svnDate,
                        reviewState, reviewStateAuthor, reviewStateDate, 
                        reviewDescription, 
                        reviewedBy,
                        reviewLongDescription, 
                        idToReviewLongDescription, 
                        idFromReviewLongDescription, 
                        linkToReviewLongDescription, 
                        linkFromReviewLongDescription, 
                        reviewDescriptionAuthor, 
                        reviewDescriptionDate, 
                        testState, testStateAuthor, testStateDate, 
                        testDescription, 
                        testedBy,
                        testLongDescription, 
                        idToTestLongDescription, 
                        idFromTestLongDescription, 
                        linkToTestLongDescription, 
                        linkFromTestLongDescription, 
                        testDescriptionAuthor, 
                        testDescriptionDate,
                        svnLabel1, svnLabel2,
                        reviewLabel1, reviewLabel2,
                        testLabel1, testLabel2, iconReview, iconTest,
                        reviewPeriodFrom, reviewPeriodTo,
                        testPeriodFrom, testPeriodTo,
                        chapterDirectory,
                        numberTableRows,
                        numberFiles,
                        numberChangedFiles,
                        numberNotChangedFiles,
                        numberDeletedFiles,
                        numberNewFiles,
                        numberChanges,
                        passedReviewed,
                        failedReviewed,
                        commentReviewed,
                        inconclusiveReviewed,
                        openReviewed,
                        emptyReviewed,
                        passedTested,
                        failedTested,
                        commentTested,
                        inconclusiveTested,
                        openTested,
                        emptyTested));
            }
        }
        
        // second pass (only if long descriptions are available)
        if (longReviewDescNr != 0) {
            int lineNrSecondPass = 0;
            for (TableEntry item : changedFileList) {
                int sizeHrefItem = item.getHref().size();
                for (int pos = 0; pos < sizeHrefItem; pos++) {
                    lineNrSecondPass++;
                    titlePart = "";
                    summaryPart = "";
                    String chapterPart = "";
                    String programName = "";
                    String reportFileName = file.getName();
                    String firstPart = "";
                    String secondPart;
                    secondPart = "Second Part";
                    String thirdPart = "";
                    String title = configEntry.getCaptionLast();
                    if (title.equals("")) {
                        title = ResourceBundle.
                                    getBundle("lxrdifferencetable/Bundle", locale).
                                    getString("labelCaption");
                    }
                    String link = "";
                    String lineNumber = "";
                    String name = "";
                    String account = "";
                    String svnVersion = "";
                    String svnDescription = "";
                    String svnAuthor = "";
                    String svnDate = "";
                    String reviewState = ""; 
                    String reviewStateAuthor = ""; 
                    String reviewStateDate = "";
                    String reviewDescription = "";
                    String reviewedBy = "";
                    String reviewLongDescription = item.getReviewDescriptionLast(pos);
                    String idToReviewLongDescription = "";
                    String reviewDescriptionAuthor = ""; 
                    String reviewDescriptionDate = "";
                    String testState = ""; 
                    String testStateAuthor = ""; 
                    String testStateDate = "";
                    String testDescription = "";
                    String testedBy = "";
                    String testLongDescription = ""; 
                    String idToTestLongDescription = "";
                    String idFromTestLongDescription = "";
                    String linkToTestLongDescription = "";
                    String linkFromTestLongDescription = "";
                    String testDescriptionAuthor = ""; 
                    String testDescriptionDate = "";
                    String svnLabel1 = "";
                    String svnLabel2 = "";
                    String reviewLabel1 = "";
                    String reviewLabel2 = "";
                    String testLabel1 = "";
                    String testLabel2 = "";
                    InputStream iconReview = null;
                    InputStream iconTest = null;

                    String linkToReviewLongDescription;
                    String linkFromReviewLongDescription;
                    String idFromReviewLongDescription;
                    if (!emptyHtml(reviewLongDescription)) {
                        // count line if line is not blank
                        lineNr++;
                        linkToReviewLongDescription = ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("report.detailedReviewDescription") + " " +
                                lineNr;
                        linkFromReviewLongDescription = ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("report.backToShortReviewDescription") + " " +
                                lineNrSecondPass;
                        idFromReviewLongDescription = "" + lineNrSecondPass;

                    } else {
                        linkToReviewLongDescription = "";
                        linkFromReviewLongDescription = "";
                        idFromReviewLongDescription = "";
                    }
                    String creationDate = reportCreationDate.format(DateTimeFormatter.
                                    ofLocalizedDateTime(FormatStyle.FULL).withLocale(locale));
                    String reviewPeriodFrom = "";
                    String reviewPeriodTo = "";
                    String testPeriodFrom = "";
                    String testPeriodTo = "";
                    String chapterDirectory = "";
                    
                    String numberTableRows="";
                    String numberFiles = "";
                    String numberChangedFiles = "";
                    String numberNotChangedFiles = "";
                    String numberDeletedFiles = "";
                    String numberNewFiles = "";
                    String numberChanges = "";
                    
                    String passedReviewed = "";
                    String failedReviewed = "";
                    String commentReviewed = "";
                    String inconclusiveReviewed = "";
                    String openReviewed = "";
                    String emptyReviewed = "";
                    String passedTested = "";
                    String failedTested = "";
                    String commentTested = "";
                    String inconclusiveTested = "";
                    String openTested = "";
                    String emptyTested = "";
                    
                    dataBeanList.add(produce(lineNr, title, 
                            programName,
                            reportFileName,
                            titlePart,
                            summaryPart,
                            chapterPart,
                            firstPart,
                            secondPart,
                            thirdPart,
                            name, account, creationDate, link, 
                            lineNumber, 
                            svnVersion, 
                            svnDescription, 
                            svnAuthor, svnDate,
                            reviewState, reviewStateAuthor, reviewStateDate, 
                            reviewDescription, 
                            reviewedBy,
                            reviewLongDescription, 
                            idToReviewLongDescription, 
                            idFromReviewLongDescription, 
                            linkToReviewLongDescription, 
                            linkFromReviewLongDescription, 
                            reviewDescriptionAuthor, 
                            reviewDescriptionDate, 
                            testState, testStateAuthor, testStateDate, 
                            testDescription, 
                            testedBy,
                            testLongDescription,
                            idToTestLongDescription, 
                            idFromTestLongDescription, 
                            linkToTestLongDescription, 
                            linkFromTestLongDescription, 
                            testDescriptionAuthor, 
                            testDescriptionDate,
                            svnLabel1, svnLabel2,
                            reviewLabel1, reviewLabel2,
                            testLabel1, testLabel2, iconReview, iconTest,
                            reviewPeriodFrom, reviewPeriodTo,
                            testPeriodFrom, testPeriodTo,
                            chapterDirectory,
                            numberTableRows,
                            numberFiles,
                            numberChangedFiles,
                            numberNotChangedFiles,
                            numberDeletedFiles,
                            numberNewFiles,
                            numberChanges,
                            passedReviewed,
                            failedReviewed,
                            commentReviewed,
                            inconclusiveReviewed,
                            openReviewed,
                            emptyReviewed,
                            passedTested,
                            failedTested,
                            commentTested,
                            inconclusiveTested,
                            openTested,
                            emptyTested));
                }
            }        
        }
 
        // third pass (only if long descriptions are available)
        if (longTestDescNr != 0) {
            int lineNrThirdPass = 0;
            for (TableEntry item : changedFileList) {
                int sizeHrefItem = item.getHref().size();
                for (int pos = 0; pos < sizeHrefItem; pos++) {
                    lineNrThirdPass++;
                    titlePart = "";
                    summaryPart = "";
                    String chapterPart = "";
                    String programName = "";
                    String reportFileName = file.getName();
                    String firstPart = "";
                    String secondPart = "";
                    String thirdPart;
                    thirdPart = "Third Part";
                    String title = configEntry.getCaptionLast();
                    if (title.equals("")) {
                        title = ResourceBundle.
                                    getBundle("lxrdifferencetable/Bundle", locale).
                                    getString("labelCaption");
                    }
                    String svnVersion = "";
                    String svnDescription = "";
                    String svnAuthor = "";
                    String svnDate = "";
                    String reviewState = ""; 
                    String reviewStateAuthor = ""; 
                    String reviewStateDate = "";
                    String reviewDescription = "";
                    String reviewedBy = "";
                    String reviewLongDescription = "";
                    String idToReviewLongDescription = "";
                    String idFromReviewLongDescription = "";
                    String linkToReviewLongDescription = "";
                    String linkFromReviewLongDescription = "";
                    String reviewDescriptionAuthor = ""; 
                    String reviewDescriptionDate = "";
                    String testState = ""; 
                    String testStateAuthor = ""; 
                    String testStateDate = "";
                    String testDescription = "";
                    String testedBy = "";
                    String testLongDescription = item.getTestDescriptionLast(pos);
                    String idToTestLongDescription = "";
                    String link = "";
                    String lineNumber = "";
                    String name = "";
                    String account = "";
                    String testDescriptionAuthor = ""; 
                    String testDescriptionDate = "";
                    String svnLabel1 = "";
                    String svnLabel2 = "";
                    String reviewLabel1 = "";
                    String reviewLabel2 = "";
                    String testLabel1 = "";
                    String testLabel2 = "";
                    InputStream iconReview = null;
                    InputStream iconTest = null;

                    String linkToTestLongDescription;
                    String linkFromTestLongDescription;
                    String idFromTestLongDescription;
                    if (!emptyHtml(testLongDescription)) {
                        // count line if line is not blank
                        lineNr++;
                        linkToTestLongDescription = ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("report.detailedTestDescription") + " " +
                                lineNr;
                        linkFromTestLongDescription = ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("report.backToShortTestDescription") + " " +
                                lineNrThirdPass;
                        idFromTestLongDescription = "" + lineNrThirdPass;
                    } else {
                        linkToTestLongDescription = "";
                        linkFromTestLongDescription = "";
                        idFromTestLongDescription = "";
                    }
                    String creationDate = reportCreationDate.format(DateTimeFormatter.
                                    ofLocalizedDateTime(FormatStyle.FULL).withLocale(locale));
                    String reviewPeriodFrom = "";
                    String reviewPeriodTo = "";
                    String testPeriodFrom = "";
                    String testPeriodTo = "";
                    String chapterDirectory = "";

                    String numberTableRows="";
                    String numberFiles = "";
                    String numberChangedFiles = "";
                    String numberNotChangedFiles = "";
                    String numberDeletedFiles = "";
                    String numberNewFiles = "";
                    String numberChanges = "";

                    String passedReviewed = "";
                    String failedReviewed = "";
                    String commentReviewed = "";
                    String inconclusiveReviewed = "";
                    String openReviewed = "";
                    String emptyReviewed = "";
                    String passedTested = "";
                    String failedTested = "";
                    String commentTested = "";
                    String inconclusiveTested = "";
                    String openTested = "";
                    String emptyTested = "";
                    
                    dataBeanList.add(produce(lineNr, title, 
                            programName,
                            reportFileName,
                            titlePart,
                            summaryPart,
                            chapterPart,
                            firstPart,
                            secondPart,
                            thirdPart,
                            name, account, creationDate, link, 
                            lineNumber, 
                            svnVersion, 
                            svnDescription, 
                            svnAuthor, svnDate,
                            reviewState, reviewStateAuthor, reviewStateDate, 
                            reviewDescription, 
                            reviewedBy,
                            reviewLongDescription, 
                            idToReviewLongDescription, 
                            idFromReviewLongDescription, 
                            linkToReviewLongDescription, 
                            linkFromReviewLongDescription, 
                            reviewDescriptionAuthor, 
                            reviewDescriptionDate, 
                            testState, testStateAuthor, testStateDate, 
                            testDescription, 
                            testedBy,
                            testLongDescription,
                            idToTestLongDescription, 
                            idFromTestLongDescription, 
                            linkToTestLongDescription, 
                            linkFromTestLongDescription, 
                            testDescriptionAuthor, 
                            testDescriptionDate,
                            svnLabel1, svnLabel2,
                            reviewLabel1, reviewLabel2,
                            testLabel1, testLabel2, iconReview, iconTest,
                            reviewPeriodFrom, reviewPeriodTo,
                            testPeriodFrom, testPeriodTo,
                            chapterDirectory,
                            numberTableRows,
                            numberFiles,
                            numberChangedFiles,
                            numberNotChangedFiles,
                            numberDeletedFiles,
                            numberNewFiles,
                            numberChanges,
                            passedReviewed,
                            failedReviewed,
                            commentReviewed,
                            inconclusiveReviewed,
                            openReviewed,
                            emptyReviewed,
                            passedTested,
                            failedTested,
                            commentTested,
                            inconclusiveTested,
                            openTested,
                            emptyTested));
                }
            }        
        }
        return dataBeanList;
    }
 
    private DataBean produce(int lineNr, 
            String title, 
            String programName,
            String reportFileName,
            String titlePart, 
            String summaryPart, 
            String chapterPart,
            String firstPart, String secondPart, String thirdPart,
            String name, 
            String account, 
            String creationDate, 
            String link, String lineNumber, String svnVersion,
            String svnDescription, 
            String svnAuthor, String svnDate,
            String reviewState, String reviewStateAuthor, 
            String reviewStateDate,
            String reviewDescription,  
            String reviewedBy,
            String reviewLongDescription, 
            String idToReviewLongDescription, 
            String idFromReviewLongDescription, 
            String linkToReviewLongDescription,
            String linkFromReviewLongDescription,
            String reviewDescriptionAuthor,
            String reviewDescriptionDate,
            String testState, String testStateAuthor, String testStateDate,
            String testDescription, 
            String testedBy,
            String testLongDescription, 
            String idToTestLongDescription, 
            String idFromTestLongDescription, 
            String linkToTestLongDescription,
            String linkFromTestLongDescription,
            String testDescriptionAuthor,
            String testDescriptionDate,
            String svnLabel1, String svnLabel2,
            String reviewLabel1, String reviewLabel2,
            String testLabel1, String testLabel2,
            InputStream iconReview, InputStream iconTest,
            String reviewPeriodFrom, String reviewPeriodTo,
            String testPeriodFrom, String testPeriodTo,
            String chapterDirectory,
            String numberTableRows,
            String numberFiles,
            String numberChangedFiles,
            String numberNotChangedFiles,
            String numberDeletedFiles,
            String numberNewFiles,
            String numberChanges,
            String passedReviewed,
            String failedReviewed,
            String commentReviewed,
            String inconclusiveReviewed,
            String openReviewed,
            String emptyReviewed,
            String passedTested,
            String failedTested,
            String commentTested,
            String inconclusiveTested,
            String openTested,
            String emptyTested) {
	DataBean dataBean = new DataBean();
 
	dataBean.setLineNr(lineNr);
	dataBean.setTitle(title);
	dataBean.setProgramName(programName);
        dataBean.setReportFileName(reportFileName);
        dataBean.setTitlePart(titlePart);
        dataBean.setSummaryPart(summaryPart);
        dataBean.setChapterPart(chapterPart);
        dataBean.setFirstPart(firstPart);
        dataBean.setSecondPart(secondPart);
        dataBean.setThirdPart(thirdPart);
        dataBean.setName(name);
        dataBean.setAccount(account);
        dataBean.setCreationDate(creationDate);
	dataBean.setLink(link);
	dataBean.setLineNumber(lineNumber);
	dataBean.setSvnVersion(svnVersion);
	dataBean.setSvnDescription(svnDescription);
	dataBean.setSvnAuthor(svnAuthor);
	dataBean.setSvnDate(svnDate);
	dataBean.setReviewState(reviewState);
	dataBean.setReviewStateAuthor(reviewStateAuthor);
	dataBean.setReviewStateDate(reviewStateDate);
	dataBean.setReviewDescription(reviewDescription);
	dataBean.setReviewedBy(reviewedBy);
	dataBean.setReviewLongDescription(reviewLongDescription);
	dataBean.setIdToReviewLongDescription(idToReviewLongDescription);
	dataBean.setIdFromReviewLongDescription(idFromReviewLongDescription);
	dataBean.setLinkToReviewLongDescription(linkToReviewLongDescription);
	dataBean.setLinkFromReviewLongDescription(linkFromReviewLongDescription);
	dataBean.setReviewDescriptionAuthor(reviewDescriptionAuthor);
	dataBean.setReviewDescriptionDate(reviewDescriptionDate);
	dataBean.setTestState(testState);
	dataBean.setTestStateAuthor(testStateAuthor);
	dataBean.setTestStateDate(testStateDate);
	dataBean.setTestDescription(testDescription);
	dataBean.setTestedBy(testedBy);
	dataBean.setTestLongDescription(testLongDescription);
	dataBean.setIdToTestLongDescription(idToTestLongDescription);
	dataBean.setIdFromTestLongDescription(idFromTestLongDescription);
	dataBean.setLinkToTestLongDescription(linkToTestLongDescription);
	dataBean.setLinkFromTestLongDescription(linkFromTestLongDescription);
	dataBean.setTestDescriptionAuthor(testDescriptionAuthor);
	dataBean.setTestDescriptionDate(testDescriptionDate);
        dataBean.setSvnLabel1(svnLabel1);
        dataBean.setSvnLabel2(svnLabel2);
        dataBean.setReviewLabel1(reviewLabel1);
        dataBean.setReviewLabel2(reviewLabel2);
        dataBean.setTestLabel1(testLabel1);
        dataBean.setTestLabel2(testLabel2);
        dataBean.setIconReview(iconReview);
        dataBean.setIconTest(iconTest);

	dataBean.setReviewPeriodFrom(reviewPeriodFrom);
	dataBean.setReviewPeriodTo(reviewPeriodTo);
	dataBean.setTestPeriodFrom(testPeriodFrom);
	dataBean.setTestPeriodTo(testPeriodTo);
        dataBean.setChapterDirectory(chapterDirectory);
        
        // some statistics
	dataBean.setNumberTableRows(numberTableRows);
        dataBean.setNumberFiles(numberFiles);
        dataBean.setNumberChangedFiles(numberChangedFiles);
        dataBean.setNumberNotChangedFiles(numberNotChangedFiles);
        dataBean.setNumberDeletedFiles(numberDeletedFiles);
        dataBean.setNumberNewFiles(numberNewFiles);
        dataBean.setNumberChanges(numberChanges);
        
        dataBean.setPassedReviewed(passedReviewed);
        dataBean.setFailedReviewed(failedReviewed);
        dataBean.setCommentReviewed(commentReviewed);
        dataBean.setInconclusiveReviewed(inconclusiveReviewed);
        dataBean.setOpentReviewed(openReviewed);
        dataBean.setEmptyReviewed(emptyReviewed);

        dataBean.setPassedTested(passedTested);
        dataBean.setFailedTested(failedTested);
        dataBean.setCommentTested(commentTested);
        dataBean.setInconclusiveTested(inconclusiveTested);
        dataBean.setOpentTested(openTested);
        dataBean.setEmptyTested(emptyTested);
        
        
	return dataBean;
    }
    
    private boolean emptyHtml(String str) {
        String beginStr = "<html dir=\"ltr\"><head></head><body contenteditable=\"true\">";
        String endStr = "</body></html>";
        boolean empty = false;
        if (str.equals("")) {
            empty = true;
        } else {
            str = str.replace(beginStr, "");
            str = str.replace(endStr, "");
            if (str.equals("")) {
                empty = true;
            }
        }
        return empty;
    }
    
}
