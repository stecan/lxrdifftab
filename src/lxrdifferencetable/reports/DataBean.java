/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.reports;

import java.io.InputStream;

/**
 *
 * @author Stefan Canali
 */
public class DataBean {
    private int lineNr;                     // line number
    private String title;                   // title of report
    private String programName;
    private String reportFileName;
    private String titlePart;               // sections of report
    private String summaryPart;
    private String chapterPart;
    private String firstPart;
    private String secondPart;
    private String thirdPart;
    private String account;                 // account of user
    private String creationDate;            // date of creation of report
    private String name;
    private String link;
    private String lineNumber;
    private String svnVersion;
    private String svnDescription;
    private String svnAuthor;
    private String svnDate;
    private String reviewState;
    private String reviewStateAuthor;
    private String reviewStateDate;
    private String reviewDescription;       // short review description
    private String reviewLongDescription;   // long review description
    private String idToReviewLongDescription;       // e.g "201" 
    private String idFromReviewLongDescription;     // e.g "1"
    private String linkToReviewLongDescription;     // name of hyperlink to desc.
    private String linkFromReviewLongDescription;   // name of hyperlink from desc.
    private String reviewDescriptionAuthor;
    private String reviewedBy;
    private String reviewDescriptionDate;
    private String testState;
    private String testStateAuthor;
    private String testStateDate;
    private String testDescription;         // short test description
    private String testLongDescription;     // long test description
    private String idToTestLongDescription;         // e.g "201" 
    private String idFromTestLongDescription;       // e.g "1"
    private String linkToTestLongDescription;       // name of hyperlink to desc.
    private String linkFromTestLongDescription;     // name of hyperlink from desc.
    private String testDescriptionAuthor;
    private String testedBy;
    private String testDescriptionDate;
    private String svnLabel1;
    private String svnLabel2;
    private String reviewLabel1;
    private String reviewLabel2;
    private String testLabel1;
    private String testLabel2;
    private InputStream iconReview;
    private InputStream iconTest;
    private String reviewPeriodFrom;
    private String reviewPeriodTo;
    private String testPeriodFrom;
    private String testPeriodTo;
    // some statistics
    private String numberTableRows;
    private String numberFiles;
    private String numberChangedFiles;
    private String numberNotChangedFiles;
    private String numberDeletedFiles;
    private String numberNewFiles;
    private String numberChanges;
    private String chapterDirectory;
    
    private String passedReviewed;
    private String failedReviewed;
    private String commentReviewed;
    private String inconclusiveReviewed;
    private String openReviewed;
    private String emptyReviewed;
    private String passedTested;
    private String failedTested;
    private String commentTested;
    private String inconclusiveTested;
    private String openTested;
    private String emptyTested;
    
 
    public void setLineNr(int lineNr) {
	this.lineNr = lineNr;
    }
 
    public int getLineNr() {
	return lineNr;
    }
 
    public void setTitle(String title) {
	this.title = title;
    }
 
    public String getTitle() {
	return title;
    }
 
    public void setProgramName(String programName) {
	this.programName = programName;
    }
 
    public String getProgramName() {
	return programName;
    }
 
    public void setReportFileName(String reportFileName) {
	this.reportFileName = reportFileName;
    }
 
    public String getReportFileName() {
	return reportFileName;
    }
 
    public void setTitlePart(String titlePart) { 
	this.titlePart = titlePart;
    }
 
    public String getTitlePart() {
	return titlePart;
    }
 
    public void setSummaryPart(String summaryPart) { 
	this.summaryPart = summaryPart;
    }
 
    public String getSummaryPart() {
	return summaryPart;
    }
 
    public void setChapterPart(String chapterPart) { 
	this.chapterPart = chapterPart;
    }
 
    public String getChapterPart() {
	return chapterPart;
    }
 
    public void setFirstPart(String firstPart) { 
	this.firstPart = firstPart;
    }
 
    public String getFirstPart() {
	return firstPart;
    }
 
    public void setSecondPart(String secondPart) { 
	this.secondPart = secondPart;
    }
 
    public String getSecondPart() {
	return secondPart;
    }
 
    public void setThirdPart(String thirdPart) { 
	this.thirdPart = thirdPart;
    }
 
    public String getThirdPart() {
	return thirdPart;
    }
 
    public void setName(String name) {
	this.name = name;
    }
 
    public String getName() {
	return name;
    }
 
    public void setAccount(String account) {
	this.account = account;
    }
 
    public String getAccount() {
	return account;
    }
 
    public void setCreationDate(String creationDate) {
	this.creationDate = creationDate;
    }
 
    public String getCreationDate() {
	return creationDate;
    }
 
    public void setLink(String link) {
	this.link = link;
    }
 
    public String getLink() {
	return link;
    }
 
    public void setLineNumber(String lineNumber) {
	this.lineNumber = lineNumber;
    }
 
    public String getLineNumber() {
	return lineNumber;
    }
 
    public void setSvnVersion(String svnVersion) {
	this.svnVersion = svnVersion;
    }
 
    public String getSvnVersion() {
	return svnVersion;
    }

    public void setSvnDescription(String svnDescription) {
	this.svnDescription = svnDescription;
    }
 
    public String getSvnDescription() {
	return svnDescription;
    }

    public void setSvnAuthor(String svnAuthor) {
	this.svnAuthor = svnAuthor;
    }
 
    public String getSvnAuthor() {
	return svnAuthor;
    }

    public void setSvnDate(String svnDate) {
	this.svnDate = svnDate;
    }
 
    public String getSvnDate() {
	return svnDate;
    }

    public void setReviewState(String reviewState) {
	this.reviewState = reviewState;
    }
 
    public String getReviewState() {
	return reviewState;
    }

    public void setReviewStateAuthor(String reviewStateAuthor) {
	this.reviewStateAuthor = reviewStateAuthor;
    }
 
    public String getReviewStateAuthor() {
	return reviewStateAuthor;
    }

    public void setReviewedBy(String reviewedBy) {
	this.reviewedBy = reviewedBy;
    }
 
    public String getReviewedBy() {
	return reviewedBy;
    }

    public void setReviewStateDate(String reviewStateDate) {
	this.reviewStateDate = reviewStateDate;
    }
 
    public String getReviewStateDate() {
	return reviewStateDate;
    }

    public void setReviewDescription(String reviewDescription) {
	this.reviewDescription = reviewDescription;
    }
 
    public String getReviewDescription() {
	return reviewDescription;
    }
    
    public void setReviewLongDescription(String reviewLongDescription) {
	this.reviewLongDescription = reviewLongDescription;
    }
 
    public String getReviewLongDescription() {
	return reviewLongDescription;
    }
    
    public void setIdToReviewLongDescription(String idToReviewLongDescription) {
	this.idToReviewLongDescription = idToReviewLongDescription;
    }
 
    public String getIdToReviewLongDescription() {
	return idToReviewLongDescription;
    }
    
    public void setIdFromReviewLongDescription(String idFromReviewLongDescription) {
	this.idFromReviewLongDescription = idFromReviewLongDescription;
    }
 
    public String getIdFromReviewLongDescription() {
	return idFromReviewLongDescription;
    }
    
    public void setLinkToReviewLongDescription(String linkToReviewLongDescription) {
	this.linkToReviewLongDescription = linkToReviewLongDescription;
    }
 
    public String getLinkToReviewLongDescription() {
	return linkToReviewLongDescription;
    }
    
    public void setLinkFromReviewLongDescription(String linkFromReviewLongDescription) {
	this.linkFromReviewLongDescription = linkFromReviewLongDescription;
    }
 
    public String getLinkFromReviewLongDescription() {
	return linkFromReviewLongDescription;
    }
    
    public void setReviewDescriptionAuthor(String reviewDescriptionAuthor) {
	this.reviewDescriptionAuthor = reviewDescriptionAuthor;
    }
 
    public String getReviewDescriptionAuthor() {
	return reviewDescriptionAuthor;
    }
    
    public void setTestedBy(String testedBy) {
	this.testedBy = testedBy;
    }
 
    public String getTestedBy() {
	return testedBy;
    }

    public void setReviewDescriptionDate(String reviewDescriptionDate) {
	this.reviewDescriptionDate = reviewDescriptionDate;
    }
 
    public String getReviewDescriptionDate() {
	return reviewDescriptionDate;
    }
    
    public void setTestState(String testState) {
	this.testState = testState;
    }
 
    public String getTestState() {
	return testState;
    }

    public void setTestStateAuthor(String testStateAuthor) {
	this.testStateAuthor = testStateAuthor;
    }
 
    public String getTestStateAuthor() {
	return testStateAuthor;
    }

    public void setTestStateDate(String testStateDate) {
	this.testStateDate = testStateDate;
    }
 
    public String getTestStateDate() {
	return testStateDate;
    }

    public void setTestDescription(String testDescription) {
	this.testDescription = testDescription;
    }
 
    public String getTestDescription() {
	return testDescription;
    }
    
    public void setTestLongDescription(String testLongDescription) {
	this.testLongDescription = testLongDescription;
    }
 
    public String getTestLongDescription() {
	return testLongDescription;
    }
    
    public void setIdToTestLongDescription(String idToTestLongDescription) {
	this.idToTestLongDescription = idToTestLongDescription;
    }
 
    public String getIdToTestLongDescription() {
	return idToTestLongDescription;
    }
    
    public void setIdFromTestLongDescription(String idFromTestLongDescription) {
	this.idFromTestLongDescription = idFromTestLongDescription;
    }
 
    public String getIdFromTestLongDescription() {
	return idFromTestLongDescription;
    }
    
    public void setLinkToTestLongDescription(String linkToTestLongDescription) {
	this.linkToTestLongDescription = linkToTestLongDescription;
    }
 
    public String getLinkToTestLongDescription() {
	return linkToTestLongDescription;
    }
    
    public void setLinkFromTestLongDescription(String linkFromTestLongDescription) {
	this.linkFromTestLongDescription = linkFromTestLongDescription;
    }
 
    public String getLinkFromTestLongDescription() {
	return linkFromTestLongDescription;
    }
    public void setTestDescriptionAuthor(String testDescriptionAuthor) {
	this.testDescriptionAuthor = testDescriptionAuthor;
    }
 
    public String getTestDescriptionAuthor() {
	return testDescriptionAuthor;
    }

    public void setTestDescriptionDate(String testDescriptionDate) {
	this.testDescriptionDate = testDescriptionDate;
    }
 
    public String getTestDescriptionDate() {
	return testDescriptionDate;
    }

    public void setSvnLabel1(String svnLabel1) {
	this.svnLabel1 = svnLabel1;
    }
 
    public String getSvnLabel1() {
	return svnLabel1;
    }

    public void setSvnLabel2(String svnLabel2) {
	this.svnLabel2 = svnLabel2;
    }
 
    public String getSvnLabel2() {
	return svnLabel2;
    }

    public void setReviewLabel1(String reviewLabel1) {
	this.reviewLabel1 = reviewLabel1;
    }
 
    public String getReviewLabel1() {
	return reviewLabel1;
    }

    public void setReviewLabel2(String reviewLabel2) {
	this.reviewLabel2 = reviewLabel2;
    }
 
    public String getReviewLabel2() {
	return reviewLabel2;
    }

    public void setTestLabel1(String testLabel1) {
	this.testLabel1 = testLabel1;
    }
 
    public String getTestLabel1() {
	return testLabel1;
    }

    public void setTestLabel2(String testLabel2) {
	this.testLabel2 = testLabel2;
    }
 
    public String getTestLabel2() {
	return testLabel2;
    }

    public void setIconReview(InputStream iconReview) {
	this.iconReview = iconReview;
    }
 
    public InputStream getIconReview() {
	return iconReview;
    }

    public void setIconTest(InputStream iconTest) {
	this.iconTest = iconTest;
    }
 
    public InputStream getIconTest() {
	return iconTest;
    }

    public String getReviewPeriodFrom() {
	return reviewPeriodFrom;
    }
 
    public void setReviewPeriodFrom(String reviewPeriodFrom) { 
	this.reviewPeriodFrom = reviewPeriodFrom;
    }

    public String getReviewPeriodTo() {
	return reviewPeriodTo;
    }
 
    public void setReviewPeriodTo(String reviewPeriodTo) { 
	this.reviewPeriodTo = reviewPeriodTo;
    }

    public String getTestPeriodFrom() {
	return testPeriodFrom;
    }
 
    public void setTestPeriodFrom(String testPeriodFrom) { 
	this.testPeriodFrom = testPeriodFrom;
    }

    public String gettestPeriodTo() {
	return testPeriodTo;
    }
 
    public void setTestPeriodTo(String testPeriodTo) { 
	this.testPeriodTo = testPeriodTo;
    }

    public String getChapterDirectory() {
	return chapterDirectory;
    }
 
    public void setChapterDirectory(String chapterDirectory) { 
	this.chapterDirectory = chapterDirectory;
    }

    public String getNumberTableRows() {
	return numberTableRows;
    }
 
    public void setNumberTableRows(String numberTableRows) { 
	this.numberTableRows = numberTableRows;
    }

    public String getNumberFiles() {
	return numberFiles;
    }
 
    public void setNumberFiles(String numberFiles) { 
	this.numberFiles = numberFiles;
    }

    public String getNumberChangedFiles() {
	return numberChangedFiles;
    }
 
    public void setNumberChangedFiles(String numberChangedFiles) { 
	this.numberChangedFiles = numberChangedFiles;
    }

    public String getNumberNotChangedFiles() {
	return numberNotChangedFiles;
    }
 
    public void setNumberNotChangedFiles(String numberNotChangedFiles) { 
	this.numberNotChangedFiles = numberNotChangedFiles;
    }

    public String getNumberDeletedFiles() {
	return numberDeletedFiles;
    }
 
    public void setNumberDeletedFiles(String numberDeletedFiles) { 
	this.numberDeletedFiles = numberDeletedFiles;
    }

    public String getNumberNewFiles() {
	return numberNewFiles;
    }
 
    public void setNumberNewFiles(String numberNewFiles) { 
	this.numberNewFiles = numberNewFiles;
    }

    public String getNumberChanges() {
	return numberChanges;
    }
 
    public void setNumberChanges(String numberChanges) { 
	this.numberChanges = numberChanges;
    }

    public String getPassedReviewed() {
	return passedReviewed;
    }
 
    public void setPassedReviewed(String passedReviewed) { 
	this.passedReviewed = passedReviewed;
    }

    public String getFailedReviewed() {
	return failedReviewed;
    }
 
    public void setFailedReviewed(String failedReviewed) { 
	this.failedReviewed = failedReviewed;
    }

    public String getCommentReviewed() {
	return commentReviewed;
    }
 
    public void setCommentReviewed(String commentReviewed) { 
	this.commentReviewed = commentReviewed;
    }

    public String getInconclusiveReviewed() {
	return inconclusiveReviewed;
    }
 
    public void setInconclusiveReviewed(String inconclusiveReviewed) { 
	this.inconclusiveReviewed = inconclusiveReviewed;
    }

    public String getOpenReviewed() {
	return openReviewed;
    }
 
    public void setOpentReviewed(String openReviewed) { 
	this.openReviewed = openReviewed;
    }

    public String getEmptyReviewed() {
	return emptyReviewed;
    }
 
    public void setEmptyReviewed(String emptyReviewed) { 
	this.emptyReviewed = emptyReviewed;
    }

    public String getPassedTested() {
	return passedTested;
    }
 
    public void setPassedTested(String passedTested) { 
	this.passedTested = passedTested;
    }

    public String getFailedTested() {
	return failedTested;
    }
 
    public void setFailedTested(String failedTested) { 
	this.failedTested = failedTested;
    }

    public String getCommentTested() {
	return commentTested;
    }
 
    public void setCommentTested(String commentTested) { 
	this.commentTested = commentTested;
    }

    public String getInconclusiveTested() {
	return inconclusiveTested;
    }
 
    public void setInconclusiveTested(String inconclusiveTested) { 
	this.inconclusiveTested = inconclusiveTested;
    }

    public String getOpenTested() {
	return openTested;
    }
 
    public void setOpentTested(String openTested) { 
	this.openTested = openTested;
    }

    public String getEmptyTested() {
	return emptyTested;
    }
 
    public void setEmptyTested(String emptyTested) { 
	this.emptyTested = emptyTested;
    }

}
