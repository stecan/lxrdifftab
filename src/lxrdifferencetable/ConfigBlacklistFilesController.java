/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import static lxrdifferencetable.LxrDifferenceTable.BLACKLIST_FILES_STAGE;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.Utils;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class ConfigBlacklistFilesController implements Initializable, ControlledStage  {

    Properties props;
    Utils utils;

    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;                        
    
    public ConfigBlacklistFilesController() {
        this.props = new Properties();
        this.utils = new Utils();
    }
    
    private StagesController myController;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    Button abortConfig;

    @FXML
    Button saveConfig;
    
    @FXML
    ListView<String> blist;     // blacklist

    @FXML
    private void handleAbort(ActionEvent event) {
        initialize(null, null);
        myController.hideStage(BLACKLIST_FILES_STAGE);
    }
    
    @FXML
    private void handleSave(ActionEvent event) {
        savePreferences();
        myController.hideStage(BLACKLIST_FILES_STAGE);
    }
    
    @FXML
    private void handleFile(ActionEvent event) {
        Stage stage;
        String lastDir;
        FileChooser fileChooser;
        List<File> selectedFiles;
        
        lastDir = props.getProperty(props.BASEDIR);
        stage = myController.getStage(BLACKLIST_FILES_STAGE);

        fileChooser = new FileChooser();
        fileChooser.setTitle(ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("buttonSelectFiles"));
        fileChooser.setInitialDirectory(new File(lastDir));
        selectedFiles = fileChooser.showOpenMultipleDialog(stage);

        if(selectedFiles != null){

            String[] data;
            String blacklist;
            ObservableList<String> itemsBlacklist;
            List<File> fileList;
            
            // get all blacklist items
            itemsBlacklist = blist.getItems();

            // create comma separated string of blacklist items
            blacklist = utils.createCommaSeparatedString(itemsBlacklist);

            // add selected items to blacklist
            fileList = utils.leaf(selectedFiles);
            blacklist = utils.addToCommaSeparatedString(
                    blacklist, 
                    fileList);

            if (blacklist.equals("")) {
                data = new String[0];
            } else {
                data = blacklist.split(",");
            }

            itemsBlacklist = FXCollections.observableArrayList();   // delete old one
            itemsBlacklist.addAll(Arrays.asList(data)); // add items
            blist.setItems(itemsBlacklist);             // show items
            
            // mark new items in blacklist ListView
            for (int i = 0; i < fileList.size(); i++) {
                String f = fileList.get(i).toString();
                blist.getSelectionModel().select(f);
            }
            
        }
    }

    @FXML
    private void handleDirectory(ActionEvent event) {
        Stage stage;
        String dir;
        
        dir = props.getProperty(props.BASEDIR);
        DirectoryChooser directoryChooser = new DirectoryChooser();
        if (!dir.equals("")) {
            File file = new File(dir);
            if (file.exists()) {
                directoryChooser.setInitialDirectory(file);
            } else {
                // if the file doesn't exists, then choose user home directory
                directoryChooser.setInitialDirectory(new File (System.getProperty("user.home")));
            }
        }
        directoryChooser.setTitle(ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("buttonSelectDirectory"));
        
        stage = myController.getStage(BLACKLIST_FILES_STAGE);
        File selectedDirectory = 
                directoryChooser.showDialog(stage);

        if(selectedDirectory != null){

            String[] data;
            String blacklist;
            ObservableList<String> itemsBlacklist;
            File file;
            
            // get all blacklist items
            itemsBlacklist = blist.getItems();

            // create comma separated string of blacklist items
            blacklist = utils.createCommaSeparatedString(itemsBlacklist);

            // add selected items to blacklist
            file = utils.leaf(selectedDirectory);
            blacklist = utils.addToCommaSeparatedString(blacklist, 
                    file);

            if (blacklist.equals("")) {
                data = new String[0];
            } else {
                data = blacklist.split(",");
            }

            itemsBlacklist = FXCollections.observableArrayList();   // delete old one
            itemsBlacklist.addAll(Arrays.asList(data)); // add items
            blist.setItems(itemsBlacklist);             // show items

            // mark new item in blacklist ListView
            blist.getSelectionModel().select(file.toString());
        }
    }
    
    @FXML
    private void handleMenuItemDelete(ActionEvent event) {
        ObservableList<String> selectedItems;
        selectedItems = blist.getSelectionModel().getSelectedItems();
        
        // copy items to dataArray
        ArrayList<String> dataArray;
        dataArray = new ArrayList<>();
        selectedItems.stream().forEach((item) -> {
            dataArray.add(item);
        });
        
        // delete selected items!
        dataArray.stream().forEach((item) -> {
            blist.getItems().remove(item);
        });
    }
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // locale settings
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);

        // init main
        ObservableList<String> items;
        String blacklist;
        String[] dataBlacklist;

        blacklist = props.getProperty(props.BLACKLISTFILES);

        if (blacklist.equals("")) {
            dataBlacklist = new String[0];
        } else {
            dataBlacklist = blacklist.split(",");
        }

        items = FXCollections.observableArrayList ();
        items.addAll(Arrays.asList(dataBlacklist));
        blist.setItems(items);    
        blist.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }    
    
    private void savePreferences() {
        ObservableList<String> items;
        String blacklist;
        
        items = blist.getItems();
        blacklist = utils.createCommaSeparatedString(items);
        props.putProperty(props.BLACKLISTFILES, blacklist);

        // Set new values to text fields!
        // 
        // Note:
        // This step is very important. 
        // If a change is taken, then the text area wouldn't be refreshed 
        // exept in case of a new start!
        initialize(null, null);
    }
    
}
