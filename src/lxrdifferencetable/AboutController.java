/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import static lxrdifferencetable.LxrDifferenceTable.ABOUT_STAGE;
import lxrdifferencetable.tools.Properties;


/**
 * FXML controller class to handle the help menu "About".
 * 
 * @author Stefan Canali
 */
public class AboutController implements Initializable, ControlledStage {
    
    private StagesController myController;
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;                        

    public AboutController () {
        this.props = new Properties();
    }
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    Label labelVersion;
    
    @FXML
    Button closeAbout;
    
    @FXML
    MenuItem about;

    @FXML
    private void handleClose(ActionEvent event) {
        myController.hideStage(ABOUT_STAGE);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // locale settings
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
        String version;
        version = props.getProperty(props.VERSION);
        version = ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("version") + ": " + version;
        labelVersion.setText(version);
    }    
    
}
