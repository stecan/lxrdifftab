/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import lxrdifferencetable.tools.Properties;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author stefan
 */
public class StagesController extends Stage {

    static final Logger logger = Logger.getLogger
        (LxrDifferenceTableController.class.getName());
    
    // Holds the stages to be displayed
    private final HashMap<String, Stage> stages;
    private final HashMap<Stage, FXMLLoader> loader;
    
    private Locale locale;                              // locale for i18n
    
    private final Properties props = new Properties();  // new preferences
    private String lang = "";                           // language preference
    private String country = "";                        // country preference
    
    
    public StagesController() {
        super();
        stages = new HashMap<>();
        loader = new HashMap<>();
    }

    //Adds the stage to the collection
    public void addStage(String name, Stage stage) {
        stages.put(name, stage);
    }

    //Returns the Node with the appropriate name
    public Stage getStage(String name) {
        return stages.get(name);
    }
    
    // Loads the fxml file, add the stage to the stages collection and
    // finally injects the stage to the controller.
    public boolean loadStage(String name, boolean reloaded) {
        try {
            // get locale settings
            ResourceBundle i18nBundle;
            lang = props.getProperty(props.LANG);
            country = props.getProperty(props.COUNTRY);
            if ((lang.equals("")) || (country.equals(""))) {
                // It seems to be the first run. 
                // Try to get the locale system settings
                lang = System.getProperty("user.language");
                country = System.getProperty("user.country");
                // Save the locale settings in the own preferences
                props.putProperty(props.LANG, lang);
                props.putProperty(props.COUNTRY, country);
            }
            locale = new Locale(lang, country);
            i18nBundle = ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle",locale);

            // load FXML document
            FXMLLoader myLoader = new FXMLLoader(getClass().
                    getResource(name + ".fxml"), i18nBundle);
            Parent parent = (Parent) myLoader.load();
            
            Scene scene = new Scene(parent);
            Stage stage = new Stage();
            stage.setTitle(i18nBundle.getString("programName"));
            stage.setScene(scene);
            stage.initModality(Modality.WINDOW_MODAL);
            addStage(name, stage);

            ControlledStage stageController;
            stageController = (ControlledStage) myLoader.getController();
            stageController.setStageParent(this, reloaded);
            loader.put(stage, myLoader);

            return true;
        } catch (IOException e) {
            logger.warning("StagesController: " + e.getMessage());
            return false;
        }
    }

    // This method tries to show the stage with a predefined name.
    // First, it makes sure the stage has been already loaded. Then the stage
    // is showed.
    public boolean showStage(final String name) {       
        if (stages.get(name) != null) {   //screen loaded

            logger.info("StagesController: " + "Beginn: Show Stage");
            getStage(name).getIcons()
                    .add(new Image("lxrdifferencetable/icons/lxrdifftab-icon.png"));
            getStage(name).show();
            logger.info("StagesController: " + "End: Show Stage");
            return true;
        } else {
            logger.warning("StagesController: " + "Stage hasn't been loaded!");
            return false;
        }
    }
    
    // This method tries to show the stage with a predefined name.
    // First, it makes sure the stage has been already loaded. Then the stage
    // is showed.
    public boolean isShowingStage(final String name) {       
        if (stages.get(name) != null) {   //screen loaded
            return getStage(name).isShowing();
        } else {
            logger.warning("StagesController: " + "Stage hasn't been loaded!");
            return false;
        }
    }

    // This method tries to hide the stage with a predefined name.
    // First, it makes sure the stage has been already loaded. Then the stage
    // is hided.
    public boolean hideStage(final String name) {
        if (stages.get(name) != null) {   //screen loaded

            getStage(name).hide();
            return true;
        } else {
            logger.warning("StagesController: " + "Stage hasn't been loaded!");
            return false;
        }
    }
    
    //This method will remove the stage with the given name from the collection 
    // of stages
    public boolean unloadSage(String name) {
        if (stages.remove(name) == null) {
            logger.warning("StagesController: " + "Stage didn't exist");
            return false;
        } else {
            return true;
        }
    }
    
    public FXMLLoader getLoader(String name) {
        if (stages.get(name) != null) {   //screen loaded
            return loader.get(getStage(name));
        } else {
            return null;
        }
    }
}
