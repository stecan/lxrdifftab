/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.tablecells;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import lxrdifferencetable.data.DescDataModel;

/**
 *
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class DescriptionEditingCell extends TableCell<DescDataModel, String> {
    private TextArea textField;
    
    @Override
    public void startEdit() {
        super.startEdit();
        if (textField == null) {
            createTextField();
        }
        setGraphic(textField);
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        Platform.runLater(() -> {
            textField.requestFocus();
            // textField.selectAll();
        });
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        String item = getItem();
        
        Text text = new Text(item);
        setGraphic(text);
        text.wrappingWidthProperty().bind(this.widthProperty());
        text.textProperty().bind(itemProperty());
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());
                }
                setGraphic(textField);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            } else {
                Text text = new Text(item);
                setGraphic(text);
                text.wrappingWidthProperty().bind(this.widthProperty());
                text.textProperty().bind(itemProperty());
            }
        }
    }
    
    private void createTextField() {
        textField = new TextArea(getString());
        textField.setWrapText(true);
        textField.setEditable(false);
        // textField.setPrefHeight(USE_PREF_SIZE);
        textField.setOnKeyPressed((KeyEvent t) -> {
            if (null != t.getCode()) switch (t.getCode()) {
                case ENTER:
                    commitEdit(textField.getText());
                    break;
                case ESCAPE:
                    cancelEdit();
                    break;
                case TAB:
                    commitEdit(textField.getText());
                    TableColumn nextColumn = getNextColumn(!t.isShiftDown());
                    if (nextColumn != null) {
                        getTableView().edit(getTableRow().getIndex(), nextColumn);
                    }   break;
                default:
                    break;
            }
        });
        
        textField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, 
                Boolean oldValue, Boolean newValue) -> {
            if (!newValue && textField != null) {
                commitEdit(textField.getText());
            }
        });
    }
    
    private String getString() {
        return getItem() == null ? "" : getItem();
    }
    
    /**
     *
     * @param forward   true gets the column to the right, false the column to the 
     *                  left of the current column
     * @return
     */
    private TableColumn<DescDataModel, ?> getNextColumn(boolean forward) {
        List<TableColumn<DescDataModel, ?>> columns = new ArrayList<>();
        for (TableColumn<DescDataModel, ?> column : getTableView().getColumns()) {
            columns.addAll(getLeaves(column));
        }
        //There is no other column that supports editing.
        if (columns.size() < 2) {
            return null;
        }
        int currentIndex = columns.indexOf(getTableColumn());
        int nextIndex = currentIndex;
        if (forward) {
            nextIndex++;
            if (nextIndex > columns.size() - 1) {
                nextIndex = 0;
            }
        } else {
            nextIndex--;
            if (nextIndex < 0) {
                nextIndex = columns.size() - 1;
            }
        }
        return columns.get(nextIndex);
    }
    
    private List<TableColumn<DescDataModel, ?>> getLeaves(TableColumn<DescDataModel, ?> root) {
        List<TableColumn<DescDataModel, ?>> columns = new ArrayList<>();
        if (root.getColumns().isEmpty()) {
            //We only want the leaves that are editable.
            if (root.isEditable()) {
                columns.add(root);
            }
            return columns;
        } else {
            for (TableColumn<DescDataModel, ?> column : root.getColumns()) {
                columns.addAll(getLeaves(column));
            }
            return columns;
        }
    }
    
}
