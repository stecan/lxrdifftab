/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import lxrdifferencetable.comp.Generate;
import lxrdifferencetable.tools.Properties;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import static lxrdifferencetable.LxrDifferenceTable.ABOUT_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.BLACKLIST_FILES_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.BLACKLIST_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.CONFIG_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.DIRECTORY_NOT_EMPTY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.DUMMY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.EXTERNAL_TOOLS_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.FAQ_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.FILE_NOT_FOUND_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.INVALID_PREFS_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.IO_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.JDOM_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.LANGUAGE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import static lxrdifferencetable.LxrDifferenceTable.NULL_POINTER_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.PROGRESS_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SAVE_STAGE;
import lxrdifferencetable.data.TableEntry;
import org.jdom2.JDOMException;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_2;
import static lxrdifferencetable.LxrDifferenceTable.NOT_A_DIRECTORY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.NO_LXR_SERVER_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.PROJECT_NOT_EMPTY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SVN_AUTH_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.TIMEOUT_ON_CTAGS_STAGE;
import lxrdifferencetable.comp.GenerateCtags;
import lxrdifferencetable.config.AnchorConfig;
import lxrdifferencetable.data.ConfigEntry;
import lxrdifferencetable.data.LogEntry;
import lxrdifferencetable.normaldialogs.DialogExternalToolsController;
import lxrdifferencetable.normaldialogs.DialogSvnAuthenticationController;
import lxrdifferencetable.normaldialogs.DummyController;
import lxrdifferencetable.tools.CtagsEntry;
import lxrdifferencetable.tools.CtagsFileEntry;
import lxrdifferencetable.tools.LocalProperties;
import lxrdifferencetable.tools.LoggingData;
import lxrdifferencetable.tools.ReadCtags;
import lxrdifferencetable.tools.SetProgress;
import lxrdifferencetable.tools.WorkingDirectory;
import lxrdifferencetable.tools.XmlData;
import lxrdifferencetable.tools.XmlHandling;

/**
 * Main controller for program LxrDifferenceTable
 *
 * @author Stefan Canali
 */
public class LxrDifferenceTableController implements Initializable, ControlledStage {

    static final Logger logger = Logger.getLogger(
            LxrDifferenceTableController.class.getName());

    private Locale locale;                      // locale for i18n
    private String lang;
    private String country;
    private String activeStage;                 // MAIN_STAGE_1 or MAIN_STAGE_2

    private static final int STATECONTINUING = 1;
    private static final int STATEREADY = 0;
    private static final int STATENULLPOINTEREXCEPTION = -1;
    private static final int STATEEXCEPTION = -2;
    private static final int STATEIOEXCEPTION = -3;
    private static final int STATEJDOMEXCEPTION = -4;
    private static final int STATEABORT = -5;
    private static final int STATE_ABORTED_DURING_REPO_SCANNING = -6;
    private static final int STATE_ABORTED_NOREPO = -7;
    private static final int STATE_TIMEOUT = -8;
    private static final int STATE_NO_LXR_SERVER = -9;
    

    private List<TableEntry> changedFileList;     // list of changed files
    private List<LogEntry> repoLog;
    private final Properties props;
    private ConfigEntry configEntry;
    private Generate gen;
    private HashMap<String, CtagsEntry> ctags1;
    private HashMap<String, CtagsEntry> ctags2;
    private HashMap<String, CtagsFileEntry> ctagsFile1;
    private HashMap<String, CtagsFileEntry> ctagsFile2;
    private SetProgress setProgress;
    private File workingDir;
    private File distribDir;

    private XmlHandling xml;
    private boolean xmlLoaded;
    private File xmlFile;

    private StagesController myController;
    private LxrDifferenceTableController lxrDifferenceTableController;
    private LxrTestTableController lxrTestTableController;
    private ProgressController progressController;
    private SaveController saveController;
    private ConfigController configController;
    private DialogExternalToolsController dialogExternalToolsController;
    private ConfigBlacklistController configBlacklistController;
    private ConfigBlacklistFilesController configBlacklistFilesController;
    private DialogSvnAuthenticationController dialogSvnAuthenticationController;
    private DummyController dummyController;

    private volatile boolean continuing;

    /**
     * Method: initLogging
     * 
     * This method generates a log file in the form
     *      ./log/stefan-pcl01-12818.log
     * 
     * @param dir   Path to logging file
     */
    private void initLogging(String dir){
        FileHandler fh;
        String user;          // user name, e.g. "stefan"
        String computerName;  // computer name, e.g. "localhost"
        String pid;           // process identity, e.g. "1345"
        user = System.getProperty("user.name");         // set user name
        String name;
        try {
            name = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            name = "localhost";
        }
        computerName = name;                            // set computer name
        name = ManagementFactory.getRuntimeMXBean().getName();
        String[] split = name.split("@");
        pid = split[0];                                 // set pid
        try {
            String file = dir + "/log/" + user + "-" + computerName + "-" 
                    + pid + ".log";
            // String file = "log/logging.log";
            fh = new FileHandler(file);        
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);              
            logger.addHandler( fh );
        } catch (IOException ex) {
            logger.severe("LxrDifferenceTableController: " + ex.getMessage());
        } catch (SecurityException ex) {
            logger.severe("LxrDifferenceTableController: " + ex.getMessage());
        }
        LocalProperties localProperties = new LocalProperties(myController);
        Level logLevel = localProperties.getLogLevel();
        logger.setLevel(logLevel);
//        try {
//            LocalProperties localProperties = new LocalProperties();
//            Level logLevel = localProperties.getLogLevel();
//            logger.setLevel(logLevel);
//        } catch (Exception ex) {
//            // do nothing
//            logger.setLevel(Level.INFO);
//        }
    }
    
    public LxrDifferenceTableController() {
        this.props = new Properties();
        this.changedFileList = new ArrayList<>();
        this.repoLog = new ArrayList<>();
        this.xmlLoaded = false;
        this.xmlFile = null;
        this.continuing = false;
        this.workingDir = null;
        this.distribDir = null;
    }
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded) {
        myController = stageParent;

        // load references to all needed controllers (except own controller,
        // because it is not initialized yet).
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.<LxrTestTableController>getController();
        loader = myController.getLoader(PROGRESS_STAGE);
        progressController = loader.<ProgressController>getController();
        loader = myController.getLoader(SAVE_STAGE);
        saveController = loader.<SaveController>getController();
        loader = myController.getLoader(CONFIG_STAGE);
        configController = loader.<ConfigController>getController();
        loader = myController.getLoader(EXTERNAL_TOOLS_STAGE);
        dialogExternalToolsController = loader.<DialogExternalToolsController>getController();
        loader = myController.getLoader(BLACKLIST_STAGE);
        configBlacklistController = loader.<ConfigBlacklistController>getController();
        loader = myController.getLoader(BLACKLIST_FILES_STAGE);
        configBlacklistFilesController = loader.<ConfigBlacklistFilesController>getController();
        loader = myController.getLoader(SAVE_STAGE);
        saveController = loader.<SaveController>getController();
        loader = myController.getLoader(SVN_AUTH_STAGE);
        dialogSvnAuthenticationController = loader.<DialogSvnAuthenticationController>getController();
        loader = myController.getLoader(DUMMY_STAGE);
        dummyController = loader.<DummyController>getController();
        
        // get saved values from "dummy controller"
        activeStage = dummyController.getActiveStage();
        gen = dummyController.getGen();
        configEntry = dummyController.getConfigEntry();
        ctags1 = dummyController.getCtags1();
        ctags2 = dummyController.getCtags2();
        ctagsFile1 = dummyController.getCtagsFile1();
        ctagsFile2 = dummyController.getCtagsFile2();
        repoLog = dummyController.getRepoLog();
        distribDir = dummyController.getDistribDir();
        

        // The call to "new File("").getAbsoluteFile()" does not return 
        // the calling directory (working directory) in every case.
        //
        // On Windows:
        // If you run "\\server\share\LxrDifferenceTable.jar", then the path
        // to "C:\Windows\system" is returned
        //
        // On Linux:
        // If you run the jar file from nautilus with OpenJDK, then the path
        // to the users home directory is returned
        //
        // The only way to get the working directory right, you have to read 
        // the system property "java.class.path"!
        //
//        File dir = new File("").getAbsoluteFile();
//        String path = dir.getAbsolutePath();
//        
//        if (path.contains("dist/run") ||           // for Linux
//                path.contains("dist\\run")) {      // for Windows only
//            // it runs under netbeans debugger!
//            dir = dir.getParentFile();
//        } 
        if (distribDir == null) {
            WorkingDirectory workingDirectory = new WorkingDirectory(myController);
            workingDir = workingDirectory.getWorkingDir();
        } else {
            workingDir = distribDir;
        }

        initLogging(workingDir.getAbsolutePath());
        collectLoggingData();   // collect logging data
        logger.info(LoggingData.getData());

        // loads first XML file from current directory
        File[] listFiles = workingDir.listFiles();
        for (File file : listFiles) {
            if (file.getName().endsWith("xml")
                    && (!file.getName().equals("build.xml"))) {
                xmlFile = file;
                break;
            }
        }

        saveController.setWorkingDirectory(workingDir);
        
        if ((xmlFile != null) && (!reloaded)) {
            xmlLoaded = true;
            myController.showStage(PROGRESS_STAGE);     // show progress stage
            progressController.setAbortState(false);
            logger.info("LxrDifferenceTableController: XmlFile = " + xmlFile.getName());
            loadXml(xmlFile);   // load XML file
     
            // Generate ctags hash tables from ctags1 and ctags2 (if available)
            generateCtags();
            // Save generated ctags1 and ctags2 hash tables to dummyController
            dummyController.setCtags1(ctags1);
            dummyController.setCtags2(ctags2);
            dummyController.setCtagsFile1(ctagsFile1);
            dummyController.setCtagsFile2(ctagsFile2);
            
        } else if (gen != null) {
            // reload old table
            lxrTestTableController.setPartList(new ArrayList<>());  // clear!
            setProgress = new SetProgress(progressController);
            // reset progress and label to intial settings
            progressController.setProgressIndicatorVisible(false);
            progressController.setProgressBarVisible(true);
            progressController.setLabelCosts(false);
            progressController.setLabelProcessing(true);
            
            myController.showStage(PROGRESS_STAGE);     // show progress stage
            progressController.setAbortState(false);
            if (gen.getLineNumberAvailable()) {
                lxrTestTableController.setLineNumberAvailable();
            } else {
                lxrTestTableController.resetLineNumberAvailable();
            }
            lxrTestTableController.setConfigEntry(configEntry);
            lxrTestTableController.setRepoLog(repoLog);
            lxrTestTableController.buildTables(gen,
                    progressController, setProgress, xmlFile, false);
            
        } else {
            xmlLoaded = false;
            logger.info("LxrDifferenceTableController: No XML file available!");
            myController.hideStage(PROGRESS_STAGE);
            myController.showStage(MAIN_STAGE_1);       // show this stage!
        }
    }

    private Task<Integer> task;
    private Task<Integer> xmlTask;

    @FXML
    TextField lxrLink;

    @FXML
    Label labelLxrLink;

    @FXML
    Button dir1;

    @FXML
    Button dir2;

    @FXML
    Button svnFile;

    @FXML
    Button generate;

    @FXML
    private void handleAbout(ActionEvent event) {
        myController.getStage(ABOUT_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemAbout"));
        myController.showStage(ABOUT_STAGE);
    }

    @FXML
    private void handleFaq(ActionEvent event) {
        myController.getStage(FAQ_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemFaq"));
        myController.showStage(FAQ_STAGE);
    }

    @FXML
    private void handleConfig(ActionEvent event) {
        String link = lxrLink.getText();                // save link 
        props.putProperty(props.LXRLINK, link);
        myController.getStage(CONFIG_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemConfig"));
        configController.initialize(null, null);    // re-read config!
        myController.showStage(CONFIG_STAGE);
    }

    @FXML
    private void handleClose(ActionEvent event) {
        if (xml != null) {
            getXmlHandling().releaseLockFile();
            getXmlHandling().clearOldLocks(true);
        }
        Platform.exit();
        System.exit(0);
    }

    @FXML
    private void handleLxrLink(ActionEvent event) {
        String link;
        link = lxrLink.getText();
        props.putProperty(props.LXRLINK, link);         // save as preference
        lxrLink.requestFocus();
    }

    @FXML
    private void handleDir1(ActionEvent event) {
        Stage stage;
        String dir1String;
        dir1String = props.getProperty(props.BASEDIR);
        DirectoryChooser directoryChooser = new DirectoryChooser();
        if (!dir1String.equals("")) {
            File file = new File(dir1String);
            if (file.exists()) {
                directoryChooser.setInitialDirectory(file);
            } else {
                // if the file doesn't exists, then choose user home directory
                directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
            }
        }
        directoryChooser.setTitle(ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("labelDirVersion0"));
        stage = myController.getStage(MAIN_STAGE_1);
        File selectedDirectory
                = directoryChooser.showDialog(stage);

        if (selectedDirectory == null) {
            dir1String = dir1.getText();
            if (dir1String.equals("")) {
                // no directory selected and no default directory available
                dir1.setText(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("noDirectory"));
            }
        } else {
            dir1String = selectedDirectory.getAbsolutePath();
            dir1.setText(dir1String);
            // save the selected directory
            props.putProperty(props.BASEDIR, dir1String);            // no I18n!
        }

        File file = new File(dir1String);
        String parent = file.getParent();
        dir1String = dir1String.replace(parent, "");
        dir1String = dir1String.replace("\\", "");  // for Windows
        dir1String = dir1String.replace("/", "");   // for Linux
        props.putProperty(props.LXRBASEVER, dir1String);

        // computing of property "props.LXRDIFF"
        // e.g. "?v=393027-V05.13.00&~v=398532-V05.13.00&!v=393027-V05.13.00"
        String diff;
        if (props.getProperty(props.FORMATDIFFVAR).endsWith("=")) {
            diff = "?" + 
                    props.getProperty(props.FORMATVERSION) + props.getProperty(props.LXRBASEVER) + 
                    props.getProperty(props.FORMATSEPARATOR) + 
                    props.getProperty(props.FORMATDIFFVAL) + props.getProperty(props.LXRCOMPVER) +
                    props.getProperty(props.FORMATSEPARATOR) + 
                    props.getProperty(props.FORMATDIFFVAR) + props.getProperty(props.LXRBASEVER);
        } else {
            diff = "?" + 
                    props.getProperty(props.FORMATVERSION) + props.getProperty(props.LXRBASEVER) + 
                    props.getProperty(props.FORMATSEPARATOR) + 
                    props.getProperty(props.FORMATDIFFVAL) + props.getProperty(props.LXRCOMPVER) +
                    props.getProperty(props.FORMATSEPARATOR) + 
                    props.getProperty(props.FORMATDIFFVAR);
        }
        props.putProperty(props.LXRDIFF, diff);         // save as preference
    }

    @FXML
    private void handleDir2(ActionEvent event) {
        Stage stage;
        String dir2String;
        dir2String = props.getProperty(props.COMPDIR);
        DirectoryChooser directoryChooser = new DirectoryChooser();
        if (!dir2String.equals("")) {
            File file = new File(dir2String);
            if (file.exists()) {
                directoryChooser.setInitialDirectory(file);
            } else {
                // if the file doesn't exists, then choose user home directory
                directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
            }
        }
        directoryChooser.setTitle(ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("labelDirVersion1"));
        stage = myController.getStage(MAIN_STAGE_1);
        File selectedDirectory
                = directoryChooser.showDialog(stage);

        if (selectedDirectory == null) {
            dir2String = dir2.getText();
            if (dir2String.equals("")) {
                // no directory selected and no default directory available
                dir2.setText(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("noDirectory"));
            }
        } else {
            dir2String = selectedDirectory.getAbsolutePath();
            dir2.setText(dir2String);
            // save the selected directory as an own preference
            props.putProperty(props.COMPDIR, dir2String);            // no I18n!
        }
        File file = new File(dir2String);
        String parent = file.getParent();
        dir2String = dir2String.replace(parent, "");
        dir2String = dir2String.replace("\\", "");  // for Windows
        dir2String = dir2String.replace("/", "");   // for Linux
        props.putProperty(props.LXRCOMPVER, dir2String);

        // computing of property "props.LXRDIFF"
        // e.g. "?v=393027-V05.13.00&~v=398532-V05.13.00&!v=393027-V05.13.00"
        String diff;
        if (props.getProperty(props.FORMATDIFFVAR).endsWith("=")) {
            diff = "?" + 
                    props.getProperty(props.FORMATVERSION) + props.getProperty(props.LXRBASEVER) + 
                    props.getProperty(props.FORMATSEPARATOR) + 
                    props.getProperty(props.FORMATDIFFVAL) + props.getProperty(props.LXRCOMPVER) +
                    props.getProperty(props.FORMATSEPARATOR) + 
                    props.getProperty(props.FORMATDIFFVAR) + props.getProperty(props.LXRBASEVER);
        } else {
            diff = "?" + 
                    props.getProperty(props.FORMATVERSION) + props.getProperty(props.LXRBASEVER) + 
                    props.getProperty(props.FORMATSEPARATOR) + 
                    props.getProperty(props.FORMATDIFFVAL) + props.getProperty(props.LXRCOMPVER) +
                    props.getProperty(props.FORMATSEPARATOR) + 
                    props.getProperty(props.FORMATDIFFVAR);
        }
        props.putProperty(props.LXRDIFF, diff);         // save as preference
    }

    @FXML
    private void handleKey(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            if (isProjectEmpty()) {
                handleGenerate();
            } else {
                myController.showStage(PROJECT_NOT_EMPTY_STAGE);
            }
        }
    }
    
    @FXML
    public void handleGenerate(ActionEvent event) {
        if (isProjectEmpty()) {
            handleGenerate();
        } else {
            myController.showStage(PROJECT_NOT_EMPTY_STAGE);
        }
    }
    

    @FXML
    public void handleExternalTool(ActionEvent event) {
        myController.getStage(EXTERNAL_TOOLS_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemExternalTools"));
        dialogExternalToolsController.initialize(null, null);
        myController.showStage(EXTERNAL_TOOLS_STAGE);
    }
    
    public void handleGenerate() {
        final File file1 = new File(dir1.getText());
        final File file2 = new File(dir2.getText());

        String link = lxrLink.getText();                // save link 
        props.putProperty(props.LXRLINK, link);
        
        if (!continuing) {
            gen = new Generate(progressController);
            setProgress = new SetProgress(progressController);
            configEntry = new ConfigEntry();        // delete old values  
        }

        // Worker task to handle the long lasting methodes of generating the 
        // list of changed files and of outputting to TextArea
        task = new Task<Integer>() {
            @Override
            protected Integer call() {
                int state;
                Generate.State genState = Generate.State.PASSED;
                state = STATEREADY;

                try {
                    // generate new output
                    if (!continuing) {

                        // only for testing purposes!
                        //
                        // throw new Exception("Test");
                        // throw new NullPointerException("Test");
                        gen.init(file1, file2);     // process the costs 

                        // set progress bar visible and hide progress indicator
                        progressController.setProgressIndicatorVisible(false);
                        progressController.setProgressBarVisible(true);

                        // set label "processing" visible and hide label "costs"
                        progressController.setLabelCosts(false);
                        progressController.setLabelProcessing(true);

                        genState = gen.firstPass();         // first pass
                        if (genState == Generate.State.PASSED) {
                            genState = gen.secondPass();    // second pass
                        }
                    } else {
                        genState = dialogSvnAuthenticationController.getState();
                    }
                    if (genState == Generate.State.PASSED) {
                        String account = dialogSvnAuthenticationController.
                                getAccount();
                        String password = dialogSvnAuthenticationController.
                                getPassword();
                        // scan repository, third pass!
                        genState = gen.repoPass(account, password,
                                dialogSvnAuthenticationController);
                        if ((genState == Generate.State.PASSED)
                                || (genState == Generate.State.ABORTED_DURING_REPO_SCANNING)) {
                            changedFileList = gen.getOutList(); // save list 

                            if (gen.getLineNumberAvailable()) {
                                lxrTestTableController.setLineNumberAvailable();
                            } else {
                                lxrTestTableController.resetLineNumberAvailable();
                            }
                        } 
                    }
                    if (!continuing) {
                        String localGeneratedView = props.getProperty(props.LOCAL_GENERATED_VIEW);
                        if (localGeneratedView.equalsIgnoreCase("true")) {
                            // fourth pass
                            GenerateCtags ctags = 
                                    new GenerateCtags(progressController, myController);
                            genState = ctags.generateCtagsFile(file1, file2);
                            if (genState.equals(Generate.State.CTAGS_ERROR)) {
                                // do nothing (no ctags-file available!)
                            } else {
                                generateCtags();
                            }
                        }
                    }
                    
                    repoLog = gen.getRepoLog();

                    switch (genState) {
                        case CONTINUING:
                            state = STATECONTINUING;
                            break;
                        case PASSED:
                        case TIMEOUT:
                        case NO_LXR_SERVER:
                        case CTAGS_ERROR:
                            if ((genState == Generate.State.PASSED) || (genState == Generate.State.CTAGS_ERROR)) {
                                state = STATEREADY;
                            } else if (genState == Generate.State.NO_LXR_SERVER) {
                                state = STATE_NO_LXR_SERVER;
                            } else {
                                state = STATE_TIMEOUT;
                            }
                            configEntry.putSvn("true");    // print repo info
                            lxrTestTableController.setConfigEntry(configEntry);
                            lxrTestTableController.setRepoLog(repoLog);
                            dummyController.setGen(gen);    // Save!
                            dummyController.setConfigEntry(configEntry);
                            dummyController.setRepoLog(repoLog);
                            dummyController.setCtags1(ctags1);
                            dummyController.setCtags2(ctags2);
                            dummyController.setCtagsFile1(ctagsFile1);
                            dummyController.setCtagsFile2(ctagsFile2);
                            lxrTestTableController.buildTables(gen,
                                    progressController, setProgress, xmlFile,
                                    false);
                            break;
                        case ABORTED:
                            state = STATEABORT;
                            // reset progress and label to intial settings
                            progressController.setProgressIndicatorVisible(true);
                            progressController.setProgressBarVisible(false);
                            progressController.setLabelCosts(true);
                            progressController.setLabelProcessing(false);
                            break;
                        case ABORTED_DURING_REPO_SCANNING:
                            state = STATE_ABORTED_DURING_REPO_SCANNING;
                            configEntry.putSvn("true");
                            lxrTestTableController.setConfigEntry(configEntry);
                            lxrTestTableController.setRepoLog(repoLog);
                            dummyController.setCtags1(ctags1);
                            dummyController.setCtags2(ctags2);
                            dummyController.setCtagsFile1(ctagsFile1);
                            dummyController.setCtagsFile2(ctagsFile2);
                            dummyController.setGen(gen);    // Save!
                            dummyController.setConfigEntry(configEntry);
                            dummyController.setRepoLog(repoLog);
                            lxrTestTableController.buildTables(gen,
                                    progressController, setProgress, xmlFile, 
                                    false);
                            break;
                        case ABORTED_NO_REPO:
                            state = STATE_ABORTED_NOREPO;
                            configEntry.putSvn("false");    // no repo!
                            lxrTestTableController.setConfigEntry(configEntry);
                            lxrTestTableController.setRepoLog(repoLog);
                            dummyController.setCtags1(ctags1);
                            dummyController.setCtags2(ctags2);
                            dummyController.setCtagsFile1(ctagsFile1);
                            dummyController.setCtagsFile2(ctagsFile2);
                            dummyController.setGen(gen);    // Save!
                            dummyController.setConfigEntry(configEntry);
                            dummyController.setRepoLog(repoLog);
                            lxrTestTableController.buildTables(gen,
                                    progressController, setProgress, xmlFile, 
                                    false);
                            break;
                        default:
                            break;
                    }
                    
                } catch (NullPointerException ex) {
                    state = STATENULLPOINTEREXCEPTION;
                } catch (Exception ex) {
                    state = STATEEXCEPTION;
                }

                return state;
            }
        };

        // Event handler to wait for completion of worker task.
        // 
        // If the worker task completes, then the PROGRESSSTAGE is hidden.
        task.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED,
                (WorkerStateEvent t) -> {
                    // wait on completion of worker task
                    Integer state;

                    state = task.getValue();
                    // ask for state of completion
                    switch (state) {
                        case STATECONTINUING:
                            // authenticate against svn server
                            myController.showStage(SVN_AUTH_STAGE);
                            continuing = true;
                            break;
                        case STATEREADY:
                            // it's all right!
                            continuing = false;
                            break;
                        case STATEEXCEPTION:
                            // An exception occured!
                            // Show the approviate message!
                            myController.showStage(EXCEPTION_STAGE);
                            continuing = false;
                            break;
                        case STATE_TIMEOUT:
                            // An exception occured!
                            // Show the approviate message!
                            myController.showStage(TIMEOUT_ON_CTAGS_STAGE);
                            continuing = false;
                            break;
                        case STATE_NO_LXR_SERVER:
                            // An exception occured!
                            // Show the approviate message!
                            myController.showStage(NO_LXR_SERVER_STAGE);
                            continuing = false;
                            break;
                        case STATEIOEXCEPTION:
                            // An exception occured!
                            // Show the approviate message!
                            myController.showStage(IO_EXCEPTION_STAGE);
                            continuing = false;
                            break;
                        case STATENULLPOINTEREXCEPTION:
                            // A null pointer exception occured!
                            // Show the approviate message!
                            myController.showStage(NULL_POINTER_EXCEPTION_STAGE);
                            continuing = false;
                            break;
                        case STATEJDOMEXCEPTION:
                            // A null pointer exception occured!
                            // Show the approviate message!
                            myController.showStage(JDOM_EXCEPTION_STAGE);
                            continuing = false;
                            break;
                        case STATEABORT:
                            // do nothing!
                            continuing = false;
                            break;
                        case STATE_ABORTED_NOREPO:
                            // do nothing!
                            continuing = false;
                            break;
                    }
                });

        if (!continuing) {
            myController.showStage(PROGRESS_STAGE);     // show progress stage
            progressController.setAbortState(false);
        }
        Thread th = new Thread(task);               // define new task
        th.setDaemon(true);                         // set task to daemon state
        th.setName("HandleGenerate");
        th.start();                                 // start task as daemon
    }

    @FXML
    private void handleLoad(ActionEvent event) {
        Stage stage;
        String lastDir;
        String lastFile;
        FileChooser fileChooser;
        File selectedFile;
        File dir;

        lastFile = props.getProperty(props.LASTPROPERTIESFILE);
        lastDir = (new File (lastFile)).getParent();
        dir = new File(lastDir);
        if ((lastDir == null) || (!dir.exists())) {
            lastFile = System.getProperty("user.home") + 
                    File.separator + "Prefs.xml";
            props.putProperty(props.LASTXMLFILE, lastFile);
            lastDir = System.getProperty("user.home");
        }

        stage = myController.getStage(MAIN_STAGE_1);

        fileChooser = new FileChooser();
        fileChooser.setTitle(ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemLoad"));
        fileChooser.setInitialDirectory(new File(lastDir));
        fileChooser.setInitialFileName(lastFile);
        selectedFile = fileChooser.showOpenDialog(stage);

        if (selectedFile != null) {
            BufferedInputStream fis;
            try {
                fis = new BufferedInputStream(new FileInputStream(selectedFile.toString()));
                Preferences.importPreferences(fis);
                reloadConfig();

            } catch (FileNotFoundException ex) {
                myController.showStage(FILE_NOT_FOUND_STAGE);
            } catch (IOException ex) {
                myController.showStage(IO_EXCEPTION_STAGE);
            } catch (InvalidPreferencesFormatException ex) {
                myController.showStage(INVALID_PREFS_STAGE);
            }
        }
    }

    @FXML
    private void handleSave(ActionEvent event) {
        String link = lxrLink.getText();                // save link 
        props.putProperty(props.LXRLINK, link);
        myController.getStage(SAVE_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemSave"));
        saveController.setMode(SaveController.Mode.PREF_MODE);
        saveController.initialize(null, null);
        myController.showStage(SAVE_STAGE);
    }
    
    @FXML
    private void handleLoadFile(ActionEvent event) {
        Stage stage;
        String lastDir;
        String lastFile;
        FileChooser fileChooser;
        File file;
        File dir;

        lastFile = props.getProperty(props.LASTXMLFILE);
        lastDir = (new File (lastFile)).getParent();
        dir = new File(lastDir);
        if ((lastDir == null) || (!dir.exists())) {
            lastFile = System.getProperty("user.home") + 
                    File.separator + "Table.xml";
            props.putProperty(props.LASTXMLFILE, lastFile);
            lastDir = System.getProperty("user.home");
        }

        stage = myController.getStage(MAIN_STAGE_1);

        fileChooser = new FileChooser();
        fileChooser.setTitle(ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemLoadTable"));
        fileChooser.setInitialDirectory(new File(lastDir));
        file = fileChooser.showOpenDialog(stage);

        if (file == null) {
            // No file selected! Do not show any message!
            // myController.showStage(FILE_NOT_FOUND_STAGE);
        } else {
            props.putProperty(props.LASTXMLFILE, file.getAbsolutePath());  // save name
            loadXml(file);      // load XML file
        }

    }

    @FXML
    private void handleCreateEmptyProject(ActionEvent event) {
        Stage stage = myController.getStage(MAIN_STAGE_1);
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(workingDir);
        File selectedDirectory = directoryChooser.showDialog(stage);
        if (selectedDirectory.isDirectory()) {
            props.putProperty(props.LASTDISTRIBDIR,
                    selectedDirectory.getAbsolutePath());
            File[] listFiles = selectedDirectory.listFiles();
            if (listFiles.length == 0) {
                // It's ok! The choosen directory is empty and can be
                // served to distribute the project.
                boolean ok = distribute(selectedDirectory);
                if (ok) {
                    dummyController.setDistribDir(selectedDirectory);

                    // Reload all stages!
                    FXMLLoader loader2 = myController.getLoader(MAIN_STAGE_1);
                    LxrDifferenceTableController lxrDifferenceTableController = 
                    loader2.<LxrDifferenceTableController>getController();
                    lxrDifferenceTableController.reloadStages();
                } else {
                    // Do nothing: Do not close this stage, because 
                    // an error occured.
                }
            } else {
                myController.showStage(DIRECTORY_NOT_EMPTY_STAGE);
            }
        } else {
            myController.showStage(NOT_A_DIRECTORY_STAGE);
        }
        
        

    }

    @FXML
    private void handleLanguage(ActionEvent event) {
        myController.getStage(LANGUAGE_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemLanguage"));
        myController.showStage(LANGUAGE_STAGE);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // locale settings
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);

        String textString;
        textString = props.getProperty(props.LXRLINK);
        if (!textString.equals("")) {
            lxrLink.setText(textString);
        } else {
            lxrLink.setText("http://localhost/lxr");
            props.putProperty(props.LXRLINK, "http://localhost/lxr");
        }
        lxrLink.requestFocus();
        
        // Lxr is disabled! Now, the property "LOCAL_GENERATED_VIEW" is 
        // enabled by default.
        disableLxr();

        // initialize main
        String dirString;
        dirString = props.getProperty(props.BASEDIR);
        if (!dirString.equals("")) {
            dir1.setText(dirString);
        }
        dirString = props.getProperty(props.COMPDIR);
        if (!dirString.equals("")) {
            dir2.setText(dirString);
        }

        // read version information
        ClassLoader cl;
        InputStream is;
        BufferedReader br;
        String line;
        StringBuilder responseData;
        String version;

        cl = AnchorConfig.class.getClassLoader();
        is = cl.getResourceAsStream("lxrdifferencetable/config/version.txt");
        br = new BufferedReader(new InputStreamReader(is));
        responseData = new StringBuilder();
        try {
            while ((line = br.readLine()) != null) {
                responseData.append(line);
            }
        } catch (IOException ex) {
            logger.severe("LxrDifferenceTableController: Could not read version information!");
        }
        version = responseData.substring(0);
        props.putProperty(props.VERSION, version);
    }
    
    private void disableLxr() {
        lxrLink.setDisable(true);
        lxrLink.setVisible(false);
        labelLxrLink.setDisable(true);
        labelLxrLink.setVisible(false);
        props.putProperty(props.LOCAL_GENERATED_VIEW, "true");
    }

    public void setXmlLoaded() {
        xmlLoaded = true;
    }

    public void resetXmlLoaded() {
        xmlLoaded = false;
    }

    /**
     * This Method reloads the whole configuration of LxrDifferenceTool.
     *
     * It iterates over all subsequent controllers and calls their initialize()
     * method.
     */
    public void reloadConfig() {
        // initialize all attached controllers
        configController.initialize(null, null);
        configBlacklistController.initialize(null, null);
        configBlacklistFilesController.initialize(null, null);
        saveController.initialize(null, null);

        // last but not lease, call the own initialize method!
        initialize(null, null);
    }

    /**
     * This method reloads all stages (to set the new language).
     *
     */
    public void reloadStages() {
        String controllers[] = LxrDifferenceTable.getControllers();

        if ((xmlLoaded) || (activeStage.equals(MAIN_STAGE_2))){
            for (String ctrl : controllers) {
                if (ctrl.equals(MAIN_STAGE_1)) {
                    myController.hideStage(ctrl);
                    myController.unloadSage(ctrl);
                    myController.loadStage(ctrl, false);
                } else if (ctrl.equals(MAIN_STAGE_2)) {
                    myController.hideStage(ctrl);
                    myController.unloadSage(ctrl);
                    myController.loadStage(ctrl, true);
                } else if (ctrl.equals(DUMMY_STAGE)) {
                    // Do nothing:
                    // 
                    // The dummy controller saves values for the 
                    // MAIN_STAGE_1 controller. 
                    //
                    // This is for switching between
                    // different languages.
                } else {
                    myController.hideStage(ctrl);
                    myController.unloadSage(ctrl);
                    myController.loadStage(ctrl, true);
                }
            }
        } else {
            for (String ctrl : controllers) {
                if (ctrl.equals(MAIN_STAGE_1)) {
                    myController.hideStage(ctrl);
                    myController.unloadSage(ctrl);
                    myController.loadStage(ctrl, true);
                    myController.showStage(ctrl);
                } else if (ctrl.equals(DUMMY_STAGE)) {
                    // Do nothing:
                    // 
                    // The dummy controller saves values for the 
                    // MAIN_STAGE_1 controller. 
                    //
                    // This is for switching between
                    // different languages.
                } else {
                    myController.unloadSage(ctrl);
                    myController.loadStage(ctrl, true);
                }
            }
        }
    }

    private boolean isProjectEmpty() {
        boolean empty = true;
        
        File workingDir;
        if (distribDir == null) {
            WorkingDirectory workingDirectory = new WorkingDirectory(myController);
            workingDir = workingDirectory.getWorkingDir();
        } else {
            workingDir = distribDir;
        }
        File[] listFiles = workingDir.listFiles();
        for (File file : listFiles) {
            if ((file.getName().endsWith("xml")) && (!file.getName().equalsIgnoreCase("build.xml"))) {
                empty = false;
                break;
            }
        }
        return empty;
    }
    
    private void loadXml(File file) {
        final Generate gen = new Generate(progressController);
        final SetProgress setProgress = new SetProgress(progressController);

        configEntry = new ConfigEntry();        // delete old values

        // Worker task to handle the long lasting methodes of generating the 
        // list of changed files and of outputting to TextArea
        xmlTask = new Task<Integer>() {
            @Override
            protected Integer call() {
                int state;

                boolean lineNumberAvailable = false;

                // generate new output
                state = STATEREADY;

                // set progress bar visible and hide progress indicator
                progressController.setProgressIndicatorVisible(false);
                progressController.setProgressBarVisible(true);

                // set label "processing" visible and hide label "costs"
                progressController.setLabelCosts(false);
                progressController.setLabelProcessing(true);

                changedFileList.clear();        // clear the list

                xml = new XmlHandling(progressController);

                try {
                    XmlData xmlData = xml.readXml(file, setProgress);
                    changedFileList = xmlData.getChangedFileList();
                    configEntry = xmlData.getConfigEntry();
                    repoLog = xmlData.getRepoLog();
                    lineNumberAvailable = xml.getLineNumberAvailable();

                    gen.setOutList(changedFileList);    // set list
                    if (lineNumberAvailable) {
                        lxrTestTableController.setLineNumberAvailable();
                    } else {
                        lxrTestTableController.resetLineNumberAvailable();
                    }
                    lxrTestTableController.setConfigEntry(configEntry);
                    if (configEntry.getCaption().size() != 0) {
                        lxrTestTableController.renameCaption(
                                configEntry.getCaptionLast());
                    }
                    lxrTestTableController.setRepoLog(repoLog);
                    dummyController.setGen(gen);    // Save!
                    dummyController.setConfigEntry(configEntry);
                    dummyController.setRepoLog(repoLog);
                    dummyController.setCtags1(ctags1);
                    dummyController.setCtags2(ctags2);
                    dummyController.setCtagsFile1(ctagsFile1);
                    dummyController.setCtagsFile2(ctagsFile2);
                    lxrTestTableController.buildTables(gen,
                            progressController, setProgress, file,
                            false);

                    // only for testing purposes!
                    //
                    // throw new IOException("Test");
                    // throw new JDOMException("Test");
                } catch (JDOMException e) {
                    state = STATEJDOMEXCEPTION;
                } catch (IOException e) {
                    state = STATEIOEXCEPTION;
                } catch (Exception e) {
                    System.out.println("An exception occured!");
                }

                return state;
            }
        };

        // Event handler to wait for completion of worker task.
        // 
        // If the worker task completes, then the PROGRESSSTAGE is hidden.
        xmlTask.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, (WorkerStateEvent t) -> {
            // wait on completion of worker task
            Integer state;

            state = xmlTask.getValue();
            // ask for state of completion
            switch (state) {
                case STATEREADY:
                    // it's all right!
                    break;
                case STATEEXCEPTION:
                    // An exception occured!
                    // Show the approviate message!
                    myController.showStage(EXCEPTION_STAGE);
                    break;
                case STATEIOEXCEPTION:
                    // An exception occured!
                    // Show the approviate message!
                    myController.showStage(IO_EXCEPTION_STAGE);
                    break;
                case STATENULLPOINTEREXCEPTION:
                    // A null pointer exception occured!
                    // Show the approviate message!
                    myController.showStage(NULL_POINTER_EXCEPTION_STAGE);
                    break;
                case STATEJDOMEXCEPTION:
                    // A null pointer exception occured!
                    // Show the approviate message!
                    myController.showStage(JDOM_EXCEPTION_STAGE);
                    break;
            }
        });

        myController.showStage(PROGRESS_STAGE);     // show progress stage
        progressController.setAbortState(false);
        Thread th = new Thread(xmlTask);            // define new task
        th.setDaemon(true);                         // set task to daemon state
        th.start();                                 // start task as daemon
    }

    public XmlHandling getXmlHandling() {
        return xml;
    }

    private void collectLoggingData() {
        LoggingData.setVersion(props.getProperty(props.VERSION));
        LoggingData.setHomeDir(System.getProperty("user.home"));
        LoggingData.setCurrentDir(System.getProperty("user.dir"));
        LoggingData.setUser(System.getProperty("user.name"));
        LoggingData.setJavaVersion(System.getProperty("java.version"));
        LoggingData.setOSName(System.getProperty("os.name"));
        LoggingData.setOSArchitecture(System.getProperty("os.arch"));
        LoggingData.setOSVersion(System.getProperty("os.version"));
        try {
            java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
            LoggingData.setMachine(localMachine.getHostName());
        } catch (UnknownHostException ex) {
            logger.severe("LxrDifferenceTableController: Cannot determine Host address! " + ex.getMessage());
        }
        long jvmMemory = Runtime.getRuntime().maxMemory();  // memory for JVM
        long freeMemory = ((com.sun.management              // free OS memory
                .OperatingSystemMXBean) ManagementFactory
                        .getOperatingSystemMXBean()).getTotalPhysicalMemorySize();
        LoggingData.setJvmMemory("" + jvmMemory);
        LoggingData.setFreeMemory("" + freeMemory);
    }

    public void setActiveStage (String activeStage) {
        this.activeStage = activeStage;
    }
    
    public String getActiveStage () {
        return activeStage;
    }
    
    public void deleteGen () {
        gen = null;
        dummyController.setGen(gen);
    }
  
    private void copyFile(File source, File dest) throws FileNotFoundException, IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            if (is != null) is.close();
            if (os != null)os.close();
        }
    }
    
    private boolean distribute(File newDirectory) {
        boolean result = true;
        try {
            // create main jar file in destination directory
            File srcJarFile = new File(workingDir.getPath() + 
                    File.separator + "lxrdifftab.jar");
            File destJarFile = new File(newDirectory.getPath() + 
                    File.separator + "lxrdifftab.jar");
            copyFile(srcJarFile, destJarFile);
            destJarFile.setExecutable(true);

            // create bat file in destination directory
            File srcBatFile = new File(workingDir.getPath() + 
                    File.separator + "lxrdifftab.bat");
            File destBatFile = new File(newDirectory.getPath() + 
                    File.separator + "lxrdifftab.bat");
            copyFile(srcBatFile, destBatFile);

            // create destination directries
            boolean created;
            File destCfgDir = new File(newDirectory.getPath() + 
                    File.separator + "cfg");
            created = destCfgDir.mkdir();
            File destLibDir = new File(newDirectory.getPath() + 
                    File.separator + "lib");
            created = destLibDir.mkdir();
            File destLogDir = new File(newDirectory.getPath() + 
                    File.separator + "log");
            created = destLogDir.mkdir();
            
            // copy cfg
            File srcCfgDir = new File(workingDir.getPath() + 
                    File.separator + "cfg");
            File[] listFiles = srcCfgDir.listFiles();
            File dest;
            String fileName;
            for (File f : listFiles) {
                fileName = f.getName();
                if (f.isFile()) {
                    dest = new File (newDirectory.getPath() + 
                            File.separator + "cfg" + File.separator + 
                            fileName);
                    copyFile(f, dest);
                } else {
                    // do not copy sub directories!
                }
            }
            
            // copy lib
            File srcLibDir = new File(workingDir.getPath() + 
                    File.separator + "lib");
            listFiles = srcLibDir.listFiles();
            for (File f : listFiles) {
                fileName = f.getName();
                if (f.isFile()) {
                    dest = new File (newDirectory.getPath() + 
                            File.separator + "lib" + File.separator + 
                            fileName);
                    copyFile(f, dest);
                } else {
                    // do not copy sub directories!
                }
            }
            
            workingDir = newDirectory;
            
        } catch (IOException ex) {
            myController.showStage(IO_EXCEPTION_STAGE);
            result = false;
        }
            
        return result;
    }
    
    /**
     *  Private method to generate the ctags1 and ctags2 hash tables
     * 
     * Output:
     * ctags1   global variable
     * ctags2   global variable
     */
    private void generateCtags() {
        String tagsFilename1 = workingDir.getAbsolutePath() + 
                File.separator + "ReviewedSources" + 
                File.separator + "tags1";
        String tagsFilename2 = workingDir.getAbsolutePath() + 
                File.separator + "ReviewedSources" + 
                File.separator + "tags2";
        File tags1 = new File(tagsFilename1);
        File tags2 = new File(tagsFilename2);
        ReadCtags readCtags1 = new ReadCtags();
        ReadCtags readCtags2 = new ReadCtags();
        readCtags1.init(tags1);
        readCtags2.init(tags2);
        ctags1 = readCtags1.getCtags();
        ctags2 = readCtags2.getCtags();
        ctagsFile1 = readCtags1.getCtagsFile();
        ctagsFile2 = readCtags2.getCtagsFile();
    }
    
}
