/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import lxrdifferencetable.ProgressController;

/**
 *
 * @author Stefan Canali
 */
public class SetProgress {
    
    // controller for updating the progress bar
    private final ProgressController progressController;
    
    public SetProgress (ProgressController progressController) {
        this.progressController = progressController;
    }

    /**
     * Range: 0% .. 60%
     * 
     * This method maps the complete input range to an output 
     * range of a progress bar of 0.0 .. 0.6 (0% .. 60%), e.g.
     * 
     *      input:  0..3000
     *      output: 0.0 .. 0.6
     * 
     * @param i
     * @param size
     */
    public void setProgressFirstPass(int i, int size) {
        final int NUMBER_OF_STEPS = 2*60;
        
        int delta;
        int tmp;
        float value;
        float offset;
        float scale;

        if (size < NUMBER_OF_STEPS) {
            delta = 1;
        } else {
            delta = size / NUMBER_OF_STEPS;
        }

        offset = ((float) size) * 0.0f;
        scale = 0.6f;

        tmp = (i / delta) * delta;
        if (tmp == i) {
            value = (((float) i) * scale + offset) / ((float) size);
            if (value < 0.0f) {
                value = 0.0f;
            }
            if (value > 0.6f) {
                value = 0.6f;
            } 
            progressController.setProgress(value);
        }
    }
    
    // Range: 60% .. 70%
    //
    // This method maps the complete input range to an output 
    // range of a progress bar of 0.6 .. 0.7 (60% .. 70%), e.g.
    // 
    //      input:  0..3000
    //      output: 0.6 .. 0.7
    //
    public void setProgressSecondPass(int i, int size) {

        final int NUMBER_OF_STEPS = 10;
        
        int delta;
        int tmp;
        float value;
        float offset;
        float scale;

        if (size < NUMBER_OF_STEPS) {
            delta = 1;
        } else {
            delta = size / NUMBER_OF_STEPS;
        }

        offset = ((float) size) * 0.6f;
        scale = 0.1f;

        tmp = (i / delta) * delta;
        if (tmp == i) {
            value = (((float) i) * scale + offset) / ((float) size);
            if (value < 0.6f) {
                value = 0.6f;
            }
            if (value > 0.7f) {
                value = 0.7f;
            }
            progressController.setProgress(value);
        }
    }

    // Range: 70% .. 80%
    //
    // This method maps the complete input range to an output 
    // range of a progress bar of 0.7 .. 0.8 (70% .. 80%), e.g.
    // 
    //      input:  0..3000
    //      output: 0.7 .. 0.8
    //
    public void setProgressThirdPass(int i, int size) {

        final int NUMBER_OF_STEPS = 10;
        
        int delta;
        int tmp;
        float value;
        float offset;
        float scale;

        if (size < NUMBER_OF_STEPS) {
            delta = 1;
        } else {
            delta = size / NUMBER_OF_STEPS;
        }

        offset = ((float) size) * 0.7f;
        scale = 0.1f;

        tmp = (i / delta) * delta;
        if (tmp == i) {
            value = (((float) i) * scale + offset) / ((float) size);
            if (value < 0.7f) {
                value = 0.7f;
            }
            if (value > 0.8f) {
                value = 0.8f;
            }
            progressController.setProgress(value);
        }
    }

    // Range: 80% .. 90%
    //
    // This method maps the complete input range to an output 
    // range of a progress bar of 0.8 .. 0.9 (80% .. 90%), e.g.
    // 
    //      input:  0..3000
    //      output: 0.8 .. 0.9
    //
    public void setProgressFourthPass(int i, int size) {

        final int NUMBER_OF_STEPS = 10;
        
        int delta;
        int tmp;
        float value;
        float offset;
        float scale;

        if (size < NUMBER_OF_STEPS) {
            delta = 1;
        } else {
            delta = size / NUMBER_OF_STEPS;
        }

        offset = ((float) size) * 0.8f;
        scale = 0.1f;

        tmp = (i / delta) * delta;
        if (tmp == i) {
            value = (((float) i) * scale + offset) / ((float) size);
            if (value < 0.8f) {
                value = 0.8f;
            }
            if (value > 0.9f) {
                value = 0.9f;
            }
            progressController.setProgress(value);
        }    
    }

    // Range: 90% .. 100%
    //
    // This method maps the complete input range to an output 
    // range of a progress bar of 0.9 .. 1.0 (90% .. 100%), e.g.
    // 
    //      input:  0..3000
    //      output: 0.9 .. 1.0
    //
    public void setProgressFifthPass(int i, int size) {

        final int NUMBER_OF_STEPS = 10;
        
        int delta;
        int tmp;
        float value;
        float offset;
        float scale;

        if (size < NUMBER_OF_STEPS) {
            delta = 1;
        } else {
            delta = size / NUMBER_OF_STEPS;
        }

        offset = ((float) size) * 0.9f;
        scale = 0.1f;

        tmp = (i / delta) * delta;
        if (tmp == i) {
            value = (((float) i) * scale + offset) / ((float) size);
            if (value < 0.9f) {
                value = 0.9f;
            }
            if (value > 1.0f) {
                value = 1.0f;
            }
            progressController.setProgress(value);
        }
    }
    
}
