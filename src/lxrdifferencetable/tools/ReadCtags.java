/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * ReadCtags is a class to read a ctags file and push all it's content into 
 * a hash table.
 * 
 * @author Stefan Canali
 */
public class ReadCtags {
    
    private final HashMap<String, CtagsEntry> ctagsMap;
    private final HashMap<String, CtagsFileEntry> ctagsFileMap;
    
    /**
     * Constructor
     */
    public ReadCtags() {
        this.ctagsMap = new HashMap<>();
        this.ctagsFileMap = new HashMap<>();
    }
    
    /**
     * Method to read the ctags file and put all it's content to a hash table.
     * 
     * @param ctagsFile     ctags file
     */
    public void init(File ctagsFile) {
        String tagname;
        String tagfile2;
        String tagfile;
        String tagaddress2;
        String tagaddress;
        String tagfield;
        String line;
        BufferedReader br;        
        CtagsEntry entry;
        CtagsFileEntry fentry;
        try {
            br = new BufferedReader(new FileReader(ctagsFile));
            while ((line = br.readLine()) != null) {
                if (line.startsWith("!")) {
                    // next line
                } else {
                    String[] split = line.split("\t");
                    tagname = split[0];
                    tagfile2 = split[1];
                    tagaddress2 = split[2];             // intermediate result
                    String[] split2 = tagaddress2.split(";\"");
                    tagaddress =split2[0];
                    String[] split3 = tagfile2.split("/ReviewedSources/");
                    tagfile = "ReviewedSources/" + split3[1];
                    if (split3.length > 2) {
                        // Error: T
                        // The string "/ReviewedSources/" is included several 
                        // times in the path of the file.
                        //
                        // An error message should be displayed!?
                    }
                    if (split.length == 4) {
                        tagfield = split[3];
                    } else {
                        tagfield = "";
                    }

                    if (ctagsMap.containsKey(tagname)) {
                        entry = ctagsMap.get(tagname);
                        entry.addTagfile(tagfile);
                        entry.addTagaddress(tagaddress);
                        entry.addTagfields(tagfield);
                    } else {
                        entry = new CtagsEntry();
                        entry.setTagname(tagname);
                        entry.addTagfile(tagfile);
                        entry.addTagaddress(tagaddress);
                        entry.addTagfields(tagfield);
                    }
                    ctagsMap.put(tagname,entry);

                    if (ctagsFileMap.containsKey(tagfile)) {
                        fentry = ctagsFileMap.get(tagfile);
                        fentry.addTagname(tagname);
                    } else {
                        fentry = new CtagsFileEntry();
                        fentry.setFilename(tagfile);
                        fentry.addTagname(tagname);
                    }
                    ctagsFileMap.put(tagfile, fentry);
                }
            }
        } catch (IOException ex) {
            // there is something wrong!
        }
    }

    /**
     * GetCtags is a method to get the hash table from the init ctags file.
     * 
     * @return hash table
     */
    public HashMap<String, CtagsEntry> getCtags() {
        return ctagsMap;
    }

    /**
     * GetCtagsFileMap is a method to get the file hash table from the init ctags file.
     * 
     * @return hash table
     */
    public HashMap<String, CtagsFileEntry> getCtagsFile() {
        return ctagsFileMap;
    }
}
