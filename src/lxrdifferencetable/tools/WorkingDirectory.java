/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxrdifferencetable.tools;

import java.io.File;
import javafx.fxml.FXMLLoader;
import static lxrdifferencetable.LxrDifferenceTable.DUMMY_STAGE;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.normaldialogs.DummyController;

/**
 *
 * @author stefan
 */
public class WorkingDirectory {
    private File workingDir;
    
    public WorkingDirectory(StagesController stageParent) {
        if (stageParent == null) {
            workingDir = extractWorkingDir();
        } else {
            StagesController myController = stageParent;

            // load references to all needed controllers (except own controller,
            // because it is not initialized yet).
            FXMLLoader loader;    

            loader = myController.getLoader(DUMMY_STAGE);
            DummyController dummyController = loader.<DummyController>getController();

            File distribDir = dummyController.getDistribDir();
            if (distribDir == null) {
                workingDir = extractWorkingDir();
            } else {
                workingDir = distribDir;
            }
        }
   }
    
    private File extractWorkingDir () {
        File dir;
        String classPath = (new File(System.getProperty("java.class.path"))).getParent();
        if (classPath == null) {
            dir = new File("").getAbsoluteFile();
        } else if (classPath.contains(":")
                || // for Linux
                classPath.contains(";")) {       // for Windows only
            // it runs under netbeans debugger!
            dir = new File("").getAbsoluteFile();
        } else {
            // normal call 
            dir = new File(classPath);
        }
        return dir;
    }
    
    public File getWorkingDir() {
        return workingDir;
    }
}
