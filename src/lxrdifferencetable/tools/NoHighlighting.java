/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.util.Collection;
import java.util.Collections;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

/**
 * NoHighlighting is a class to initialize highlighting for unknown programming
 * languages.
 * 
 * @author Stefan Canali
 */
public class NoHighlighting {
    
    public static StyleSpans<Collection<String>> computeHighlighting(String text) {
        
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        spansBuilder.add(Collections.emptyList(), text.length());
        StyleSpans<Collection<String>> created = spansBuilder.create();
        return created;

    }
}
