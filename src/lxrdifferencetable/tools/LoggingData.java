/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

/**
 *
 * @author Stefan Canali
 */
public class LoggingData {

    private static String user;
    private static String machine;
    private static String homeDir;
    private static String currentDir;
    private static String version;
    private static String javaVersion;
    private static String osName;
    private static String osArchitecture;
    private static String osVersion;
    private static String jvmMemory;
    private static String freeMemory;
    
    private static String data;
    
    public static void setUser(String name) {
        user = name;
        setData();
    }
    
    public static String getUser () {
        return user;
    }
    
    public static void setMachine(String name) {
        machine = name;
        setData();
    }
    
    public static String getMachine () {
        return machine;
    }

    public static String getHomeDir () {
        return homeDir;
    }

    public static void setHomeDir (String name) {
        homeDir = name;
        setData();
    }
    
    public static String getCurrentDir () {
        return currentDir;
    }

    public static void setCurrentDir (String name) {
        currentDir = name;
        setData();
    }
    
    public static String getVersion () {
        return version;
    }

    public static void setVersion (String name) {
        version = name;
        setData();
    }

    public static String getJavaVersion () {
        return javaVersion;
    }

    public static void setJavaVersion (String name) {
        javaVersion = name;
        setData();
    }

    public static String getOSName () {
        return osName;
    }

    public static void setOSName (String name) {
        osName = name;
        setData();
    }

    public static String getOSArchitecture () {
        return osArchitecture;
    }

    public static void setOSArchitecture (String name) {
        osArchitecture = name;
        setData();
    }

    public static String getOSVersion () {
        return osVersion;
    }

    public static void setOSVersion (String name) {
        osVersion = name;
        setData();
    }

    public static String getJvmMemory () {
        return jvmMemory;
    }

    public static void setJvmMemory (String memory) {
        jvmMemory = memory;
        setData();
    }

    public static String getFreeMemory () {
        return freeMemory;
    }

    public static void setFreeMemory (String memory) {
        freeMemory = memory;
        setData();
    }
    public static String getData () {
        return data;
    }
 
    private static void setData() {
        data =  "Home-Directory = " + homeDir + "\n" +
                "Current-Working-Directory = " + currentDir + "\n" +
                "Version = " + version + "\n" +
                "Java-Version = " + javaVersion + "\n" +
                "User = " + user + "\n" + 
                "Machine = " + machine + "\n" + 
                "OS-Name = " + osName + "\n" + 
                "OS-Architecture = " + osArchitecture + "\n" + 
                "OS-Version = " + osVersion + "\n" +
                "JVM-Memory = " + jvmMemory + "\n" + 
                "Free-Memory = " + freeMemory + "\n";
        
        data = data.replace("\\", "/");
    }

}
