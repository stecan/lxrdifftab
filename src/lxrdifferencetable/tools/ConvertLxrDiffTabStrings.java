/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.tools;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class to convert / translate Lxr Difference Table internal strings to clear 
 * text strings.
 * 
 * The following strings are affected:
 *      - automaticComment
 *      - automaticDublicate
 *      - dublicatedFile
 *      - newFile
 *      - newDirectory
 *      - fileDeleted
 *      - notChanged
 * 
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class ConvertLxrDiffTabStrings {
    
    private final Locale locale;                      // locale for i18n
    
    public ConvertLxrDiffTabStrings (Locale locale) {
        this.locale = locale;
    }
    
    /**
     * Method to translate the Lxr Difference Table internal string, that are
     * used in the table columns "Review Description" and "Test Description".
     * 
     * These strings are:
     *      - dublicatedFile
     *      - newFile
     *      - newDirectory
     *      - fileDeleted
     *      - notChanged
     * 
     * 
     * @param   desc    - string to translate
     * @return          - translated string
     */
    public String convertLineNumberString (String desc) {
        String text;
        text = desc;
        text = text.replaceAll("dublicatedFile", ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("dublicatedFile"));
        text = text.replaceAll("newFile", ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("newFile"));
        text = text.replaceAll("newDirectory", ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("newDirectory"));
        text = text.replaceAll("fileDeleted", ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("fileDeleted"));
        text = text.replaceAll("notChanged", ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("notChanged"));
        return text;
    }
    
    /**
     * Method to translate the Lxr Difference Table internal string, that are
     * used in the table columns "Review Description" and "Test Description".
     * 
     * These strings are:
     *      - automaticComment
     *      - automaticDublicate
     * 
     * 
     * @param   desc    - string to translate
     * @return          - translated string
     */
    public String convertDescriptionString (String desc) {
        String text;
        text = desc;
        text = text.replaceAll("automaticComment", ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("automaticComment"));
        text = text.replaceAll("automaticDublicate", ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("automaticDublicate"));
        return text;
    }

    /**
     * Method to translate to Lxr Difference Table internal string. 
     * If there is no internal string to translate, then the original 
     * string is returned.
     * 
     * These strings are:
     *      - automaticComment
     *      - automaticDublicate
     *      - dublicatedFile
     *      - newFile
     *      - newDirectory
     *      - fileDeleted
     *      - notChanged
     * 
     * Example1: 
     * 1.       desc    = "Automatisch erkannt!"
     *          return  = "automaticComment"
     * 
     * 2.       desc    = "Automatisch erkannt und modifiziert"
     *          return  = "Automatisch erkannt und modifiziert"
     * 
     * 
     * @param   desc    - string to translate
     * @return          - translated string
     */
    
    public String toInternalString (String desc) {
        String text;
        if (desc.equals(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("automaticComment"))) {
            text = "automaticComment";
        }
        else if (desc.equals(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("automaticDublicate"))) {
            text = "automaticDublicate";
        } else if (desc.equals(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("dublicatedFile"))) {
            text = "dublicatedFile";
        } else if (desc.equals(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("newFile"))) {
            text = "newFile";
        } else if (desc.equals(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("newDirectory"))) {
            text = "newDirectory";
        } else if (desc.equals(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("fileDeleted"))) {
            text = "fileDeleted";
        } else if (desc.equals(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("notChanged"))) {
            text = "notChanged";
        } else {
            text = desc;
        }
        return text;
    }

}
