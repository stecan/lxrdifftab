/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lxrdifferencetable.ConfigController;
import lxrdifferencetable.ProgressController;
import lxrdifferencetable.data.ConfigEntry;
import lxrdifferencetable.data.EntryPosition;
import lxrdifferencetable.data.LogEntry;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.data.TableEntry.State;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author Stefan Canali
 */
public class XmlHandling {
    
    private final int MAX_RETRY_COUNT = 10; // 10 * 1s = 10 seconds timeout!
    
    public enum LockType {notLocked, ownLocked, locked};
    public enum RowType {reviewState, reviewShortDesc, testState, testDesc, caption, unknown};
    
    private final static Logger LOGGER = Logger.getLogger
            (ConfigController.class.getName());
    
    private final Properties props;
    private final ProgressController progressController;
    
    private ConfigEntry configEntry;
    private List<TableEntry> changedFileList;
    private List<LogEntry> repoLog;
    
    private File file;                  // base xml file
    private File lockDir;               // base lock directory
    private File lockFile;              // own lock file
    private File exclusiveLockFile;     // exclusive lock for main xml file
    private FileLock exclusiveLock;     // global lock of attached xml file
    private FileLock ownLock;           // local lock
    private List<File> lockFiles;       // list of active lock files
    private final String lockName;      // own lock name
    private final List<String> lockNameList;    // list of all active "logins"
    private final List<LockedRowsList> lockedRowsList;  // list of locked rows
    
    private final String user;          // user name, e.g. "stefan"
    private final String computerName;  // computer name, e.g. "localhost"
    private final String pid;           // process identity, e.g. "1345"
    
    private boolean lineNumberAvailable;    // is line numbering available?
    
    private boolean readOnlyFS;         // marker for read-only file systems
    
    /**
     * Constructor
     * 
     * @param progressController
     */
    public XmlHandling(ProgressController progressController) {
        readOnlyFS = false;                             // default: writeable!
        props = new Properties();
        this.progressController = progressController;
        // initialize local variables
        user = System.getProperty("user.name");         // set user name
        String name;
        try {
            name = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            name = "localhost";
        }
        computerName = name;                            // set computer name
        name = ManagementFactory.getRuntimeMXBean().getName();
        String[] split = name.split("@");
        pid = split[0];                                 // set pid
        lockName = user + "-" + computerName + "-" + pid;
        lineNumberAvailable = false;
        lockNameList = new ArrayList<>();
        lockNameList.add(lockName);                     // add own lock name
        lockedRowsList = new ArrayList<>();
    }

    /**
     * This method gets an exclusive (write-) lock to the file
     *              ./.lock/.lock
     * 
     * This lock is to prohibit any access to the root xml file.
     */
    
    private void exclusiveLock() {
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(exclusiveLockFile);   // open output file
            exclusiveLock = fos.getChannel().lock();
        } catch (IOException ex) {
            LOGGER.info("An exception occured. No lock is gotten!");
        }
    }

    /**
     * This method releases the exclusive (write-) lock to the file
     *              ./.lock/.lock
     */
    private void releaseExclusiveLock() {
        try {
            exclusiveLock.release();
        } catch (IOException ex) {
            LOGGER.info("An exception occured. Lock could not be released!");
        }
    }

    /**
     * This method removes lock files of the form
     *          .stefan-pce00017-4260.lock
     * @param terminate     true  -> delete own lock
     *                      false -> own lock is not deleted
     */
    public void clearOldLocks(boolean terminate) {
        lockFiles = new ArrayList();
        File[] listFiles = lockDir.listFiles();
        for (File f : listFiles) {
            String str = f.getName();
            String regexp = "^\\.(.*)\\.lock";
            Pattern pattern = Pattern.compile(regexp, 
                    Pattern.CASE_INSENSITIVE);
            Matcher matcher;
            matcher = pattern.matcher(str);
            if (matcher.find()) {
                String name = matcher.group(1);
                
                if ((lockFile.equals(f)) && (!terminate)) {
                    // own lock file!
                    lockFiles.add(f);
                    addLogin(name);
                } else {
                    FileOutputStream fos;
                    try {
                        fos = new FileOutputStream(f);      // open output file
                        FileLock lock = fos.getChannel().tryLock();
                        if (lock == null) {
                            // Could not get the lock
                            lockFiles.add(f);
                            addLogin(name);
                        } else {
                            // The lock is gotten
                            lock.channel().close();  // release lock
                            if (!f.delete()) {       // delete lock file
                                LOGGER.info("Could not delete own lock file!");
                            }
                            deleteLogin(name);      // delete login name

                        }
                    } catch (Exception ex) {
                        // Could not get the lock
                        lockFiles.add(f);
                        addLogin(name);
                    }
                }
            }
        }
    }
    
    /**
     * This method reads locked xml files of the form
     *          1-0_stefan-pce00017-30221.lock.xml
     * 
     * Implicitly, the following member variables are modified
     * - changedFileList
     * - configEntry
     * 
     * @param f             File to read
     * @param toDelete      true  -> lock file should be deleted
     *                      false -> lock file should not be deleted
     * @return              entry position, that is changed
     *                      -1 -> an error occured!
     */
    public EntryPosition readLockedXml(File f, boolean toDelete) {
        EntryPosition pos = new EntryPosition();
        pos.putChangedLineNumber(-1);
        String regexp1 = "^(\\d*)-(\\d*)_(.*)_(.*)\\.lock.xml";
        String regexp2 = "^(\\d*)-(\\d*)_(.*)\\.lock";
        Pattern pattern1 = Pattern.compile(regexp1, Pattern.CASE_INSENSITIVE);
        Pattern pattern2 = Pattern.compile(regexp2, Pattern.CASE_INSENSITIVE);
        Matcher matcher1;
        Matcher matcher2;
        matcher1 = pattern1.matcher(f.getName());
        matcher2 = pattern2.matcher(f.getName());
        if (matcher1.find()) {
            String posString = matcher1.group(1);
            String type = matcher1.group(3);
            String xmlLockFileNumberString = matcher1.group(2);
            String loginName = matcher1.group(4);
            pos.putFileNumber(Integer.parseInt(posString) - 1);
            try {
                if (type.equals("Caption")) {
                    pos.putChangedLineNumber(readCaptionXml(f,
                            Integer.parseInt(xmlLockFileNumberString), 
                            loginName));
                } else {
                    pos.putChangedLineNumber(readXml(f, pos.getFileNumber(), 
                            Integer.parseInt(xmlLockFileNumberString), loginName));
                }
            } catch (IOException | JDOMException ex) {
                LOGGER.info("Can't read xml file!");
            }
            if (!isReadOnly(file)) {            // do only for writeable access!
                if (toDelete) {
                    if (type.equals("Caption")) {
                        configEntry.putXmlLockFileNumber(-1);
                        if (!f.delete()) {
                            LOGGER.info("Can't delete XML file!");
                        }
                    } else {
                        // initialize XmlLockFileNumber (Xml lock files are deleted!)
                        changedFileList.get(pos.getFileNumber()).putXmlLockFileNumber(-1);
                        if (!f.delete()) {
                            LOGGER.info("Can't delete XML file!");
                        }
                    }
                }
            }
        } else if (matcher2.find()) {
            if (!isReadOnly(file)) {            // do only for writeable access!
                if (toDelete) {
                    if (!f.delete()) {
                        LOGGER.info("Can't delete XML file!");
                    }
                }
            }
        } else {
           // It's not a locked xml file (e.g. it is a normal lock like
           // ".stecan-PCD22798-6696.lock"). 
           //
           // Do nothing!
       }
        return pos;
    }
    
    /**
     * This method returns wheater the given entry "i" is locked, not locked or 
     * own locked. 
     * 
     * @param i             position of entry
     * @return  LockType    {notLocked, ownLocked, Locked}
     */
    public LockType isLocked (int i) {
        
        LockType locked;
        String loginName = changedFileList.get(i).getLoginName();
        if (loginName.equals("")) {
            locked = LockType.notLocked;
        } else if (loginName.equals(lockName)) {
            locked = LockType.ownLocked;
        } else if (isLoginValid(loginName)) {
            if (isLoginValid(loginName)) {
                locked = LockType.locked;
            } else {
                locked = LockType.notLocked;
            }
        } else {
            locked = LockType.notLocked;
        }
        return locked;
    }
    
    /**
     * This method returns wheater the caption is locked, not locked or 
     * own locked. 
     * 
     * @return  LockType    {notLocked, ownLocked, Locked}
     */
    public LockType isCaptionLocked () {
        
        LockType locked;
        String loginName = configEntry.getLoginName();
        if (loginName.equals("")) {
            locked = LockType.notLocked;
        } else if (loginName.equals(lockName)) {
            locked = LockType.ownLocked;
        } else if (isLoginValid(loginName)) {
            if (isLoginValid(loginName)) {
                locked = LockType.locked;
            } else {
                locked = LockType.notLocked;
            }
        } else {
            locked = LockType.notLocked;
        }
        return locked;
    }

    /**
     * This method returns if the xml file is locked, not locked or own locked. 
     * 
     * @param f             xml file
     * @return  LockType    {notLocked, ownLocked, Locked}
     */
    public LockType isLocked(File f) {
        LockType locked = LockType.notLocked;
        
        String regexpOwn = "^(\\d*)-(\\d*)_(.*_)(.*)" + lockName + ".lock.xml";
        Pattern patternOwn = Pattern.compile(regexpOwn, Pattern.CASE_INSENSITIVE);
        Matcher matcherOwn;

        String regexp = "^(\\d*)-(\\d*)_(.*_)(.*)\\.lock.xml";
        Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
        Matcher matcher;

        String str = f.getName();
        matcherOwn = patternOwn.matcher(str);
        if (matcherOwn.find()) {
            // own lock
            locked = LockType.ownLocked;
        } else {
            matcher = pattern.matcher(str);
            if (matcher.find()) {
                // foreign lock
                String loginName = matcher.group(4);
                if (isLoginValid(loginName)) {
                    locked = LockType.locked;
                }
            }
        }
        return locked;
    }

    /**
     * This method extracts the login name out of xml file name.
     * 
     * Example:
     *      file name  = "1-2_ReviewResult_stecan-PCD22798-9756.lock.xml"
     *      login name = "stecan-PCD22798-9756"
     * 
     * @param f             xml file
     * @return string       login name
     */
    public String getXMLLockName(File f) {
        String name = "";
        String regexp = "^(\\d*)-(\\d*)_(.*)_(.*)\\.lock.xml";
        Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
        Matcher matcher;
        String str = f.getName();
        matcher = pattern.matcher(str);
        if (matcher.find()) {
            name = matcher.group(4);
        }
        return name;
    }
    
    /**
     * This method extracts the login name out of xml file name.
     * 
     * Example:
     *      file name  = "1-2_ReviewResult_stecan-PCD22798-9756.lock.xml"
     *      login name = "stecan-PCD22798-9756"
     * 
     * @param f             xml file
     * @return string       login name
     */
    public RowType getRowType (File f) {
        RowType rowType;
        String name = "";
        String regexp = "^(\\d*)-(\\d*)_(.*)_(.*)\\.lock.xml";
        Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
        Matcher matcher;
        String str = f.getName();
        matcher = pattern.matcher(str);
        if (matcher.find()) {
            name = matcher.group(3);
        }
        switch (name) {
            case "ReviewResult":
                rowType = RowType.reviewState;
                break;
            case "ReviewShortDescription":
                rowType = RowType.reviewShortDesc;
                break;
            case "TestResult":
                rowType = RowType.testState;
                break;
            case "TestShortDescription": 
                rowType = RowType.testDesc;
                break;
            case "Caption": 
                rowType = RowType.caption;
                break;
            default:
                rowType = RowType.unknown;
                break;
        }
        return rowType;
    }

    /**
     * This method cleans up locked xml files of the form
     *          1-0_stefan-pce00017-30221.lock.xml
     */
    private void cleanUpXmlLockFiles() {
        boolean locksRead = false;

        // read all locked xml files
        long time1 = System.nanoTime();
        File[] listFiles = lockDir.listFiles();
        long time2 = System.nanoTime();
        System.out.println("Zeit zum Auflisten der Dateien = " + 
                (time2 - time1)/1000000 + "ms");
        
        if (listFiles == null) {
            // No writeable access: Do nothing!
        } else {
            Comparator<? super String> comparator = new Comparator<String>() 
            {
                public int compare(String name1, String name2) {
                    String regexp = "^(\\d*)-(\\d*)_(.*)\\.lock";
                    Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
                    Matcher matcher1;
                    Matcher matcher2;
                    matcher1 = pattern.matcher(name1);
                    matcher2 = pattern.matcher(name2);

                    boolean numberedLockFile1 = matcher1.find();
                    boolean numberedLockFile2 = matcher2.find();
                    if (numberedLockFile1 && numberedLockFile2) {
                        // e.g. 
                        // name1 = "1-1_ReviewResult.lock"
                        // name2 = "2-3_ReviewResult.lock"
                        //
                        int num1 = Integer.parseInt(matcher1.group(1));
                        int num1_1 = Integer.parseInt(matcher1.group(2));
                        int num2 = Integer.parseInt(matcher2.group(1));
                        int num2_1 = Integer.parseInt(matcher2.group(2));
                        String type1 = matcher1.group(3);
                        String type2 = matcher2.group(3);
                        // < : Increasing order 
                        // > : Decreasing order
                        if(num1 < num2) {
                            // e.g. 
                            // name1 = "1-1_ReviewResult.lock"
                            // name2 = "2-3_ReviewResult.lock"
                            return -1; // 1;
                        } else if (num1 == num2) {
                            if (num1_1 < num2_1) {
                                // e.g. 
                                // name1 = "2-1_ReviewResult.lock"
                                // name2 = "2-3_ReviewResult.lock"
                                return -1; // 1;
                            } else if (num1_1 == num2_1) {
                                // e.g. 
                                // name1 = "2-3_ReviewResult.lock"
                                // name2 = "2-3_ReviewDescription.lock"
                                return type1.compareTo(type2);
                            } else {
                                // e.g. 
                                // name1 = "2-3_ReviewResult.lock"
                                // name2 = "2-1_ReviewResult.lock"
                                return 1; // -1;
                            }
                        } else {    // num1 > num2!
                            return 1; // -1;
                        }
                    } else {
                        return name1.compareTo(name2);
                    }
                }
            };            
            
            long time3 = System.nanoTime();
            HashMap<String, Integer> map = new HashMap<>();
            List<String> listFileNames = new ArrayList<String>();
            int i = 0;
            for (File f : listFiles) {
                String fileName = f.getName();
                listFileNames.add(fileName);
                map.put(fileName, i);   // save mapping to original "listFiles"
                i++;
            }
            Collections.sort(listFileNames, comparator);
            long time4 = System.nanoTime();
            System.out.println("Zeit zum Sortieren der Dateien = " + 
                    (time4 - time3)/1000000 + "ms");
            
            long time5 = System.nanoTime();
            for (String str : listFileNames) {
                File f = listFiles[map.get(str)];
                if ((!str.equals(lockFile.getName())) && 
                        (!str.equals(exclusiveLockFile.getName()))) {
                    // Read locked xml files and delete 
                    // them, if no lock is set!

                    // The function lockFiles.isEmpty() does not function because
                    // of own lock!!!
                    // readLockedXml(f, lockFiles.isEmpty());
                    if (lockFiles == null) {
                        readLockedXml(f, false);            // no writeable access!
                    } else if (lockFiles.size() <= 1) {
                        readLockedXml(f, true);
                    } else {
                        readLockedXml(f, false);
                    }
                    locksRead = true;       // a minimum of one change is made
                }
            }
            long time6 = System.nanoTime();
            System.out.println("Zeit zum Lesen der Dateien = " + 
                    (time6 - time5)/1000000 + "ms");
            if (!isReadOnly(file)) {                        // do only for writable access!
                // if (lockFiles.isEmpty()) {               // lockFiles.empty() does 
                if (lockFiles.size() <= 1) {                // not function here because
                                                            // of own lock!
                    // No foreign lock files are available! Now, it is time to clean up the 
                    // locked xml files and writes it to the base xml file
                    try {
                        if (locksRead) {
                            // Only writes xml file, if a minimum of one change is made.
                            writeXml(file,                  // base file
                                    configEntry,            // old config entry
                                    changedFileList,        // old file list
                                    repoLog,                // log info for scan
                                    false);                 // lock is not required 
                                                            // (because it is locked now)!
                            long time7 = System.nanoTime();
                            System.out.println("Zeit zum Neuschreiben der Haupt-XML Datei = " + 
                                    (time7 - time6)/1000000 + "ms");
                        }
                    } catch (IOException ex) {
                        LOGGER.info("Can't write xml file!");
                    }
                }
            }
        }
    }
    
    /**
     * This method creates a lock of the following form:
     *          .stefan-pce00017-4260.lock
     * 
     * The lock ist created in the directory "./.lock". 
     * If the directory doesn't exist, then it is created.
     * 
     */
    private void createLockFile () {
        try {
            lockDir = new File(this.file.getParent() + "/.lock");
            exclusiveLockFile = new File(lockDir + "/.lock");

            if (!lockDir.exists()) {
                try {
                    Files.createDirectory(lockDir.toPath());
                } catch (Exception ex) {
                    readOnlyFS = true;
                    LOGGER.info("An exception occured!");
                }
            }
            lockFile = new File (lockDir.getAbsolutePath() + "/." + lockName + ".lock");
            try {
                FileOutputStream fos;
                fos = new FileOutputStream(lockFile);   // open output file
                ownLock = fos.getChannel().lock();
            } catch (FileNotFoundException ex) {
                readOnlyFS = true;
                LOGGER.info("An exception occured: Could not create own lock!");
            } catch (IOException ex) {
                readOnlyFS = true;
                LOGGER.info("An exception occured: Could not lock own lock!");
            }
        } catch (Exception ex) {
            String name = ex.getClass().getName();
            if (name.equals("java.nio.channels.OverlappingFileLockException")) {
                // It's ok!
                //
                // On reloading the stage (e.g. change the language settings), 
                // an OverlappingFileLockException occurs because a file lock 
                // cannot be locked again by the same programm on different 
                // threads!
            } else {
                LOGGER.info("An exception occured: Could not lock own lock!");
            }
        }
    }
    
    /**
     * This method releases the own lock file.
     * 
     * The own lock file is of the following form:
     *              .stefan-pce00017-4260.lock
     */
    public void releaseLockFile() {
        try {
            if (ownLock != null) {
                // ownLock.release();          // does not function as expected!
                ownLock.channel().close();
            }
        } catch (IOException ex) {
            LOGGER.info("An exception occured: Could not release own lock!");
        }
    }

    /**
     * This method deletes the own lock file.
     */
    private void deleteLockFile() {
        if (lockFile.exists()) {
            if (!lockFile.delete()) {
                LOGGER.info("Cannot delete lock file!");
            }
        }
    }
    
    /**
     * This method writes an lock for the "review result". The lock is of form:
     *          1-0_ReviewResult.lock
     *          8-5_ReviewResult.lock
     *          ...
     * 
     * @param   entry
     * @return  true    -> file lock is gotten
     *          false   -> file lock is not gotten (an error occured)
     */
    public boolean writeReviewResultLock(TableEntry entry) {
        boolean finished = false;
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String intermediateFileName = lockDir + "/" + entry.getFileCount() + "-" +
                    j + "_ReviewResult.lock";
            FileOutputStream fos;
            fos = new FileOutputStream(intermediateFileName);   // open input file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            //
            // Note:
            // tryLock() is a shorthand for tryLock(0L, Long.MAX_VALUE, false), 
            // i.e. it requests an exclusive write-lock. Thus, tryLock()
            // throws an exception on an input channel!
            //
            try {
                FileLock lock = fos.getChannel().tryLock();
                if (lock == null) {
                    LOGGER.info("File lock not acquired! Sleeping for 1 second! ...");
                } else {
                    finished = true;
                }
            } catch (OverlappingFileLockException ex) {
                finished = true;
            }

        } catch (IOException ex) {
            LOGGER.info("An exception occured in reviewResultLock");
        } 
        // File lock automatically released (by try-resource mecahnism)            

        return finished;
        
    }
    
    /**
     * This method writes the "Review Result" of table entry i to the lock
     * file of name
     *          i-j_stefan-pce00017-30221.lock.xml
     * (with the integer j)
     * 
     * @param pos       position in table entry
     * @param entry     table entry
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeReviewResult (int pos, TableEntry entry)
            throws FileNotFoundException, IOException {
        Element element = new Element ("Files");    // root element
        Document doc = new Document(element);

        Element changed = new Element ("Changed");
        changed.setAttribute(new Attribute("Filename", entry.getFile()));
        
        Element e = new Element("Review-Result-Author-" + String.valueOf(pos));
        e.setAttribute("Review-Result-Author-" + String.valueOf(pos), 
                entry.getReviewResAuthorLast(pos));
        changed.addContent(e);
        Element e1 = new Element("Review-Result-Date-" + String.valueOf(pos));
        e1.setAttribute("Review-Result-Date-" + String.valueOf(pos), 
                entry.getReviewResDateLast(pos));
        changed.addContent(e1);
        Element e2 = new Element("Review-Result-" + String.valueOf(pos));
        e2.setAttribute("Review-Result-" + String.valueOf(pos), 
                entry.getReviewStateLast(pos).toString());
        changed.addContent(e2);

        doc.getRootElement().addContent(changed);

        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());
        
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String fileName = lockDir + "/" + entry.getFileCount() + "-" +
            j + "_ReviewResult_" + lockName + ".lock.xml";

            FileOutputStream fos;
            fos = new FileOutputStream(fileName);   // open output file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            try (FileLock lock = fos.getChannel().lock()) {
                xmlOutput.output(doc, fos);
            }
            // save 
            entry.putXmlLockFileNumber(j);  // save lock file number


        } catch (IOException ex) {
            LOGGER.info("An exception occured in writeReviewResult");
        } 
        // File lock automatically released (by try-resource mecahnism)            
    }
    
    /**
     * This method writes the "Caption" to the lock
     * file of name
     *          0-j_stefan-pce00017-30221.lock.xml
     * (with the integer j)
     * 
     * @param entry     table entry
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeCaption (ConfigEntry entry)
            throws FileNotFoundException, IOException {
        Element element = new Element ("Files");    // root element
        Document doc = new Document(element);

        Element captioned = new Element ("Caption");
        
        Element e = new Element("Author");
        e.setAttribute("Author", entry.getCaptionAuthorLast());
        captioned.addContent(e);
        Element e1 = new Element("Date");
        e1.setAttribute("Date", entry.getCaptionDateLast());
        captioned.addContent(e1);
        Element e2 = new Element("Caption");
        e2.setAttribute("Caption", entry.getCaptionLast());
        captioned.addContent(e2);

        Element configured = new Element ("Configuration");
        configured.addContent(captioned);
        doc.getRootElement().addContent(configured);

        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());
        
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String fileName = lockDir + "/0-" + j + "_Caption_" 
                    + lockName + ".lock.xml";

            FileOutputStream fos;
            fos = new FileOutputStream(fileName);   // open output file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            try (FileLock lock = fos.getChannel().lock()) {
                xmlOutput.output(doc, fos);
            }
            // save 
            entry.putXmlLockFileNumber(j);  // save lock file number


        } catch (IOException ex) {
            LOGGER.info("An exception occured in writeCaption");
        } 
        // File lock automatically released (by try-resource mecahnism)            
    }

    /**
     * This method writes an lock for the "review description". The lock is of form:
     *          1-0_ReviewShortDescription.lock
     *          8-5_ReviewShortDescription.lock
     *          ...
     * 
     * @param   entry
     * @return  true    -> file lock is gotten
     *          false   -> file lock is not gotten (an error occured)
     */
    public boolean writeReviewShortDescriptionLock(TableEntry entry) {
        boolean finished = false;
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String intermediateFileName = lockDir + "/" + entry.getFileCount() + "-" +
                    j + "_ReviewShortDescription.lock";
            FileOutputStream fos;
            fos = new FileOutputStream(intermediateFileName);   // open input file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            //
            // Note:
            // tryLock() is a shorthand for tryLock(0L, Long.MAX_VALUE, false), 
            // i.e. it requests an exclusive write-lock. Thus, tryLock()
            // throws an exception on an input channel!
            //
            try {
                FileLock lock = fos.getChannel().tryLock();
                if (lock == null) {
                    LOGGER.info("File lock not acquired! Sleeping for 1 second! ...");
                } else {
                    finished = true;
                }
            } catch (OverlappingFileLockException ex) {
                finished = true;
            }

        } catch (IOException ex) {
            LOGGER.info("An exception occured in writeReviewShortDescription");
        } 
        // File lock automatically released (by try-resource mecahnism)            

        return finished;
        
    }
    
    /**
     * This method writes an lock for the "test description". The lock is of form:
     *          1-0_TestShortDescription.lock
     *          8-5_TestShortDescription.lock
     *          ...
     * 
     * @param   entry
     * @return  true    -> file lock is gotten
     *          false   -> file lock is not gotten (an error occured)
     */
    public boolean writeTestShortDescriptionLock(TableEntry entry) {
        boolean finished = false;
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String intermediateFileName = lockDir + "/" + entry.getFileCount() + "-" +
                    j + "_TestShortDescription.lock";
            FileOutputStream fos;
            fos = new FileOutputStream(intermediateFileName);   // open input file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            //
            // Note:
            // tryLock() is a shorthand for tryLock(0L, Long.MAX_VALUE, false), 
            // i.e. it requests an exclusive write-lock. Thus, tryLock()
            // throws an exception on an input channel!
            //
            try {
                FileLock lock = fos.getChannel().tryLock();
                if (lock == null) {
                    LOGGER.info("File lock not acquired! Sleeping for 1 second! ...");
                } else {
                    finished = true;
                }
            } catch (OverlappingFileLockException ex) {
                finished = true;
            }

        } catch (IOException ex) {
            LOGGER.info("An exception occured in testShortDescriptionLock");
        } 
        // File lock automatically released (by try-resource mecahnism)            

        return finished;
        
    }

    /**
     * This method writes an lock for the "caption". The lock is of form:
     *          0-0_Caption.lock
     *          0-1_Caption.lock
     *          ...
     * 
     * @param   entry
     * @return  true    -> file lock is gotten
     *          false   -> file lock is not gotten (an error occured)
     */
    public boolean writeCaptionLock(ConfigEntry entry) {
        boolean finished = false;
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String intermediateFileName = lockDir + "/0-" +
                    j + "_Caption.lock";
            FileOutputStream fos;
            fos = new FileOutputStream(intermediateFileName);   // open input file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            //
            // Note:
            // tryLock() is a shorthand for tryLock(0L, Long.MAX_VALUE, false), 
            // i.e. it requests an exclusive write-lock. Thus, tryLock()
            // throws an exception on an input channel!
            //
            try {
                FileLock lock = fos.getChannel().tryLock();
                if (lock == null) {
                    LOGGER.info("File lock not acquired! Sleeping for 1 second! ...");
                } else {
                    finished = true;
                }
            } catch (OverlappingFileLockException ex) {
                finished = true;
            }

        } catch (IOException ex) {
            LOGGER.info("An exception occured in captionLock");
        } 
        // File lock automatically released (by try-resource mecahnism)            

        return finished;
        
    }

    /**
     * This method writes the "Review Description" of table entry i to the lock
     * file of name
     *          i-j_stefan-pce00017-30221.lock.xml
     * (with the integer j)
     * 
     * @param pos       position in table entry
     * @param entry     table entry
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeReviewShortDescription (int pos, TableEntry entry)
            throws FileNotFoundException, IOException {
        Element element = new Element ("Files");    // root element
        Document doc = new Document(element);

        Element changed = new Element ("Changed");
        changed.setAttribute(new Attribute("Filename", entry.getFile()));
        
        Element e = new Element("Review-Description-Author-" + String.valueOf(pos));
        e.setAttribute("Review-Description-Author-" + String.valueOf(pos), 
                entry.getReviewDescAuthorLast(pos));
        changed.addContent(e);
        Element e1 = new Element("Review-Description-Date-" + String.valueOf(pos));
        e1.setAttribute("Review-Description-Date-" + String.valueOf(pos), 
                entry.getReviewDescDateLast(pos));
        changed.addContent(e1);
        Element e2 = new Element("Review-Short-Description-" + String.valueOf(pos));
        e2.setAttribute("Review-Short-Description-" + String.valueOf(pos), 
                entry.getReviewShortDescriptionLast(pos));
        changed.addContent(e2);
        Element e3 = new Element("Review-Description-" + String.valueOf(pos));
        e3.setAttribute("Review-Description-" + String.valueOf(pos), 
                entry.getReviewDescriptionLast(pos));
        changed.addContent(e3);

        doc.getRootElement().addContent(changed);

        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());
        
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String fileName = lockDir + "/" + entry.getFileCount() + "-" +
            j + "_ReviewShortDescription_" + lockName + ".lock.xml";

            FileOutputStream fos;
            fos = new FileOutputStream(fileName);   // open output file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            try (FileLock lock = fos.getChannel().lock()) {
                xmlOutput.output(doc, fos);
            }
            // save 
            entry.putXmlLockFileNumber(j);  // save lock file number


        } catch (IOException ex) {
            LOGGER.info("An exception occured in writeReviewShortDescription");
        } 
        // File lock automatically released (by try-resource mecahnism)            
    }

    /**
     * This method writes the "Test Description" of table entry i to the lock
     * file of name
     *          i-j_stefan-pce00017-30221.lock.xml
     * (with the integer j)
     * 
     * @param pos       position in table entry
     * @param entry     table entry
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeTestShortDescription (int pos, TableEntry entry)
            throws FileNotFoundException, IOException {
        Element element = new Element ("Files");    // root element
        Document doc = new Document(element);

        Element changed = new Element ("Changed");
        changed.setAttribute(new Attribute("Filename", entry.getFile()));
        
        Element e = new Element("Test-Description-Author-" + String.valueOf(pos));
        e.setAttribute("Test-Description-Author-" + String.valueOf(pos), 
                entry.getTestDescAuthorLast(pos));
        changed.addContent(e);
        Element e1 = new Element("Test-Description-Date-" + String.valueOf(pos));
        e1.setAttribute("Test-Description-Date-" + String.valueOf(pos), 
                entry.getTestDescDateLast(pos));
        changed.addContent(e1);
        Element e2 = new Element("Test-Short-Description-" + String.valueOf(pos));
        e2.setAttribute("Test-Short-Description-" + String.valueOf(pos), 
                entry.getTestShortDescriptionLast(pos));
        changed.addContent(e2);
        Element e3 = new Element("Test-Description-" + String.valueOf(pos));
        e3.setAttribute("Test-Description-" + String.valueOf(pos), 
                entry.getTestDescriptionLast(pos));
        changed.addContent(e3);

        doc.getRootElement().addContent(changed);

        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());
        
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String fileName = lockDir + "/" + entry.getFileCount() + "-" +
            j + "_TestShortDescription_" + lockName + ".lock.xml";

            FileOutputStream fos;
            fos = new FileOutputStream(fileName);   // open output file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            try (FileLock lock = fos.getChannel().lock()) {
                xmlOutput.output(doc, fos);
            }
            // save 
            entry.putXmlLockFileNumber(j);  // save lock file number


        } catch (IOException ex) {
            LOGGER.info("An exception occured in writeTestShortDescription");
        } 
        // File lock automatically released (by try-resource mecahnism)            
    }
    
    /**
     * This method writes an lock for the "test result". The lock is of form:
     *          1-0_TestResult.lock
     *          8-5_TestResult.lock
     *          ...
     * 
     * @param   entry
     * @return  true    -> file lock is gotten
     *          false   -> file lock is not gotten (an error occured)
     */
    public boolean writeTestResultLock(TableEntry entry) {
        boolean finished = false;
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String intermediateFileName = lockDir + "/" + entry.getFileCount() + "-" +
                    j + "_TestResult.lock";
            FileOutputStream fos;
            fos = new FileOutputStream(intermediateFileName);   // open input file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            //
            // Note:
            // tryLock() is a shorthand for tryLock(0L, Long.MAX_VALUE, false), 
            // i.e. it requests an exclusive write-lock. Thus, tryLock()
            // throws an exception on an input channel!
            //
            try {
                FileLock lock = fos.getChannel().tryLock();
                if (lock == null) {
                    LOGGER.info("File lock not acquired! Sleeping for 1 second! ...");
                } else {
                    finished = true;
                }
            } catch (OverlappingFileLockException ex) {
                finished = true;
            }

        } catch (IOException ex) {
            LOGGER.info("An exception occured in writeTestResult");
        } 
        // File lock automatically released (by try-resource mecahnism)            

        return finished;
        
    }
    
    /**
     * This method writes the "Test Result" of table entry i to the lock
     * file of name
     *          i-j_stefan-pce00017-30221.lock.xml
     * (with the integer j)
     * 
     * @param pos       position in table entry
     * @param entry     table entry
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeTestResult (int pos, TableEntry entry)
            throws FileNotFoundException, IOException {
        Element element = new Element ("Files");    // root element
        Document doc = new Document(element);

        Element changed = new Element ("Changed");
        changed.setAttribute(new Attribute("Filename", entry.getFile()));
        
        Element e = new Element("Test-Result-Author-" + String.valueOf(pos));
        e.setAttribute("Test-Result-Author-" + String.valueOf(pos), 
                entry.getTestResAuthorLast(pos));
        changed.addContent(e);
        Element e1 = new Element("Test-Result-Date-" + String.valueOf(pos));
        e1.setAttribute("Test-Result-Date-" + String.valueOf(pos), 
                entry.getTestResDateLast(pos));
        changed.addContent(e1);
        Element e2 = new Element("Test-Result-" + String.valueOf(pos));
        e2.setAttribute("Test-Result-" + String.valueOf(pos), 
                entry.getTestStateLast(pos).toString());
        changed.addContent(e2);

        doc.getRootElement().addContent(changed);

        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());
        
        try {
            // Note:
            // Beware of "-" and "_" after "i" and "j"!!!
            //
            int j = entry.getXmlLockFileNumber();
            j++;            // increment to next free lock file number

            String fileName = lockDir + "/" + entry.getFileCount() + "-" +
            j + "_TestResult_" + lockName + ".lock.xml";

            FileOutputStream fos;
            fos = new FileOutputStream(fileName);   // open output file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            try (FileLock lock = fos.getChannel().lock()) {
                xmlOutput.output(doc, fos);
            }
            // save 
            entry.putXmlLockFileNumber(j);  // save lock file number


        } catch (IOException ex) {
            LOGGER.info("An exception occured in writeTestResult");
        } 
        // File lock automatically released (by try-resource mecahnism)            
    }


    /**
     * This method writes the config entry and the changed file list to the 
     * given "file". 
     * 
     * @param file              file to write
     * @param configEntry       config entry to write
     * @param changedFileList   changed file list to write
     * @param writeLock         true -> write additional lock file
     *                          false -> do not write additional lock file
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void writeXml(File file, 
            ConfigEntry configEntry,
            List<TableEntry> changedFileList,
            List<LogEntry> repoLog,
            boolean writeLock) 
            throws FileNotFoundException, IOException {

        this.file = file;
        boolean svn = configEntry.getSvn().equals("true");
        Element element = new Element ("Files");    // root element
        Document doc = new Document(element);

        Element config = new Element ("Configuration");
        // 1st configuration entry
        Element svnElement = new Element("Svn");
        svnElement.setAttribute(new Attribute("Svn", Boolean.toString(svn)));
        config.addContent(svnElement);
        // 2nd configuration entry
        Element versionElement = new Element("Version");
        versionElement.setAttribute(new Attribute("Version", 
                props.getProperty(props.VERSION)));
        config.addContent(versionElement);
        // 3nd configuration entry
        Element captionElement = new Element("Caption");
        for (String str : configEntry.getCaption()) {
            Element e0 = new Element("Caption");
            e0.setAttribute("Caption", str);
            captionElement.addContent(e0);
        }
        for (String str : configEntry.getCaptionAuthor()) {
            Element e0 = new Element("Author");
            e0.setAttribute("Author", str);
            captionElement.addContent(e0);
        }
        for (String str : configEntry.getCaptionDate()) {
            Element e0 = new Element("Date");
            e0.setAttribute("Date", str);
            captionElement.addContent(e0);
        }
        config.addContent(captionElement);
        doc.getRootElement().addContent(config);
        
        
        Element log = new Element ("Log");
        for (int i = 0; i < repoLog.size(); i++) {
            LogEntry logEntry = repoLog.get(i);
            Element l0 = new Element("Description");
            l0.setAttribute("Description", logEntry.getDescription());
            if (!logEntry.getMessage1().equals("")) {
                l0.setAttribute("Message1", logEntry.getMessage1());
            }
            if (!logEntry.getMessage2().equals("")) {
                l0.setAttribute("Message2", logEntry.getMessage2());
            }
            if (!logEntry.getMessage3().equals("")) {
                l0.setAttribute("Message3", logEntry.getMessage3());
            }
            if (!logEntry.getMessage4().equals("")) {
                l0.setAttribute("Message4", logEntry.getMessage4());
            }
            log.addContent(l0);
        }
        doc.getRootElement().addContent(log);

        
        for (TableEntry entry : changedFileList) {

            Element changed = new Element ("Changed");
            changed.setAttribute(new Attribute("Filename", entry.getFile()));

            Element diff = new Element("Diff");
            diff.setAttribute("Diff", entry.getDiff());
            changed.addContent(diff);

            Element FileCount = new Element("File-Count");
            FileCount.setAttribute("File-Count", entry.getFileCount());
            changed.addContent(FileCount);

            int i = -1;
            for (List<String> list : entry.getHref()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Href-" + String.valueOf(i));
                    e.setAttribute("Href-" + String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getHrefText()) {
                i++;
                for (String str : list) {
                    Element e = new Element("HrefText-" + String.valueOf(i));
                    e.setAttribute("HrefText-" + String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getLineNumber()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Line-Number-" + String.valueOf(i));
                    e.setAttribute("Line-Number-" + String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            for (String str : entry.getSvnAuthor()) {
                Element e = new Element("SVN-Author");
                e.setAttribute("SVN-Author", str);
                changed.addContent(e);
            }
            for (String str : entry.getSvnDate()) {
                Element e = new Element("SVN-Date");
                e.setAttribute("SVN-Date", str);
                changed.addContent(e);
            }
            for (String str : entry.getSvnDesc()) {
                Element e = new Element("SVN-Description");
                e.setAttribute("SVN-Description", str);
                changed.addContent(e);
            }
            for (String str : entry.getSvnVersion()) {
                Element e = new Element("SVN-Version");
                e.setAttribute("SVN-Version", str);
                changed.addContent(e);
            }

            i = -1;
            for (List<State> list : entry.getReviewState()) {
                i++;
                for (State str : list) {
                    Element e = new Element("Review-Result-" + 
                            String.valueOf(i));
                    e.setAttribute("Review-Result-" + String.valueOf(i), 
                            str.toString());
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getReviewResAuthor()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Review-Result-Author-" + 
                            String.valueOf(i));
                    e.setAttribute("Review-Result-Author-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getReviewResDate()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Review-Result-Date-" + 
                            String.valueOf(i));
                    e.setAttribute("Review-Result-Date-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getReviewShortDescription()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Review-Short-Description-" + 
                            String.valueOf(i));
                    e.setAttribute("Review-Short-Description-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getReviewDescription()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Review-Description-" + 
                            String.valueOf(i));
                    e.setAttribute("Review-Description-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getReviewDescAuthor()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Review-Description-Author-" + 
                            String.valueOf(i));
                    e.setAttribute("Review-Description-Author-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getReviewDescDate()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Review-Description-Date-" + 
                            String.valueOf(i));
                    e.setAttribute("Review-Description-Date-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }


            i = -1;
            for (List<State> list : entry.getTestState()) {
                i++;
                for (State str : list) {
                    Element e = new Element("Test-Result-" + 
                            String.valueOf(i));
                    e.setAttribute("Test-Result-" + String.valueOf(i), 
                            str.toString());
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getTestResAuthor()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Test-Result-Author-" + 
                            String.valueOf(i));
                    e.setAttribute("Test-Result-Author-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getTestResDate()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Test-Result-Date-" + 
                            String.valueOf(i));
                    e.setAttribute("Test-Result-Date-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getTestShortDescription()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Test-Short-Description-" + 
                            String.valueOf(i));
                    e.setAttribute("Test-Short-Description-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getTestDescription()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Test-Description-" + 
                            String.valueOf(i));
                    e.setAttribute("Test-Description-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getTestDescAuthor()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Test-Description-Author-" + 
                            String.valueOf(i));
                    e.setAttribute("Test-Description-Author-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }
            i = -1;
            for (List<String> list : entry.getTestDescDate()) {
                i++;
                for (String str : list) {
                    Element e = new Element("Test-Description-Date-" + 
                            String.valueOf(i));
                    e.setAttribute("Test-Description-Date-" + 
                            String.valueOf(i), str);
                    changed.addContent(e);
                }
            }

            doc.getRootElement().addContent(changed);
        }

        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());
        
        try {
            FileOutputStream fos;
            fos = new FileOutputStream(file);           // open output file

            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            try (FileLock lock = fos.getChannel().lock()) {
                if (writeLock) {
                    createLockFile();
                    // do not clear locks!
                    // clearOldLocks();
                }
                xmlOutput.output(doc, fos);
            }
        } catch (Exception ex) {
            LOGGER.info("File lock not acquired!");
        }
        // File lock automatically released (by try-resource mecahnism)
    }
    
    /**
     * This method reads the xml "file".
     * 
     * @param file          file to read
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @throws JDOMException
     */
    public XmlData readXml(File file, SetProgress setProgress)
            throws FileNotFoundException, IOException, JDOMException {
        
        this.file = file;
        XmlData xmlData = new XmlData();
        configEntry = new ConfigEntry();
        changedFileList = new ArrayList<>();
        repoLog = new ArrayList<>();
        
        // SetProgress setProgress = new SetProgress(progressController);
        
        createLockFile();
        if (!isReadOnly(file)) {
            clearOldLocks(false);
            exclusiveLock();
        } 

        // The function lockFiles.isEmpty() does not function because
        // of own lock!!!
        // if (!lockFiles.isEmpty()) {
        if (!isReadOnly(file)) {
            if (lockFiles.size() > 1) {
                releaseExclusiveLock();
            }
        }
        try {
            FileInputStream fis;
            fis = new FileInputStream(file);           // open output file
            
            // Use the file channel to create a lock on the file.
            // This method blocks until it can retrieve the lock.
            //
            // Note:
            // tryLock() is a shorthand for tryLock(0L, Long.MAX_VALUE, false), 
            // i.e. it requests an exclusive write-lock. Thus, tryLock()
            // throws an exception on an input channel!
            //
            try (FileLock lock = fis.getChannel().tryLock(0L, Long.MAX_VALUE, true)){
                SAXBuilder saxBuilder = new SAXBuilder();
                Document document = saxBuilder.build(fis);
                Element rootElement = document.getRootElement();

                int count = 0;
                List<Element> list = rootElement.getChildren();
                for (Element element : list) {
                    count++;

                    switch (element.getName()) {
                        case "Configuration":
                            try {
                                // Parse the actual configuration!
                                String name;
                                List<Element> children = element.getChildren();
                                for (Element child : children) {
                                    name = child.getName();
                                    switch (name) {
                                        case "Svn":
                                            String svn = child.getAttributeValue(name);
                                            configEntry.putSvn(svn);
                                            break;
                                        case "Version":
                                            String version = child.getAttributeValue(name);
                                            configEntry.putVersion(version);
                                            break;
                                        case "Caption":
                                            String name1;
                                            List<Element> children1 = child.getChildren();
                                            for (Element child1 : children1) {
                                                name1 = child1.getName();
                                                switch (name1) {
                                                    case "Caption":
                                                        String caption = child1.
                                                                getAttributeValue(name1);
                                                        configEntry.putCaption(caption);
                                                        break;
                                                    case "Author":
                                                        String author = child1.
                                                                getAttributeValue(name1);
                                                        configEntry.putCaptionAuthor(author);
                                                        break;
                                                    case "Date":
                                                        String date = child1.
                                                                getAttributeValue(name1);
                                                        configEntry.putCaptionDate(date);
                                                        break;
                                                    // do nothing
                                                    default:
                                                        break;
                                                }
                                            }
                                            break;
                                        // do nothing
                                        default:
                                            break;
                                    }
                                }

                            } catch (Exception ex) {
                                // The configuration is not found. 
                                // Proceed!
                            }
                            break;
                        case "Log":
                            try {
                                // Parse the actual log!
                                String name;
                                List<Element> children = element.getChildren();
                                for (Element child : children) {
                                    name = child.getName();
                                    switch (name) {
                                        case "Description":
                                            LogEntry logEntry = new LogEntry();
                                            List<Attribute> attributes = child.getAttributes();
                                            boolean valid = true;
                                            for (Attribute attr : attributes) {
                                                String name2 = attr.getName();
                                                switch (name2) {
                                                    case "Description":
                                                        logEntry.setDescription(attr.getValue());
                                                        break;
                                                    case "Message1":
                                                        logEntry.setMessage1(attr.getValue());
                                                        break;
                                                    case "Message2":
                                                        logEntry.setMessage2(attr.getValue());
                                                        break;
                                                    case "Message3":
                                                        logEntry.setMessage3(attr.getValue());
                                                        break;
                                                    case "Message4":
                                                        logEntry.setMessage4(attr.getValue());
                                                        break;
                                                    default:
                                                        // An invalid XML tag is
                                                        // scanned.
                                                        // Do not save it!
                                                        valid = false;
                                                        break;
                                                }
                                            }
                                            if (valid) {
                                                repoLog.add(logEntry);
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            } catch (Exception ex) {
                                // Unknown element: Skip!
                            }
                            break;
                        case "Changed":
                            TableEntry tableEntry = new TableEntry();
                            tableEntry.putFile(element.getAttribute("Filename").
                                    getValue());

                            String name;
                            int i = -1;
                            List<Element> children = element.getChildren();
                            for (Element child : children) {
                                i++;
                                name = child.getName();

                                // Note:
                                // Do not exchange the sequences of the following
                                // if statement!
                                //
                                // Reason: 
                                // For example, the two comparisons 
                                //    - name.contains("Review-Result-Author-")
                                //    - name.contains("Review-Result-")
                                // must be called in the correct manner, because
                                // string 2 ("Review-Result-") is a substring of
                                // string 1 ("Review-Result-Author-").
                                // If it is called in the wrong sequence, an 
                                // exception would be thrown.
                                //
                                if (name.contains("Href-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(5, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putHref(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in "
                                                + "scanning the Href XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("HrefText-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(9, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putHrefText(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the HrefText XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("Line-Number-")) {
                                    try {
                                        lineNumberAvailable = true;
                                        CharSequence subSequence = name.subSequence(12, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putLineNumber(pos, child.
                                                getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Line-Number XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("Review-Result-Author-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(21, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putReviewResAuthor(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Review-Result-Author XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("Review-Result-Date-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(19, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putReviewResDate(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Review-Result-Date XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("Review-Result-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(14, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        String stateString = child.getAttributeValue(name);
                                        stateString = stateString.toUpperCase();
                                        State state = State.getEnum(stateString);
                                        tableEntry.putReviewState(pos, state);
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Review-Result XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("Review-Short-Description-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(25, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putReviewShortDescription(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Review-Short-Description "
                                                + "XML object! Continuing ... ");
                                    }
                                } else if (name.contains("Review-Description-Author-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(26, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putReviewDescAuthor(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Review-Description-Author XML "
                                                + "object! Continuing ... ");
                                    }
                                } else if (name.contains("Review-Description-Date-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(24, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putReviewDescDate(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Review-Description-Date "
                                                + "XML object! Continuing ... ");
                                    }
                                } else if (name.contains("Review-Description-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(19, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putReviewDescription(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Review-Description XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("Test-Result-Author-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(19, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putTestResAuthor(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Test-Result-Author XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("Test-Result-Date-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(17, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putTestResDate(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Test-Result-Date XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("Test-Result-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(12, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putTestState(pos, 
                                                State.getEnum(child.
                                                        getAttributeValue(name)));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Test-Result XML object! "
                                                + "Continuing ... ");
                                    }
                                } else if (name.contains("Test-Short-Description-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(23, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putTestShortDescription(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Test-Short-Description XML "
                                                + "object! Continuing ... ");
                                    }
                                } else if (name.contains("Test-Description-Author-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(24, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putTestDescAuthor(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Test-Description-Author XML "
                                                + "object! Continuing ... ");
                                    }
                                } else if (name.contains("Test-Description-Date-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(22, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putTestDescDate(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Test-Description-Date XML "
                                                + "object! Continuing ... ");
                                    }
                                } else if (name.contains("Test-Description-")) {
                                    try {
                                        CharSequence subSequence = name.subSequence(17, 
                                                name.length());
                                        String str = (String) subSequence;
                                        int pos = Integer.valueOf(str);
                                        tableEntry.putTestDescription(pos, 
                                                child.getAttributeValue(name));
                                    } catch (Exception ex) {
                                        // An exception occured. Log the exception
                                        // and continue.
                                        LOGGER.info("An exception occured in scanning "
                                                + "the Test-Description XML object! "
                                                + "Continuing ... ");
                                    }
                                }
                                switch (name) {
                                    case "Diff":
                                        tableEntry.putDiff(child.
                                                getAttributeValue(name));
                                        break;
                                    case "File-Count":
                                        tableEntry.putFileCount(child.
                                                getAttributeValue(name));
                                        break;
                                    case "SVN-Author":
                                        tableEntry.putSvnAuthor(child.
                                                getAttributeValue(name));
                                        break;
                                    case "SVN-Date":
                                        tableEntry.putSvnDate(child.
                                                getAttributeValue(name));
                                        break;
                                    case "SVN-Description":
                                        tableEntry.putSvnDesc(child.
                                                getAttributeValue(name));
                                        break;
                                    case "SVN-Version":
                                        tableEntry.putSvnVersion(child.
                                                getAttributeValue(name));
                                        break;
                                    default:
                                        break;
                                }
                            }
                            changedFileList.add(tableEntry);
                            break;
                        default:
                            // Unknown element: Skip!
                            break;
                    }
                    if (progressController.getAbortState()) {
                        break;
                    }
                    // update progress bar
                    setProgress.setProgressFirstPass(count,list.size());
                }
                cleanUpXmlLockFiles();
            }
        } catch (IOException | JDOMException ex) {
            String name = ex.getClass().getName();
            if (name.equals("java.nio.channels.ClosedChannelException")) {
                // it's ok!
            } else {
                LOGGER.info("File lock not acquired!");
            }
        } 
        // File lock automatically released (by try-resource mecahnism)
        if (!isReadOnly(file)) {
            releaseExclusiveLock();
        }
        
        xmlData.putChangedFileList(changedFileList);
        xmlData.putConfigEntry(configEntry);
        xmlData.putRepoLog(repoLog);
        return xmlData;
    }

    /**
     * This method reads the locked xml files of the form
     *          1-5_stefan-pce00017-30221.lock.xml
     *          ...
     * 
     * @param file                  xml file to read
     * @param fileNumber            file number postion in table entry (here: "1")
     * @param xmlLockFileNumber     locked xml file number (here: "5")
     * @param loginName             login name (here: "stefan-pce00017-30221")
     * return int                   changed line number position in table entry
     * @throws FileNotFoundException
     * @throws IOException
     * @throws JDOMException
     */
    private int readXml(File file, int fileNumber, int xmlLockFileNumber, 
            String loginName)
            throws FileNotFoundException, IOException, JDOMException {
        int pos = 0;
        
        // The xml files under the directory .lock are read asynchronouly.
        // Therefore, it can happen, that the read mechanism fails. In that 
        // case, a retry mechanism is implemented. 
        int retryCount = 0;
        while (retryCount < MAX_RETRY_COUNT) {
            retryCount++;
            try {
                FileInputStream fis;
                fis = new FileInputStream(file);           // open output file

                // Use the file channel to create a lock on the file.
                // This method blocks until it can retrieve the lock.
                //
                // Note:
                // tryLock() is a shorthand for tryLock(0L, Long.MAX_VALUE, false), 
                // i.e. it requests an exclusive write-lock. Thus, tryLock()
                // throws an exception on an input channel!
                //
                try (FileLock lock = fis.getChannel().tryLock(0L, Long.MAX_VALUE, true)){
                    SAXBuilder saxBuilder = new SAXBuilder();
                    Document document = saxBuilder.build(fis);
                    Element rootElement = document.getRootElement();

                    int count = 0;
                    List<Element> list = rootElement.getChildren();
                    for (Element element : list) {
                        count++;

                        TableEntry tableEntry = changedFileList.get(fileNumber);
                        tableEntry.putFile(element.getAttribute("Filename").
                                getValue());
                        tableEntry.putXmlLockFileNumber(xmlLockFileNumber);
                        tableEntry.putLoginName(loginName);

                        String name;
                        int i = -1;
                        List<Element> children = element.getChildren();
                        for (Element child : children) {
                            i++;
                            name = child.getName();

                            // Note:
                            // Do not exchange the sequences of the following
                            // if statement!
                            //
                            // Reason: 
                            // For example, the two comparisons 
                            //    - name.contains("Review-Result-Author-")
                            //    - name.contains("Review-Result-")
                            // must be called in the correct manner, because
                            // string 2 ("Review-Result-") is a substring of
                            // string 1 ("Review-Result-Author-").
                            // If it is called in the wrong sequence, an 
                            // exception would be thrown.
                            //
                            if (name.contains("Href-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(5, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putHref(pos, child.
                                            getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Href XML object! "
                                            + "Continuing ... ");
                                }
                            } else if (name.contains("HrefText-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(9, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putHrefText(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the HrefText XML "
                                            + "object! Continuing ... ");
                                }
                            } else if (name.contains("Line-Number-")) {
                                try {
                                    lineNumberAvailable = true;
                                    CharSequence subSequence = name.subSequence(12, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putLineNumber(pos, child.
                                            getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Line-Number XML "
                                            + "object! Continuing ... ");
                                }
                            } else if (name.contains("Review-Result-Author-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(21, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putReviewResAuthor(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Review-Result-"
                                            + "Author XML object! "
                                            + "Continuing ... ");
                                }
                            } else if (name.contains("Review-Result-Date-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(19, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putReviewResDate(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Review-Result-Date "
                                            + "XML object! Continuing ... ");
                                }
                            } else if (name.contains("Review-Result-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(14, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putReviewState(pos, 
                                            State.getEnum(child.
                                                    getAttributeValue(name)));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Review-Result XML "
                                            + "object! Continuing ... ");
                                }
                            } else if (name.contains("Review-Short-Description-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(25, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putReviewShortDescription(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Review-Short-"
                                            + "Description XML object! "
                                            + "Continuing ... ");
                                }
                            } else if (name.contains("Review-Description-Author-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(26, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putReviewDescAuthor(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Review-Description-"
                                            + "Author XML object! "
                                            + "Continuing ... ");
                                }
                            } else if (name.contains("Review-Description-Date-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(24, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putReviewDescDate(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Review-Description-"
                                            + "Date XML object! "
                                            + "Continuing ... ");
                                }
                            } else if (name.contains("Review-Description-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(19, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putReviewDescription(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Review-Description "
                                            + "XML object! Continuing ... ");
                                }
                            } else if (name.contains("Test-Result-Author-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(19, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putTestResAuthor(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Test-Result-"
                                            + "Author XML object! "
                                            + "Continuing ... ");
                                }
                            } else if (name.contains("Test-Result-Date-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(17, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putTestResDate(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Test-Result-Date "
                                            + "XML object! Continuing ... ");
                                }
                            } else if (name.contains("Test-Result-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(12, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putTestState(pos, 
                                            State.getEnum(child.
                                                    getAttributeValue(name)));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Test-Result XML "
                                            + "object! Continuing ... ");
                                }
                            } else if (name.contains("Test-Short-Description-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(23, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putTestShortDescription(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Test-Short-"
                                            + "Description XML object! "
                                            + "Continuing ... ");
                                }
                            } else if (name.contains("Test-Description-Author-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(24, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putTestDescAuthor(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Test-Description-"
                                            + "Author XML object! "
                                            + "Continuing ... ");
                                }
                            } else if (name.contains("Test-Description-Date-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(22, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putTestDescDate(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Test-Description-"
                                            + "Date XML object! "
                                            + "Continuing ... ");
                                }
                            } else if (name.contains("Test-Description-")) {
                                try {
                                    CharSequence subSequence = name.subSequence(17, 
                                            name.length());
                                    String str = (String) subSequence;
                                    pos = Integer.valueOf(str);
                                    tableEntry.putTestDescription(pos, 
                                            child.getAttributeValue(name));
                                } catch (Exception ex) {
                                    // An exception occured. Log the exception
                                    // and continue.
                                    LOGGER.info("An exception occured in "
                                            + "scanning the Test-Description "
                                            + "XML object! Continuing ... ");
                                }
                            }
                            switch (name) {
                                case "Diff":
                                    tableEntry.putDiff(child.
                                            getAttributeValue(name));
                                    break;
                                case "File-Count":
                                    tableEntry.putFileCount(child.
                                            getAttributeValue(name));
                                    break;
                                case "SVN-Author":
                                    tableEntry.putSvnAuthor(child.
                                            getAttributeValue(name));
                                    break;
                                case "SVN-Date":
                                    tableEntry.putSvnDate(child.
                                            getAttributeValue(name));
                                    break;
                                case "SVN-Description":
                                    tableEntry.putSvnDesc(child.
                                            getAttributeValue(name));
                                    break;
                                case "SVN-Version":
                                    tableEntry.putSvnVersion(child.
                                            getAttributeValue(name));
                                    break;
                            // do nothing
                                default:
                                    break;
                            }
                        }
                    }
                }
            } catch (IOException | JDOMException ex) {
                String name = ex.getClass().getName();
                if (name.equals("java.nio.channels.ClosedChannelException")) {
                    // it's ok!
                    retryCount = MAX_RETRY_COUNT;
                } else {
                    LOGGER.info("File lock not acquired! Sleeping for 1 second! ...");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex1) {
                        LOGGER.info("An error occured during Thread.sleep(1000)!");
                    }
                }
            } 
        }
        // File lock automatically released (by try-resource mecahnism)
        return pos;
    }
    
    /**
     * This method reads the locked xml files of the form
     *          0-5_Caption_stefan-pce00017-30221.lock.xml
     *          ...
     * 
     * @param file                  xml file to read
     * @param fileNumber            file number postion in table entry (here: "1")
     * @param xmlLockFileNumber     locked xml file number (here: "5")
     * @param loginName             login name (here: "stefan-pce00017-30221")
     * return int                   0
     * @throws FileNotFoundException
     * @throws IOException
     * @throws JDOMException
     */
    private int readCaptionXml(File file, int xmlLockFileNumber, String loginName)
            throws FileNotFoundException, IOException, JDOMException {

        // The xml files under the directory .lock are read asynchronouly.
        // Therefore, it can happen, that the read mechanism fails. In that 
        // case, a retry mechanism is implemented. 
        int retryCount = 0;
        while (retryCount < MAX_RETRY_COUNT) {
            retryCount++;
            try {
                FileInputStream fis;
                fis = new FileInputStream(file);           // open output file

                // Use the file channel to create a lock on the file.
                // This method blocks until it can retrieve the lock.
                //
                // Note:
                // tryLock() is a shorthand for tryLock(0L, Long.MAX_VALUE, false), 
                // i.e. it requests an exclusive write-lock. Thus, tryLock()
                // throws an exception on an input channel!
                //
                try (FileLock lock = fis.getChannel().tryLock(0L, Long.MAX_VALUE, true)){
                    SAXBuilder saxBuilder = new SAXBuilder();
                    Document document = saxBuilder.build(fis);
                    Element rootElement = document.getRootElement();

                    List<Element> list = rootElement.getChildren();
                    for (Element element : list) {
                        try {
                            configEntry.putLoginName(loginName);
                            configEntry.putXmlLockFileNumber(xmlLockFileNumber);
                            // Parse the actual configuration!
                            String name;
                            List<Element> children = element.getChildren();
                            for (Element child : children) {
                                name = child.getName();
                                switch (name) {
                                    case "Svn":
                                        String svn = child.getAttributeValue(name);
                                        configEntry.putSvn(svn);
                                        break;
                                    case "Version":
                                        String version = child.getAttributeValue(name);
                                        configEntry.putVersion(version);
                                        break;
                                    case "Caption":
                                        String name1;
                                        List<Element> children1 = child.getChildren();
                                        for (Element child1 : children1) {
                                            name1 = child1.getName();
                                            switch (name1) {
                                                case "Caption":
                                                    String caption = child1.
                                                            getAttributeValue(name1);
                                                    configEntry.putCaption(caption);
                                                    break;
                                                case "Author":
                                                    String author = child1.
                                                            getAttributeValue(name1);
                                                    configEntry.putCaptionAuthor(author);
                                                    break;
                                                case "Date":
                                                    String date = child1.
                                                            getAttributeValue(name1);
                                                    configEntry.putCaptionDate(date);
                                                    break;
                                                // do nothing
                                                default:
                                                    break;
                                            }
                                        }
                                        break;
                                    // do nothing
                                    default:
                                        break;
                                }
                            }
                        } catch (Exception ex) {
                            // The configuration is not found. 
                            // Proceed!
                        }
                    }
                }
            } catch (IOException | JDOMException ex) {
                String name = ex.getClass().getName();
                if (name.equals("java.nio.channels.ClosedChannelException")) {
                    // it's ok!
                    retryCount = MAX_RETRY_COUNT;
                } else {
                    LOGGER.info("File lock not acquired! Sleeping for 1 second! ...");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex1) {
                        LOGGER.info("An error occured during Thread.sleep(1000)!");
                    }
                }
            } 
        }
        // File lock automatically released (by try-resource mecahnism)
        return 0;
                
    }
                
                
    /**
     * This method returns the lock directoy.
     * 
     * @return      lock directory
     */
    public File getLockDir() {
        return lockDir;
    }

    /**
     * This method returns the availability of line numbering schema.
     * 
     * @return      true  -> line numbers are available
     *              false -> line numers are not available
     */
    public boolean getLineNumberAvailable() {
        return lineNumberAvailable;
    }

    /**
     * This method adds the actual login name to the member variable
     * lockNameList.
     * 
     * @param name  active login name
     */
    public void addLogin(String name) {
        if (!name.equals("")) {     // empty names are invalid!
            boolean found = false;
            for (String str : lockNameList) {
                if (str.equals(name)) {
                    found = true;
                }
            }
            if (!found) {
                lockNameList.add(name);
            }
        } 
    }

    /**
     * This method deletes the given login name from lockNameList.
     * 
     * @param name  login name to delete
     */
    public void deleteLogin(String name) {
        for (String str : lockNameList) {
            if (str.equals(name)) {
                if (!lockNameList.remove(name)) {
                    LOGGER.info("Error on removing login name: " + name);
                }
                break;
            }
        }
    }
    
    /**
     * This methode computes, if an active login with the given name is 
     * available.
     * 
     * @param name      login name
     * @return          true  -> login valid
     *                  false -> no login available
     */
    public boolean isLoginValid(String name) {
        boolean found = false;
        for (String str : lockNameList) {
            if (str.equals(name)) {
                found = true;
            }
        }
        return found;
    }
    
    /**
     * This method extracts the active login name from the lock file.
     * 
     * The login name is of the followin form:
     *      ".user-computerName-pid"
     * 
     * @param file          name of lock file
     * @return loginName    name of active login
     */
    public String extractLoginName(File file) {
        String fileName = file.getName();
        String loginName = "";
        String regexp = "^\\.(.*)\\.lock";
        Pattern pattern = Pattern.compile(regexp, 
                Pattern.CASE_INSENSITIVE);
        Matcher matcher;
        matcher = pattern.matcher(fileName);
        if (matcher.find()) {
            loginName = matcher.group(1);
        } else {
            // Do nothing. No login name available!
            
            String regexp1 = "^(\\d*)-(\\d*)_(.*)_(.*)\\.lock.xml";
            Pattern pattern1 = Pattern.compile(regexp1, 
                    Pattern.CASE_INSENSITIVE);
            Matcher matcher1;
            matcher1 = pattern1.matcher(fileName);
            if (matcher1.find()) {
                loginName = matcher1.group(4);
            }
        }
        return loginName;
    }
    
    /**
     * This method puts an entry into the lockedRowsList.
     * 
     * @param name          locked name
     * @param row           locked row number
     * @param fileNumber    associated file
     */
    public void putLockedRowsList (String name, int row, int fileNumber) {
        lockedRowsList.add(new LockedRowsList(row, name, fileNumber));
    }
    
    /**
     * This method deletes the element, that matches "name" and "pos".
     * 
     * @param name          locked name
     * @param row           locked row number
     * @param fileNumber    associated file
     */
    public void deleteLockedRowsList (String name, int row, int fileNumber) {
        LockedRowsList e = new LockedRowsList(row, name, fileNumber);
        for (LockedRowsList entry : lockedRowsList) {
            if (entry.equals(e)) {
                lockedRowsList.remove(e);
            }
        }
    }
    
    /**
     * This method returns a list of all locked rows.
     * 
     * @param name      locked name
     * @return          list of all locked rows
     */
    public List<Integer> getLockedRows (String name) {
        List<Integer> rowList = new ArrayList<>();
        for (LockedRowsList entry : lockedRowsList) {
            if (entry.login.equals(name)) {
                rowList.add(entry.position);
            }
        }
        return rowList;
    }
            
    /**
     * This method returns a list of all locked file numbers.
     * 
     * @param name      locked name
     * @return          list of all locked file numbers
     */
    public List<Integer> getLockedFiles (String name) {
        List<Integer> rowList = new ArrayList<>();
        for (LockedRowsList entry : lockedRowsList) {
            if (entry.login.equals(name)) {
                rowList.add(entry.fileNumber);
            }
        }
        return rowList;
    }
    
    public boolean isReadOnly(File file) {
        boolean readOnly = false;
        if (file.canWrite()) {
            // do nothing
        } else if (file.canRead()) {
            readOnly = true;
        }
        readOnlyFS = readOnly || readOnlyFS;
        return readOnlyFS;
    }
            
    public boolean isReadOnly() {
        return readOnlyFS;
    }

    private class LockedRowsList {
        private int position;
        private String login;
        private int fileNumber;
        
        public LockedRowsList () {
            position = -1;
            login = "";
            fileNumber = 0;
        }
        
        public LockedRowsList (int position, String login, int fileNumber) {
            this.position = position;
            this.login = login;
            this.fileNumber = fileNumber;
        }

        public void putPosition (int pos) {
            position = pos;
        }
        
        public int getPosition () {
            return position;
        }
        
        public void putLogin (String login) {
            this.login = login;
        }
        
        public String getLogin () {
            return login;
        }

        public void putFileNumber (int fileNumber) {
            this.fileNumber = fileNumber;
        }
        
        public int getFileNumber () {
            return fileNumber;
        }
    }
}

