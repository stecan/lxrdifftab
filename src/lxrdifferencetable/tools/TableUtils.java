/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.tools;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.data.TableEntry.State;

/**
 *
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class TableUtils {

    static final Logger logger = Logger.getLogger
        (LxrDifferenceTableController.class.getName());

    public TableUtils() {
        this.numberFormatter = NumberFormat.getNumberInstance();
    }

    private final NumberFormat numberFormatter;


    /**
     * Install the keyboard handler:
     *   + CTRL + C = copy to clipboard
     *   + CTRL + V = paste from clipboard
     * @param table
     */
    public void installCopyPasteHandler(TableView<?> table) {

        // install copy/paste keyboard handler
        table.setOnKeyPressed(new TableKeyEventHandler());

    }

    /**
     * Copy/Paste keyboard event handler.
     * The handler uses the keyEvent's source for the clipboard data. 
     * The source must be of type TableView.
     */
    public  class TableKeyEventHandler implements EventHandler<KeyEvent> {

        KeyCodeCombination copyKeyCodeCompination = 
                new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
        KeyCodeCombination pasteKeyCodeCompination = 
                new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_ANY);

        @Override
        public void handle(final KeyEvent keyEvent) {
            if (copyKeyCodeCompination.match(keyEvent)) {
                if( keyEvent.getSource() instanceof TableView) {
                    // copy to clipboard
                    copySelectionToClipboard( (TableView<?>) keyEvent.getSource());
                    // event is handled, consume it
                    keyEvent.consume();
                }
            } 
            else if (pasteKeyCodeCompination.match(keyEvent)) {
                if( keyEvent.getSource() instanceof TableView) {
                    // copy to clipboard
                    pasteFromClipboard( (TableView<?>) keyEvent.getSource());
                    // event is handled, consume it
                    keyEvent.consume();
                }
            } 
        }
    }

    /**
     * Get table selection and copy it to the clipboard.
     * @param table
     */
    public void copySelectionToClipboard(TableView<?> table) {
        StringBuilder clipboardString = new StringBuilder();
        ObservableList<TablePosition> positionList = 
                table.getSelectionModel().getSelectedCells();

        int prevRow = -1;
        for (TablePosition position : positionList) {
            int row = position.getRow();
            int col = position.getColumn();

            // determine whether we advance in a row (tab) or a column
            // (newline).
            if (prevRow == row) {
                clipboardString.append('\t');
            } else if (prevRow != -1) {
                clipboardString.append('\n');
            }

            if (col == -1) {
                col = 0;
            }
            // create string from cell
            String text;
            Object observableValue;
            observableValue = (Object) table.getColumns().get(col)
                    .getCellObservableValue(row);

            // null-check: provide empty string for nulls
            if (observableValue == null) {
                text = "";
            }
            else if( observableValue instanceof DoubleProperty) {
                text = numberFormatter.format( ((DoubleProperty) observableValue).get());
            }
            else if( observableValue instanceof IntegerProperty) { 
                text = numberFormatter.format( ((IntegerProperty) observableValue).get());
            }			    	
            else if( observableValue instanceof StringProperty) { 
                text = ((StringProperty) observableValue).get();
            }
            else {
                Class<?> aClass = table.getColumns().get(col).getCellData(row).getClass();
                if (aClass.equals( (new ComboBox()).getClass())) {
                    // get text from state-combobox
                    ComboBox<State> c = (ComboBox<State>) (table.getColumns().get(col).getCellData(row));
                    text = c.getItems().get(0).toString();
                } else {
                    // normal text
                    text = table.getColumns().get(col).getCellData(row).toString();
                }
            }
            if (text.contains("\n")) {
                // surround text by double quotes
                // text = text.replace("\n", "</br>");
                text = "\"" + text + "\"";
            }
            // add new item to clipboard
            clipboardString.append(text);
            // remember previous
            prevRow = row;
        }
        // create clipboard content
        final ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(clipboardString.toString());
        // set clipboard content
        Clipboard.getSystemClipboard().setContent(clipboardContent);
    }

    public void pasteFromClipboard( TableView<?> table) {
        // abort if there's not cell selected to start with
        if( table.getSelectionModel().getSelectedCells().isEmpty()) {
            return;
        }
        // get the cell position to start with
        TablePosition pasteCellPosition = table.getSelectionModel()
                .getSelectedCells().get(0);
        String pasteString = Clipboard.getSystemClipboard().getString();
        int rowClipboard = -1;
        StringTokenizer rowTokenizer = 
                new StringTokenizer( pasteString, "\n");
        while( rowTokenizer.hasMoreTokens()) {
            rowClipboard++;
            String rowString = rowTokenizer.nextToken();
            StringTokenizer columnTokenizer = 
                    new StringTokenizer( rowString, "\t");
            int colClipboard = -1;
            while( columnTokenizer.hasMoreTokens()) {
                colClipboard++;
                // get next cell data from clipboard
                String clipboardCellContent = columnTokenizer.nextToken();
                // calculate the position in the table cell
                int rowTable = pasteCellPosition.getRow() + rowClipboard;
                int colTable = pasteCellPosition.getColumn() + colClipboard;

                // skip if we reached the end of the table
                if( rowTable >= table.getItems().size()) {
                    continue;
                }
                if( colTable >= table.getColumns().size()) {
                    continue;
                }

                // get cell
                TableColumn tableColumn = table.getColumns().get(colTable);
                ObservableValue observableValue = tableColumn
                        .getCellObservableValue(rowTable);

                // TODO: handle boolean, etc
                if( observableValue instanceof DoubleProperty) { 
                    try {
                        double value = numberFormatter
                                .parse(clipboardCellContent).doubleValue();
                        ((DoubleProperty) observableValue).set(value);
                    } catch (ParseException e) {
                        logger.info("TableUtils: " + "TODO: Should be removed!");
                    }
                }
                else if( observableValue instanceof IntegerProperty) { 
                    try {
                        int value = NumberFormat.getInstance()
                                .parse(clipboardCellContent).intValue();
                        ((IntegerProperty) observableValue).set(value);
                    } catch (ParseException e) {
                        logger.info("TableUtils: " + "TODO: Should be removed!");
                    }
                }			    	
                else if( observableValue instanceof StringProperty) { 
                    ((StringProperty) observableValue).set(clipboardCellContent);
                } else {
                    logger.info("TableUtils: " + 
                            "Unsupported observable value: " + 
                            observableValue);
                }
            }
        }
    }
}
