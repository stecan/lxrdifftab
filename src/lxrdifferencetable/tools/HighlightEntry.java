/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

/**
 * HighlightEntry is a class to structure different information for coloring
 * changes in a partially modified line.
 * 
 * @author Stefan Canali
 */
public class HighlightEntry {
    private int line;
    private int begin;
    private int end;
    
    /**
     * Constructor
     * 
     * @param line      line number that is changed and should be coloured
     * @param begin     begin character in line that should be colored
     * @param end       end character in line that should be colored
     */
    public HighlightEntry(int line, int begin, int end) {
        this.line = line;
        this. begin = begin;
        this.end = end;
    }
    
    /**
     * Method to set the changed line number.
     * 
     * @param line
     */
    public void putLine (int line) {
        this.line = line;
    }
    
    /**
     * Method to set the beginning of the change.
     * 
     * @param begin
     */
    public void putBegin (int begin) {
        this.begin = begin;
    }
    
    /**
     * Method to set the end of the change.
     * 
     * @param end
     */
    public void putEnd (int end) {
        this.end = end;
    }
    
    /**
     * Method to get the changed line number.
     * 
     * @return line     changed line number 
     */
    public int getLine() {
        return this.line;
    }
    
    /**
     * Method to get the beginning of the change.
     * 
     * @return begin    beginning of change
     */
    public int getBegin() {
        return this.begin;
    }
    
    /**
     * Method to get the end of the change.
     * 
     * @return end      end of change
     */
    public int getEnd() {
        return this.end;
    }
}
