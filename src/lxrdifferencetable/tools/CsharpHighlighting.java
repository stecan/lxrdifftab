/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

/**
 * CsharpHighlighting is a class for highlighting C# source files.
 * 
 * @author Stefan Canali
 */
public class CsharpHighlighting {
    
    private static final String[] KEYWORDS = new String[] {
            "abstract", "as", "base", "bool", "break", 
            "byte", "case", "catch", "char", "checked",
            "class", "const", "continue", "decimal", "default", 
            "delegate", "do", "double", "else",
            "enum", "event", "explicit", "extern", 
            "false", "finally", "fixed", "float",
            "for", "foreach", "goto", "if", 
            "implicit", "in", "int", "interface", "internal", 
            "is", "lock", "long", 
            "namespace", "new", "null", "object", "operator", 
            "out", "override", "params", "private", "protected",
            "public", "readonly", "ref", "return", 
            "sbyte", "sealed", "short", "sizeof", 
            "stackalloc", "static", "string", "struct",
            "switch", "this", "throw", "true", "try", 
            "typedef", "uint", "ulong", "unchecked", "unsafe", 
            "ushort", "using", "virtual", "void", 
            "volatile", "while"
    };

    private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
    private static final String PAREN_PATTERN = "\\(|\\)";
    private static final String BRACE_PATTERN = "\\{|\\}";
    private static final String BRACKET_PATTERN = "\\[|\\]";
    private static final String SEMICOLON_PATTERN = "\\;";
    private static final String STRING_PATTERN = "\".*?\"";
    private static final String COMMENT_PATTERN = "//[^\n]*" + "|" + "/\\*.*?\\*/";

    public static StyleSpans<Collection<String>> computeHighlighting(String text, 
            List<String> tagnames) {
        
        String pattern;
        if (tagnames == null) {
            pattern = "(?<KEYWORD>" + KEYWORD_PATTERN + ")"
                    + "|(?<PAREN>" + PAREN_PATTERN + ")"
                    + "|(?<BRACE>" + BRACE_PATTERN + ")"
                    + "|(?<BRACKET>" + BRACKET_PATTERN + ")"
                    + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
                    + "|(?<STRING>" + STRING_PATTERN + ")"
                    + "|(?<COMMENT>" + COMMENT_PATTERN + ")";
        } else {
            String TAGS_PATTERN = "\\b(" + String.join("|", tagnames) + ")\\b";
            pattern = "(?<KEYWORD>" + KEYWORD_PATTERN + ")"
                    + "|(?<PAREN>" + PAREN_PATTERN + ")"
                    + "|(?<BRACE>" + BRACE_PATTERN + ")"
                    + "|(?<BRACKET>" + BRACKET_PATTERN + ")"
                    + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
                    + "|(?<STRING>" + STRING_PATTERN + ")"
                    + "|(?<COMMENT>" + COMMENT_PATTERN + ")"
                    + "|(?<TAG>" + TAGS_PATTERN + ")";
        }
        
        Pattern PATTERN = Pattern.compile(pattern);
        Matcher matcher = PATTERN.matcher(text);
        int lastKwEnd = 0;
        int start = 0;
        int end = 0;
        int len = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        while(matcher.find()) {
            String styleClass =
                    matcher.group("KEYWORD") != null ? "keyword" :
                    matcher.group("PAREN") != null ? "paren" :
                    matcher.group("BRACE") != null ? "brace" :
                    matcher.group("BRACKET") != null ? "bracket" :
                    matcher.group("SEMICOLON") != null ? "semicolon" :
                    matcher.group("STRING") != null ? "string" :
                    matcher.group("COMMENT") != null ? "comment" :
                    matcher.group("TAG") != null ? "tag" :
                    null; /* never happens */ assert styleClass != null;
            start = matcher.start();
            end = matcher.end();
            if (start >= lastKwEnd) {
                spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            } else {
                spansBuilder.add(Collections.emptyList(), 0);
            }
            if (end >= start) {
                spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            } else {
                spansBuilder.add(Collections.singleton(styleClass), 0);
            }
            lastKwEnd = matcher.end();
        }
        len = text.length();
        if (len >= lastKwEnd) {
            spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        } else {
            spansBuilder.add(Collections.emptyList(), 0);
        }
        StyleSpans<Collection<String>> created = spansBuilder.create();
        return created;
    }
}
