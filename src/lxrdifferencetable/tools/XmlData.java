/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.util.List;
import lxrdifferencetable.data.ConfigEntry;
import lxrdifferencetable.data.LogEntry;
import lxrdifferencetable.data.TableEntry;

/**
 *
 * @author Stefan Canali
 */
public class XmlData {
    private ConfigEntry configEntry;
    private List<TableEntry> changedFileList;
    private List<LogEntry> repoLog;
    
    public void putConfigEntry(ConfigEntry configEntry) {
        this.configEntry = configEntry;
    }
    
    public ConfigEntry getConfigEntry () {
        return this.configEntry;
    }
    
    public void putChangedFileList (List<TableEntry> changedFileList) {
        this.changedFileList = changedFileList;
    }
    
    public List<TableEntry> getChangedFileList () {
        return this.changedFileList;
    }
    
    public void putRepoLog(List<LogEntry> repoLog) {
        this.repoLog = repoLog;
    }
    
    public List<LogEntry> getRepoLog() {
        return this.repoLog;
    }
}
