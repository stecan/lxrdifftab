/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.util.prefs.Preferences;

/**
 * Saves properties.
 * 
 * On Linux, the properties are saved in 
 *            ~/.java/.userPrefs/LxrDifferenceTable/prefs.xml
 * On Windows, the properties are save in the registry under
 *            HKEY_USERS\S-1-5-21-1141300712-2603535634-2436614545-23903\
 *            Software\JavaSoft\Prefs\/Lxr/Difference/Table
 * 
 * The properties are saved by key. The keys are defined as public strings.
 * 
 * 
 * @author Stefan Canali
 */
public class Properties {

    public String HIDEDUBLICATEDFILES;
    public String HIDEDELETEDFILES;
    public String HIDENEWFILES;
    public String MAXIMIZEDSCREEN;  // e.g. "true"
    public String BASEDIR;
    public String COMPDIR;
    public String LXRLINK;
    public String LXRDIFF;          // e.g. "?v=4.9&~v=4.10&!v=4.9"
    public String LXRBASEVER;       // e.g. "4.9"
    public String LXRCOMPVER;       // e.g. "4.10"
    public String FORMATVERSION;    // e.g. "v="
    public String FORMATDIFFVAR;    // e.g. "~v="
    public String FORMATDIFFVAL;    // e.g. "!v="
    public String FORMATSEPARATOR;  // e.g. "&"
    public String FORMATLINENR;     // e.g. "#" or "#L"
    public String LANG;         // selected language
    public String COUNTRY;      // selected country
    public String LANGS;        // all available languages
    public String COUNTRIES;    // all available countries
    public String LANGSDE;      // all available languages in German
    public String LANGSEN;      // all available languages in English
    public String REPOLIST;     // e.g "svn,git"
    public String BLACKLIST;
    public String BLACKLISTAVAILABLE;
    public String WHITELIST;
    public String WHITELISTAVAILABLE;
    public String BLACKLISTFILES;
    public String LASTPROPERTIESFILE;
    public String LASTREPORTFILE;
    public String LASTXMLFILE;
    public String LASTFILE;
    public String LASTDIR;
    public String LASTDISTRIBDIR;
    public String EXTERNALTOOL_GIT;
    public String EXTERNALTOOL_CTAGS;
    public String ENCODING;
    public String REPORT;
    public String REPORT_TITLEPAGE;
    public String REPORT_SUMMARYPAGE;
    public String REVIEW;
    public String LOCAL_GENERATED_VIEW;
    public String SEARCH_FILE_COUNT;
    public String SEARCH_LINK;
    public String SEARCH_LINE_NR;
    public String SEARCH_SVN_VERSION;
    public String SEARCH_SVN_DESC;
    public String SEARCH_SVN_AUTHOR;
    public String SEARCH_SVN_DATE;
    public String SEARCH_REVIEW_STATE;
    public String SEARCH_REVIEW_DESC;
    public String SEARCH_TEST_STATE;
    public String SEARCH_TEST_DESC;
    public String SEARCH_HISTORY;
    public String SEARCH_REVIEW_AUTHOR;
    public String SEARCH_REVIEW_DATE;
    public String SEARCH_TEST_AUTHOR;
    public String SEARCH_TEST_DATE;
    public String SVNUSER;
    public String VERSION;
    
    private final Preferences root;
    private final Preferences props;
    private final String[] keys;
    
    /**
     * Constructor
     */
    public Properties () {
        keys = new String[] {
            HIDEDUBLICATEDFILES = "HideDublicatedFiles",
            HIDEDELETEDFILES    = "HideDeletedFiles",
            HIDENEWFILES        = "HideNewFiles",
            MAXIMIZEDSCREEN     = "MaximizedScreen", 
            BASEDIR             = "BaseDirectory", 
            COMPDIR             = "ComparableDirectory", 
            LXRLINK             = "LxrLink", 
            LXRDIFF             = "LxrDiff", 
            LXRBASEVER          = "LxrBaseVersion", 
            LXRCOMPVER          = "LxrComparableVersion", 
            FORMATVERSION       = "FormatVersion",
            FORMATDIFFVAL       = "FormatDiffVal",
            FORMATDIFFVAR       = "FormatDiffVar",
            FORMATSEPARATOR     = "FormatSeparator",
            FORMATLINENR        = "FormatLineNr",
            LANG                = "lang", 
            COUNTRY             = "country", 
            LANGS               = "langs", 
            COUNTRIES           = "countrys", 
            LANGSDE             = "langsDE", 
            LANGSEN             = "langsEN", 
            REPOLIST            = "Repolist", 
            BLACKLIST           = "Blacklist", 
            BLACKLISTAVAILABLE  = "BlacklistAvailable", 
            WHITELIST           = "Whitelist", 
            WHITELISTAVAILABLE  = "WhitelistAvailable", 
            BLACKLISTFILES      = "BlacklistFiles", 
            LASTPROPERTIESFILE  = "LastPropertiesFile",
            LASTREPORTFILE      = "LastReportFile",
            LASTXMLFILE         = "LastXmlFile",
            LASTDISTRIBDIR      = "LastDistribDir",
            EXTERNALTOOL_GIT    = "ExternalToolGit",
            EXTERNALTOOL_CTAGS  = "ExternalToolCtags",
            ENCODING            = "Encoding",
            REPORT              = "Report",
            REPORT_TITLEPAGE    = "ReportTitlePage",
            REPORT_SUMMARYPAGE  = "ReportSummaryPage",
            REVIEW              = "Review",
            LOCAL_GENERATED_VIEW = "LocalGeneratedView",
            SEARCH_FILE_COUNT   = "SearchFileCount",
            SEARCH_LINK         = "SearchLink",
            SEARCH_LINE_NR      = "SearchLineNr",
            SEARCH_SVN_VERSION  = "SearchSvnVersion",
            SEARCH_SVN_DESC     = "SearchSvnDesc",
            SEARCH_SVN_AUTHOR   = "SearchSvnAuthor",
            SEARCH_SVN_DATE     = "SearchSvnDate",
            SEARCH_REVIEW_STATE = "SearchReviewState",
            SEARCH_REVIEW_DESC  = "SearchReviewDesc",
            SEARCH_TEST_STATE   = "SearchTestState",
            SEARCH_TEST_DESC    = "SearchTestDesc",
            SEARCH_HISTORY      = "SearchHistory",
            SEARCH_REVIEW_AUTHOR    = "SearchReviewAuthor",
            SEARCH_REVIEW_DATE  = "SearchReviewDate",
            SEARCH_TEST_AUTHOR  = "SearchReviewAuthor",
            SEARCH_TEST_DATE    = "SearchReviewDate",
            SVNUSER             = "SvnUser",
            VERSION             = "Version"
        };  
        root = Preferences.userRoot();
        props = root.node("LxrDifferenceTable");
    }

    /**
     * Reads saved property by key.
     * 
     * @param   key
     * @return  property
     */
    public String getProperty(String key) {
        return props.get(key, "");
    }

    /**
     * Saves property by key.
     * 
     * @param key
     * @param value
     */
    public void putProperty(String key, String value) {
        props.put(key, value);
    }

    /**
     * Reads the properties
     * 
     * @return Preferences
     */
    public Preferences readProperties() {
        return props;
    }
    
}
