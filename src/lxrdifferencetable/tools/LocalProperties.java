/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxrdifferencetable.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import lxrdifferencetable.StagesController;

/**
 *
 * @author stefan
 */
public class LocalProperties {
    
    private Level logLevel;
    private boolean increaseMemoryForJVM;
    
    public LocalProperties(StagesController stageParent) {
        logLevel=Level.INFO;
        increaseMemoryForJVM=false;
        
        WorkingDirectory workingDirectory = new WorkingDirectory(stageParent);
        File workingDir = workingDirectory.getWorkingDir();
        
        Properties prop = new Properties();
        String fileName = workingDir.getAbsolutePath() + "/cfg/setup.properties";
        InputStream is = null;
        try {
            is = new FileInputStream(fileName);
        } catch (FileNotFoundException ex) {
            // JOptionPane.showMessageDialog(null,"Exception = " + ex.getMessage(), "Fehler", JOptionPane.PLAIN_MESSAGE);
        }
        try {
            prop.load(is);
        } catch (IOException ex) {
            // JOptionPane.showMessageDialog(null,"Exception = " + ex.getMessage(), "Fehler", JOptionPane.PLAIN_MESSAGE);
        }
        String l = prop.getProperty("LogLevel");
        String level = l.toUpperCase();
        switch (level) {
            case "ALL":
                logLevel = Level.ALL;
            case "CONFIG":
                logLevel = Level.CONFIG;
            case "FINE":
                logLevel = Level.FINE;
            case "FINER":
                logLevel = Level.FINER;
            case "FINEST":
                logLevel = Level.FINEST;
            case "INFO":
                logLevel = Level.INFO;
            case "OFF":
                logLevel = Level.OFF;
            case "SEVERE":
                logLevel = Level.SEVERE;
            case "WARNING":
                logLevel = Level.WARNING;
            default:
                logLevel = Level.INFO;
        }
        String inc = prop.getProperty("IncreaseMemoryForJVM");
        String increase = inc.toUpperCase();
        if (increase.equals("TRUE")) {
            increaseMemoryForJVM = true;
        }
    }
    
    public boolean getIncreaseMemoryForJVM() {
        return increaseMemoryForJVM;
    }
    
    public Level getLogLevel() {
        return logLevel;
    }
    
}
