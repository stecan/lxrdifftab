/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.io.File;

/**
 * The class ProgrammingLanguage is to obain the appropriate programming
 * language from the file extension.
 * 
 * @author Stefan Canali
 */
public class ProgrammingLanguage {
    
    public static enum Language {UNKNOWN, JAVA, C, CPP, CSHARP, BASH};

    public static Language registerProgrammingLanguage(File file) {
        // All the recognised suffixes are detailed at
        // http://gcc.gnu.org/onlinedocs/gcc-4.4.1/gcc/Overall-Options.html#index-file-name-suffix-71
        //
        Language progLang;
        String name = file.getName();
        int i = name.lastIndexOf('.');
        String ext = i > 0 ? name.substring(i + 1) : "";
        switch (ext) {
            case "java":
                progLang = Language.JAVA;
                break;
            case "c":
            case "i":
            case "h":
                progLang = Language.C;
                break;
            case "cpp":
            case "CPP":
            case "cxx":
            case "c++":
            case "ii":
            case "cc":
            case "cp":
            case "C":
            case "hpp":
            case "HPP":
            case "hxx":
            case "h++":
            case "tcc":
            case "hh":
            case "hp":
            case "sh":
                progLang = Language.BASH;
                break;
            case "H":
                progLang = Language.CPP;
                break;
            case "cs":
                progLang = Language.CSHARP;
                break;
            default:
                progLang = Language.UNKNOWN;
                break;
        }
        return progLang;
    }
    
    
}
