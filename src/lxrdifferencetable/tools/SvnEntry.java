/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.tools;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lxrdifferencetable.ProgressController;
import lxrdifferencetable.comp.Generate.State;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.normaldialogs.DialogSvnAuthenticationController;
import org.tmatesoft.svn.core.SVNAuthenticationException;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;


/**
 *
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class SvnEntry {
    
    private State state;
    private String svn;
    private String svn2;
    private int revision1;
    private int revision2;
    
    private final List<TableEntry> outList;
    private final HashMap<String, Integer> map;
    private final HashMap<Integer, Integer> revisionMap;
    
    /**
     * Constructor
     * 
     * Initialises
     *
     * @param repoRoot1     directory to repository, e.g. "/home/0.9.69/.svn"
     * @param repoRoot2     directory to repository, e.g. "/home/0.9.10/.svn"
     * @param outList       list of changed files
     */
    public SvnEntry (File repoRoot1, File repoRoot2, 
            List<TableEntry> outList, HashMap<String, Integer> map) {
        state = State.PASSED;
        this.outList = outList;
        this.revision1 = 0;
        this.revision2 = 0;
        
        // Maps all changed files to repository e.g.
        //
        // "/stefan/0.9.10/LxrDiffTab/.svn"
        // "/stefan/0.9.68/LxrDiffTab/trunc/src/../DialogJRExceptionController.java"
        // -------------------------------------------------
        // key in map:               "trunc/src/../DialogJRExceptionController.java""
        //
        if (map != null) {
            this.map = map;
        } else {
            this.map = new HashMap<>();
        }
        revisionMap = new HashMap<>();
        
        try {
            String file;
            String fileTmp;
            for (int i = 0; i < outList.size(); i++) {
                file = outList.get(i).getFile();
                fileTmp = file;
                file = file.replace(repoRoot1.getParent() + "/", "");
                file = file.replace(repoRoot1.getParent() + "\\", "");  // Windows
                file = file.replace(repoRoot2.getParent() + "/", "");
                file = file.replace(repoRoot2.getParent() + "\\", "");  // Windows
                if (!file.equals(fileTmp)) {
                    file = file.replace("\\", "/");
                    this.map.put(file, i);
                } else {
                    // do nothingA
                }
            }
        
            Class.forName("org.sqlite.JDBC");
            
            // connect to first repository and get 
            //    1. URL of repository
            //    2. revision
            Connection c1 = DriverManager.getConnection("jdbc:sqlite:" + 
                    repoRoot1 + "/wc.db");
            Statement stmt1 = c1.createStatement();
            ResultSet rs1 = stmt1.executeQuery( "select root from repository;" );
            // first repo
            svn = rs1.getString("root");
            rs1 = stmt1.executeQuery(
                    "SELECT max(revision), file_external FROM nodes where file_external is null;" );
            revision1 = Integer.parseInt(rs1.getString("max(revision)"));

      
            // connect to second repository and get 
            //    1. URL of repository
            //    2. revision
            Connection c2 = DriverManager.getConnection("jdbc:sqlite:" + 
                    repoRoot2 + "/wc.db");
            Statement stmt2 = c2.createStatement();
            ResultSet rs2 = stmt2.executeQuery( "select root from repository;" );
            // second repo: only for comparison
            svn2 = rs2.getString("root");
            rs2 = stmt2.executeQuery(
                    "SELECT max(revision), file_external FROM nodes where file_external is null;" );
            revision2 = Integer.parseInt(rs2.getString("max(revision)"));

            ResultSet rs3 = stmt2.executeQuery( 
                    "select changed_revision from nodes_base where local_relpath != '';" );

            // Generate a revision map with all revisions, that are 
            // affected (in case of branching).
            int i = 0;
            int key;
            while (rs3.next()) {
                key = Integer.parseInt(rs3.getString("changed_revision"));
                if (revision1 < revision2) {
                    if ((key < revision1) || (key > revision2)) {
                        // do nothing
                    } else {
                        if (revisionMap.get(key) == null) {
                            revisionMap.put(key, i);
                            i++;
                        }
                    }
                } else {
                    if ((key < revision2) || (key > revision1)) {
                        // do nothing
                    } else {
                        if (revisionMap.get(key) == null) {
                            revisionMap.put(key, i);
                            i++;
                        }
                    }
                }
            }

            rs1.close();
            stmt1.close();
            c1.close();
            rs2.close();
            stmt2.close();
            c2.close();
            
        } catch (ClassNotFoundException | SQLException ex) {
            state = State.ABORTED_NO_REPO;
        }
    }
    
    public State init (String account, String password, 
            ProgressController progressController,
            DialogSvnAuthenticationController dialogSvnAuthenticationController)  {
        
        state = State.PASSED;
        dialogSvnAuthenticationController.setPathToRepo(svn);
        FSRepositoryFactory.setup();

        SVNRepository repository;
        try {
            repository = SVNRepositoryFactory.create( SVNURL
                    .parseURIEncoded(svn));
            ISVNAuthenticationManager authManager = SVNWCUtil
                    .createDefaultAuthenticationManager(account, 
                            password.toCharArray());
            repository.setAuthenticationManager(authManager);

            for (Map.Entry<Integer, Integer> entry : revisionMap.entrySet()) {
                int revision = entry.getKey();
                    
                Collection logEntries;
                logEntries = repository.log( new String[] { "" } , null , 
                        revision, revision, true , true );
                for ( Iterator entries = logEntries.iterator( ); entries.hasNext( ); ) {
                    SVNLogEntry logEntry = ( SVNLogEntry ) entries.next( );

                    if (progressController.getAbortState()) {
                        state = State.ABORTED;
                        break;          // exit loop!
                    }

                    if ( logEntry.getChangedPaths( ).size( ) > 0 ) {

                        Set changedPathsSet = logEntry.getChangedPaths( ).keySet( );

                        for ( Iterator changedPaths = changedPathsSet.iterator( ); 
                                changedPaths.hasNext( ); ) {
                            Map<String, SVNLogEntryPath> map1 = logEntry.getChangedPaths();
                            SVNLogEntryPath entryPath = map1.get(changedPaths.next());

                            String file = entryPath.getPath();
                            if ((file.charAt(0) == '/') || (file.charAt(0) == '\\')) {
                                file = file.substring(1);
                            }

                            // At this point, we don't know the repo path (wc.db
                            // doesn't contain the correct paths!). Though, we test 
                            // all paths against the outlist.
                            String[] split = file.split("/");
                            String file2;
                            Integer index2 = null;
                            String str = "";
                            for (String s : split) {
                                str = str + s + "/";
                                file2 = file.replaceFirst(str, "");
                                index2 = map.get(file2);
                                if (index2 != null) {
                                    outList.get(index2).putSvnAuthor(logEntry.getAuthor());
                                    outList.get(index2).putSvnDesc(logEntry.getMessage());
                                    outList.get(index2).putSvnDate(logEntry.getDate().toString());
                                    outList.get(index2).putSvnVersion("" + logEntry.getRevision());
                                    break;  // element found: exit loop
                                }
                            }

                            if (progressController.getAbortState()) {
                                state = State.ABORTED;
                                break;          // exit loop!
                            }
                        }
                    }
                }
            }
        } catch (SVNAuthenticationException ex) {
            state = State.CONTINUING;
        } catch (SVNException ex) {
            state = State.ABORTED_NO_REPO;
        }

        return state;
    }
    
    public String getRepo () {
        return svn;
    }
    
    public String getRepo2 () {
        return svn2;
    }

    public HashMap<String, Integer> getMap() {
        return map;
    }
    
    public int getRevision1() {
        return revision1;
    }

    public int getRevision2() {
        return revision2;
    }
}
