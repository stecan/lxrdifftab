/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import lxrdifferencetable.ProgressController;
import lxrdifferencetable.comp.Generate.State;
import lxrdifferencetable.data.TableEntry;

/**
 *
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class GitEntry {
    private final List<TableEntry> outList;
    private final List<String> files;
    
    private final File repoRoot;    // Time(repoRoot) >= Time(repoBase)
    private final File repoBase;
    private final Properties props;
    
    private String git;
    private boolean gitAvailable;
    
    /**
     * Constructor
     * 
     * @param repoRoot1 - base repo
     * @param repoRoot2 - repo to campare
     */
    public GitEntry (File repoRoot1, File repoRoot2, 
            List<TableEntry> outList) {
       
        this.props = new Properties();
        String str = props.getProperty(props.EXTERNALTOOL_GIT);
        if ("".equals(str)) {
            // path to git not set
            git = "git";
        } else {
            git = str;
        }
        
        String command;
        command = git;
        try {
            Runtime.getRuntime().exec(command);
            gitAvailable = true;
        } catch (IOException | NumberFormatException ex) {
            gitAvailable = false;
        }
        
        if (gitAvailable) {
            // calculate time of newest repo
            int time1 = getTime(repoRoot1);
            int time2 = getTime(repoRoot2);
            if (time1 <= time2) {
                repoRoot = repoRoot2;
                repoBase = repoRoot1;
            } else {
                repoRoot = repoRoot1;
                repoBase = repoRoot2;
            }

        } else {
            repoRoot = null;
            repoBase = null;
        }
        files = new ArrayList<>();
        String file;
        for (int i = 0; i < outList.size(); i++) {
            file = outList.get(i).getFile();
            file = file.replace(repoRoot1.getParent() + "\\", "");  // Windows
            file = file.replace(repoRoot2.getParent() + "\\", "");  // Windows
            file = file.replace(repoRoot1.getParent() + "/", "");   // Linux
            file = file.replace(repoRoot2.getParent() + "/", "");   // Linux
            files.add(file);
        }
        this.outList = outList;
    }
    
    public State init (ProgressController progressController) {
        
        State state = State.PASSED;

        int j;
        String fullDescription;
        String file;
        String commitBaseTime;
        
        for (int i = 0; i < files.size(); i++) {
            if (progressController.getAbortState()) {
                state = State.ABORTED;
                break;
            }
            if (!gitAvailable) {
                // git is not available. Break loop!
                state = State.ABORTED_NO_REPO;
                break;
            }
            file = files.get(i);
            commitBaseTime = getLastBaseCommitTime(file);
            if ("".equals(commitBaseTime)) {
                // new file
                commitBaseTime = getLastRootCommitTime(file);                
            } 
            int count = getNumberOfCommits(file, commitBaseTime);
            if (count == 1) {
                List<String> allCommits = getAllCommits(file, count);
                List<String> allAuthors = getAllAuthors(file, count);
                List<String> allAuthorDates = getAllAuthorDates(file, count);
                List<String> allSubjects = getAllSubjects(file, count);
                List<String> allBodies = getAllBodies(file, count);
                j = 0;
                for (String commit : allCommits) {
                    outList.get(i).putSvnVersion(commit);
                    outList.get(i).putSvnAuthor(allAuthors.get(j));
                    outList.get(i).putSvnDate(allAuthorDates.get(j));
                    if (j < allSubjects.size()) {
                        if (j < allBodies.size()) {
                            fullDescription = allSubjects.get(j) + 
                                    "\n" + "-----------" + "\n" +
                                    allBodies.get(j) + "\n";
                        } else {
                            fullDescription = allSubjects.get(j) + 
                                    "\n" + "-----------" + "\n";
                        }
                    } else {
                        fullDescription = "\n";
                    }
                    outList.get(i).putSvnDesc(fullDescription);
                    j++;
                }
            } else {
                List<String> allCommits = getAllCommits(file, count-1);
                List<String> allAuthors = getAllAuthors(file, count-1);
                List<String> allAuthorDates = getAllAuthorDates(file, count-1);
                List<String> allSubjects = getAllSubjects(file, count-1);
                List<String> allBodies = getAllBodies(file, count-1);
                j = 0;
                for (String commit : allCommits) {
                    outList.get(i).putSvnVersion(commit);
                    outList.get(i).putSvnAuthor(allAuthors.get(j));
                    outList.get(i).putSvnDate(allAuthorDates.get(j));
                    if (j < allSubjects.size()) {
                        if (j < allBodies.size()) {
                            fullDescription = allSubjects.get(j) + 
                                    "\n" + "-----------" + "\n" +
                                    allBodies.get(j) + "\n";
                        } else {
                            fullDescription = allSubjects.get(j) + 
                                    "\n" + "-----------" + "\n";
                        }
                    } else {
                        fullDescription = "\n";
                    }
                    outList.get(i).putSvnDesc(fullDescription);
                    j++;
                }
            }
        }
        return state;
    }
    
    private String getLastBaseCommitTime(String file) {
        String command = git + " --git-dir=" + repoBase +
                " log -n 1 --pretty=format:%at_:_ -- " + file;
        Process exec;
        String next;
        String commitTime = "";

        try {
            exec = Runtime.getRuntime().exec(command);
            Scanner s1 = new Scanner( exec.getInputStream() ).useDelimiter( "_:_" );
            while (s1.hasNext()) {
                next = s1.next();
                if (!"".equals(next)) {
                    if (next.charAt(0) == ("\n").charAt(0)) {
                        commitTime = next.substring(1);
                    } else {
                        commitTime = next;
                    }
                    break;
                }
            }
        } catch (Exception ex) {
            // Do nothing.
        }
        return commitTime;
    }
    
    private String getLastRootCommitTime(String file) {
        String command = git + " --git-dir=" + repoRoot + 
                " log -n 1 --reverse --pretty=format:%at_:_ -- " + file;
        Process exec;
        String next;
        String commitTime = "";

        try {
            exec = Runtime.getRuntime().exec(command);
            Scanner s1 = new Scanner( exec.getInputStream() ).useDelimiter( "_:_" );
            while (s1.hasNext()) {
                next = s1.next();
                if (!"".equals(next)) {
                    if (next.charAt(0) == ("\n").charAt(0)) {
                        commitTime = next.substring(1);
                    } else {
                        commitTime = next;
                    }
                    break;
                }
            }
        } catch (Exception ex) {
            // Do nothing.
        }
        return commitTime;
    }

    private int getNumberOfCommits(String file, String baseCommitTime) {
        String command = git + " --git-dir=" + repoRoot + 
                " log --after={" + baseCommitTime + "} --pretty=format:%h -- " + 
                file;
        int count = 0;
        Process exec;
        String next;

        try {
            exec = Runtime.getRuntime().exec(command);
            Scanner s1 = new Scanner( exec.getInputStream() ).useDelimiter( "\\Z" );
            if (s1.hasNext()) {
                next = s1.next();
                String[] split = next.split("\n");
                count = split.length;
            }
        } catch (Exception ex) {
            // Do nothing.
        }
        return count;
    }

    private List<String> getAllCommits(String file, int count) {
        String command = git + " --git-dir=" + repoRoot + 
                " log -n " + count + " --pretty=format:%h_:_ -- " + file;
        Process exec;
        String next;
        List<String> list = new ArrayList<>();

        try {
            exec = Runtime.getRuntime().exec(command);
            Scanner s1 = new Scanner( exec.getInputStream() ).useDelimiter( "_:_" );
            while (s1.hasNext()) {
                next = s1.next();
                if (!"".equals(next)) {
                    if (next.charAt(0) == ("\n").charAt(0)) {
                        list.add(next.substring(1));
                    } else {
                        list.add(next);
                    }
                }
            }
        } catch (Exception ex) {
            // Do nothing.
        }
        return list;
    }
    
    private List<String> getAllAuthors(String file, int count) {
        String command = git + " --git-dir=" + repoRoot + 
                " log -n " + count + " --pretty=format:%an_:_ -- " + file;
        Process exec;
        String next;
        List<String> list = new ArrayList<>();

        try {
            exec = Runtime.getRuntime().exec(command);
            Scanner s1 = new Scanner( exec.getInputStream() ).useDelimiter( "_:_" );
            while (s1.hasNext()) {
                next = s1.next();
                if (!"".equals(next)) {
                    if (next.charAt(0) == ("\n").charAt(0)) {
                        list.add(next.substring(1));
                    } else {
                        list.add(next);
                    }
                }
            }
        } catch (Exception ex) {
            // Do nothing.
        }
        return list;
    }

    private List<String> getAllAuthorDates(String file, int count) {
        String command = git + " --git-dir=" + repoRoot + 
                " log -n " + count + " --pretty=format:%ad_:_ -- " + file;
        Process exec;
        String next;
        List<String> list = new ArrayList<>();

        try {
            exec = Runtime.getRuntime().exec(command);
            Scanner s1 = new Scanner( exec.getInputStream() ).useDelimiter( "_:_" );
            while (s1.hasNext()) {
                next = s1.next();
                if (!"".equals(next)) {
                    if (next.charAt(0) == ("\n").charAt(0)) {
                        list.add(next.substring(1));
                    } else {
                        list.add(next);
                    }
                }
            }
        } catch (Exception ex) {
            // Do nothing.
        }
        return list;
    }

    private List<String> getAllSubjects(String file, int count) {
        String command = git + " --git-dir=" + repoRoot + 
                " log -n " + count + " --pretty=format:%s_:_ -- " + file;
        Process exec;
        String next;
        List<String> list = new ArrayList<>();

        try {
            exec = Runtime.getRuntime().exec(command);
            Scanner s1 = new Scanner( exec.getInputStream() ).useDelimiter( "_:_" );
            while (s1.hasNext()) {
                next = s1.next();
                if (!"".equals(next)) {
                    if (next.charAt(0) == ("\n").charAt(0)) {
                        list.add(next.substring(1));
                    } else {
                        list.add(next);
                    }
                }
            }
        } catch (Exception ex) {
            // Do nothing.
        }
        return list;
    }

    private List<String> getAllBodies(String file, int count) {
        String command = git + " --git-dir=" + repoRoot + 
                " log -n " + count + " --pretty=format:%b_:_ -- " + file;
        Process exec;
        String next;
        List<String> list = new ArrayList<>();

        try {
            exec = Runtime.getRuntime().exec(command);
            Scanner s1 = new Scanner( exec.getInputStream() ).useDelimiter( "_:_" );
            while (s1.hasNext()) {
                next = s1.next();
                if (!"".equals(next)) {
                    String substring1 = replaceString(next, "Reviewed-by:");
                    String substring2 = replaceString(substring1, "Signed-off-by:");
                    String substring3 = replaceString(substring2, "Reported-by");
                    String substring4 = replaceString(substring3, "Suggested-by:");
                    String substring5 = replaceString(substring4, "Acked-by: ");
                    String substring6 = replaceString(substring5, "Tested-by:");
                    String substring7 = replaceString(substring6, "Cc:");
                    if (substring7.length() > 0) {
                        if (substring7.charAt(0) == ("\n").charAt(0)) {
                            list.add(substring7.substring(1));
                        } else {
                            list.add(substring7);
                        }
                    } else {
                        list.add(substring7);
                    }
                }
            }
        } catch (Exception ex) {
            // do nothing!
        }
        return list;
    }

    private int getTime (File repoRoot) {
        Process exec;
        String command;
        String next;
        int commitTime = 0;
        
        command = git + " --git-dir=" + repoRoot + 
                " log -1 --format=%cd_:_ --date=raw";
        try {
            exec = Runtime.getRuntime().exec(command);
            Scanner s1 = new Scanner( exec.getInputStream() ).useDelimiter( "_:_" );
            while (s1.hasNext()) {
                next = s1.next();
                if (!"".equals(next)) {
                    // e.g. next = "1481483874 -0800"
                    // Note: There is a space before the dash!
                    String[] split = next.split(" -");
                    commitTime = Integer.parseInt(split[0]);
                    break;
                }
            }
        } catch (IOException | NumberFormatException ex) {
            commitTime = 0;
        }
        return commitTime;
    }
    
    private String replaceString (String original, String replacement) {
        int indexOf = original.indexOf(replacement);
        String substring;
        if (indexOf >= 0) {
            substring = original.substring(0, indexOf-1);
        } else {
            substring = original;
        }
        return substring;
    }
}
