/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * CtagsEntry is a class to hold a ctags entry.
 * 
 * The format of a ctags file is describe in
 * https://github.com/universal-ctags/ctags/blob/master/old-docs/website/FORMAT
 * 
 * @author Stefan Canali
 */
public class CtagsEntry {
    
    private String tagname;
    private final List<String> tagfiles;
    private final List<String> tagaddresses;
    private final List<String> tagfields;
    
    /**
     * Constructor
     */
    public CtagsEntry() {
        this.tagname = "";
        this.tagfiles = new ArrayList<>();
        this.tagaddresses = new ArrayList<>();
        this.tagfields = new ArrayList<>();
    }
    
    /**
     * Constructor
     * 
     * @param tagname   {tagname} of a ctags file
     */
    public CtagsEntry(String tagname) {
        this.tagname = tagname;
        this.tagfiles = new ArrayList<>();
        this.tagaddresses = new ArrayList<>();
        this.tagfields = new ArrayList<>();
    }
    
    /**
     * Method to set the tagname
     * 
     * @param tagname
     */
    public void setTagname(String tagname) {
        this.tagname = tagname;
    }
    
    /**
     * Method to add a tagfile
     * 
     * @param tagfile
     */
    public void addTagfile(String tagfile) {
        this.tagfiles.add(tagfile);
    }
    
    /**
     * Method to add a tagaddress
     * 
     * @param tagaddress
     */
    public void addTagaddress(String tagaddress) {
        this.tagaddresses.add(tagaddress);
    }
    
    /**
     * Method to add a tagfield
     * 
     * @param tagfields
     */
    public void addTagfields(String tagfields) {
        this.tagfields.add(tagfields);
    }
    
    /**
     * Method to get the tagname
     * 
     * @return String           tagname
     */
    public String getTagname() {
        return this.tagname;
    }

    /**
     * Method to get all tagfiles
     * 
     * @return List of Strings  tagfiles
     */
    public List<String> getTagfiles() {
        return this.tagfiles;
    }

    /**
     * Method to get all tagaddresses
     * 
     * @return List of Strings  tagaddresses
     */
    public List<String> getTagaddresses() {
        return this.tagaddresses;
    }

    /**
     * Method to get all tagfields
     * 
     * @return List of Strings  tagfields
     */
    public List<String> getTagfields() {
        return this.tagfields;
    }
}
