/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * CtagsFileEntry is a class to hold ctags file entries.
 * 
 * The format of a ctags file is describe in
 * https://github.com/universal-ctags/ctags/blob/master/old-docs/website/FORMAT
 * 
 * @author Stefan Canali
 */
public class CtagsFileEntry {
    
    private String filename;
    private final List<String> tagname;
    
    /**
     * Constructor
     */
    public CtagsFileEntry() {
        this.filename = "";
        this.tagname = new ArrayList<>();
    }
    
    /**
     * Constructor
     * 
     * @param filename   {filename} of a ctags file
     */
    public CtagsFileEntry(String filename) {
        this.filename = filename;
        this.tagname = new ArrayList<>();
    }
    
    /**
     * Method to set the filename
     * 
     * @param filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    /**
     * Method to add a tagname
     * 
     * @param tagname
     */
    public void addTagname(String tagname) {
        this.tagname.add(tagname);
    }
    
    /**
     * Method to get the filename
     * 
     * @return String           filename
     */
    public String getFilename() {
        return this.filename;
    }

    /**
     * Method to get all tagnames
     * 
     * @return List of Strings  tagnames
     */
    public List<String> getTagnames() {
        return this.tagname;
    }
}
