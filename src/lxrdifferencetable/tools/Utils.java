/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javafx.collections.ObservableList;

/**
 * Some Utilities
 * 
 * 
 * @author Stefan Canali
 */
public class Utils {
    
    private final Properties props;
    private final String[] blacklist;
    private final String[] blacklistFiles;
    private final Boolean[] activeBlacklistFiles;
    private final Boolean[] activeBlacklist;
    private final String base1;
    private final String base2;
    private final String version1;
    private final String version2;
    

    /**
     * Constructor
     */
    public Utils() {
        String tmp;
        this.props = new Properties();
        tmp = this.props.getProperty(this.props.BLACKLIST);
        if (tmp.equals("")) {
            blacklist = new String[0];
        } else {
            blacklist = tmp.split(",");
        }
        tmp = this.props.getProperty(this.props.BLACKLISTFILES);
        if (tmp.equals("")) {
            blacklistFiles = new String[0];
        } else {
            blacklistFiles = tmp.split(",");
        }
        // initialize boolean array with false (no elements)
        activeBlacklistFiles = new Boolean[blacklistFiles.length];
        for (int i = 0; i < activeBlacklistFiles.length; i++) {
            activeBlacklistFiles[i] = false;
        }
        activeBlacklist = new Boolean[blacklist.length];
        for (int i = 0; i < activeBlacklist.length; i++) {
            activeBlacklist[i] = false;
        }
        base1 = this.props.getProperty(this.props.BASEDIR);
        base2 = this.props.getProperty(this.props.COMPDIR);
        version1 = this.props.getProperty(props.LXRBASEVER);
        version2 = this.props.getProperty(props.LXRCOMPVER);
    }
    
    /**
     * Gets leaf directory, e.g. if the directory is 
     *          "D:\SVN\Vergleich\170777-V05.10.00"
     * then the associated leaf is
     *          "170777-V05.10.00"
     * 
     * 
     * @param   in
     * @return  pathname
     */
    public String getLeafDirectory(String in) {
        
        String pathName;
        String fileSeparator = File.separator;
        String[] data = in.split("\\" + fileSeparator);
        
        pathName = data[data.length - 1];
        return pathName;
    }
    
    /**
     * Reads all the lines of a given file, e.g. if a file has 103 lines, then
     * 103 lines are returned.
     * 
     * A line is considered to be terminated by any one of a line feed ('\n'), 
     * a carriage return ('\r'), or a carriage return followed immediately 
     * by a linefeed. 
     * 
     * 
     * @param   file
     * @return  lines
     */
    public List<String> fileToLines (File file) {
        String line;
        List<String> lines = new ArrayList<>();
        BufferedReader reader;
        
        try {
            reader = new BufferedReader(new FileReader(file));
            try {
                line = "";
                while (line != null) {
                    line = reader.readLine();
                    lines.add(line);
                }
            } catch (IOException ex) {
                // do nothing
            }
        } catch (FileNotFoundException ex) {
            // do nothing
        }
        
        return lines;
    }
    
    /**
     * Tells, whether the file is blacklisted or not.
     * 
     * Note: If a file is blacklisted, then it is not used
     * in the context of the programm "LxrDifferenceTable".
     * 
     * 
     * @param   file
     * @return  true or false
     */
    public boolean blacklist(File file) {
        boolean ok = false;
        int i = -1;
        for (String data : blacklist) {
            i++;
            boolean extensionOK = (file.toString()).toLowerCase().endsWith(data.toLowerCase());
            ok = ok || extensionOK;
            if (extensionOK) {
                activeBlacklist[i] = true;
            }
        }
        if (!ok) {
            i=-1;
            for (String file0 : blacklistFiles) {
                i++;
                File file1;
                File file2;
                file1 = leaf(file);
                file2 = leaf(new File(file0));
                if (file1.equals(file2)) {
                    ok = true;
                    break;      // exit loop!

                } else if (file1.toString().contains(file2 + "/")) {
                    // for Linux only
                    ok = true;
                    break;      // exit loop!
                } else if (file1.toString().contains(file2 + "\\")) {
                    // for Windows only
                    ok = true;
                    break;      // exit loop!
                }
            }
            if (ok) {
                activeBlacklistFiles[i] = true;
            }
        }
        return ok;
    }
    
    public String[] getActiveBlacklistFiles() {
        List<String> listResult = new ArrayList<>();
        int i = -1;
        for (Boolean b : activeBlacklistFiles) {
            i++;
            if (b) {
                listResult.add(blacklistFiles[i]);
            }
        }
        String[] result = new String [listResult.size()];
        int j = -1;
        for (String str : listResult) {
            j++;
            result[j] = str;
        }
        return result;
    }
    
    public String[] getActiveBlacklist() {
        List<String> listResult = new ArrayList<>();
        int i = -1;
        for (Boolean b : activeBlacklist) {
            i++;
            if (b) {
                listResult.add(blacklist[i]);
            }
        }
        String[] result = new String [listResult.size()];
        int j = -1;
        for (String str : listResult) {
            j++;
            result[j] = str;
        }
        return result;
    }

    /**
     * Sorts a comma separated string alphabetically.
     * 
     * Example:
     *      in     = "exe,txt,cgi,html,h,c,cpp"
     *      output = "c,cgi,cpp,exe,h,html,txt"
     * 
     * @param in    comma separates string
     * @return      sorted, comma separated string
     */
    public String sortString(String in) {
        String list;
        String[] dataList;
        ArrayList<String> dataArray = new ArrayList<>();
        if (in.equals("")) {
            // For an empty string, the method split returns 
            // the given regular expression. Here, the method
            // returns an ",".
            // Thus, if the input string is empty, an empty
            // list is returned!
            list = "";
        } else {
            dataList = in.split(",");

            dataArray.addAll(Arrays.asList(dataList));
            Collections.sort(dataArray);
            for (int i = 0; i < dataArray.size(); i++) {
                dataList[i] = (String) dataArray.get(i);
            }

            list = "";
            int i = 0;
            for (String entry : dataList) {
                if (i == (dataList.length - 1)) {
                    // Do not use a trailing comma (",")!
                    list = list + entry;
                } else {
                    list = list + entry + ",";
                }
                i++;
            }
        }

        return list;
    }
    
    /**
     * Creates a comma separated string alphabetically.
     * 
     * Example:
     *      in[0]   = "Software/DX6/common"
     *      in[1]   = "Software/Boot_C167/Common"
     *      output  = "Software/Boot_C167/Common, Software/DX6/common"
     * 
     * @param  items    Observable List of Strings
     * @return          sorted, comma separated string
     */
    public String createCommaSeparatedString(ObservableList<String> items) {
        String list;
        list = "";
        for (int i = 0; i < items.size(); i++) {
            if (i == 0) {
                // Do not use a leading comma (",")!
                list = items.get(i);
            } else {
                list = list + "," + items.get(i);
            }
        }

        list = sortString(list);
        return list;
    }
    
    /**
     * Adds a list of files to a given list and outputs a comma separated string 
     * alphabetically.
     * 
     * Example:
     *      list           = "Software/DX6/common,Software/Boot_C167/Common"
     *      selectedFiles  = "Software/DX42/Common, Software/DX91/common"
     *      output         = "Software/Boot_C167/Common,Software/DX42/Common,
     *                        Software/DX6/common, Software/DX91/common"
     * 
     * @param list          comma separated string
     * @param selectedFiles List of Files
     * @return              sorted, comma separated string
     */
    public String addToCommaSeparatedString(String list, List<File> selectedFiles) {
        for (int i = 0; i < selectedFiles.size(); i++) {
            if (list.equals("")) {
                // Do not use a leading comma (",")!
                list = selectedFiles.get(i).toString(); 
            } else {
                list = list + "," + selectedFiles.get(i).toString(); 
            }
        }

        list = sortString(list);
        return list;
    }

    /**
     * Adds a file to a given list and outputs a comma separated string 
     * alphabetically.
     * 
     * Example:
     *      list           = "Software/DX6/common,Software/Boot_C167/Common"
     *      selectedFile   = "Software/DX42/Common"
     *      output         = "Software/Boot_C167/Common,Software/DX6/common, 
     *                        Software/DX91/common"
     *
     * @param list          comma separated string
     * @param selectedFile  File to add
     * @return              sorted, comma separated string
     */
    public String addToCommaSeparatedString(String list, File selectedFile) {
        if (list.equals("")) {
            // Do not use a leading comma (",")!
            list = selectedFile.toString(); 
        } else {
            list = list + "," + selectedFile.toString(); 
        }

        list = sortString(list);
        return list;
    }

    /**
     * Adds a file to a given list and outputs a comma separated string 
     * alphabetically.
     * 
     * Example:
     *      list                = "Software/DX6/common,Software/Boot_C167/Common"
     *      selectedItems[0]    = "Software/DX42/Common"
     *      selectedItems[1]    = "Software/DX91/Common"
     *      output              = "Software/Boot_C167/Common,Software/DX42/common, 
     *                             Software/DX6/common,Software/DX91/common"
     *
     * @param list              comma separated string
     * @param selectedItems     Observable List of Strings
     * @return                  sorted, comma separated string
     */
    public String addToCommaSeparatedString(String list, ObservableList<String> selectedItems) {
        for (int i = 0; i < selectedItems.size(); i++) {
            if (list.equals("")) {
                // Do not use a leading comma (",")!
                list = selectedItems.get(i); 
            } else {
                list = list + "," + selectedItems.get(i); 
            }
        }

        list = sortString(list);
        return list;
    }
    
    /**
     * This method returns a file, that pathname is subtracted by
     * base1 or base2.
     * 
     *      in      = "/home/stefan/work/Vergleich/170777-V05.10.00/Software/Boot_C167/Common"
     *      base1   = "/home/stefan/work/Vergleich/170777-V05.10.00"
     *      output  = "Software/Boot_C167/Common"
     * 
     * @param   in  file
     * @return      file with a pathname subtracted by base1 or base2.
     */
    public File leaf(File in) {
        String file;
        if (in.toString().contains(base1)) {
            file = "";
            for (int i = base1.length() + 1; i < in.toString().length(); i++) {
                file = file + in.toString().charAt(i);
            }
        } else if (in.toString().contains(base2)) {
            file = "";
            for (int i = base2.length() + 1; i < in.toString().length(); i++) {
                file = file + in.toString().charAt(i);
            }
            
        } else {
            file = in.toString();
        }

        return new File(file);
    }

    /**
     * This method returns a list of file, that pathnames are subtracted by
     * base1 or base2.
     * Example:
     *      in[0] = "/home/stefan/work/Vergleich/170777-V05.10.00/Software/Boot_C167/Common"
     *      in[1] = "/home/stefan/work/Vergleich/170777-V05.10.00/Software/DX6/Common"
     *      base  = "/home/stefan/work/Vergleich/170777-V05.10.00"
     * 
     *      out[0] = "Software/Boot_C167/Common"
     *      out[1] = "Software/DX6/Common"
     * 
     * @param in    file list
     * @return      file list with pathnames subtracted by base1 or base2
     */
    public List<File> leaf(List<File> in) {
        List<File> out;
        String file;
        
        out = new ArrayList<>();
        
        for (int i = 0; i < in.size(); i++) {
            if (in.get(i).toString().contains(base1)) {
                file = "";
                for (int j = base1.length() + 1; j < in.get(i).toString().length(); j++) {
                    file = file + in.get(i).toString().charAt(j);
                }
                out.add(new File(file));
            } else if (in.get(i).toString().contains(base2)) {
                file = "";
                for (int j = base2.length() + 1; j < in.get(i).toString().length(); j++) {
                    file = file + in.get(i).toString().charAt(j);
                }
                out.add(new File(file));
            } else {
                file = in.get(i).toString();
                out.add(new File(file));
            }
        }
        return out;
    }
    
    /**
     * This method returns the version string.
     * 
     * Example:
     * file = "/var/www/html/R1/sources/170777-V05.10.00/Software/DX6/Sources/Dx06/p2_06_datasrv.c"
     * LXR-version string = "?!v=170777-V05.10.00"
     * 
     * @param   file
     * @return version string
     */
    public String getVersion(File file) {
        if (file.toString().contains(base1)) {
            return version1;
        } else if (file.toString().contains(base2)) {
            return version2;
        } else {
            return "";
        }
    }
}
