/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import lxrdifferencetable.tools.Properties;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import static lxrdifferencetable.LxrDifferenceTable.BLACKLIST_FILES_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.BLACKLIST_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.CONFIG_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import lxrdifferencetable.tools.Utils;


/**
 *
 * @author Stefan Canali
 */
public class ConfigController implements Initializable, ControlledStage {
    
    static final Logger logger = Logger.getLogger
        (LxrDifferenceTableController.class.getName());

    private LxrDifferenceTableController lxrDifferenceTableController;
    
    Properties props;
    Utils utils;

    public ConfigController() {
        this.props = new Properties();
        this.utils = new Utils();
    }
    
    private StagesController myController;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    Button abortConfig;

    @FXML
    Button saveConfig;
    
    @FXML
    Button blacklistFileExtensions;

    @FXML
    Button blacklistFiles;

    @FXML
    MenuItem config;

    @FXML
    TextField lxrLink;

    @FXML
    TextField lxrDiff;

    @FXML
    TextField lxrBaseVersion;

    @FXML
    TextField lxrComparableVersion;

    @FXML
    TextField extensions;

    @FXML
    TextField formatVersion;

    @FXML
    TextField formatDiffVal;

    @FXML
    TextField formatDiffVar;

    @FXML
    TextField formatSeparator;

    @FXML
    TextField formatLineNr;

    @FXML
    CheckBox checkboxReview;

    @FXML
    CheckBox checkboxLocalGeneratedView;

    @FXML
    private void switchAction(ActionEvent event) throws IOException {
        if  (event.getSource() == abortConfig) {
            initialize(null, null);
            myController.hideStage(CONFIG_STAGE);
        } else if (event.getSource() == saveConfig) {
            savePreferences();
            
            FXMLLoader loader;
            loader = myController.getLoader(MAIN_STAGE_1);
            lxrDifferenceTableController = loader.<LxrDifferenceTableController>getController();
            lxrDifferenceTableController.initialize(null, null);
            myController.hideStage(CONFIG_STAGE);
        } else if (event.getSource() == blacklistFileExtensions) {
            myController.showStage(BLACKLIST_STAGE);
        } else if (event.getSource() == blacklistFiles) {
            myController.showStage(BLACKLIST_FILES_STAGE);
        }
    }

    @FXML
    private void handleFormatVersion() {
        String format = formatVersion.getText();      // e.g. "v="
        format = format.replace(" ", "");             // spaces are not allowed!
        props.putProperty(props.FORMATVERSION, format);
        String diff;
        if (formatDiffVar.getText().endsWith("=")) {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText() + lxrBaseVersion.getText();
        } else {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText();
        }
        props.putProperty(props.LXRDIFF, diff);         // save as preference
        lxrDiff.setText(diff);
    }
    
    @FXML
    private void handleDiffVal() {
        String format = formatDiffVal.getText();
        format = format.replace(" ", "");   // spaces are not allowed!
        props.putProperty(props.FORMATDIFFVAL, format);
        String diff;
        if (formatDiffVar.getText().endsWith("=")) {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText() + lxrBaseVersion.getText();
        } else {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText();
        }
        props.putProperty(props.LXRDIFF, diff);         // save as preference
        lxrDiff.setText(diff);
    }
    
    @FXML
    private void handleDiffVar() {
        String format = formatDiffVar.getText();
        format = format.replace(" ", "");   // spaces are not allowed!
        props.putProperty(props.FORMATDIFFVAR, format);
        String diff;
        if (formatDiffVar.getText().endsWith("=")) {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText() + lxrBaseVersion.getText();
        } else {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText();
        }
        props.putProperty(props.LXRDIFF, diff);         // save as preference
        lxrDiff.setText(diff);
    }
    
    @FXML
    private void handleSeparator() {
        String format = formatSeparator.getText();
        format = format.replace(" ", "");   // spaces are not allowed!
        props.putProperty(props.FORMATSEPARATOR, format);
        String diff;
        if (formatDiffVar.getText().endsWith("=")) {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText() + lxrBaseVersion.getText();
        } else {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText();
        }
        props.putProperty(props.LXRDIFF, diff);         // save as preference
        lxrDiff.setText(diff);
    }

    @FXML
    private void handleLineNr() {
        String format = formatLineNr.getText();
        format = format.replace(" ", "");   // spaces are not allowed!
        props.putProperty(props.FORMATLINENR, format);
        String diff;
        if (formatDiffVar.getText().endsWith("=")) {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText() + lxrBaseVersion.getText();
        } else {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText();
        }
        props.putProperty(props.LXRDIFF, diff);         // save as preference
        lxrDiff.setText(diff);
    }

    @FXML
    private void handleLocalGeneratedView() {
        if (checkboxLocalGeneratedView.isSelected()) {
            // On localGeneratedView some settings are predefined!
            props.putProperty(props.LXRLINK, "http://localhost/lxr");
            lxrLink.setText("http://localhost/lxr");
            props.putProperty(props.FORMATVERSION, "v=");
            formatVersion.setText("v=");
            props.putProperty(props.FORMATDIFFVAL, "~v=");
            formatDiffVal.setText("~v=");
            props.putProperty(props.FORMATDIFFVAR, "!v=");
            formatDiffVar.setText("!v=");
            props.putProperty(props.FORMATSEPARATOR, "&");
            formatSeparator.setText("&");
            props.putProperty(props.FORMATLINENR, "#");
            formatLineNr.setText("#");
            
            lxrLink.setDisable(true);
            formatVersion.setDisable(true);
            formatDiffVal.setDisable(true);
            formatDiffVar.setDisable(true);
            formatSeparator.setDisable(true);
            formatLineNr.setDisable(true);
        } else {
            lxrLink.setDisable(false);
            formatVersion.setDisable(false);
            formatDiffVal.setDisable(false);
            formatDiffVar.setDisable(false);
            formatSeparator.setDisable(false);
            formatLineNr.setDisable(false);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String textString;
        textString = props.getProperty(props.LXRLINK);
        if (!textString.equals("")) {
            lxrLink.setText(textString);
        }

        String versionString;
        versionString = props.getProperty(props.LXRBASEVER);
        if (!versionString.equals("")) {
            if (versionString.contains("?!v=")) {
                // replace old stuff!
                versionString = versionString.replace("?!v=", "");
                props.putProperty(props.LXRBASEVER, versionString);
            }
            lxrBaseVersion.setText(versionString);
        }
        versionString = props.getProperty(props.LXRCOMPVER);
        if (!versionString.equals("")) {
            if (versionString.contains("?!v=")) {
                // replace old stuff!
                versionString = versionString.replace("?!v=", "");
                props.putProperty(props.LXRCOMPVER, versionString);
            }
            lxrComparableVersion.setText(versionString);
        }
        
        String format;
        format = props.getProperty(props.FORMATVERSION);
        if (!format.equals("")) {
            formatVersion.setText(format);
        } else {
            // set to default
            format = "v=";
            formatVersion.setText(format);
            props.putProperty(props.FORMATVERSION, format);
        }
        format = props.getProperty(props.FORMATDIFFVAL);
        if (!format.equals("")) {
            formatDiffVal.setText(format);
        } else {
            // set to default
            format = "!v=";
            formatDiffVal.setText(format);
            props.putProperty(props.FORMATDIFFVAL, format);
        }
        format = props.getProperty(props.FORMATDIFFVAR);
        if (!format.equals("")) {
            formatDiffVar.setText(format);
        } else {
            // set to default
            format = "~v=";
            formatDiffVar.setText(format);
            props.putProperty(props.FORMATDIFFVAR, format);
        }
        format = props.getProperty(props.FORMATSEPARATOR);
        if (!format.equals("")) {
            formatSeparator.setText(format);
        } else {
            // set to default
            format = "&";
            formatSeparator.setText(format);
            props.putProperty(props.FORMATSEPARATOR, format);
        }
        format = props.getProperty(props.FORMATLINENR);
        if (!format.equals("")) {
            formatLineNr.setText(format);
        } else {
            // set to default
            format = "#";
            formatLineNr.setText(format);
            props.putProperty(props.FORMATLINENR, format);
        }

        String diff;
        if (formatDiffVar.getText().endsWith("=")) {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText() + lxrBaseVersion.getText();
        } else {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText();
        }
        handleLocalGeneratedView();                     // predefined settings
                                                        // on localGeneratedView
        
        props.putProperty(props.LXRDIFF, diff);         // save as preference
        lxrDiff.setText(diff);
        lxrDiff.setEditable(false);
        lxrDiff.setDisable(true);

        String repolist;
        repolist = props.getProperty(props.REPOLIST);
        if (repolist.equals("")) {
            repolist = "svn,git";
            props.putProperty(props.REPOLIST, repolist);
        }

        String blacklist;
        String blacklistAvailable;
        String whitelist;
        String whitelistAvailable;
        String[] dataBlacklist;

        blacklist = props.getProperty(props.BLACKLIST);
        blacklistAvailable = props.getProperty(props.BLACKLISTAVAILABLE);
        if (blacklist.equals("") && !blacklistAvailable.equals("true")) {
            blacklist = "gz,xz,bz,bz2,tar,tgz,git,svn,lib,dll,so,exe,gif,ico," + 
                    "png,doc,docx,xls,xlsx,ppt,pptx,ods,odt";
            blacklist = utils.sortString(blacklist);
            blacklistAvailable = "true";
            props.putProperty(props.BLACKLIST, blacklist);
            props.putProperty(props.BLACKLISTAVAILABLE, blacklistAvailable);
        }         
        whitelist = props.getProperty(props.WHITELIST);
        whitelistAvailable = props.getProperty(props.WHITELISTAVAILABLE);
        if (whitelist.equals("") && !whitelistAvailable.equals("true")) {
            whitelist = "c,cpp,h,hpp,java,txt,text,tst," + 
                    "sh,cgi,conf,xml,html,htm,css,js,in,mk," + 
                    "lint,int,mak,lnt,inc";
            whitelist = utils.sortString(whitelist);
            whitelistAvailable = "true";
            props.putProperty(props.WHITELIST, whitelist);
            props.putProperty(props.WHITELISTAVAILABLE, whitelistAvailable);
        }
        if (blacklist.length() == 0) {
            dataBlacklist = new String[0];
        } else {
            dataBlacklist = blacklist.split(",");
        }

        // all elements are shown
        String tmp = "";
        for (int i=0; i < dataBlacklist.length; i++) {
            if (i == (dataBlacklist.length - 1)) {
                tmp = tmp + dataBlacklist[i];
            } else {
                tmp = tmp + dataBlacklist[i] + ", ";
            }
//            // shows only the first 7 elements
//            if (i > 5) {
//                tmp = tmp + "...";
//                break;
//            }
        }
        extensions.setText(tmp);
        extensions.setEditable(false);      // field is not editable, 
                                            // because it is auto generated!
        extensions.setDisable(true);
        
        String reviewString;
        reviewString = props.getProperty(props.REVIEW);
        if (reviewString.equals("")) {
            // initialize property "Review"
            props.putProperty(props.REVIEW, Boolean.toString(false));
        } else {
            if (Boolean.valueOf(reviewString)) {
                checkboxReview.selectedProperty().set(true);
            } else {
                checkboxReview.selectedProperty().set(false);
            }
        }

        String localGeneratedViewString;
        localGeneratedViewString = props.getProperty(props.LOCAL_GENERATED_VIEW);
        if (localGeneratedViewString.equals("")) {
            // initialize property LOCAL_GENERATED_VIEW
            props.putProperty(props.LOCAL_GENERATED_VIEW, Boolean.toString(false));
        } else {
            if (Boolean.valueOf(localGeneratedViewString)) {
                checkboxLocalGeneratedView.selectedProperty().set(true);
            } else {
                checkboxLocalGeneratedView.selectedProperty().set(false);
            }
        }

    }    
    
    private void savePreferences() {
        // save content of TextFields to own preferences
        String link;
        link = lxrLink.getText();
        props.putProperty(props.LXRLINK, link);         // save as preference
 
        String version1;
        String version2;
        version1 = lxrBaseVersion.getText();
        props.putProperty(props.LXRBASEVER, version1);  // save as preference
        version2 = lxrComparableVersion.getText();
        props.putProperty(props.LXRCOMPVER, version2);  // save as preference
        
        String diff;
        if (formatDiffVar.getText().endsWith("=")) {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText() + lxrBaseVersion.getText();
        } else {
            diff = "?" + 
                    formatVersion.getText() + lxrBaseVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVal.getText() + lxrComparableVersion.getText() + 
                    formatSeparator.getText() +
                    formatDiffVar.getText();
        }
        props.putProperty(props.LXRDIFF, diff);         // save as preference

        if (checkboxReview.isSelected()) {
            props.putProperty(props.REVIEW, Boolean.toString(true));
        } else {
            props.putProperty(props.REVIEW, Boolean.toString(false));
        }
        
        if (checkboxLocalGeneratedView.isSelected()) {
            props.putProperty(props.LOCAL_GENERATED_VIEW, Boolean.toString(true));
        } else {
            props.putProperty(props.LOCAL_GENERATED_VIEW, Boolean.toString(false));
        }
        
        // Set new values to text fields!
        // 
        // Note:
        // This step is very important. 
        // If a change is taken, then the text area wouldn't be refreshed 
        // exept in case of a new start!
        initialize(null, null);
    }
}
