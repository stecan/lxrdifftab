/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.SplashScreen;
import java.awt.geom.Rectangle2D;
import java.util.Locale;
import java.util.ResourceBundle;
import lxrdifferencetable.tools.Properties;

/**
 *
 * @author Stefan Canali
 */
public class LxrDifferenceTable extends Application {
    
    static SplashScreen mySplash;                   // instantiated by JVM we 
                                                    // use it to get graphics
    static Graphics2D splashGraphics;               // graphics context for overlay 
                                                    // of the splash image
    static Rectangle2D.Double splashTextArea;       // area where we draw the text
    static Rectangle2D.Double splashProgressArea;   // area where we draw the 
                                                    // progress bar
    static Font font;                               // used to draw our text

    static int i = 0;                               // progress counter
    static int STEP = 7;                            // 7 percent per step
    static int pctDone;                             // counter for process bar
    
    public static String ABOUT_STAGE;
    public static String FAQ_STAGE;
    public static String CONFIG_STAGE;
    public static String BLACKLIST_STAGE;
    public static String BLACKLIST_FILES_STAGE;
    public static String PROGRESS_STAGE;
    public static String SAVE_STAGE;
    public static String INVALID_PREFS_STAGE;
    public static String FILE_NOT_FOUND_STAGE;
    public static String IO_EXCEPTION_STAGE;
    public static String JDOM_EXCEPTION_STAGE;
    public static String JR_EXCEPTION_STAGE;
    public static String BACKING_STORE_EXCEPTION_STAGE;
    public static String NULL_POINTER_EXCEPTION_STAGE;
    public static String EXCEPTION_STAGE;
    public static String ERROR_MESSAGE_STAGE;
    public static String OVERWRITE_STAGE;
    public static String PERMISSION_DENIED_STAGE;
    public static String TIMEOUT_ON_CTAGS_STAGE;
    public static String NO_LXR_SERVER_STAGE;
    public static String COULD_NOT_DELETE_FILE_STAGE;
    public static String COULD_NOT_CREATE_DIRECTORY_STAGE;
    public static String DIRECTORY_NOT_EMPTY_STAGE;
    public static String PROJECT_NOT_EMPTY_STAGE;
    public static String NOT_A_DIRECTORY_STAGE;
    public static String COULD_NOT_RENAME_FILE_STAGE;
    public static String LOCKED_STAGE;
    public static String UNSAVED_MODIFICATIONS_STAGE;
    public static String REPORT_OK_STAGE;
    public static String SVN_AUTH_STAGE;
    public static String SCAN_INFO_STAGE;
    public static String DUMMY_STAGE;
    public static String EXTERNAL_TOOLS_STAGE;
    public static String SEARCH_STAGE;     // search with svn / repo
    public static String HIDDEN_STAGE;
    public static String LOCKED_XML_FILE_STAGE;
    public static String READ_ONLY_MODE_STAGE;
    public static String LOCKED_DESCRIPTION_STAGE;
    public static String REVIEW_DESCRIPTION_STAGE;
    public static String TEST_DESCRIPTION_STAGE;
    public static String BROWSE_DIFF_STAGE;
    public static String BROWSE_STAGE;
    public static String DELETE_FILE_STAGE;
    public static String LANGUAGE_STAGE;
    public static String CAPTION_RENAME_STAGE;
    public static String FILE_RENAME_STAGE;
    public static String OBSOLETE_STAGE;
    public static String NEW_DIR_STAGE;
    public static String DESC_HISTORY_STAGE;
    public static String DESC_HISTORY_STAGE1;
    public static String STATE_HISTORY_STAGE;
    public static String CAPTION_HISTORY_STAGE;
    public static String SVN_HISTORY_STAGE;
    public static String CTAGS_STAGE;
    public static String REPORT_STAGE;
    public static String MAIN_STAGE_1;
    public static String MAIN_STAGE_2;
    
    public static String[] ctrls;       // List of Controllers

    private LxrDifferenceTableController lxrDifferenceTableController;
    private LxrTestTableController lxrTestTableController;

    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;                        
    
    /**
     * Constructor
     */
    public LxrDifferenceTable() {
        props = new Properties();

        LxrDifferenceTable.ctrls = new String[] {
            // main stages
            ABOUT_STAGE                      = "About",
            FAQ_STAGE                        = "Faq",
            CONFIG_STAGE                     = "Config",
            BLACKLIST_STAGE                  = "ConfigBlacklist",
            BLACKLIST_FILES_STAGE            = "ConfigBlacklistFiles",
            PROGRESS_STAGE                   = "Progress",
            SAVE_STAGE                       = "Save",
            // exception dialog stages
            INVALID_PREFS_STAGE              = "exceptiondialogs/DialogInvalidPrefs",
            FILE_NOT_FOUND_STAGE             = "exceptiondialogs/DialogFileNotFound",
            IO_EXCEPTION_STAGE               = "exceptiondialogs/DialogIOException",
            JDOM_EXCEPTION_STAGE             = "exceptiondialogs/DialogJDOMException",
            JR_EXCEPTION_STAGE               = "exceptiondialogs/DialogJRException",
            BACKING_STORE_EXCEPTION_STAGE    = "exceptiondialogs/DialogBackingStoreException",
            NULL_POINTER_EXCEPTION_STAGE     = "exceptiondialogs/DialogNullPointerException",
            EXCEPTION_STAGE                  = "exceptiondialogs/DialogException",
            // error dialog stages
            ERROR_MESSAGE_STAGE              = "errordialogs/DialogErrorMessage",
            OVERWRITE_STAGE                  = "errordialogs/DialogOverwriteFile",
            PERMISSION_DENIED_STAGE          = "errordialogs/DialogPermissionDeniedFile",
            TIMEOUT_ON_CTAGS_STAGE           = "errordialogs/DialogTimeoutOnCtags",
            NO_LXR_SERVER_STAGE              = "errordialogs/DialogNoLxrServer",
            COULD_NOT_DELETE_FILE_STAGE      = "errordialogs/DialogCouldNotDeleteFile",
            COULD_NOT_CREATE_DIRECTORY_STAGE = "errordialogs/DialogCouldNotCreateDirectory",
            DIRECTORY_NOT_EMPTY_STAGE        = "errordialogs/DialogDirectoryNotEmpty",
            PROJECT_NOT_EMPTY_STAGE          = "errordialogs/DialogProjectNotEmpty",
            NOT_A_DIRECTORY_STAGE            = "errordialogs/DialogNotADirectory",
            COULD_NOT_RENAME_FILE_STAGE      = "errordialogs/DialogCouldNotRenameFile",
            LOCKED_STAGE                     = "errordialogs/DialogLocked",
            UNSAVED_MODIFICATIONS_STAGE      = "errordialogs/DialogUnsavedModifications",
            // normal dialog stages
            DUMMY_STAGE                      = "normaldialogs/Dummy",
            EXTERNAL_TOOLS_STAGE             = "normaldialogs/DialogExternalTools",
            SEARCH_STAGE                     = "normaldialogs/DialogSearch",
            HIDDEN_STAGE                     = "normaldialogs/DialogHidden",
            LOCKED_XML_FILE_STAGE            = "normaldialogs/DialogLockedXmlFile",
            LOCKED_DESCRIPTION_STAGE         = "normaldialogs/DialogLockedDescription",
            READ_ONLY_MODE_STAGE             = "normaldialogs/DialogReadOnlyMode",
            REVIEW_DESCRIPTION_STAGE         = "normaldialogs/DialogReviewDescription",
            TEST_DESCRIPTION_STAGE           = "normaldialogs/DialogTestDescription",
            BROWSE_DIFF_STAGE                = "normaldialogs/DialogBrowseDiff",
            BROWSE_STAGE                     = "normaldialogs/DialogBrowse",
            DELETE_FILE_STAGE                = "normaldialogs/DialogDeleteFile",
            LANGUAGE_STAGE                   = "normaldialogs/DialogLanguage",
            CAPTION_RENAME_STAGE             = "normaldialogs/DialogRenameCaption",
            FILE_RENAME_STAGE                = "normaldialogs/DialogRenameFile",
            OBSOLETE_STAGE                   = "normaldialogs/DialogObsolete",
            NEW_DIR_STAGE                    = "normaldialogs/DialogNewDir",
            STATE_HISTORY_STAGE              = "normaldialogs/DialogStateHistory",
            CAPTION_HISTORY_STAGE            = "normaldialogs/DialogCaptionHistory",
            DESC_HISTORY_STAGE               = "normaldialogs/DialogDescriptionHistory",
            DESC_HISTORY_STAGE1              = "normaldialogs/DialogDescriptionHistory1",
            SVN_HISTORY_STAGE                = "normaldialogs/DialogSvnHistory",
            CTAGS_STAGE                      = "normaldialogs/DialogTagToFile",
            REPORT_STAGE                     = "normaldialogs/DialogReport",
            REPORT_OK_STAGE                  = "normaldialogs/DialogReportOK",
            SVN_AUTH_STAGE                   = "normaldialogs/DialogSvnAuthentication",
            SCAN_INFO_STAGE                  = "normaldialogs/DialogScanInfo",
            // the two main stages
            MAIN_STAGE_2                     = "LxrTestTable",
            MAIN_STAGE_1                     = "LxrDifferenceTable"
        };
    }

    @Override
    public void start(Stage stage) throws Exception {

        StagesController mainContainer;
        mainContainer = new StagesController();

        splashInit();           // initialize splash overlay drawing parameters
        
        mainContainer.loadStage(LANGUAGE_STAGE, false); // should be initialized 
                                                        // first
        // After loading the first stage, the language settings are 
        // initialized. Though, we can load the active language.
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
        
        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initLanguage"));
        String programName = ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName");
        
        mainContainer.loadStage(PROGRESS_STAGE, false);
        
        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initConfig"));
        mainContainer.loadStage(FAQ_STAGE, false);
        mainContainer.loadStage(CONFIG_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initBlacklist"));
        mainContainer.loadStage(BLACKLIST_STAGE, false);
        mainContainer.loadStage(BLACKLIST_FILES_STAGE, false);
        
        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initError"));
        mainContainer.loadStage(SAVE_STAGE, false);
        mainContainer.loadStage(INVALID_PREFS_STAGE, false);
        mainContainer.loadStage(FILE_NOT_FOUND_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initIOException"));
        mainContainer.loadStage(IO_EXCEPTION_STAGE, false);
        mainContainer.loadStage(JDOM_EXCEPTION_STAGE, false);
        mainContainer.loadStage(JR_EXCEPTION_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initNullPointerException"));
        mainContainer.loadStage(BACKING_STORE_EXCEPTION_STAGE, false);
        mainContainer.loadStage(NULL_POINTER_EXCEPTION_STAGE, false);
        mainContainer.loadStage(EXCEPTION_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initOverwrite"));
        mainContainer.loadStage(ERROR_MESSAGE_STAGE, false);
        mainContainer.loadStage(OVERWRITE_STAGE, false);
        mainContainer.loadStage(PERMISSION_DENIED_STAGE, false);
        mainContainer.loadStage(TIMEOUT_ON_CTAGS_STAGE, false);
        mainContainer.loadStage(NO_LXR_SERVER_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initFileHandling"));
        mainContainer.loadStage(COULD_NOT_DELETE_FILE_STAGE, false);
        mainContainer.loadStage(COULD_NOT_CREATE_DIRECTORY_STAGE, false);
        mainContainer.loadStage(DIRECTORY_NOT_EMPTY_STAGE, false);
        mainContainer.loadStage(PROJECT_NOT_EMPTY_STAGE, false);
        mainContainer.loadStage(NOT_A_DIRECTORY_STAGE, false);
        mainContainer.loadStage(COULD_NOT_RENAME_FILE_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initLocking"));
        mainContainer.loadStage(LOCKED_STAGE, false);
        mainContainer.loadStage(DELETE_FILE_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initModification"));
        mainContainer.loadStage(UNSAVED_MODIFICATIONS_STAGE, false);
        mainContainer.loadStage(LOCKED_DESCRIPTION_STAGE, false);
        mainContainer.loadStage(REPORT_OK_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initReadOnly"));
        mainContainer.loadStage(READ_ONLY_MODE_STAGE, false);
        mainContainer.loadStage(LOCKED_XML_FILE_STAGE, false);
        mainContainer.loadStage(DUMMY_STAGE, false);
        mainContainer.loadStage(EXTERNAL_TOOLS_STAGE, false);
        mainContainer.loadStage(SEARCH_STAGE, false);
        mainContainer.loadStage(HIDDEN_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initDescription"));
        mainContainer.loadStage(REVIEW_DESCRIPTION_STAGE, false);
        mainContainer.loadStage(TEST_DESCRIPTION_STAGE, false);
        mainContainer.loadStage(BROWSE_DIFF_STAGE, false);
        mainContainer.loadStage(BROWSE_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initCaption"));
        mainContainer.loadStage(CAPTION_RENAME_STAGE, false);
        mainContainer.loadStage(FILE_RENAME_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initNewDir"));
        mainContainer.loadStage(OBSOLETE_STAGE, false);
        mainContainer.loadStage(NEW_DIR_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initLanguage"));
        mainContainer.loadStage(STATE_HISTORY_STAGE, false);
        mainContainer.loadStage(CAPTION_HISTORY_STAGE, false);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initDescriptionHistory"));
        mainContainer.loadStage(DESC_HISTORY_STAGE, false);
        mainContainer.loadStage(DESC_HISTORY_STAGE1, false);
        mainContainer.loadStage(SVN_HISTORY_STAGE, false);
        mainContainer.loadStage(CTAGS_STAGE, false);
        mainContainer.loadStage(REPORT_STAGE, false);
        mainContainer.loadStage(SVN_AUTH_STAGE, false);
        mainContainer.loadStage(SCAN_INFO_STAGE, false);

        // initializing title of windows
        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initDescriptionHistory"));
        // normal error stages are shown with additional title "Info"
        String title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.Info");
        mainContainer.getStage(ERROR_MESSAGE_STAGE).setTitle(programName + title);
        mainContainer.getStage(OVERWRITE_STAGE).setTitle(programName + title);
        mainContainer.getStage(PERMISSION_DENIED_STAGE).setTitle(programName + title);
        mainContainer.getStage(TIMEOUT_ON_CTAGS_STAGE).setTitle(programName + title);
        mainContainer.getStage(NO_LXR_SERVER_STAGE).setTitle(programName + title);
        mainContainer.getStage(COULD_NOT_DELETE_FILE_STAGE).setTitle(programName + title);
        mainContainer.getStage(COULD_NOT_CREATE_DIRECTORY_STAGE).setTitle(programName + title);
        mainContainer.getStage(DIRECTORY_NOT_EMPTY_STAGE).setTitle(programName + title);
        mainContainer.getStage(PROJECT_NOT_EMPTY_STAGE).setTitle(programName + title);
        mainContainer.getStage(NOT_A_DIRECTORY_STAGE).setTitle(programName + title);
        mainContainer.getStage(COULD_NOT_RENAME_FILE_STAGE).setTitle(programName + title);
        mainContainer.getStage(LOCKED_STAGE).setTitle(programName + title);
        mainContainer.getStage(UNSAVED_MODIFICATIONS_STAGE).setTitle(programName + title);
        // exceptions are shown with additional title "Exception"
        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.Exception");
        mainContainer.getStage(INVALID_PREFS_STAGE).setTitle(programName + title);
        mainContainer.getStage(FILE_NOT_FOUND_STAGE).setTitle(programName + title);
        mainContainer.getStage(IO_EXCEPTION_STAGE).setTitle(programName + title);
        mainContainer.getStage(JDOM_EXCEPTION_STAGE).setTitle(programName + title);
        mainContainer.getStage(JR_EXCEPTION_STAGE).setTitle(programName + title);
        mainContainer.getStage(BACKING_STORE_EXCEPTION_STAGE).setTitle(programName + title);
        mainContainer.getStage(NULL_POINTER_EXCEPTION_STAGE).setTitle(programName + title);
        mainContainer.getStage(EXCEPTION_STAGE).setTitle(programName + title);
        
        // 
        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.Locked");
        mainContainer.getStage(LOCKED_XML_FILE_STAGE).setTitle(programName + title);
        mainContainer.getStage(LOCKED_DESCRIPTION_STAGE).setTitle(programName + title);
        
        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.Auth");
        mainContainer.getStage(SVN_AUTH_STAGE).setTitle(programName + title);
        mainContainer.getStage(SCAN_INFO_STAGE).setTitle(programName + title);

        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.Report");
        mainContainer.getStage(REPORT_STAGE).setTitle(programName + title);
        mainContainer.getStage(REPORT_OK_STAGE).setTitle(programName + title);
        
        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.File");
        mainContainer.getStage(DELETE_FILE_STAGE).setTitle(programName + title);
        mainContainer.getStage(FILE_RENAME_STAGE).setTitle(programName + title);
        mainContainer.getStage(NEW_DIR_STAGE).setTitle(programName + title);
        
        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.Obsolete");
        mainContainer.getStage(OBSOLETE_STAGE).setTitle(programName + title);

        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.Caption");
        mainContainer.getStage(CAPTION_RENAME_STAGE).setTitle(programName + title);

        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.ReadOnly");
        mainContainer.getStage(READ_ONLY_MODE_STAGE).setTitle(programName + title);

        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.Dummy");
        mainContainer.getStage(DUMMY_STAGE).setTitle(programName + title);

        // Pre-Initialization of titles, that were set in the calling methods
        title = " - " + java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("title.Info");
        mainContainer.getStage(SEARCH_STAGE).setTitle(programName + title);
        mainContainer.getStage(HIDDEN_STAGE).setTitle(programName + title);
        mainContainer.getStage(LANGUAGE_STAGE).setTitle(programName + title);

        splashHandling(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("initMain"));
        mainContainer.loadStage(MAIN_STAGE_2, false);   // should be initialized 
                                                        // before MAIN_STAGE1!
        mainContainer.loadStage(MAIN_STAGE_1, false);   // initializes version
        mainContainer.loadStage(ABOUT_STAGE, false);    // shows version
        

        // The following command is not needed anymore!
        // The command "showStage(MAIN_STAGE_1);" is invoked in the method
        // "setStageParent" in the "LxrDifferenceTableController"-class during
        // loading!
        // mainContainer.showStage(MAIN_STAGE_1);
        
        FXMLLoader loader;
        loader = mainContainer.getLoader(MAIN_STAGE_1);
        lxrDifferenceTableController = loader.
                <LxrDifferenceTableController>getController();
        loader = mainContainer.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();

        if (mySplash != null) {     // check if we really had a spash screen
            mySplash.close();       // we're done with it
        }
    }

    @Override
    public void stop() throws Exception {
        // If an close request is made (X-Button), then all actions are handled
        // here to shutdown the application.
        if (lxrDifferenceTableController.getXmlHandling() != null) {
            lxrDifferenceTableController.getXmlHandling().releaseLockFile();
            lxrDifferenceTableController.getXmlHandling().clearOldLocks(true);
        }
        if (lxrTestTableController.isMaximizedScreen()) {
            props.putProperty(props.MAXIMIZEDSCREEN, "true");
        } else {
            props.putProperty(props.MAXIMIZEDSCREEN, "false");
        }
        Platform.exit();
        System.exit(0);        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    /**
     * This method is called from "reloadStages" from 
     * LxrDifferenceTableController. It is to reload all stages.
     * 
     * @return list of all available controllers
     */
    public static String[] getControllers () {
        return ctrls;
    }
    
    /**
     * Prepare the global variables for the other splash functions
     */
    private static void splashInit()
    {
        // the splash screen object is created by the JVM, if it is displaying 
        // a splash image
        
        mySplash = SplashScreen.getSplashScreen();
        if (mySplash != null) {
            System.out.println("Splash: OK");
        } else {
            System.out.println("Splash: NOK");
        }
        // if there are any problems displaying the splash image
        // the call to getSplashScreen will returned null

        if (mySplash != null)
        {
            // get the size of the image now being displayed
            Dimension ssDim = mySplash.getSize();
            int height = ssDim.height;
            int width = ssDim.width;
            
            if ((height > 199) && (width > 199)) {
                // REQ108 - Func/General - Splash-Screen - Minimal Size
                // 
                // Only for a splash screen greater than the minimal size, status
                // information is displayed. Otherwise, the splash screen is 
                // shown without status information.
                //
                // stake out some area for our status information
                splashTextArea = new Rectangle2D.Double(
                        16.0,               // X coordinate of this Rectangle2D
                        height - 80,        // Y coordinate of this Rectangle2D
                        width/2 - 50,       // width
                        32.);               // heigth
                splashProgressArea = new Rectangle2D.Double(
                        width/2 + 50,       // X coordinate of this Rectangle2D
                        height - 80,        // Y coordinate of this Rectangle2D
                        width/2 - 66,       // width
                        32 );               // heigth

                // create the Graphics environment for drawing status info
                splashGraphics = mySplash.createGraphics();
                font = new Font("Dialog", Font.PLAIN, 14);
                splashGraphics.setFont(font);

                // initialize the status info
                splashText("");
                splashProgress(0);
            } else {
                // Do not show status information if the splash image is too 
                // small!
            }

        }
    }
    /**
     * Display text in status area of Splash.  Note: no validation it will fit.
     * @param str - text to be displayed
     */
    public static void splashText(String str)
    {
        if (mySplash != null && mySplash.isVisible())
        {   // important to check here so no other methods need to know if there
            // really is a Splash being displayed

            // erase the last status text
            splashGraphics.setPaint(Color.LIGHT_GRAY);
            splashGraphics.fill(splashTextArea);

            // draw the text
            splashGraphics.setPaint(Color.BLACK);
            splashGraphics.drawString(str, (int)(splashTextArea.getX() + 10),
                    (int)(splashTextArea.getY() + 15));

            // make sure it's displayed
            mySplash.update();
        }
    }
    /**
     * Display a (very) basic progress bar
     * @param pct how much of the progress bar to display 0-100
     */
    public static void splashProgress(int pct)
    {
        if (mySplash != null && mySplash.isVisible())
        {

            // Note: 3 colors are used here to demonstrate steps
            // erase the old one
            splashGraphics.setPaint(Color.LIGHT_GRAY);
            splashGraphics.fill(splashProgressArea);

            // draw an outline
            splashGraphics.setPaint(Color.BLUE);
            splashGraphics.draw(splashProgressArea);

            // Calculate the width corresponding to the correct percentage
            int x = (int) splashProgressArea.getMinX();
            int y = (int) splashProgressArea.getMinY();
            int wid = (int) splashProgressArea.getWidth();
            int hgt = (int) splashProgressArea.getHeight();

            int doneWidth = Math.round(pct*wid/100.f);
            doneWidth = Math.max(0, Math.min(doneWidth, wid-1)); // limit 0-width

            // fill the done part one pixel smaller than the outline
            splashGraphics.setPaint(Color.GREEN);
            splashGraphics.fillRect(x, y+1, doneWidth, hgt-1);

            // make sure it's displayed
            mySplash.update();
        }
    }
    
    private static void splashHandling(String message) {
        pctDone = i++ * STEP;     // calculate progress
        splashText(message);      // tell the user what init task is being done
        splashProgress(pctDone);  // give them an idea how much we have completed
    }
    
 }
