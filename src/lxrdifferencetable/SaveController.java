/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import lxrdifferencetable.normaldialogs.DialogNewDirController;
import lxrdifferencetable.normaldialogs.DialogRenameFileController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import static lxrdifferencetable.LxrDifferenceTable.BACKING_STORE_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.COULD_NOT_CREATE_DIRECTORY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.COULD_NOT_DELETE_FILE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.COULD_NOT_RENAME_FILE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.DELETE_FILE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.DIRECTORY_NOT_EMPTY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.DUMMY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.ERROR_MESSAGE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.FILE_NOT_FOUND_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.FILE_RENAME_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.IO_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.LOCKED_XML_FILE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_2;
import static lxrdifferencetable.LxrDifferenceTable.NEW_DIR_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.NOT_A_DIRECTORY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.OVERWRITE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.PERMISSION_DENIED_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.PROGRESS_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.REPORT_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SAVE_STAGE;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.errordialogs.DialogErrorMessageController;
import lxrdifferencetable.normaldialogs.DialogReportController;
import lxrdifferencetable.normaldialogs.DummyController;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.XmlHandling;
import org.apache.commons.io.FilenameUtils;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class SaveController implements Initializable, ControlledStage {

    static final Logger logger = Logger.getLogger(
        LxrDifferenceTableController.class.getName());

    /**
     * Definition of local operating modes
     */
    public enum Mode {
        PREF_MODE, XML_MODE, REPORT_MODE, DISTRIBUTION_MODE
    }
    
    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;                        
    private File workingDirectory;

    private Mode mode;          // operating mode of the controller
    private File prefDir;       // e.g. "/home/stefan"
    private File reportDir;     // e.g. "/home/stefan"
    private File xmlDir;        // e.g. "/home/stefan"
    private File distribDir;    // e.g. "/home/stefan"
    private File prefFile;      // e.g. "/home/stefan/prefs.xml"
    private File reportFile;    // e.g. "/home/stefan/Review.pdf"
    private File xmlFile;       // e.g. "/home/stefan/table.xml"
    private StagesController myController;
    private List<TableEntry> changedFileList;     // list of changed files
    
    private File preselectedDistribDir;
    private File preselectedXmlDir;
    private File preselectedPrefDir;
    private File preselectedReportDir;
    private boolean localGeneratedView;

    private final Properties props;
    private final List<String> selectedDirs;
    
    private DummyController dummyController;
    private volatile boolean abort;

    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    public SaveController() {
        mode = Mode.PREF_MODE;
        selectedDirs = new ArrayList<>();
        props = new Properties();
        changedFileList = new ArrayList<>();
        workingDirectory = null;
        localGeneratedView = false;
        abort = false;
    }

    @FXML
    Label labelChoice;
    
    @FXML
    Button buttonAbort;
    
    @FXML
    Button buttonSave;

    @FXML
    Button buttonNewDirectory;
    
    @FXML
    Button buttonDeleteFile;
    
    @FXML
    Button buttonRenameFile;

    @FXML
    ChoiceBox<String> choiceBoxDir;

    @FXML
    TextField textFieldFile;
    
    @FXML
    ProgressBar progressBar;

    @FXML
    private ListView<String> dirField;

    @FXML
    private ListView<String> fileField;

    @FXML
    private void handleAbort(ActionEvent event) {
        myController.hideStage(SAVE_STAGE);
        abort = true;
    }
    
    @FXML
    private void handleSave(ActionEvent event) {
        saveSettings();
        // Do not hide the stage here! It is hidden in 
        // "saveSettings"
        // myController.hideStage(SAVESTAGE);
    }
    
    @FXML
    private void handleKey(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {        
            handleSave(null);
        }
    }
    
    @FXML
    private void handleListViewDirField(MouseEvent event) {
        String selectedItem;
        File selectedDir;
        switch (mode) {
            case DISTRIBUTION_MODE:
                selectedDir = distribDir;
                break;
            case XML_MODE:
                selectedDir = xmlDir;
                break;
            case REPORT_MODE:
                selectedDir = reportDir;
                break;
            case PREF_MODE:
                selectedDir = prefDir;
                break;
            default:
                selectedDir = prefDir;
                break;
        }
        selectedItem = dirField.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            switch (selectedItem) {
                case ".":
                    // do nothing
                    break;
                case "..":
                    // do nothing
                    break;
                default:
                    // go to selected directory
                    selectedDir = new File (selectedDir.toString() + 
                            File.separator + selectedItem);
            }
            switch (mode) {
                case XML_MODE:
                    preselectedXmlDir = selectedDir;
                    break;
                case DISTRIBUTION_MODE:
                    preselectedDistribDir = selectedDir;
                    break;
                case REPORT_MODE:
                    preselectedReportDir = selectedDir;
                    break;
                case PREF_MODE:
                    preselectedPrefDir = selectedDir;
                    break;
                default:
                    preselectedPrefDir = selectedDir;
                    break;
            }
        } else {
            preselectedXmlDir = null;
            preselectedDistribDir = null;
            preselectedReportDir = null;
            preselectedPrefDir = null;
        }
        
        if(event.getButton().equals(MouseButton.PRIMARY)){
            if(event.getClickCount() == 2){
                // double clicked!
                selectedItem = dirField.getSelectionModel().getSelectedItem();
                if (selectedItem != null) {
                    switch (selectedItem) {
                        case ".":
                            // do nothing
                            break;
                        case "..":
                            // go to parent directory
                            File parent = selectedDir.getParentFile();
                            if (parent == null) {
                                // in case of an error, set the config dir to the 
                                // root directory
                                selectedDir = new File (File.separator);
                            } else {
                                selectedDir = parent;
                            }
                            break;
                        default:
                            // go to selected directory
                            String file;
                            File f;
                            file = selectedDir.toString() + File.separator + selectedItem;
                            f = new File(file);
                            if (f.canRead()) {
                                // Read permissions are available. Directory can be opened.
                                selectedDir = f;
                            }   break;
                    }
                    switch (mode) {
                        case XML_MODE:
                            xmlDir = selectedDir;
                            break;
                        case DISTRIBUTION_MODE:
                            distribDir = selectedDir;
                            break;
                        case REPORT_MODE:
                            reportDir = selectedDir;
                            break;
                        case PREF_MODE:
                            prefDir = selectedDir;
                            break;
                        default:
                            prefDir = selectedDir;
                            break;
                    }
                    setListViews();
                    setChoiceBoxDir();
                    setChoiceBoxView();
                } else {
                    // do nothing
                }
            }
        }
    }
    
    @FXML
    private void handleListViewFilesField(MouseEvent event) {
        if (fileField.getSelectionModel().isEmpty()) {
            buttonRenameFile.setDisable(true);
            buttonDeleteFile.setDisable(true);
        } else {
            String selectedItem;

            selectedItem = fileField.getSelectionModel().getSelectedItem();
            switch (mode) {
                case XML_MODE:
                    xmlFile = new File(xmlDir + File.separator + selectedItem);
                    props.putProperty(props.LASTXMLFILE, xmlFile.getAbsolutePath());
                    break;
                case DISTRIBUTION_MODE:
                    // distribDir = new File(distribDir + File.separator + selectedItem);
                    break;
                case REPORT_MODE:
                    reportFile = new File(reportDir + File.separator + selectedItem);
                    break;
                case PREF_MODE:
                    prefFile = new File(prefDir + File.separator + selectedItem);
                    props.putProperty(props.LASTPROPERTIESFILE, prefFile.getAbsolutePath());
                    break;
                default:
                    prefFile = new File(prefDir + File.separator + selectedItem);
                    props.putProperty(props.LASTPROPERTIESFILE, prefFile.getAbsolutePath());
                    break;
            }
            setTextFieldFile();

            buttonRenameFile.setDisable(false);
            buttonDeleteFile.setDisable(false);
        }
    }

    @FXML
    public void handleRenameFile(ActionEvent event) {
        String selectedItem;
        FXMLLoader loader;
        DialogRenameFileController dialogRenameFileController;
        
        // get selected file name
        selectedItem = fileField.getSelectionModel().getSelectedItem();
        
        loader = myController.getLoader(FILE_RENAME_STAGE);
        dialogRenameFileController = loader.<DialogRenameFileController>getController();
        dialogRenameFileController.setFileName(selectedItem);
        
        File file;
        switch (mode) {
            case XML_MODE:
                file = new File (xmlDir.getAbsoluteFile() + File.separator + selectedItem);
                break;
            case PREF_MODE:
                file = new File (prefDir.getAbsoluteFile() + File.separator + selectedItem);
                break;
            case DISTRIBUTION_MODE:
                file = distribDir;
                break;
            case REPORT_MODE:
                file = new File (reportDir.getAbsoluteFile() + File.separator + selectedItem);
                break;
            default:
                file = new File (xmlDir.getAbsoluteFile() + File.separator + selectedItem);
                break;
        }
        if (canBeRenamed(file)) {
            myController.showStage(FILE_RENAME_STAGE);
        } else {
            DialogErrorMessageController dialogErrorMessageController;
            loader = myController.getLoader(ERROR_MESSAGE_STAGE);
            dialogErrorMessageController = loader.
                    <DialogErrorMessageController>getController();
            dialogErrorMessageController.setErrorMessage(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("labelCouldntRenameFileInActiveLxrDiffTabProject"));
            myController.showStage(ERROR_MESSAGE_STAGE);
        }
    }
    
    /**
     * This method is called from DialogRenameFileController.
     * It renames the selected file from fileView to the given name.
     * 
     * @param name   new file name
     */
    public void renameFile(String name) {
        boolean renamed;
        String selectedItem;
        String orgFileName;
        String newFileName;
        
        File orgFile;
        File newFile;
        
        selectedItem = fileField.getSelectionModel().getSelectedItem();

        switch (mode) {
            case XML_MODE:
                newFileName = xmlDir + File.separator + name;
                orgFileName = xmlDir + File.separator + selectedItem;
                orgFile = new File (orgFileName);
                newFile = new File (newFileName);
                renamed = orgFile.renameTo(newFile);
                if (renamed) {
                    xmlFile = newFile;
                    props.putProperty(props.LASTXMLFILE, xmlFile.getAbsolutePath());
                    setTextFieldFile();     // set new file to text field
                    setListViews();         // show new file
                } else {
                    // show message to user!
                    myController.showStage(COULD_NOT_RENAME_FILE_STAGE);
                }
                break;
            case PREF_MODE:
                newFileName = prefDir + File.separator + name;
                orgFileName = prefDir + File.separator + selectedItem;
                orgFile = new File (orgFileName);
                newFile = new File (newFileName);
                renamed = orgFile.renameTo(newFile);
                if (renamed) {
                    prefFile = newFile;
                    props.putProperty(props.LASTPROPERTIESFILE, prefFile.getAbsolutePath());
                    setTextFieldFile();     // set new file to text field
                    setListViews();         // show new file
                } else {
                    // show message to user!
                    myController.showStage(COULD_NOT_RENAME_FILE_STAGE);
                }
                break;
            case DISTRIBUTION_MODE:
                // It makes no sense to rename files in distribution mode!
                break;
            case REPORT_MODE:
                newFileName = reportDir + File.separator + name;
                orgFileName = reportDir + File.separator + selectedItem;
                orgFile = new File (orgFileName);
                newFile = new File (newFileName);
                renamed = orgFile.renameTo(newFile);
                if (renamed) {
                    reportFile = newFile;
                    props.putProperty(props.LASTREPORTFILE, reportFile.getAbsolutePath());
                    setTextFieldFile();     // set new file to text field
                    setListViews();         // show new file
                } else {
                    // show message to user!
                    myController.showStage(COULD_NOT_RENAME_FILE_STAGE);
                }
                break;
            default:
                break;
        }
    }

    @FXML
    private void handleDeleteFile () {
        String selectedItem;

        // get selected file name
        selectedItem = fileField.getSelectionModel().getSelectedItem();
        File file;
        switch (mode) {
            case XML_MODE:
                file = new File (xmlDir.getAbsoluteFile() + File.separator + selectedItem);
                break;
            case PREF_MODE:
                file = new File (prefDir.getAbsoluteFile() + File.separator + selectedItem);
                break;
            case DISTRIBUTION_MODE:
                file = distribDir;
                break;
            case REPORT_MODE:
                file = new File (reportDir.getAbsoluteFile() + File.separator + selectedItem);
                break;
            default:
                file = new File (xmlDir.getAbsoluteFile() + File.separator + selectedItem);
                break;
        }
        if (canBeDeleted(file)) {
            myController.showStage(DELETE_FILE_STAGE);
        } else {
            DialogErrorMessageController dialogErrorMessageController;
            FXMLLoader loader;
            loader = myController.getLoader(ERROR_MESSAGE_STAGE);
            dialogErrorMessageController = loader.
                    <DialogErrorMessageController>getController();
            dialogErrorMessageController.setErrorMessage(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("labelCouldntDeleteFileInActiveLxrDiffTabProject"));
            myController.showStage(ERROR_MESSAGE_STAGE);
        }
    }

    @FXML
    private void handleNewDirectory () {
        String selectedItem;
        FXMLLoader loader;
        DialogNewDirController dialogNewDirController;
        
        // get selected file name
        selectedItem = dirField.getSelectionModel().getSelectedItem();
        loader = myController.getLoader(NEW_DIR_STAGE);
        dialogNewDirController = loader.<DialogNewDirController>getController();
        dialogNewDirController.setDirName(selectedItem);
        myController.showStage(NEW_DIR_STAGE);
    }

    /**
     * This method is called from DialogRenameFileController.
     * It creates the selected directory in dirView.
     * 
     * @param name   new file name
     */
    public void CreateDirectory(String name) {
        File file;
        boolean created = true;
        
        if (name != null) {
            switch (mode) {
                case XML_MODE:
                    file = new File (xmlDir + File.separator + name);
                    created = file.mkdir();
                    break;
                case PREF_MODE:
                    file = new File (prefDir + File.separator + name);
                    created = file.mkdir();
                    break;
                case DISTRIBUTION_MODE:
                    file = new File (distribDir + File.separator + name);
                    created = file.mkdir();
                    if (created) {
                        preselectedDistribDir = file;
                    }
                    break;
                case REPORT_MODE:
                    file = new File (reportDir + File.separator + name);
                    created = file.mkdir();
                    break;
                default:
                    file = new File (prefDir + File.separator + name);
                    created = file.mkdir();
                    break;
            }
        }
        if (created) {
            // Do nothing. It's all right.
        } else {
            // show message to user.
            myController.showStage(COULD_NOT_CREATE_DIRECTORY_STAGE);
        }
        setListViews();
        dirField.getSelectionModel().select(name);
        dirField.scrollTo(name);
    }
    

    /**
     * Initializes the controller class.
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Set focus to textfield
        textFieldFile.requestFocus();
        
        // locale settings
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
        
        String localView = props.getProperty(props.LOCAL_GENERATED_VIEW);
        localGeneratedView = localView.equalsIgnoreCase("true");
        
        progressBar.setVisible(false);
        progressBar.setProgress(0.0);

        // initialize main
        String lastFile;
        String lastDistribDir;

        // Event handler for changed selections in the choicebox 
        choiceBoxDir.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                    choiceBoxDir.getSelectionModel().select((int) newValue);
                    switch (mode) {
                        case XML_MODE:
                            xmlDir = new File (selectedDirs.get((int) newValue));
                            break;
                        case REPORT_MODE:
                            reportDir = new File (selectedDirs.get((int) newValue));
                            break;
                        case DISTRIBUTION_MODE:
                            distribDir = new File (selectedDirs.get((int) newValue));
                            break;
                        case PREF_MODE:
                            prefDir = new File (selectedDirs.get((int) newValue));
                            break;
                        default:
                            prefDir = new File (selectedDirs.get((int) newValue));
                            break;
                    }
                    setListViews();
                }
            });
        
        // read properties
        switch (mode) {
            case PREF_MODE:
                lastFile = props.getProperty(props.LASTPROPERTIESFILE);
                if (lastFile.equals("") || (!(new File(lastFile)).exists())) {
                    lastFile = System.getProperty("user.home") + 
                            File.separator + "prefs.xml";
                    props.putProperty(props.LASTPROPERTIESFILE, lastFile);
                }
                prefFile = new File (lastFile);
                prefDir = prefFile.getParentFile();
                break;
            case XML_MODE:
                lastFile = props.getProperty(props.LASTXMLFILE);
                if (lastFile.equals("") || (!(new File(lastFile)).exists())) {
                    lastFile = System.getProperty("user.home") + 
                            File.separator + "Table.xml";
                    props.putProperty(props.LASTXMLFILE, lastFile);
                }
                xmlFile = new File (lastFile);
                xmlDir = xmlFile.getParentFile();
                break;
            case REPORT_MODE:
                lastFile = props.getProperty(props.LASTREPORTFILE);
                if (lastFile.equals("") || (!(new File(lastFile)).exists())) {
                    lastFile = System.getProperty("user.home") + 
                            File.separator + "Review.pdf";
                    props.putProperty(props.LASTXMLFILE, lastFile);
                }
                reportFile = new File (lastFile);
                reportDir = reportFile.getParentFile();
                break;
            case DISTRIBUTION_MODE:
                lastDistribDir = props.getProperty(props.LASTDISTRIBDIR);
                if (lastDistribDir.equals("") || (!(new File(lastDistribDir)).exists())) {
                    lastDistribDir = System.getProperty("user.home");
                    props.putProperty(props.LASTDISTRIBDIR, lastDistribDir);
                }
                distribDir = new File (lastDistribDir);
                break;
        }
        setTextFieldFile();
        setChoiceBoxDir();
        setChoiceBoxView();
        setListViews();
    }    
    
    public void saveSettings() {
        FXMLLoader loader;
        String fileString;
        File file;
        switch (mode) {
            case REPORT_MODE:
                reportFile = new File (reportDir + File.separator + 
                        textFieldFile.getText());
                props.putProperty(props.LASTREPORTFILE,
                        reportFile.toString());
                setTextFieldFile();     // set new file to text field
                final DialogReportController dialogReportController;
                loader = myController.getLoader(REPORT_STAGE);
                dialogReportController = loader.<DialogReportController>getController();
                dialogReportController.setReportFile(reportFile);
                // It's all right. Everything worked fine.
                // The stage can be closed / hidden.
                myController.hideStage(SAVE_STAGE);
                break;
            case DISTRIBUTION_MODE:
                if (preselectedDistribDir != null) {
                    distribDir = preselectedDistribDir;
                }
                if (distribDir.isDirectory()) {
                    props.putProperty(props.LASTDISTRIBDIR,
                            distribDir.getAbsolutePath());
                    File[] listFiles = distribDir.listFiles();
                    if (listFiles.length == 0) {
                        // It's ok! The choosen directory is empty and can be
                        // served to distribute the project.
                        handleDistribute();
                    } else {
                        myController.showStage(DIRECTORY_NOT_EMPTY_STAGE);
                    }
                } else {
                    myController.showStage(NOT_A_DIRECTORY_STAGE);
                }
                break;
            case PREF_MODE:
                fileString = prefDir + File.separator + textFieldFile.getText();
                String lockString;
                File lockFile;
                lockString = prefDir + File.separator + ".lock";
                lockFile = new File(lockString);
                if (lockFile.exists()) {
                    // A lock file exists. Show a message to the user, that the
                    // given file is locked.
                    myController.showStage(LOCKED_XML_FILE_STAGE);
                } else {
                    if (prefDir.canWrite()) {
                        file = new File (fileString);
                        if (file.exists()) {
                            prefFile = file;
                            props.putProperty(props.LASTPROPERTIESFILE, prefFile.getAbsolutePath());
                            setTextFieldFile();     // set new file to text field
                            myController.showStage(OVERWRITE_STAGE);
                        } else {
                            prefFile = file;
                            props.putProperty(props.LASTPROPERTIESFILE, prefFile.getAbsolutePath());
                            writeFile(fileString);
                            setTextFieldFile();     // set new file to text field
                            
                            // It's all right. Everything worked fine.
                            // The stage can be closed / hidden.
                            myController.hideStage(SAVE_STAGE);
                        }
                    } else {
                        myController.showStage(PERMISSION_DENIED_STAGE);
                    }
                    setListViews();     // refresh list view!
                }
                break;
            case XML_MODE:
                fileString = xmlDir + File.separator + textFieldFile.getText();
                lockString = xmlDir + File.separator + ".lock";
                lockFile = new File(lockString);
                if (lockFile.exists()) {
                    // A lock file exists. Show a message to the user, that the
                    // given file is locked.
                    myController.showStage(LOCKED_XML_FILE_STAGE);
                } else {
                    if (xmlDir.canWrite()) {
                        file = new File (fileString);
                        if (file.exists()) {
                            xmlFile = file;
                            props.putProperty(props.LASTXMLFILE, xmlFile.getAbsolutePath());
                            setTextFieldFile();     // set new file to text field
                            myController.showStage(OVERWRITE_STAGE);
                        } else {
                            xmlFile = file;
                            props.putProperty(props.LASTXMLFILE, xmlFile.getAbsolutePath());
                            writeXmlFile(xmlFile);
                            setTextFieldFile();     // set new file to text field
                            
                            // It's all right. Everything worked fine.
                            // The stage can be closed / hidden.
                            myController.hideStage(SAVE_STAGE);
                        }
                    } else {
                        myController.showStage(PERMISSION_DENIED_STAGE);
                    }
                    setListViews();     // refresh list view!
                }
                break;
            default:
                break;
        }
    }
    
    private void setTextFieldFile() {
        switch (mode) {
            case REPORT_MODE:
                textFieldFile.setText(reportFile.getName());
                break;
            case PREF_MODE:
                textFieldFile.setText(prefFile.getName());
                break;
            case XML_MODE:
                textFieldFile.setText(xmlFile.getName());
                break;
            case DISTRIBUTION_MODE:
                textFieldFile.setText("");
                break;
            default:
                textFieldFile.setText(prefFile.getName());
        }
    }
    
    private void setChoiceBoxDir() {
        File f;
        switch (mode) {
            case REPORT_MODE:
                f = reportDir;
                break;
            case DISTRIBUTION_MODE:
                f = distribDir;
                break;
            case XML_MODE:
                f = xmlDir;
                break;
            case PREF_MODE:
                f = prefDir;
                break;
            default:
                f = prefDir;
                break;
        }
        while (f != null) {
            String fileString = f.toString();
            if (!selectedDirs.contains(fileString)) {
                selectedDirs.add(fileString);
                choiceBoxDir.getItems().add(fileString);
            }
            f = f.getParentFile();
        }

        // For Windows: List all mounted file systems!
        File[] files = File.listRoots();
        for (File file : files) {
            if (file.isDirectory()) {
                String fileString = file.toString();
                if (!selectedDirs.contains(fileString)) {
                    selectedDirs.add(fileString);
                    choiceBoxDir.getItems().add(fileString);
                }
            }
        }
    }
    
    private void setChoiceBoxView() {
        String item;
        switch (mode) {
            case REPORT_MODE:
                item = reportDir.toString();
                break;
            case DISTRIBUTION_MODE:
                item = distribDir.toString();
                break;
            case XML_MODE:
                item = xmlDir.toString();
                break;
            case PREF_MODE:
                item = prefDir.toString();
                break;
            default:
                item = prefDir.toString();
                break;
        }
        choiceBoxDir.getSelectionModel().select(item);
    }

    private void setListViews() {
        List<String> dirList = new ArrayList<>();
        List<String> fileList = new ArrayList<>();
        File[] listOfFiles;
        File file;

        
        switch (mode) {
            case PREF_MODE:
                listOfFiles = prefDir.listFiles();
                if (listOfFiles == null) {
                    // The prefDir and prefFile seems to be deleted! Though, set 
                    // perfDir and prefFile to their default values.
                    prefFile = new File (System.getProperty("user.home") + 
                            File.separator + "prefs.xml");
                    props.putProperty(props.LASTPROPERTIESFILE, prefFile.getAbsolutePath());
                    prefDir = prefFile.getParentFile();
                    listOfFiles = prefDir.listFiles();
                }
                file = prefFile;
                break;
            case XML_MODE:
                listOfFiles = xmlDir.listFiles();
                if (listOfFiles == null) {
                    // The xmlDir and xmlFile seems to be deleted! Though, set 
                    // xmlDir and xmlFile to their default values.
                    xmlFile = new File (System.getProperty("user.home") + 
                            File.separator + "Table.xml");
                    props.putProperty(props.LASTXMLFILE, xmlFile.getAbsolutePath());
                    xmlDir = xmlFile.getParentFile();
                    listOfFiles = xmlDir.listFiles();
                }
                file = xmlFile;
                break;
            case REPORT_MODE:
                listOfFiles = reportDir.listFiles();
                if (listOfFiles == null) {
                    // The reportDir and reportFile seems to be deleted! Though, set 
                    // reportDir and reportFile to their default values.
                    reportFile = new File (System.getProperty("user.home") + 
                            File.separator + "Review.pdf");
                    props.putProperty(props.LASTREPORTFILE, reportFile.getAbsolutePath());
                    reportDir = reportFile.getParentFile();
                    listOfFiles = reportDir.listFiles();
                }
                file = reportFile;
                break;
            case DISTRIBUTION_MODE:
                listOfFiles = distribDir.listFiles();
                if (listOfFiles == null) {
                    // The distribDir seems to be deleted! Though, set distribDir to 
                    // users home directory 
                    String lastDir;
                    lastDir = System.getProperty("user.home");
                    props.putProperty(props.LASTDISTRIBDIR, lastDir);
                    distribDir = new File (lastDir);
                    listOfFiles = distribDir.listFiles();
                }
                file = distribDir;
                break;
            default:
                listOfFiles = prefDir.listFiles();
                if (listOfFiles == null) {
                    // The prefDir and prefFile seems to be deleted! Though, set 
                    // prefDir and prefFile to their default values.
                    prefFile = new File (System.getProperty("user.home") + 
                            File.separator + "prefs.xml");
                    props.putProperty(props.LASTPROPERTIESFILE, prefFile.getAbsolutePath());
                    prefDir = prefFile.getParentFile();
                    listOfFiles = prefDir.listFiles();
                }
                file = prefFile;
                break;
        }
        
        for (File f : listOfFiles) {
            if (f.isDirectory()) {
                dirList.add(f.getName());
            } else {
                fileList.add(f.getName());
            }
        }
        dirList.add(".");
        dirList.add("..");
        Collections.sort(dirList);
        Collections.sort(fileList);
        dirField.getItems().setAll(dirList);
        fileField.getItems().setAll(fileList);
        fileField.getSelectionModel().select(file.getName());
        int i = fileField.getSelectionModel().getSelectedIndex();
        fileField.getFocusModel().focus(i);
        int j = i+3;
        if (j < dirList.size()) {
            fileField.scrollTo(j);  // adjust selected row
        } else {
            fileField.scrollTo(i);  // do not adust selected row
        }
        if (fileList.contains(file.getName())) {
            buttonRenameFile.setDisable(false);
            buttonDeleteFile.setDisable(false);
        } else {
            buttonRenameFile.setDisable(true);
            buttonDeleteFile.setDisable(true);
        }
    }
    
    /**
     * This method is called from DialogOverwriteFileController.
     * 
     * If overwrite is set, then the specified properties file 
     * is overwritten, otherwise nothing is done.
     * 
     * @param overwrite
     */
    public void handleOverwrite(boolean overwrite) {
        String fileString;
        File file;

        if (overwrite) {
            switch (mode) {
                case PREF_MODE:
                    fileString = prefDir + File.separator + textFieldFile.getText();
                    writeFile(fileString);
                    props.putProperty(props.LASTPROPERTIESFILE, fileString);
                    break;
                case XML_MODE:
                    fileString = xmlDir + File.separator + textFieldFile.getText();
                    file = new File(fileString);

                    FXMLLoader loader;
                    LxrTestTableController lxrTestTableController;
                    ProgressController progressController;
                    
                    loader = myController.getLoader(MAIN_STAGE_2);
                    lxrTestTableController = loader.
                            <LxrTestTableController>getController();
                    loader = myController.getLoader(PROGRESS_STAGE);
                    progressController = loader.<ProgressController>getController();
                    
                    XmlHandling xml = new XmlHandling(progressController);
                    try {
                        xml.writeXml(file,
                                lxrTestTableController.getConfigEntry(),
                                changedFileList, 
                                lxrTestTableController.getRepoLog(),
                                false);
                        // only for testing purposes!
                        //
                        // throw new IOException("Test");
                        // throw new FileNotFoundException("Test");

                    } catch (FileNotFoundException ex) {
                        myController.showStage("FILENOTFOUNDSTAGE");
                    } catch (IOException ex) {
                        myController.showStage("IOEXCEPTIONSTAGE");
                    }
                    break;
                case DISTRIBUTION_MODE:
                case REPORT_MODE:
                    break;
                default:
                    break;
            }
            myController.hideStage(SAVE_STAGE);
        } else {
            // Do nothing! Do not leave / hide stage because
            // config file is not saved!
        }
    }
    
    /**
     * This method is called from DialogDeleteFileController and
     * deletes the selected file from "fileField".
     * 
     */
    public void deleteFile() {
        String selectedItem;
        String selectedFile;
        int selectedIndex;
        int numberOfElements;
        File file;
        boolean deleted;
        
        selectedItem = fileField.getSelectionModel().getSelectedItem();
        selectedIndex = fileField.getSelectionModel().getSelectedIndex();
        switch (mode) {
            case PREF_MODE:
                selectedFile = prefDir + File.separator + selectedItem;
                break;
            case XML_MODE:
                selectedFile = xmlDir + File.separator + selectedItem;
                break;
            case DISTRIBUTION_MODE:
                // It makes no sense to delete files in distribution mode.
                selectedFile = "";
                break;
            case REPORT_MODE:
                selectedFile = reportDir + File.separator + selectedItem;
                break;
            default:
                selectedFile = prefDir + File.separator + selectedItem;
                break;
        }
        file = new File(selectedFile);
        if (canBeDeleted(file)) {
            deleted = file.delete();                        // delete file
            if (!deleted) {
                // File could not be deleted. Show message to user.
                myController.showStage(COULD_NOT_DELETE_FILE_STAGE);
            } else {
                fileField.getItems().remove(selectedItem);  // remove file
                numberOfElements = fileField.getItems().size();
                if (selectedIndex > numberOfElements) {
                    selectedIndex = numberOfElements;
                }
                fileField.getSelectionModel().select(selectedIndex);
                selectedItem = fileField.getSelectionModel().getSelectedItem();
                switch (mode) {
                    case PREF_MODE:
                        prefFile = new File(prefDir + File.separator + 
                                selectedItem);
                        break;
                    case XML_MODE:
                        xmlFile = new File(xmlDir + File.separator + 
                                selectedItem);
                        break;
                    case REPORT_MODE:
                        reportFile = new File(reportDir + File.separator + 
                                selectedItem);
                        break;
                    case DISTRIBUTION_MODE:
                        break;
                    default:
                        prefFile = new File(prefDir + File.separator + 
                                selectedItem);
                        break;
                }
                setTextFieldFile();                         // show new config file
            }
        } else {
            FXMLLoader loader;
            DialogErrorMessageController dialogErrorMessageController;
            loader = myController.getLoader(ERROR_MESSAGE_STAGE);
            dialogErrorMessageController = loader.
                    <DialogErrorMessageController>getController();
            dialogErrorMessageController.setErrorMessage(java.util.ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString("labelCouldntDeleteFileInActiveLxrDiffTabProject"));
            myController.showStage(ERROR_MESSAGE_STAGE);
        }
    }

    /**
     * This method sets the local operation mode of the controller.
     * 
     * @param mode      local operating mode
     */
    public void setMode (Mode mode) {
        this.mode = mode;
        switch (mode) {
            case DISTRIBUTION_MODE:
                buttonSave.setText(java.util.ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("buttonOK"));
                setVisibility(false);
                break;
            case PREF_MODE:
                buttonSave.setText(java.util.ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("buttonSave"));
                setVisibility(true);
                break;
            case XML_MODE:
                buttonSave.setText(java.util.ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("buttonSave"));
                setVisibility(true);
                break;
            case REPORT_MODE:
                buttonSave.setText(java.util.ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("buttonOK"));
                setVisibility(true);
                break;
            default:
                break;
        }
        // Set focus to textfield
        textFieldFile.requestFocus();
    }
            
    /**
     * This routine gets the list of changed files and writes it to the 
     * local variable changedFileList.
     * 
     * @param changedFileList
     */
    public void setOutputList (List<TableEntry> changedFileList) {
        this.changedFileList = changedFileList;
    }
    
    private void setVisibility(boolean visible) {
        buttonDeleteFile.setVisible(visible);
        buttonRenameFile.setVisible(visible);
        labelChoice.setVisible(visible);
        textFieldFile.setVisible(visible);
    }
    
    private void writeFile(String file) {
        try {
            FileOutputStream fos;
            fos = new FileOutputStream(file);           // open output file

            // writes to output file
            (props.readProperties()).exportSubtree(fos);
            
            // only for testing purposes!
            //
            // throw new BackingStoreException("Test");
            // throw new IOException("Test");
            // throw new FileNotFoundException("Test");
            
        } catch (FileNotFoundException ex) {
            myController.showStage(FILE_NOT_FOUND_STAGE);
        } catch (IOException ex) {
            myController.showStage(IO_EXCEPTION_STAGE);
        } catch (BackingStoreException ex) {
            myController.showStage(BACKING_STORE_EXCEPTION_STAGE);
        }
    }

    public void setWorkingDirectory(File dir) {
        workingDirectory = dir;
    }
    
    private boolean canBeDeleted(File file) {
        boolean result = true;
        String lockString;
        File lockFile;
        if (file.isDirectory()) {
            lockString = file.getAbsolutePath() + File.separator + ".lock";
        } else {
            lockString = file.getParent() + File.separator + ".lock";
        }
        lockFile = new File(lockString);
        if (lockFile.exists()) {
            String extension = FilenameUtils.getExtension(file.getName());
            switch (extension) {
                case "xml": case "jar": 
                    result = false;
                    break;
                default:
                    result = true;
                    break;
            }
        }
        return result;
    }

    private boolean canBeRenamed(File file) {
        return canBeDeleted(file);
    }

    public void writeXmlFile(File xmlTable) {
        LxrTestTableController lxrTestTableController;
        ProgressController progressController;

        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();
        loader = myController.getLoader(PROGRESS_STAGE);
        progressController = loader.<ProgressController>getController();

        XmlHandling xml = new XmlHandling(progressController);
        try {
            xml.writeXml(xmlTable,
                    lxrTestTableController.getConfigEntry(),
                    changedFileList,
                    lxrTestTableController.getRepoLog(),
                    false);
            // only for testing purposes!
            //
            // throw new IOException("Test");
            // throw new FileNotFoundException("Test");

        } catch (FileNotFoundException ex) {
            myController.showStage("FILENOTFOUNDSTAGE");
        } catch (IOException ex) {
            myController.showStage("IOEXCEPTIONSTAGE");
        }
    }
    
    private void copyFile(File source, File dest) throws FileNotFoundException, IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            if (is != null) is.close();
            if (os != null)os.close();
        }
    }
    
    private Task<Boolean> task;
    
    private void handleDistribute() {
        // Worker task to handle the long lasting methodes of generating the 
        // list of changed files and of outputting to TextArea
        task = new Task<Boolean>() {
            @Override
            protected Boolean call() {
                boolean state;
                state = distribute();
                return state;
            }
        };

        // Event handler to wait for completion of worker task.
        // 
        // If the worker task completes, then the PROGRESSSTAGE is hidden.
        task.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED,
                (WorkerStateEvent t) -> {
                    // wait on completion of worker task
                    Boolean state;

                    progressBar.setVisible(false);

                    state = task.getValue();
                    // ask for state of completion
                    if (state) {
                        // it's all right!
                        FXMLLoader loader;
                        loader = myController.getLoader(DUMMY_STAGE);
                        dummyController = loader.<DummyController>getController();
                        dummyController.setDistribDir(distribDir);

                        myController.hideStage(SAVE_STAGE);         // deactivate controller!

                        // Close watcher thread of test table controller
                        FXMLLoader loader1 = myController.getLoader(MAIN_STAGE_2);
                        LxrTestTableController lxrTestTableController = 
                                loader1.<LxrTestTableController>getController();
                        lxrTestTableController.closeWatcherThread();

                        // Reload all stages!
                        FXMLLoader loader2 = myController.getLoader(MAIN_STAGE_1);
                        LxrDifferenceTableController lxrDifferenceTableController = 
                                loader2.<LxrDifferenceTableController>getController();
                        lxrDifferenceTableController.reloadStages();
                    } else {
                        // An exception occured!
                        if (abort) {
                            // do nothing, only abort
                        } else {
                            // Show the approviate message!
                            myController.showStage(EXCEPTION_STAGE);
                        }
                    }


                });

        progressBar.setVisible(true);               // init progressBar
        progressBar.setProgress(0.0);
        abort = false;                              // init abort
        myController.hideStage(EXCEPTION_STAGE);    // hide (previous shown) stage

        Thread th = new Thread(task);               // define new task
        th.setDaemon(true);                         // set task to daemon state
        th.setName("HandleDistribute");
        th.start();                                 // start task as daemon
    }
    
    private boolean distribute() {
        boolean result = true;
        try {
            // create main jar file in destination directory
            File srcJarFile = new File(workingDirectory.getPath() + 
                    File.separator + "lxrdifftab.jar");
            File destJarFile = new File(distribDir.getPath() + 
                    File.separator + "lxrdifftab.jar");
            copyFile(srcJarFile, destJarFile);
            destJarFile.setExecutable(true);
            Platform.runLater(() -> progressBar.setProgress(0.05));
            // progressBar.setProgress(0.5);

            // create bat file in destination directory
            File srcBatFile = new File(workingDirectory.getPath() + 
                    File.separator + "lxrdifftab.bat");
            File destBatFile = new File(distribDir.getPath() + 
                    File.separator + "lxrdifftab.bat");
            copyFile(srcBatFile, destBatFile);
            Platform.runLater(() -> progressBar.setProgress(0.1));
            //progressBar.setProgress(0.1);

            // create destination directries
            File destCfgDir = new File(distribDir.getPath() + 
                    File.separator + "cfg");
            destCfgDir.mkdir();
            File destLibDir = new File(distribDir.getPath() + 
                    File.separator + "lib");
            destLibDir.mkdir();
            File destLogDir = new File(distribDir.getPath() + 
                    File.separator + "log");
            destLogDir.mkdir();
            Platform.runLater(() -> progressBar.setProgress(0.15));
            // progressBar.setProgress(0.15);
            
            // copy cfg
            File srcCfgDir = new File(workingDirectory.getPath() + 
                    File.separator + "cfg");
            File[] listFiles = srcCfgDir.listFiles();
            File dest;
            String fileName;
            for (File f : listFiles) {
                fileName = f.getName();
                if (abort) {
                    break;
                }
                if (f.isFile()) {
                    dest = new File (distribDir.getPath() + 
                            File.separator + "cfg" + File.separator + 
                            fileName);
                    copyFile(f, dest);
                } else {
                    // do not copy sub directories!
                }
            }
            Platform.runLater(() -> progressBar.setProgress(0.2));
            // progressBar.setProgress(0.2);
            
            // copy lib
            File srcLibDir = new File(workingDirectory.getPath() + 
                    File.separator + "lib");
            listFiles = srcLibDir.listFiles();
            for (File f : listFiles) {
                fileName = f.getName();
                if (abort) {
                    break;
                }
                if (f.isFile()) {
                    dest = new File (distribDir.getPath() + 
                            File.separator + "lib" + File.separator + 
                            fileName);
                    copyFile(f, dest);
                } else {
                    // do not copy sub directories!
                }
            }
            Platform.runLater(() -> progressBar.setProgress(0.3));
            // progressBar.setProgress(0.3);

            // copy ReviewedSources directory if it exists and localGeneratedView is enabled
            if (localGeneratedView) {
                File srcDir = new File(workingDirectory.getPath() + 
                        "/ReviewedSources");
                File destFile = new File(distribDir.getPath() + 
                        "/ReviewedSources");
                Path sourceDir = Paths.get(srcDir.getPath());
                Path destinationDir = Paths.get(destFile.getPath());            
                Files.walk(sourceDir)
                        .forEach(sourcePath -> {
                                Path targetPath = destinationDir.resolve(sourceDir.
                                        relativize(sourcePath));
                                try {
                                    if (abort) {
                                        return;
                                    }
                                    Files.copy(sourcePath, targetPath, 
                                            StandardCopyOption.REPLACE_EXISTING);
                                    // progressBar.setProgress(0.20 + iarr[0]);
                                } catch (IOException ex) {
                                    logger.warning("IO exception on copying file " + 
                                            targetPath.toString());
                                }
                            });
            }

            Platform.runLater(() -> progressBar.setProgress(0.9));
            // progressBar.setProgress(0.9);
            // generate xml file
            File xmlTable = new File (distribDir.getAbsolutePath() + 
                    File.separator + distribDir.getName() + ".xml");
            writeXmlFile(xmlTable);
            Platform.runLater(() -> progressBar.setProgress(1.0));
            // progressBar.setProgress(0.95);
            
        } catch (IOException ex) {
            // myController.showStage(IO_EXCEPTION_STAGE);
            result = false;
        }
        if (abort) {
            result = false;
        }
        return result;
    }
}
