/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import static lxrdifferencetable.LxrDifferenceTable.PROGRESS_STAGE;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class ProgressController implements Initializable, ControlledStage {

    private StagesController myController;
    
    private volatile boolean abortTask;
    
    /**
     * Constructor
     * 
     */
    public ProgressController() {
        abortTask = false;
    }

    @FXML
    Button abort;
    
    @FXML
    ProgressBar progressBar;

    @FXML
    ProgressIndicator progressIndicator;
    
    @FXML
    Label labelCosts;       // label for message "Determining costs ..."
                            // (default: visible)
    
    @FXML
    Label labelProcessing;  // label for message "Processing data ..."
                            // (default: invisible)
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    private void handleAbort(ActionEvent event) {
        myController.hideStage(PROGRESS_STAGE);
        setAbortState(true);        // asynchronously abort 
                                    // long lasting computation
    }
    
    /**
     * This method sets the progress of the progress bar.
     * 
     * @param progress  0..1
     */
    public void setProgress(double progress) {
        if (Platform.isFxApplicationThread()) {
            progressBar.setProgress(progress);
        } else {
            Platform.runLater(() -> progressBar.setProgress(progress));
        }
    }
    
    /**
     * Sets the visibility of progress bar.
     * 
     * @param visible   true is visible
     */
    public void setProgressBarVisible(boolean visible) {
        progressBar.setVisible(visible);
    }
    
    /**
     * Sets the visibility of progress indicator.
     * 
     * @param visible   true is visible
     */
    public void setProgressIndicatorVisible(boolean visible) {
        progressIndicator.setVisible(visible);
    }
    
    /**
     * Sets the visibility of label "costs".
     *
     * @param visible   true is visible
     */
    public void setLabelCosts(boolean visible) {
        labelCosts.setVisible(visible);
    }

    /**
     * Sets the visibility of label "processing".
     *
     * @param visible   true is visible
     */
    public void setLabelProcessing(boolean visible) {
        labelProcessing.setVisible(visible);
    }

    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    /**
     * This method returns the state of the global (volatile)
     * varable "abortState"
     * 
     * @return  true    task should be aborted
     *          false   task runs
     */
    public boolean getAbortState() {
        return abortTask;
    }
    
    /**
     * This method sets the abort state.
     * 
     * @param abort
     */
    public void setAbortState(boolean abort) {
        abortTask = abort;
    }
    
}
