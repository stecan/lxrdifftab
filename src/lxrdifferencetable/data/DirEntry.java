/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.data;

import java.io.File;

/**
 * Helper class to bind different data
 *
 * @author Stefan Canali
 */
public class DirEntry {
    private final File dir1;
    private final File dir2;
    
   
    /**
     * Constructor 1
     */
    public DirEntry() {
        // empty data!
        dir1 = null;
        dir2 = null;
    }
    
    /**
     * Constructor 2
     * 
     * @param dir1
     * @param dir2
     */
    public DirEntry(File dir1, File dir2) {
        this.dir1 = dir1;
        this.dir2 = dir2;
    }
            
    /**
     * Getter for private member variable "dir1"
     *
     * @return
     */
    public File Dir1() {
        return dir1;
    }
    
    /**
     * Getter for private member variable "dir2"
     *
     * @return
     */
    public File Dir2() {
        return dir2;
    }
}

