/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.data;

import javafx.beans.property.SimpleObjectProperty;

/**
 * 
 * see: http://javafxstuff.blogspot.co.uk/2013/05/nodes-in-tabellen-combobox-textfield.html
 * 
 * @author stecan / Niels Gundermann
 */
public class ScanInfoDataModel {
    private final SimpleObjectProperty<String> description;
    private final SimpleObjectProperty<String> message1;
    
    
    public ScanInfoDataModel () {
        description = new SimpleObjectProperty<>();
        message1 = new SimpleObjectProperty<>();
    }
    
    public String getDescription() {
        return description.get();
    }

    public void setDescription(String description) {
        this.description.set(description);
    }
    
    public String getMessage1() {
        return message1.get();
    }

    public void setMessage1(String message) {
        this.message1.set(message);
    }
}
