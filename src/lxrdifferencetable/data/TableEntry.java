/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.data;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;

/**
 * Helper class to bind different data
 *
 * @author Stefan Canali
 */
public class TableEntry {
    
    // data for difference table
    private final List<List<String>> href;          // e.g. "http://pcx00251/..."
    private final List<List<String>> hrefText;      // e.g. "test.cpp"
    private final List<List<String>> lineNumber;    // e.g. "0743"

    // review results
    private final List<List<State>> reviewState;                // e.g. "PASSED"
    private final List<List<String>> reviewResAuthor;           // e.g. "stecan"
    private final List<List<String>> reviewResDate;             // e.g. "2016-02-05 13 ..."
    private final List<List<String>> reviewShortDescription;    // e.g. "OK!"
    private final List<List<String>> reviewDescription;         // e.g. "A simple test..."
    private final List<List<String>> reviewDescAuthor;          // e.g. "stecan"
    private final List<List<String>> reviewDescDate;            // e.g. "2016-02-05 13 ..."
    
    // test results
    private final List<List<State>> testState;                  // e.g. "PASSED"
    private final List<List<String>> testResAuthor;             // e.g. "stecan"
    private final List<List<String>> testResDate;               // e.g. "2016-02-05 13 ..."
    private final List<List<String>> testShortDescription;      // e.g. "OK!"
    private final List<List<String>> testDescription;           // e.g. "A simple test..."
    private final List<List<String>> testDescAuthor;            // e.g. "stecan"
    private final List<List<String>> testDescDate;              // e.g. "2016-02-05 13 ..."

    // svn data
    private final List<String> svnVersion;  // e.g. "r214831"
    private final List<String> svnDesc;     // e.g. "fixed BUG21883"
    private final List<String> svnAuthor;   // e.g. "stecan"
    private final List<String> svnDate;     // e.g. "2016-02-05 13 ..."
    private final List<Integer> dublicateEntry;

    // helper data to table view
    private final List<Integer> tableViewLineNumber;      // referenced line number in table view

    // helper data
    private String file;                    // e.g. "test.cpp"
    private String FileCount;               // e.g. "532"
    private String diff;                    // e.g. "true"
    private int xmlLockFileNumber;          // e.g. "3" from xml lock file
                                            // "1-3_ReviewResult_XRAYAdmin-PCX00135-1168.lock"
    private String loginName;               // e.g. "XRAYAdmin-PCX00135-1168"


    /**
     * Constructor
     */
    public TableEntry() {
        href = new ArrayList<>();
        hrefText = new ArrayList<> ();
        lineNumber = new ArrayList<> ();
        
        reviewState = new ArrayList<> ();
        reviewResAuthor = new ArrayList<> ();
        reviewResDate = new ArrayList<> ();
        reviewShortDescription = new ArrayList<> ();
        reviewDescription = new ArrayList<> ();
        reviewDescAuthor = new ArrayList<> ();
        reviewDescDate = new ArrayList<> ();

        testState = new ArrayList<> ();
        testResAuthor = new ArrayList<> ();
        testResDate = new ArrayList<> ();
        testShortDescription = new ArrayList<> ();
        testDescription = new ArrayList<> ();
        testDescAuthor = new ArrayList<> ();
        testDescDate = new ArrayList<> ();

        svnVersion = new ArrayList<> ();
        svnDesc = new ArrayList<> ();
        svnAuthor = new ArrayList<> ();
        svnDate = new ArrayList<> ();
        dublicateEntry = new ArrayList<> ();
        
        tableViewLineNumber = new ArrayList<> ();

        file = "";
        FileCount = "";
        diff = Boolean.toString(false);
        xmlLockFileNumber = -1;
        loginName = "";
    }
    
    /**
     * Setter for private member variable "href".
     * 
     * @param href          String
     */
    public void putHref (String href) {
        this.href.add(new ArrayList<>());   // create new list entry
        int pos = this.href.size() - 1 ;    // get position of new list entry
        this.href.get(pos).add(href);       // save "href"
    }
    
    /**
     * Setter for private member variable "href".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param href          String
     */
    public void putHref (int pos, String href) {
        while (pos >= this.href.size()) {
            this.href.add(new ArrayList<>());   // add new entry
        }
        this.href.get(pos).add(href);
    }

    /**
     * Getter for private member variable "href".
     * 
     * @return      
     */
    public List<List<String>> getHref () {
        return this.href;
    }

    /**
     * Getter for private member variable "href".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getHref (int pos) {
        if (pos < this.href.size()) {
            return this.href.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "href".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getHrefLast (int pos) {
        String value;
        if (this.href.size() <= pos) {
            value = "";
        } else if (this.href.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.href.get(pos).size() - 1;
            value = this.href.get(pos).get(last);
        }
        return value;
    }

    /**
     * Setter for private member variable "hrefText".
     * 
     * @param hrefText  String
     */
    public void putHrefText (String hrefText) {
        this.hrefText.add(new ArrayList<>());   // create new list entry
        int pos = this.hrefText.size() - 1 ;    // get position of new list entry
        this.hrefText.get(pos).add(hrefText);   // save "hrefText"
    }
    
    /**
     * Setter for private member variable "hrefText".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param hrefText      String
     */
    public void putHrefText (int pos, String hrefText) {
        while (pos >= this.hrefText.size()) {
            this.hrefText.add(new ArrayList<>());   // add new entry
        }
        this.hrefText.get(pos).add(hrefText);
    }

    /**
     * Getter for private member variable "hrefText".
     * 
     * @return      
     */
    public List<List<String>> getHrefText () {
        return this.hrefText;
    }

    /**
     * Getter for private member variable "hrefText".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getHrefText (int pos) {
        if (pos < this.hrefText.size()) {
            return this.hrefText.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "hrefText".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getHrefTextLast (int pos) {
        String value;
        if (this.hrefText.size() <= pos) {
            value = "";
        } else if (this.hrefText.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.hrefText.get(pos).size() - 1;
            value = this.hrefText.get(pos).get(last);
        }
        return value;
    }

    
    /**
     * Setter for private member variable "lineNumber".
     * 
     * @param lineNumber          String
     */
    public void putLineNumber (String lineNumber) {
        this.lineNumber.add(new ArrayList<>());   // create new list entry
        int pos = this.lineNumber.size() - 1 ;    // get position of new list entry
        this.lineNumber.get(pos).add(lineNumber); // save "lineNumber"
    }
    
    /**
     * Setter for private member variable "lineNumber".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param lineNumber          String
     */
    public void putLineNumber (int pos, String lineNumber) {
        while (pos >= this.lineNumber.size()) {
            this.lineNumber.add(new ArrayList<>());   // add new entry
        }
        this.lineNumber.get(pos).add(lineNumber);
    }

    /**
     * Directly writes the last position
     * 
     * @param pos
     * @param lineNumber
     */
    public void writeLineNumberLast (int pos, String lineNumber) {
        if (pos < this.lineNumber.size()) {
            try {
                int pos2 = this.lineNumber.get(pos).size() - 1;
                this.lineNumber.get(pos).remove(pos2);
                this.lineNumber.get(pos).add(lineNumber);
            } catch (Exception ex) {
                System.out.println("Test");
            }
        }
    }

    /**
     * Getter for private member variable "lineNumber".
     * 
     * @return      
     */
    public List<List<String>> getLineNumber () {
        return this.lineNumber;
    }

    /**
     * Getter for private member variable "lineNumber".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getLineNumber (int pos) {
        if (pos < this.lineNumber.size()) {
            return this.lineNumber.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "lineNumber".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getLineNumberLast (int pos) {
        String value;
        if (this.lineNumber.size() <= pos) {
            value = "";
        } else if (this.lineNumber.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.lineNumber.get(pos).size() - 1;
            value = this.lineNumber.get(pos).get(last);
        }
        return value;
    }

    public void clearLineNumber () {
        lineNumber.clear();
    }
    
    /**
     * Setter for private member variable "reviewState".
     * 
     * @param reviewState          String
     */
    public void putReviewState (State reviewState) {
        this.reviewState.add(new ArrayList<>());       // create new list entry
        int pos = this.reviewState.size() - 1 ;        // get position of new list entry
        this.reviewState.get(pos).add(reviewState);    // save "reviewState"
    }
    
    /**
     * Setter for private member variable "reviewState".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param reviewState          String
     */
    public void putReviewState (int pos, State reviewState) {
        while (pos >= this.reviewState.size()) {
            this.reviewState.add(new ArrayList<>());   // add new entry
        }
        this.reviewState.get(pos).add(reviewState);
    }

    /**
     * Getter for private member variable "reviewState".
     * 
     * @return      
     */
    public List<List<State>> getReviewState () {
        return this.reviewState;
    }

    /**
     * Getter for private member variable "reviewState".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<State> getReviewState (int pos) {
        if (pos < this.reviewState.size()) {
            return this.reviewState.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "reviewState".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public State getReviewStateLast (int pos) {
        State value;
        if (this.reviewState.size() <= pos) {
            value = State.EMPTY;
        } else if (this.reviewState.get(pos).isEmpty()) {
            value = State.EMPTY;
        } else {
            int last = this.reviewState.get(pos).size() - 1;
            value = this.reviewState.get(pos).get(last);
        }
        return value;
    }

    public void clearReviewState () {
        reviewState.clear();
    }
    
    /**
     * Setter for private member variable "reviewResAuthor".
     * 
     * @param reviewResAuthor          String
     */
    public void putReviewResAuthor (String reviewResAuthor) {
        this.reviewResAuthor.add(new ArrayList<>());        // create new list entry
        int pos = this.reviewResAuthor.size() - 1 ;         // get position of new list entry
        this.reviewResAuthor.get(pos).add(reviewResAuthor); // save "reviewResAuthor"
    }
    
    /**
     * Setter for private member variable "reviewResAuthor".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param reviewResAuthor          String
     */
    public void putReviewResAuthor (int pos, String reviewResAuthor) {
        while (pos >= this.reviewResAuthor.size()) {
            this.reviewResAuthor.add(new ArrayList<>());   // add new entry
        }
        this.reviewResAuthor.get(pos).add(reviewResAuthor);
    }

    /**
     * Getter for private member variable "reviewResAuthor".
     * 
     * @return      
     */
    public List<List<String>> getReviewResAuthor () {
        return this.reviewResAuthor;
    }

    /**
     * Getter for private member variable "reviewResAuthor".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getReviewResAuthor (int pos) {
        if (pos < this.reviewResAuthor.size()) {
            return this.reviewResAuthor.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "reviewResAuthor".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getReviewResAuthorLast (int pos) {
        String value;
        if (this.reviewResAuthor.size() <= pos) {
            value = "";
        } else if (this.reviewResAuthor.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.reviewResAuthor.get(pos).size() - 1;
            value = this.reviewResAuthor.get(pos).get(last);
        }
        return value;
    }

    public void clearReviewResAuthor () {
        reviewResAuthor.clear();
    }
    

    /**
     * Setter for private member variable "reviewShortDescription".
     * 
     * @param reviewShortDescription          String
     */
    public void putReviewShortDescription (String reviewShortDescription) {
        this.reviewShortDescription.add(new ArrayList<>());   // create new list entry
        int pos = this.reviewShortDescription.size() - 1 ;    // get position of new list entry
        this.reviewShortDescription.get(pos).add(reviewShortDescription);
    }
    
    /**
     * Setter for private member variable "reviewShortDescription".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param reviewShortDescription          String
     */
    public void putReviewShortDescription (int pos, String reviewShortDescription) {
        while (pos >= this.reviewShortDescription.size()) {
            this.reviewShortDescription.add(new ArrayList<>());   // add new entry
        }
        this.reviewShortDescription.get(pos).add(reviewShortDescription);
    }

    /**
     * Getter for private member variable "reviewShortDescription".
     * 
     * @return      
     */
    public List<List<String>> getReviewShortDescription () {
        return this.reviewShortDescription;
    }

    /**
     * Getter for private member variable "reviewShortDescription".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getReviewShortDescription (int pos) {
        if (pos < this.reviewShortDescription.size()) {
            return this.reviewShortDescription.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "reviewShortDescription".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getReviewShortDescriptionLast (int pos) {
        String value;
        if (this.reviewShortDescription.size() <= pos) {
            value = "";
        } else if (this.reviewShortDescription.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.reviewShortDescription.get(pos).size() - 1;
            value = this.reviewShortDescription.get(pos).get(last);
        }
        return value;
    }

    public void clearReviewShortDescription () {
        reviewShortDescription.clear();
    }

    /**
     * Setter for private member variable "reviewDescription".
     * 
     * @param reviewDescription          String
     */
    public void putReviewDescription (String reviewDescription) {
        this.reviewDescription.add(new ArrayList<>());          // create new list entry
        int pos = this.reviewDescription.size() - 1 ;           // get position of new list entry
        this.reviewDescription.get(pos).add(reviewDescription); // save "reviewDescription"
    }
    
    /**
     * Setter for private member variable "reviewDescription".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param reviewDescription          String
     */
    public void putReviewDescription (int pos, String reviewDescription) {
        while (pos >= this.reviewDescription.size()) {
            this.reviewDescription.add(new ArrayList<>());   // add new entry
        }
        this.reviewDescription.get(pos).add(reviewDescription);
    }

    /**
     * Getter for private member variable "reviewDescription".
     * 
     * @return      
     */
    public List<List<String>> getReviewDescription () {
        return this.reviewDescription;
    }

    /**
     * Getter for private member variable "reviewDescription".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getReviewDescription (int pos) {
        if (pos < this.reviewDescription.size()) {
            return this.reviewDescription.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "reviewDescription".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getReviewDescriptionLast (int pos) {
        String value;
        if (this.reviewDescription.size() <= pos) {
            value = "";
        } else if (this.reviewDescription.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.reviewDescription.get(pos).size() - 1;
            value = this.reviewDescription.get(pos).get(last);
        }
        return value;
    }

    public void clearReviewDescription () {
        reviewDescription.clear();
    }

    /**
     * Setter for private member variable "reviewDescAuthor".
     * 
     * @param reviewDescAuthor          String
     */
    public void putReviewDescAuthor (String reviewDescAuthor) {
        this.reviewDescAuthor.add(new ArrayList<>());           // create new list entry
        int pos = this.reviewDescAuthor.size() - 1 ;            // get position of new list entry
        this.reviewDescAuthor.get(pos).add(reviewDescAuthor);   // save "reviewDescAuthor"
    }
    
    /**
     * Setter for private member variable "reviewDescAuthor".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param reviewDescAuthor          String
     */
    public void putReviewDescAuthor (int pos, String reviewDescAuthor) {
        while (pos >= this.reviewDescAuthor.size()) {
            this.reviewDescAuthor.add(new ArrayList<>());   // add new entry
        }
        this.reviewDescAuthor.get(pos).add(reviewDescAuthor);
    }

    /**
     * Getter for private member variable "reviewDescAuthor".
     * 
     * @return      
     */
    public List<List<String>> getReviewDescAuthor () {
        return this.reviewDescAuthor;
    }

    /**
     * Getter for private member variable "reviewDescAuthor".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getReviewDescAuthor (int pos) {
        if (pos < this.reviewDescAuthor.size()) {
            return this.reviewDescAuthor.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "reviewDescAuthor".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getReviewDescAuthorLast (int pos) {
        String value;
        if (this.reviewDescAuthor.size() <= pos) {
            value = "";
        } else if (this.reviewDescAuthor.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.reviewDescAuthor.get(pos).size() - 1;
            value = this.reviewDescAuthor.get(pos).get(last);
        }
        return value;
    }

    public void clearReviewDescAuthor () {
        reviewDescAuthor.clear();
    }

    /**
     * Setter for private member variable "reviewDescDate".
     * 
     * @param reviewDescDate          String
     */
    public void putReviewDescDate (String reviewDescDate) {
        this.reviewDescDate.add(new ArrayList<>());         // create new list entry
        int pos = this.reviewDescDate.size() - 1 ;          // get position of new list entry
        this.reviewDescDate.get(pos).add(reviewDescDate);   // save "reviewDescDate"
    }
    
    /**
     * Setter for private member variable "reviewDescDate".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param reviewDescDate          String
     */
    public void putReviewDescDate (int pos, String reviewDescDate) {
        while (pos >= this.reviewDescDate.size()) {
            this.reviewDescDate.add(new ArrayList<>());   // add new entry
        }
        this.reviewDescDate.get(pos).add(reviewDescDate);
    }

    /**
     * Getter for private member variable "reviewDescDate".
     * 
     * @return      
     */
    public List<List<String>> getReviewDescDate () {
        return this.reviewDescDate;
    }

    /**
     * Getter for private member variable "reviewDescDate".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getReviewDescDate (int pos) {
        if (pos < this.reviewDescDate.size()) {
            return this.reviewDescDate.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "reviewDescDate".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getReviewDescDateLast (int pos) {
        String value;
        if (this.reviewDescDate.size() <= pos) {
            value = "";
        } else if (this.reviewDescDate.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.reviewDescDate.get(pos).size() - 1;
            value = this.reviewDescDate.get(pos).get(last);
        }
        return value;
    }

    public void clearReviewDescDate () {
        reviewDescDate.clear();
    }

    /**
     * Setter for private member variable "reviewResDate".
     * 
     * @param reviewResDate          String
     */
    public void putReviewResDate (String reviewResDate) {
        this.reviewResDate.add(new ArrayList<>());      // create new list entry
        int pos = this.reviewResDate.size() - 1 ;       // get position of new list entry
        this.reviewResDate.get(pos).add(reviewResDate); // save "reviewResDate"
    }
    
    /**
     * Setter for private member variable "reviewResDate".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param reviewResDate          String
     */
    public void putReviewResDate (int pos, String reviewResDate) {
        while (pos >= this.reviewResDate.size()) {
            this.reviewResDate.add(new ArrayList<>());   // add new entry
        }
        this.reviewResDate.get(pos).add(reviewResDate);
    }

    /**
     * Getter for private member variable "reviewResDate".
     * 
     * @return      
     */
    public List<List<String>> getReviewResDate () {
        return this.reviewResDate;
    }

    /**
     * Getter for private member variable "reviewResDate".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getReviewResDate (int pos) {
        if (pos < this.reviewResDate.size()) {
            return this.reviewResDate.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "reviewResDate".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getReviewResDateLast (int pos) {
        String value;
        if (this.reviewResDate.size() <= pos) {
            value = "";
        } else if (this.reviewResDate.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.reviewResDate.get(pos).size() - 1;
            value = this.reviewResDate.get(pos).get(last);
        }
        return value;
    }

    public void clearReviewResDate () {
        reviewResDate.clear();
    }


    /**
     * Setter for private member variable "testState".
     * 
     * @param testState          String
     */
    public void putTestState (State testState) {
        this.testState.add(new ArrayList<>());     // create new list entry
        int pos = this.testState.size() - 1 ;      // get position of new list entry
        this.testState.get(pos).add(testState);    // save "testState"
    }
    
    /**
     * Setter for private member variable "testState".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param testState          String
     */
    public void putTestState (int pos, State testState) {
        while (pos >= this.testState.size()) {
            this.testState.add(new ArrayList<>());   // add new entry
        }
        this.testState.get(pos).add(testState);
    }

    /**
     * Getter for private member variable "testState".
     * 
     * @return      
     */
    public List<List<State>> getTestState () {
        return this.testState;
    }

    /**
     * Getter for private member variable "testState".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<State> getTestState (int pos) {
        if (pos < this.testState.size()) {
            return this.testState.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "testState".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public State getTestStateLast (int pos) {
        State value;
        if (this.testState.size() <= pos) {
            value = State.EMPTY;
        } else if (this.testState.get(pos).isEmpty()) {
            value = State.EMPTY;
        } else {
            int last = this.testState.get(pos).size() - 1;
            value = this.testState.get(pos).get(last);
        }
        return value;
    }

    public void clearTestState () {
        testState.clear();
    }
    

    /**
     * Setter for private member variable "testResAuthor".
     * 
     * @param testResAuthor          String
     */
    public void putTestResAuthor (String testResAuthor) {
        this.testResAuthor.add(new ArrayList<>());      // create new list entry
        int pos = this.testResAuthor.size() - 1 ;       // get position of new list entry
        this.testResAuthor.get(pos).add(testResAuthor); // save "testResAuthor"
    }
    
    /**
     * Setter for private member variable "testResAuthor".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param testResAuthor          String
     */
    public void putTestResAuthor (int pos, String testResAuthor) {
        while (pos >= this.testResAuthor.size()) {
            this.testResAuthor.add(new ArrayList<>());   // add new entry
        }
        this.testResAuthor.get(pos).add(testResAuthor);
    }

    /**
     * Getter for private member variable "testResAuthor".
     * 
     * @return      
     */
    public List<List<String>> getTestResAuthor () {
        return this.testResAuthor;
    }

    /**
     * Getter for private member variable "testResAuthor".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getTestResAuthor (int pos) {
        if (pos < this.testResAuthor.size()) {
            return this.testResAuthor.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "testResAuthor".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getTestResAuthorLast (int pos) {
        String value;
        if (this.testResAuthor.size() <= pos) {
            value = "";
        } else if (this.testResAuthor.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.testResAuthor.get(pos).size() - 1;
            value = this.testResAuthor.get(pos).get(last);
        }
        return value;
    }

    public void clearTestResAuthor () {
        testResAuthor.clear();
    }

    /**
     * Setter for private member variable "testShortDescription".
     * 
     * @param testShortDescription          String
     */
    public void putTestShortDescription (String testShortDescription) {
        this.testShortDescription.add(new ArrayList<>());               // create new list entry
        int pos = this.testShortDescription.size() - 1 ;                // get position of new list entry
        this.testShortDescription.get(pos).add(testShortDescription);   // save "testShortDescription"
    }
    
    /**
     * Setter for private member variable "testShortDescription".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param testShortDescription          String
     */
    public void putTestShortDescription (int pos, String testShortDescription) {
        while (pos >= this.testShortDescription.size()) {
            this.testShortDescription.add(new ArrayList<>());   // add new entry
        }
        this.testShortDescription.get(pos).add(testShortDescription);
    }

    /**
     * Getter for private member variable "testShortDescription".
     * 
     * @return      
     */
    public List<List<String>> getTestShortDescription () {
        return this.testShortDescription;
    }

    /**
     * Getter for private member variable "testShortDescription".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getTestShortDescription (int pos) {
        if (pos < this.testShortDescription.size()) {
            return this.testShortDescription.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "testShortDescription".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getTestShortDescriptionLast (int pos) {
        String value;
        if (this.testShortDescription.size() <= pos) {
            value = "";
        } else if (this.testShortDescription.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.testShortDescription.get(pos).size() - 1;
            value = this.testShortDescription.get(pos).get(last);
        }
        return value;
    }

    public void clearTestShortDescription () {
        testShortDescription.clear();
    }


    /**
     * Setter for private member variable "testDescription".
     * 
     * @param testDescription          String
     */
    public void putTestDescription (String testDescription) {
        this.testDescription.add(new ArrayList<>());        // create new list entry
        int pos = this.testDescription.size() - 1 ;         // get position of new list entry
        this.testDescription.get(pos).add(testDescription); // save "testDescription"
    }
    
    /**
     * Setter for private member variable "testDescription".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param testDescription          String
     */
    public void putTestDescription (int pos, String testDescription) {
        while (pos >= this.testDescription.size()) {
            this.testDescription.add(new ArrayList<>());   // add new entry
        }
        this.testDescription.get(pos).add(testDescription);
    }

    /**
     * Getter for private member variable "testDescription".
     * 
     * @return      
     */
    public List<List<String>> getTestDescription () {
        return this.testDescription;
    }

    /**
     * Getter for private member variable "testDescription".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getTestDescription (int pos) {
        if (pos < this.testDescription.size()) {
            return this.testDescription.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "testDescription".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getTestDescriptionLast (int pos) {
        String value;
        if (this.testDescription.size() <= pos) {
            value = "";
        } else if (this.testDescription.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.testDescription.get(pos).size() - 1;
            value = this.testDescription.get(pos).get(last);
        }
        return value;
    }

    public void clearTestDescription () {
        testDescription.clear();
    }

    /**
     * Setter for private member variable "testDescAuthor".
     * 
     * @param testDescAuthor          String
     */
    public void putTestDescAuthor (String testDescAuthor) {
        this.testDescAuthor.add(new ArrayList<>());         // create new list entry
        int pos = this.testDescAuthor.size() - 1 ;          // get position of new list entry
        this.testDescAuthor.get(pos).add(testDescAuthor);   // save "testDescAuthor"
    }
    
    /**
     * Setter for private member variable "testDescAuthor".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param testDescAuthor          String
     */
    public void putTestDescAuthor (int pos, String testDescAuthor) {
        while (pos >= this.testDescAuthor.size()) {
            this.testDescAuthor.add(new ArrayList<>());   // add new entry
        }
        this.testDescAuthor.get(pos).add(testDescAuthor);
    }

    /**
     * Getter for private member variable "testDescAuthor".
     * 
     * @return      
     */
    public List<List<String>> getTestDescAuthor () {
        return this.testDescAuthor;
    }

    /**
     * Getter for private member variable "testDescAuthor".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getTestDescAuthor (int pos) {
        if (pos < this.testDescAuthor.size()) {
            return this.testDescAuthor.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "testDescAuthor".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getTestDescAuthorLast (int pos) {
        String value;
        if (this.testDescAuthor.size() <= pos) {
            value = "";
        } else if (this.testDescAuthor.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.testDescAuthor.get(pos).size() - 1;
            value = this.testDescAuthor.get(pos).get(last);
        }
        return value;
    }

    public void clearTestDescAuthor () {
        testDescAuthor.clear();
    }

    /**
     * Setter for private member variable "testDescDate".
     * 
     * @param testDescDate          String
     */
    public void putTestDescDate (String testDescDate) {
        this.testDescDate.add(new ArrayList<>());       // create new list entry
        int pos = this.testDescDate.size() - 1 ;        // get position of new list entry
        this.testDescDate.get(pos).add(testDescDate);   // save "testDescDate"
    }
    
    /**
     * Setter for private member variable "testDescDate".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param testDescDate          String
     */
    public void putTestDescDate (int pos, String testDescDate) {
        while (pos >= this.testDescDate.size()) {
            this.testDescDate.add(new ArrayList<>());   // add new entry
        }
        this.testDescDate.get(pos).add(testDescDate);
    }

    /**
     * Getter for private member variable "testDescDate".
     * 
     * @return      
     */
    public List<List<String>> getTestDescDate () {
        return this.testDescDate;
    }

    /**
     * Getter for private member variable "testDescDate".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getTestDescDate (int pos) {
        if (pos < this.testDescDate.size()) {
            return this.testDescDate.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "testDescDate".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getTestDescDateLast (int pos) {
        String value;
        if (this.testDescDate.size() <= pos) {
            value = "";
        } else if (this.testDescDate.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.testDescDate.get(pos).size() - 1;
            value = this.testDescDate.get(pos).get(last);
        }
        return value;
    }

    public void clearTestDescDate () {
        testDescDate.clear();
    }

    /**
     * Setter for private member variable "testResDate".
     * 
     * @param testResDate          String
     */
    public void putTestResDate (String testResDate) {
        this.testResDate.add(new ArrayList<>());    // create new list entry
        int pos = this.testResDate.size() - 1 ;     // get position of new list entry
        this.testResDate.get(pos).add(testResDate); // save "testResDate"
    }
    
    /**
     * Setter for private member variable "testResDate".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @param testResDate          String
     */
    public void putTestResDate (int pos, String testResDate) {
        while (pos >= this.testResDate.size()) {
            this.testResDate.add(new ArrayList<>());   // add new entry
        }
        this.testResDate.get(pos).add(testResDate);
    }

    /**
     * Getter for private member variable "testResDate".
     * 
     * @return      
     */
    public List<List<String>> getTestResDate () {
        return this.testResDate;
    }

    /**
     * Getter for private member variable "testResDate".
     * 
     * The method is used to administrate the history of entry "pos".
     * 
     * @param pos
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty list "" is returned.
     */
    public List<String> getTestResDate (int pos) {
        if (pos < this.testResDate.size()) {
            return this.testResDate.get(pos);
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * Getter for private member variable "testResDate".
     * Gets the last element.
     * 
     * @param pos
     * @return      
     */
    public String getTestResDateLast (int pos) {
        String value;
        if (this.testResDate.size() <= pos) {
            value = "";
        } else if (this.testResDate.get(pos).isEmpty()) {
            value = "";
        } else {
            int last = this.testResDate.get(pos).size() - 1;
            value = this.testResDate.get(pos).get(last);
        }
        return value;
    }

    public void clearTestResDate () {
        testResDate.clear();
    }


    /**
     * Setter for private member variable "svnVersion".
     * 
     * @param svnVersion    String
     */
    public void putSvnVersion (String svnVersion) {
        this.svnVersion.add(svnVersion);
    }
    
    /**
     * Getter for private member variable "svnVersion".
     * 
     * @return svnVersion   List of strings
     */
    public List<String> getSvnVersion () {
        return svnVersion;
    }
    
    /**
     * Getter for private member variable "svnVersion".
     * It returns the i-th element of svnVersion.
     * 
     * If the i-th element is not available, then an empty string
     * is returned.
     * 
     * @param i     i-th element
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty string "" is returned.
     */
    public String getSvnVersion (int i) {
        if (i < svnVersion.size()) {
            return svnVersion.get(i);
        } else {
            return "";
        }
    }
    
    /**
     * Setter for private member variable "svnDesc".
     * 
     * @param svnDesc       String
     */
    public void putSvnDesc(String svnDesc) {
        this.svnDesc.add(svnDesc);
    }
    
    /**
     * Getter for private member variable "svnDesc".
     * 
     * @return svnDesc      List of strings
     */
    public List<String> getSvnDesc() {
        return svnDesc;
    }

    /**
     * Getter for private member variable "svnDesc".
     * It returns the i-th element of svnDesc.
     * 
     * If the i-th element is not available, then an empty string
     * is returned.
     * 
     * @param i     i-th element
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty string "" is returned.
     */
    public String getSvnDesc (int i) {
        if (i < svnDesc.size()) {
            return svnDesc.get(i);
        } else {
            return "";
        }
    }
    
    
    /**
     * Setter for private member variable "svnAuthor".
     * 
     * @param svnAuthor     String
     */
    public void putSvnAuthor (String svnAuthor) {
        this.svnAuthor.add(svnAuthor);
    }
    
    /**
     * Getter for private member variable "author".
     * 
     * @return author       List of strings
     */
    public List<String> getSvnAuthor () {
        return svnAuthor;
    }
    
    /**
     * Getter for private member variable "svnAuthor".
     * It returns the i-th element of svnAuthor.
     * 
     * If the i-th element is not available, then an empty string
     * is returned.
     * 
     * @param i     i-th element
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty string "" is returned.
     */
    public String getSvnAuthor (int i) {
        if (i < svnAuthor.size()) {
            return svnAuthor.get(i);
        } else {
            return "";
        }
    }
    
    /**
     * Setter for private member variable "svnDate".
     * 
     * @param svnDate       String
     */
    public void putSvnDate (String svnDate) {
        this.svnDate.add(svnDate);
    }
    
    /**
     * Getter for private member variable "svnDate".
     * 
     * @return svnDate      List of strings
     */
    public List<String> getSvnDate () {
        return svnDate;
    }
    
    /**
     * Getter for private member variable "svnDate".
     * It returns the i-th element of svnDate.
     * 
     * If the i-th element is not available, then an empty string
     * is returned.
     * 
     * @param i     i-th element
     * @return      if the element is available, then it is returned. 
     *              Otherwise an empty string "" is returned.
     */
    public String getSvnDate (int i) {
        if (i < svnDate.size()) {
            return svnDate.get(i);
        } else {
            return "";
        }
    }
    
    /**
     * Setter for private member variable "dublicateEntry".
     * 
     * @param dublicateEntry       integer
     */
    public void putDublicateEntry (int dublicateEntry) {
        this.dublicateEntry.add(dublicateEntry);
    }
    
    /**
     * Getter for private member variable "dublicateEntry".
     * 
     * @return dublicateEntry      List of Integers
     */
    public List<Integer> getDublicateEntry () {
        return dublicateEntry;
    }
    
    /**
     * Getter for private member variable "dublicateEntry".
     * It returns the i-th element of dublicateEntry.
     * 
     * If the i-th element is not available, then an -1
     * is returned.
     * 
     * @param i     i-th element
     * @return      if the element is available, then it is returned. 
     *              Otherwise an -1 is returned.
     */
    public int getSDublicateEntry (int i) {
        if (i < dublicateEntry.size()) {
            return dublicateEntry.get(i);
        } else {
            return -1;
        }
    }

    /**
     * Setter for private member variable "file".
     * 
     * @param file          String
     */
    public void putFile(String file) {
        this.file = file;
    }
    
    /**
     * Getter for private member variable "file".
     * 
     * @return file         String
     */
    public String getFile() {
        return file;
    }

    /**
     * Setter for private member variable "FileCount".
     * 
     * @param number        String
     */
    public void putFileCount (String number) {
        this.FileCount = number;
    }
    
    /**
     * Getter for private member variable "FileCount".
     * 
     * @return number       String
     */
    public String getFileCount () {
        return FileCount;
    }
    
    /**
     * Setter for private member variable "diff".
     * 
     * @param diff          String
     */
    public void putDiff(String diff) {
        this.diff = diff;
    }
    
    /**
     * Getter for private member variable "diff".
     * 
     * @return diff         String
     */
    public String getDiff () {
        return diff;
    }

    /**
     * Setter for private member variable "xmlLockFileNumber".
     * 
     * @param xmlLockFileNumber          int
     */
    public void putXmlLockFileNumber (int xmlLockFileNumber) {
        this.xmlLockFileNumber = xmlLockFileNumber;
    }
    
    /**
     * Getter for private member variable "xmlLockFileNumber".
     * 
     * @return xmlLockFileNumber         integer
     */
    public int getXmlLockFileNumber () {
        return xmlLockFileNumber;
    }

    /**
     * Setter for private member variable "loginName".
     * 
     * @param loginName          String
     */
    public void putLoginName(String loginName) {
        this.loginName = loginName;
    }
    
    /**
     * Getter for private member variable "tableRow".
     * 
     * @return loginName         String
     */
    public String getLoginName() {
        return loginName;
    }

    
    /**
     * Setter for private member variable "tableViewLineNumber".
     * 
     * @param tableViewLineNumber          integer
     */
    public void putTableViewLineNumber (int tableViewLineNumber) {
        this.tableViewLineNumber.add(tableViewLineNumber);   // create new list entry
    }
    
    
    /**
     * Getter for private member variable "tableViewLineNumber".
     * 
     * @return tableViewLineNumber   List of integer
     */
    public List<Integer> getTableViewLineNumber () {
        return tableViewLineNumber;
    }
    
    /**
     * Getter for private member variable "tableViewLineNumber".
     * It returns the i-th element of tableViewLineNumber.
     * 
     * If the i-th element is not available, then a zero is returned
     * 
     * @param i     i-th element
     * @return      if the element is available, then it is returned. 
     *              Otherwise a zero is returned.
     */
    public int getTableViewLineNumber (int i) {
        if (i < tableViewLineNumber.size()) {
            return tableViewLineNumber.get(i);
        } else {
            return 0;
        }
    }
    
    /**
     * The enumeration "State" holds the values for the review and
     * test comboboxes
     */
    public enum State {
    
        PASSED("PASSED", new Image("lxrdifferencetable/icons/32/greenCheck.png")),
        COMMENT("COMMENT", new Image("lxrdifferencetable/icons/32/greenCheck.png")),
        NOTAPPLICABLE("N.A.", new Image("lxrdifferencetable/icons/32/greenCheck.png")),
        FAILED("FAILED", new Image("lxrdifferencetable/icons/32/errorCheck.png")),
        INCONCLUSIVE("INCONCLUSIVE", 
            new Image("lxrdifferencetable/icons/32/highImportanceCheck.png")),
        OPEN("OPEN", new Image("lxrdifferencetable/icons/32/questionCheck.png")),
        EMPTY("", null);

        private final String name;
        private final Image icon;

        State(String name, Image icon) {
          this.name = name;
          this.icon = icon;
        }

        public Image getIcon() {
          return icon;
        }

        public String getName() {
          return name;
        }

        @Override
        public String toString() {
          return name;
        }
        
        /**
         * This method converts a String to a State.
         * 
         * This method should be called in place of "valueOf", because the 
         * empty value "" is not handled right by the method "valueOf".
         * 
         * @param value     String
         * @return          State
         */
        public static State getEnum(String value) {
            for(State v : values())
                if(v.getName().equalsIgnoreCase(value)) return v;
            throw new IllegalArgumentException();
        }        
    }
}
