/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxrdifferencetable.data;

/**
 *
 * @author stecan
 */
public class EntryPosition {
    private int fileNumber;
    private int changedLineNumber;
    private int historyNumber;
    
    public EntryPosition() {
        fileNumber = 0;
        changedLineNumber = 0;
        historyNumber = 0;
    }
    
    public int getFileNumber () {
        return fileNumber;
    }
    
    public void putFileNumber (int fileNumber) {
        this.fileNumber = fileNumber;
    }
    
    public int getChangedLineNumber () {
        return changedLineNumber;
    }
    
    public void putChangedLineNumber (int changedLineNumber) {
        this.changedLineNumber = changedLineNumber;
    }

    public int getHistoryNumberr () {
        return historyNumber;
    }
    
    public void putHistoryNumber (int historyNumber) {
        this.historyNumber = historyNumber;
    }

}
