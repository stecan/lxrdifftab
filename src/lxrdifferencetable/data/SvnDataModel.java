/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.data;

import javafx.beans.property.SimpleObjectProperty;

/**
 * 
 * see: http://javafxstuff.blogspot.co.uk/2013/05/nodes-in-tabellen-combobox-textfield.html
 * 
 * @author stecan / Niels Gundermann
 */
public class SvnDataModel {
 
    private final SimpleObjectProperty<String> svnVersion;
    private final SimpleObjectProperty<String> svnDesc;
    private final SimpleObjectProperty<String> svnAuthor;
    private final SimpleObjectProperty<String> svnDate;
    
    /**
     * Constructor
     */
    public SvnDataModel(){
        svnVersion = new SimpleObjectProperty<>();
        svnDesc = new SimpleObjectProperty<>();
        svnAuthor = new SimpleObjectProperty<>();
        svnDate = new SimpleObjectProperty<>();
    }

    public String getSvnVersion() {
        return svnVersion.get();
    }

    public void setSvnVersion(String svnVersion) {
        this.svnVersion.set(svnVersion);
    }

    public String getSvnDesc() {
        return svnDesc.get();
    }

    public void setSvnDesc(String svnDesc) {
        this.svnDesc.set(svnDesc);
    }

    public String getSvnAuthor() {
        return svnAuthor.get();
    }

    public void setSvnAuthor(String svnAuthor) {
        this.svnAuthor.set(svnAuthor);
    }

    public String getSvnDate() {
        return svnDate.get();
    }

    public void setSvnDate(String svnDate) {
        this.svnDate.set(svnDate);
    }
}
