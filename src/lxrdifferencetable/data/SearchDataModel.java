/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.data;

import java.util.Locale;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;

/**
 * 
 * see: http://javafxstuff.blogspot.co.uk/2013/05/nodes-in-tabellen-combobox-textfield.html
 * 
 * @author stecan / Niels Gundermann
 */
public class SearchDataModel {
 
    private final SimpleObjectProperty<String> hrefText;        // visible!
    private final SimpleObjectProperty<String> fileCount;       // visible!
    private final SimpleObjectProperty<String> lineNumber;      // visible!
    private final SimpleObjectProperty<String> svnVersion;      // visible!
    private final SimpleObjectProperty<String> svnDesc;         // visible!
    private final SimpleObjectProperty<String> svnAuthor;       // visible!
    private final SimpleObjectProperty<String> svnDate;         // visible!
    private final SimpleObjectProperty<String> reviewState;     // visible!
    private final SimpleObjectProperty<String> reviewStateAuthor;
    private final SimpleObjectProperty<String> reviewStateDate;
    private final SimpleObjectProperty<String> reviewShortDescription;  // visible!
    private final SimpleObjectProperty<String> reviewDescriptionAuthor;
    private final SimpleObjectProperty<String> reviewDescriptionDate;
    private final SimpleObjectProperty<String> testState;       // visible!
    private final SimpleObjectProperty<String> testStateAuthor;
    private final SimpleObjectProperty<String> testStateDate;
    private final SimpleObjectProperty<String> testShortDescription;    // visible!
    private final SimpleObjectProperty<String> testDescriptionAuthor;
    private final SimpleObjectProperty<String> testDescriptionDate;
    

    /**
     * Constructor
     */
    public SearchDataModel(){
        fileCount = new SimpleObjectProperty<>();
        hrefText = new SimpleObjectProperty<>();
        lineNumber = new SimpleObjectProperty<>();
        svnVersion = new SimpleObjectProperty<>();
        svnDesc = new SimpleObjectProperty<>();
        svnAuthor = new SimpleObjectProperty<>();
        svnDate = new SimpleObjectProperty<>();
        reviewState = new SimpleObjectProperty<>();
        reviewStateAuthor = new SimpleObjectProperty<>();
        reviewStateDate = new SimpleObjectProperty<>();
        testState = new SimpleObjectProperty<>();
        testStateAuthor = new SimpleObjectProperty<>();
        testStateDate = new SimpleObjectProperty<>();
        reviewShortDescription = new SimpleObjectProperty<>();
        reviewDescriptionAuthor = new SimpleObjectProperty<>();
        reviewDescriptionDate = new SimpleObjectProperty<>();
        testShortDescription = new SimpleObjectProperty<>();
        testDescriptionAuthor = new SimpleObjectProperty<>();
        testDescriptionDate = new SimpleObjectProperty<>();
    }

    public String getReviewState() {
        return reviewState.get();
    }

    public void setReviewState(String reviewState) {
        this.reviewState.set(reviewState);
    }

    public String getReviewStateAuthor() {
        return reviewStateAuthor.get();
    }

    public void setReviewStateAuthor(String reviewStateAuthor) {
        this.reviewStateAuthor.set(reviewStateAuthor);
    }
 
    public String getReviewStateDate() {
        return reviewStateDate.get();
    }

    public void setReviewStateDate(String reviewStateDate) {
        this.reviewStateDate.set(reviewStateDate);
    }

    public String getTestState() {
        return testState.get();
    }

    public void setTestState(String testState) {
        this.testState.set(testState);
    }

    public String getTestStateAuthor() {
        return testStateAuthor.get();
    }

    public void setTestStateAuthor(String testStateAuthor) {
        this.testStateAuthor.set(testStateAuthor);
    }
 
    public String getTestStateDate() {
        return testStateDate.get();
    }

    public void setTestStateDate(String testStateDate) {
        this.testStateDate.set(testStateDate);
    }

    public String getReviewShortDescription() {
        return reviewShortDescription.get();
    }

    public void setReviewShortDescription(String reviewShortDescription, 
            Locale locale) {
        switch (reviewShortDescription) {
            case "automaticComment":
            case "automaticDublicate":
                reviewShortDescription = (ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString(reviewShortDescription));
                this.reviewShortDescription.set(reviewShortDescription);
                break;
            default:
                this.reviewShortDescription.set(reviewShortDescription);
                break;
        }
    }
 
    public String getReviewDescriptionAuthor() {
        return reviewDescriptionAuthor.get();
    }

    public void setReviewDescriptionAuthor(String reviewDescriptionAuthor) {
        this.reviewDescriptionAuthor.set(reviewDescriptionAuthor);
    }
 
    public String getReviewDescriptionDate() {
        return reviewDescriptionDate.get();
    }

    public void setReviewDescriptionDate(String reviewDescriptionDate) {
        this.reviewDescriptionDate.set(reviewDescriptionDate);
    }
    
    public String getTestShortDescription() {
        return testShortDescription.get();
    }

    public void setTestShortDescription(String testShortDescription, 
            Locale locale) {
        switch (testShortDescription) {
            case "automaticComment":
            case "automaticDublicate":
                testShortDescription = ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString(testShortDescription);
                this.testShortDescription.set(testShortDescription);
                break;
            default:
                this.testShortDescription.set(testShortDescription);
                break;
        }
    }
 
    public String getTestDescriptionAuthor() {
        return testDescriptionAuthor.get();
    }

    public void setTestDescriptionAuthor(String testDescriptionAuthor) {
        this.testDescriptionAuthor.set(testDescriptionAuthor);
    }
 
    public String getTestDescriptionDate() {
        return testDescriptionDate.get();
    }

    public void setTestDescriptionDate(String testDescriptionDate) {
        this.testDescriptionDate.set(testDescriptionDate);
    }
    
    public String getFileCount() {
        return fileCount.get();
    }

    public void setFileCount(String fileCount) {
        this.fileCount.set(fileCount);
    }

    public String getHrefText() {
        return hrefText.get();
    }

    public void setHrefText(String link) {
        this.hrefText.set(link);
    }

    public String getLineNumber() {
        return lineNumber.get();
    }

    public void setLineNumber(String lineNumber, Locale locale) {
        switch (lineNumber) {
            case "directoryDeleted":
            case "fileDeleted":
            case "newDirectory":
            case "newFile":
            case "notChanged":
            case "dublicatedFile":
            case "automaticComment":
            case "automaticDublicate":
                String str = ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString(lineNumber);
                this.lineNumber.set(str);
                
                break;
            default:
                this.lineNumber.set(lineNumber);
                break;
        }
    }

    public String getSvnVersion() {
        return svnVersion.get();
    }

    public void setSvnVersion(String svnVersion) {
        this.svnVersion.set(svnVersion);
    }

    public String getSvnDesc() {
        return svnDesc.get();
    }

    public void setSvnDesc(String svnDesc) {
        this.svnDesc.set(svnDesc);
    }

    public String getSvnAuthor() {
        return svnAuthor.get();
    }

    public void setSvnAuthor(String svnAuthor) {
        this.svnAuthor.set(svnAuthor);
    }

    public String getSvnDate() {
        return svnDate.get();
    }

    public void setSvnDate(String svnDate) {
        this.svnDate.set(svnDate);
    }
}
