/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.data;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.data.TableEntry.State;

/**
 * 
 * see: http://javafxstuff.blogspot.co.uk/2013/05/nodes-in-tabellen-combobox-textfield.html
 * 
 * @author stecan / Niels Gundermann
 */
public class TestDataModel {
 
    static final Logger logger = Logger.getLogger
        (LxrDifferenceTableController.class.getName());
    
    private Hyperlink href;
    private final SimpleObjectProperty<Text> hrefText;          // visible!
    private final SimpleObjectProperty<Integer> fileCount;      // visible!
    private final SimpleObjectProperty<String> lineNumber;      // visible!
    private final SimpleObjectProperty<String> svnVersion;      // visible!
    private final SimpleObjectProperty<String> svnDesc;         // visible!
    private final SimpleObjectProperty<String> svnAuthor;       // visible!
    private final SimpleObjectProperty<String> svnDate;         // visible!
    private final SimpleObjectProperty<ComboBox<State>> reviewState;        // visible!
    private final SimpleObjectProperty<TextField> reviewStateAuthor;
    private final SimpleObjectProperty<TextField> reviewStateDate;
    private final SimpleObjectProperty<TextField> reviewShortDescription;   // visible!
    private final SimpleObjectProperty<TextField> reviewDescriptionAuthor;
    private final SimpleObjectProperty<TextField> reviewDescriptionDate;
    private final SimpleObjectProperty<ComboBox<State>> testState;      // visible!
    private final SimpleObjectProperty<TextField> testStateAuthor;
    private final SimpleObjectProperty<TextField> testStateDate;
    private final SimpleObjectProperty<TextField> testShortDescription; // visible!
    private final SimpleObjectProperty<TextField> testDescriptionAuthor;
    private final SimpleObjectProperty<TextField> testDescriptionDate;
    
    // It's a trick! 
    // Here, a TextField is used as a lightweight HTMLEditor!
    private final SimpleObjectProperty<TextField> reviewHTMLResult;
    private final SimpleObjectProperty<TextField> testHTMLResult;

    /**
     * Constructor
     */
    public TestDataModel(){
        fileCount = new SimpleObjectProperty<>();
        href = new Hyperlink();
        hrefText = new SimpleObjectProperty<>();
        lineNumber = new SimpleObjectProperty<>();
        svnVersion = new SimpleObjectProperty<>();
        svnDesc = new SimpleObjectProperty<>();
        svnAuthor = new SimpleObjectProperty<>();
        svnDate = new SimpleObjectProperty<>();
        reviewState = new SimpleObjectProperty<>();
        reviewStateAuthor = new SimpleObjectProperty<>();
        reviewStateDate = new SimpleObjectProperty<>();
        testState = new SimpleObjectProperty<>();
        testStateAuthor = new SimpleObjectProperty<>();
        testStateDate = new SimpleObjectProperty<>();
        reviewShortDescription = new SimpleObjectProperty<>();
        reviewDescriptionAuthor = new SimpleObjectProperty<>();
        reviewDescriptionDate = new SimpleObjectProperty<>();
        testShortDescription = new SimpleObjectProperty<>();
        testDescriptionAuthor = new SimpleObjectProperty<>();
        testDescriptionDate = new SimpleObjectProperty<>();
        reviewHTMLResult = new SimpleObjectProperty<>();
        testHTMLResult = new SimpleObjectProperty<>();
    }

    public ComboBox<State> getReviewState() {
        return reviewState.get();
    }

    public void setReviewState(ComboBox<State> reviewState) {
        this.reviewState.set(reviewState);
    }

    public TextField getReviewStateAuthor() {
        return reviewStateAuthor.get();
    }

    public void setReviewStateAuthor(TextField reviewStateAuthor) {
        this.reviewStateAuthor.set(reviewStateAuthor);
    }
 
    public TextField getReviewStateDate() {
        return reviewStateDate.get();
    }

    public void setReviewStateDate(TextField reviewStateDate) {
        this.reviewStateDate.set(reviewStateDate);
    }

    public ComboBox<State> getTestState() {
        return testState.get();
    }

    public void setTestState(ComboBox<State> testState) {
        this.testState.set(testState);
    }

    public TextField getTestStateAuthor() {
        return testStateAuthor.get();
    }

    public void setTestStateAuthor(TextField testStateAuthor) {
        this.testStateAuthor.set(testStateAuthor);
    }
 
    public TextField getTestStateDate() {
        return testStateDate.get();
    }

    public void setTestStateDate(TextField testStateDate) {
        this.testStateDate.set(testStateDate);
    }

    public TextField getReviewShortDescription() {
        return reviewShortDescription.get();
    }

    public void setReviewShortDescription(TextField reviewShortDescription, 
            Locale locale) {
        switch (reviewShortDescription.getText()) {
            case "automaticComment":
            case "automaticDublicate":
                reviewShortDescription.setText(ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString(reviewShortDescription.getText()));
                this.reviewShortDescription.set(reviewShortDescription);
                break;
            default:
                this.reviewShortDescription.set(reviewShortDescription);
                break;
        }
    }
 
    public TextField getReviewDescriptionAuthor() {
        return reviewDescriptionAuthor.get();
    }

    public void setReviewDescriptionAuthor(TextField reviewDescriptionAuthor) {
        this.reviewDescriptionAuthor.set(reviewDescriptionAuthor);
    }
 
    public TextField getReviewDescriptionDate() {
        return reviewDescriptionDate.get();
    }

    public void setReviewDescriptionDate(TextField reviewDescriptionDate) {
        this.reviewDescriptionDate.set(reviewDescriptionDate);
    }
    
    public TextField getTestShortDescription() {
        return testShortDescription.get();
    }

    public void setTestShortDescription(TextField testShortDescription, 
            Locale locale) {
        switch (testShortDescription.getText()) {
            case "automaticComment":
            case "automaticDublicate":
                testShortDescription.setText(ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString(testShortDescription.getText()));
                this.testShortDescription.set(testShortDescription);
                break;
            default:
                this.testShortDescription.set(testShortDescription);
                break;
        }
    }
 
    public TextField getTestDescriptionAuthor() {
        return testDescriptionAuthor.get();
    }

    public void setTestDescriptionAuthor(TextField testDescriptionAuthor) {
        this.testDescriptionAuthor.set(testDescriptionAuthor);
    }
 
    public TextField getTestDescriptionDate() {
        return testDescriptionDate.get();
    }

    public void setTestDescriptionDate(TextField testDescriptionDate) {
        this.testDescriptionDate.set(testDescriptionDate);
    }
    
    public TextField getReviewHTMLResult() {
        return reviewHTMLResult.get();
    }

    public void setReviewHTMLResult(TextField reviewHTMLResult) {
        this.reviewHTMLResult.set(reviewHTMLResult);
    }
 
    public TextField getTestHTMLResult() {
        return testHTMLResult.get();
    }

    public void setTestHTMLResult(TextField testHTMLResult) {
        this.testHTMLResult.set(testHTMLResult);
    }
 
    public int getFileCount() {
        if (fileCount.get() != null) {
            return fileCount.get();
        } else {
            return -1;
        }
    }

    public void setFileCount(int fileCount) {
        if (fileCount == -1) {
            logger.info("TestDataModel: Wrong file count of -1!");
        }
        this.fileCount.set(fileCount);
    }

    public Hyperlink getHref() {
        return href;
    }

    public void setHref(Hyperlink link) {
        this.href = link;
    }

    public Text getHrefText() {
        return hrefText.get();
    }

    public void setHrefText(Text link) {
        this.hrefText.set(link);
        // this.hrefText.setTooltip(new Tooltip(hrefText.getText()));
    }

    public String getLineNumber() {
        if (lineNumber.get() != null) {
            return lineNumber.get();
        } else {
            return "";
        }
    }

    public void setLineNumber(String lineNumber, Locale locale) {
        switch (lineNumber) {
            case "directoryDeleted":
            case "fileDeleted":
            case "newDirectory":
            case "newFile":
            case "notChanged":
            case "dublicatedFile":
            case "automaticComment":
            case "automaticDublicate":
                this.lineNumber.set(ResourceBundle.
                        getBundle("lxrdifferencetable/Bundle", locale).
                        getString(lineNumber));
                break;
            default:
                this.lineNumber.set(lineNumber);
                break;
        }
    }

    public String getSvnVersion() {
        return svnVersion.get();
    }

    public void setSvnVersion(String svnVersion) {
        this.svnVersion.set(svnVersion);
    }

    public String getSvnDesc() {
        return svnDesc.get();
    }

    public void setSvnDesc(String svnDesc) {
        this.svnDesc.set(svnDesc);
    }

    public String getSvnAuthor() {
        return svnAuthor.get();
    }

    public void setSvnAuthor(String svnAuthor) {
        this.svnAuthor.set(svnAuthor);
    }

    public String getSvnDate() {
        return svnDate.get();
    }

    public void setSvnDate(String svnDate) {
        this.svnDate.set(svnDate);
    }
}
