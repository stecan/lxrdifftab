/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class to bind different data
 *
 * @author Stefan Canali
 */
public class ConfigEntry {
    
    // data for table
    private String svn;                         // e.g. "true"
    private String version;                     // e.g. "1.0"
    private final List<String> caption;         // e.g. "System Test V1.0"
    private final List<String> captionAuthor;   // e.g. "stecan"
    private final List<String> captionDate;     // e.g. "2016-02-05 13 ..."
    private int xmlLockFileNumber;              // e.g. "3" from xml lock file
                                                // "0-3_Caption_XRAYAdmin-PCX00135-1168.lock"
    private String loginName;                   // e.g. "XRAYAdmin-PCX00135-1168"

    /**
     * Constructor
     */
    public ConfigEntry() {
        svn = "";
        version = "";
        caption = new ArrayList<>();
        captionAuthor = new ArrayList<>();
        captionDate = new ArrayList<>();
        xmlLockFileNumber = -1;
        loginName = "";
    }
    
    /**
     * Setter for private member variable "svn".
     * 
     * @param svn           String
     */
    public void putSvn (String svn) {
        this.svn = svn;
    }
    
    /**
     * Getter for private member variable "svn".
     * 
     * @return svn          String
     */
    public String getSvn () {
        return svn;
    }
    
    /**
     * Setter for private member variable "version".
     * 
     * @param version       String
     */
    public void putVersion (String version) {
        this.version = version;
    }
    
    /**
     * Getter for private member variable "version".
     * 
     * @return version      String
     */
    public String getVersion () {
        return version;
    }

    /**
     * Setter for private member variable "caption".
     * 
     * @param caption       String
     */
    public void putCaption (String caption) {
        this.caption.add(caption);
    }
    
    /**
     * Getter for private member variable "caption".
     * 
     * @return caption      list of strings
     */
    public List<String> getCaption () {
        return caption;
    }

    /**
     * Getter for private member variable "caption".
     * 
     * @return caption      list of strings
     */
    public String getCaptionLast () {
        int size = caption.size();
        if (size > 0) {
            return caption.get(size-1);
        } else {
            return "";
        }
    }

    /**
     * Getter for private member variable "caption".
     * 
     * @param i             i-th value of list
     * @return caption      string
     */
    public String getCaption (int i) {
        if (i < caption.size()) {
            return caption.get(i);
        } else {
            return "";
        }
    }

    /**
     * Setter for private member variable "captionAuthor".
     * 
     * @param author       String
     */
    public void putCaptionAuthor (String author) {
        this.captionAuthor.add(author);
    }
    
    /**
     * Getter for private member variable "captionAuthor".
     * 
     * @return captionAuthor      list of strings
     */
    public List<String> getCaptionAuthor () {
        return captionAuthor;
    }

    /**
     * Getter for private member variable "captionAuthor".
     * 
     * @return captionAuthor      list of strings
     */
    public String getCaptionAuthorLast () {
        int size = captionAuthor.size();
        if (size > 0) {
            return captionAuthor.get(size-1);
        } else {
            return "";
        }
    }

    /**
     * Getter for private member variable "captionAuthor".
     * 
     * @param i                     i-th value of list
     * @return captionAuthor        string
     */
    public String getCaptionAuthor (int i) {
        if (i < captionAuthor.size()) {
            return captionAuthor.get(i);
        } else {
            return "";
        }
    }

    /**
     * Setter for private member variable "captionDate".
     * 
     * @param date       String
     */
    public void putCaptionDate (String date) {
        this.captionDate.add(date);
    }
    
    /**
     * Getter for private member variable "captionAuthor".
     * 
     * @return captionDate      list of strings
     */
    public List<String> getCaptionDate () {
        return captionDate;
    }

    /**
     * Getter for private member variable "captionDate".
     * 
     * @return captionDate      string
     */
    public String getCaptionDateLast () {
        int size = captionDate.size();
        if (size > 0) {
            return captionDate.get(size-1);
        } else {
            return "";
        }
    }

    /**
     * Getter for private member variable "captionDate".
     * 
     * @param i                     i-th value of list
     * @return captionDate          string
     */
    public String getCaptionDate (int i) {
        if (i < captionDate.size()) {
            return captionDate.get(i);
        } else {
            return "";
        }
    }

    /**
     * Setter for private member variable "xmlLockFileNumber".
     * 
     * @param xmlLockFileNumber          int
     */
    public void putXmlLockFileNumber (int xmlLockFileNumber) {
        this.xmlLockFileNumber = xmlLockFileNumber;
    }
    
    /**
     * Getter for private member variable "xmlLockFileNumber".
     * 
     * @return xmlLockFileNumber         integer
     */
    public int getXmlLockFileNumber () {
        return xmlLockFileNumber;
    }

    /**
     * Setter for private member variable "loginName".
     * 
     * @param loginName          String
     */
    public void putLoginName(String loginName) {
        this.loginName = loginName;
    }
    
    /**
     * Getter for private member variable "tableRow".
     * 
     * @return loginName         String
     */
    public String getLoginName() {
        return loginName;
    }
}
