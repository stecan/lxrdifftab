/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNEStableFieldS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.data;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.text.Text;

/**
 * 
 * see: http://javafxstuff.blogspot.co.uk/2013/05/nodes-in-tabellen-combobox-textfield.html
 * 
 * @author stecan / Niels Gundermann
 */
public class CtagsDataModel {
 
    private final SimpleObjectProperty<String> file;
    private final SimpleObjectProperty<Text> line;
    
    /**
     * Constructor
     */
    public CtagsDataModel(){
        file = new SimpleObjectProperty<>();
        line = new SimpleObjectProperty<>();
    }

    public String getFile() {
        return file.get();
    }

    public void setFile(String file) {
        this.file.set(file);
    }

    public Text getLine() {
        return line.get();
    }

    public void setLine(Text line) {
        this.line.set(line);
    }
}
