/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.data;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.ComboBox;
import lxrdifferencetable.data.TableEntry.State;

/**
 * 
 * see: http://javafxstuff.blogspot.co.uk/2013/05/nodes-in-tabellen-combobox-textfield.html
 * 
 * @author stecan / Niels Gundermann
 */
public class StateDataModel {
 
    private final SimpleObjectProperty<ComboBox<State>> state;
    private final SimpleObjectProperty<String> author;
    private final SimpleObjectProperty<String> date;
    
    /**
     * Constructor
     */
    public StateDataModel(){
        state = new SimpleObjectProperty<>();
        author = new SimpleObjectProperty<>();
        date = new SimpleObjectProperty<>();
    }

    public ComboBox<State> getState() {
        return state.get();
    }

    public void setState(ComboBox<State> state) {
        this.state.set(state);
    }

    public String getAuthor() {
        return author.get();
    }

    public void setAuthor(String author) {
        this.author.set(author);
    }

    public String getDate() {
        return date.get();
    }

    public void setDate(String date) {
        this.date.set(date);
    }
}
