/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.data;

import lxrdifferencetable.data.TableEntry.State;

/**
 *
 * @author Stefan Canali
 */
public final class StatisticalData {
    
    private int numberOfPages;
    private int numberOfEntries;
    private int numberOfFiles;
    private int numberOfChangedFiles;
    private int numberOfUnmodifiedFiles;
    private int numberOfDublicatedFiles;
    private int numberOfDeletedFiles;
    private int numberOfNewFiles;
    private int numberOfChanges;
    
    private int reviewPassed;
    private int reviewFailed;
    private int reviewComment;
    private int reviewInconclusive;
    private int reviewOpen;
    private int reviewNotApplicable;
    private int reviewEmpty;
    
    private int testPassed;
    private int testFailed;
    private int testComment;
    private int testInconclusive;
    private int testOpen;
    private int testNotApplicable;
    private int testEmpty;
    
    private int numberOfHiddenLines;
    
    public StatisticalData() {
        clear();
    }
    
    public void clear() {
        numberOfPages = 0;
        numberOfEntries = 0;
        numberOfFiles = 0;
        numberOfChangedFiles = 0;
        numberOfUnmodifiedFiles = 0;
        numberOfDublicatedFiles = 0;
        numberOfDeletedFiles = 0;
        numberOfNewFiles = 0;
        numberOfChanges = 0;
        
        reviewPassed = 0;
        reviewFailed = 0;
        reviewComment = 0;
        reviewInconclusive = 0;
        reviewOpen = 0;
        reviewNotApplicable = 0;
        reviewEmpty = 0;

        testPassed = 0;
        testFailed = 0;
        testComment = 0;
        testInconclusive = 0;
        testOpen = 0;
        testNotApplicable = 0;
        testEmpty = 0;
        
        numberOfHiddenLines = 0;
    }
    
    public void incrementByOneNumberOfPages() {
        numberOfPages++;
    }
    
    public void decrementByOneNumberOfPages() {
        numberOfPages--;
    }
    public int getNumberOfPages() {
        return numberOfPages;
    }
    
    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public void incrementByOneNumberOfEntries() {
        numberOfEntries++;
    }
    
    public void decrementByOneNumberOfEntries() {
        numberOfEntries--;
    }
    public int getNumberOfEntries() {
        return numberOfEntries;
    }
    
    public void incrementByOneNumberOfFiles() {
        numberOfFiles++;
    }
    
    public void decrementByOneNumberOfFiles() {
        numberOfFiles--;
    }
    public int getNumberOfFiles() {
        return numberOfFiles;
    }
    
    public void incrementByOneNumberOfChangedFiles() {
        numberOfChangedFiles++;
    }
    
    public void decrementByOneNumberOfChangedFiles() {
        numberOfChangedFiles--;
    }
    public int getNumberOfChangedFiles() {
        return numberOfChangedFiles;
    }
    
    public void incrementByOneNumberOfUnmodifiedFiles() {
        numberOfUnmodifiedFiles++;
    }
    
    public void decrementByOneNumberOfUnmodifiedFiles() {
        numberOfUnmodifiedFiles--;
    }
    
    public int getNumberOfUnmodifiedFiles() {
        return numberOfUnmodifiedFiles;
    }

    public void incrementByOneNumberOfDublicatedFiles() {
        numberOfDublicatedFiles++;
    }
    
    public void decrementByOneNumberOfDublicatedFiles() {
        numberOfDublicatedFiles--;
    }
    
    public int getNumberOfDublicatedFiles() {
        return numberOfDublicatedFiles;
    }

    public void setNumberOfDublicatedFiles(int number) {
        numberOfDublicatedFiles = number;
    }

    public void incrementByOneNumberOfDeletedFiles() {
        numberOfDeletedFiles++;
    }
    
    public void decrementByOneNumberOfDeletedFiles() {
        numberOfDeletedFiles--;
    }
    
    public int getNumberOfDeletedFiles() {
        return numberOfDeletedFiles;
    }

    public void incrementByOneNumberOfNewFiles() {
        numberOfNewFiles++;
    }
    
    public void decrementByOneNumberOfNewFiles() {
        numberOfNewFiles--;
    }
    
    public int getNumberOfNewFiles() {
        return numberOfNewFiles;
    }
    
    public void incrementByOneNumberOfChanges() {
        numberOfChanges++;
    }
    
    public void decrementByOneNumberOfChanges() {
        numberOfChanges--;
    }
    public int getNumberOfChanges() {
        return numberOfChanges;
    }
    
    public void incrementByOneReviewPassed() {
        reviewPassed++;
    }
    
    public void decrementByOneReviewPassed() {
        reviewPassed--;
    }
    
    public int getReviewPassed() {
        return reviewPassed;
    }
    
    public void incrementByOneReviewFailed() {
        reviewFailed++;
    }
    
    public void decrementByOneReviewFailed() {
        reviewFailed--;
    }
    
    public int getReviewFailed() {
        return reviewFailed;
    }
    
    public void incrementByOneReviewComment() {
        reviewComment++;
    }
    
    public void decrementByOneReviewComment() {
        reviewComment--;
    }
    
    public int getReviewComment() {
        return reviewComment;
    }
    
    public void incrementByOneReviewInconclusive() {
        reviewInconclusive++;
    }
    
    public void decrementByOneReviewInconclusive() {
        reviewInconclusive--;
    }
    
    public int getReviewInconclusive() {
        return reviewInconclusive;
    }
    
    public void incrementByOneReviewOpen() {
        reviewOpen++;
    }
    
    public void decrementByOneReviewOpen() {
        reviewOpen--;
    }
    
    public int getReviewOpen() {
        return reviewOpen;
    }
    
    public void incrementByOneReviewNotApplicable() {
        reviewNotApplicable++;
    }
    
    public void decrementByOneReviewNotApplicable() {
        reviewNotApplicable--;
    }
    
    public int getReviewNotApplicable() {
        return reviewNotApplicable;
    }
    
    public void incrementByOneReviewEmpty() {
        reviewEmpty++;
    }
    
    public void decrementByOneReviewEmpty() {
        reviewEmpty--;
    }
    
    public int getReviewEmpty() {
        return reviewEmpty;
    }
    
    public void incrementByOneTestPassed() {
        testPassed++;
    }
    
    public void decrementByOneTestPassed() {
        testPassed--;
    }
    
    public int getTestPassed() {
        return testPassed;
    }
    
    public void incrementByOneTestFailed() {
        testFailed++;
    }
    
    public void decrementByOneTestFailed() {
        testFailed--;
    }
    
    public int getTestFailed() {
        return testFailed;
    }
    
    public void incrementByOneTestComment() {
        testComment++;
    }
    
    public void decrementByOneTestComment() {
        testComment--;
    }
    
    public int getTestComment() {
        return testComment;
    }
    
    public void incrementByOneTestInconclusive() {
        testInconclusive++;
    }
    
    public void decrementByOneTestInconclusive() {
        testInconclusive--;
    }
    
    public int getTestInconclusive() {
        return testInconclusive;
    }
    
    public void incrementByOneTestOpen() {
        testOpen++;
    }
    
    public void decrementByOneTestOpen() {
        testOpen--;
    }
    
    public int getTestOpen() {
        return testOpen;
    }

    public void incrementByOneTestNotApplicable() {
        testNotApplicable++;
    }
    
    public void decrementByOneTestNotApplicable() {
        testNotApplicable--;
    }
    
    public int getTestNotApplicable() {
        return testNotApplicable;
    }

    public void incrementByOneTestEmpty() {
        testEmpty++;
    }
    
    public void decrementByOneTestEmpty() {
        testEmpty--;
    }
    
    public int getTestEmpty() {
        return testEmpty;
    }

    public void updateReview(State newValue, State oldValue) {
        if (newValue == oldValue) {
            // do nothing
        } else {
            switch (newValue) {
                case PASSED:
                    incrementByOneReviewPassed();
                    break;
                case FAILED:
                    incrementByOneReviewFailed();
                    break;
                case COMMENT:
                    incrementByOneReviewComment();
                    break;
                case INCONCLUSIVE:
                    incrementByOneReviewInconclusive();
                    break;
                case OPEN:
                    incrementByOneReviewOpen();
                    break;
                case NOTAPPLICABLE:
                    incrementByOneReviewNotApplicable();
                    break;
                case EMPTY:
                    incrementByOneReviewEmpty();
                    break;
                default:
                    break;
            }
            switch (oldValue) {
                case PASSED:
                    decrementByOneReviewPassed();
                    break;
                case FAILED:
                    decrementByOneReviewFailed();
                    break;
                case COMMENT:
                    decrementByOneReviewComment();
                    break;
                case INCONCLUSIVE:
                    decrementByOneReviewInconclusive();
                    break;
                case OPEN:
                    decrementByOneReviewOpen();
                    break;
                case NOTAPPLICABLE:
                    decrementByOneReviewNotApplicable();
                    break;
                case EMPTY:
                    decrementByOneReviewEmpty();
                    break;
                default:
                    break;
            }
        }
    }

    public void updateTest(State newValue, State oldValue) {
        if (newValue == oldValue) {
            // do nothing
        } else {
            switch (newValue) {
                case PASSED:
                    incrementByOneTestPassed();
                    break;
                case FAILED:
                    incrementByOneTestFailed();
                    break;
                case COMMENT:
                    incrementByOneTestComment();
                    break;
                case INCONCLUSIVE:
                    incrementByOneTestInconclusive();
                    break;
                case OPEN:
                    incrementByOneTestOpen();
                    break;
                case NOTAPPLICABLE:
                    incrementByOneTestNotApplicable();
                    break;
                case EMPTY:
                    incrementByOneTestEmpty();
                    break;
                default:
                    break;
            }
            switch (oldValue) {
                case PASSED:
                    decrementByOneTestPassed();
                    break;
                case FAILED:
                    decrementByOneTestFailed();
                    break;
                case COMMENT:
                    decrementByOneTestComment();
                    break;
                case INCONCLUSIVE:
                    decrementByOneTestInconclusive();
                    break;
                case OPEN:
                    decrementByOneTestOpen();
                    break;
                case NOTAPPLICABLE:
                    incrementByOneTestNotApplicable();
                    break;
                case EMPTY:
                    decrementByOneTestEmpty();
                    break;
                default:
                    break;
            }
        }
    }

    public void incrementNumberOfHiddenLines() {
        numberOfHiddenLines++;
    }
    
    public void decrementNumberOfHiddenLines() {
        numberOfHiddenLines--;
    }
    public int getNumberOfHiddenLines() {
        return numberOfHiddenLines;
    }
    
    public void setNumberOfHiddenLines(int numberOfHiddenLines) {
        this.numberOfHiddenLines = numberOfHiddenLines;
    }
}
