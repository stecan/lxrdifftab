/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.data;

/**
 *
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class LogEntry {
    
    private String description;     // general description
    private String message1;        // message for repo1
    private String message2;        // message for repo2
    private String message3;        // message
    private String message4;        // message
    
    /**
     * Constructor 1
     */
    public LogEntry() {
        // empty data!
        description = "";
        message1 = "";
        message2 = "";
        message3 = "";
        message4 = "";
    }
    
    /**
     * Constructor 2
     * @param description
     * @param message1
     * @param message2
     * @param message3
     * @param message4
     */
    public LogEntry(String description, 
            String message1, String message2, String message3, 
            String message4) {
        // empty data!
        this.description = description;
        this.message1 = message1;
        this.message2 = message2;
        this.message3 = message3;
        this.message4 = message4;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getMessage1() {
        return message1;
    }
    
    public void setMessage1(String message) {
        this.message1 = message;
    }
    
    public String getMessage2() {
        return message2;
    }
    
    public void setMessage2(String message) {
        this.message2 = message;
    }
    
    public String getMessage3() {
        return message3;
    }
    
    public void setMessage3(String message) {
        this.message3 = message;
    }
    
    public String getMessage4() {
        return message4;
    }
    
    public void setMessage4(String message) {
        this.message4 = message;
    }
    
}
