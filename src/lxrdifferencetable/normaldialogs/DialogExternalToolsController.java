/*
 * Copyright (C) 2018 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.normaldialogs;

import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.EXTERNAL_TOOLS_STAGE;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.tools.Properties;

/**
 * FXML Controller class
 *
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class DialogExternalToolsController implements Initializable, ControlledStage {

    
    private StagesController myController;
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;
    private String country;
    private File gitFile;
    private File gitDir;
    private File ctagsFile;
    private File ctagsDir;
    

    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }

    public DialogExternalToolsController() {
        this.props = new Properties();
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // locale settings
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
        
        String str;
        str = props.getProperty(props.EXTERNALTOOL_GIT);
        if ("".equals(str)) {
            gitDir = new File(System.getProperty("user.home"));
            gitFile = null;
        } else {
            File file = new File (str);
            if (file.exists()) {
                if (file.isDirectory()) {
                    gitDir = file;
                    gitFile = null;
                } else {
                    gitDir = file.getParentFile();
                    gitFile = file;
                    buttonPathToGit.setText(gitFile.getPath());
                }
            }
        }
        str = props.getProperty(props.EXTERNALTOOL_CTAGS);
        if ("".equals(str)) {
            ctagsDir = new File(System.getProperty("user.home"));
            ctagsFile = null;
        } else {
            File file = new File (str);
            if (file.exists()) {
                if (file.isDirectory()) {
                    ctagsDir = file;
                    ctagsFile = null;
                } else {
                    ctagsDir = file.getParentFile();
                    ctagsFile = file;
                    buttonPathToCtags.setText(ctagsFile.getPath());
                }
            }
        }
        labelWarningNotAnApp.setVisible(false);
    }    
    
    @FXML
    Button buttonPathToGit;
    
    @FXML
    Button buttonPathToCtags;
    
    @FXML
    Label labelWarningNotAnApp;

    @FXML
    private void handleOK(ActionEvent event) {
        if (gitFile != null) {
            props.putProperty(props.EXTERNALTOOL_GIT, gitFile.getAbsolutePath());
        }
        if (ctagsFile != null) {
            props.putProperty(props.EXTERNALTOOL_CTAGS, ctagsFile.getAbsolutePath());
        }
        myController.hideStage(EXTERNAL_TOOLS_STAGE);
    }

    @FXML
    private void handleAbort(ActionEvent event) {
        myController.hideStage(EXTERNAL_TOOLS_STAGE);
    }

    @FXML
    private void handlePathToGit(ActionEvent event) {
        Stage stage;
        FileChooser fileChooser;
        File selectedFile;

        stage = myController.getStage(EXTERNAL_TOOLS_STAGE);

        fileChooser = new FileChooser();
        fileChooser.setTitle(ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemExternalTools"));
        fileChooser.setInitialDirectory(gitDir);
        selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile == null) {
            // do nothing
        } else if (selectedFile.isDirectory()) {
            labelWarningNotAnApp.setVisible(true);
//            buttonPathToGit.setText(ResourceBundle.getBundle(
//                    "lxrdifferencetable/Bundle", locale).
//                        getString("labelWarningNotAnApp"));
            gitDir = selectedFile;
            gitFile = null;
        } else if (selectedFile.canExecute()) {
            labelWarningNotAnApp.setVisible(false);
            buttonPathToGit.setText(selectedFile.getPath());
            gitDir = selectedFile.getParentFile();
            gitFile = selectedFile;
        } else {
            labelWarningNotAnApp.setVisible(true);
//            buttonPathToGit.setText(ResourceBundle.getBundle(
//                    "lxrdifferencetable/Bundle", locale).
//                        getString("labelWarningNotAnApp"));
            gitDir = selectedFile.getParentFile();
            gitFile = null;
        }
    }

    @FXML
    private void handlePathToCtags(ActionEvent event) {
        Stage stage;
        FileChooser fileChooser;
        File selectedFile;

        stage = myController.getStage(EXTERNAL_TOOLS_STAGE);

        fileChooser = new FileChooser();
        fileChooser.setTitle(ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemExternalTools"));
        fileChooser.setInitialDirectory(ctagsDir);
        selectedFile = fileChooser.showOpenDialog(stage);
        if (selectedFile == null) {
            // do nothing
        } else if (selectedFile.isDirectory()) {
            labelWarningNotAnApp.setVisible(true);
//            buttonPathToCtags.setText(ResourceBundle.getBundle(
//                    "lxrdifferencetable/Bundle", locale).
//                        getString("labelWarningNotAnApp"));
            ctagsDir = selectedFile;
            ctagsFile = null;
        } else if (selectedFile.canExecute()) {
            labelWarningNotAnApp.setVisible(false);
            buttonPathToCtags.setText(selectedFile.getPath());
            ctagsDir = selectedFile.getParentFile();
            ctagsFile = selectedFile;
        } else {
            labelWarningNotAnApp.setVisible(true);
//            buttonPathToCtags.setText(ResourceBundle.getBundle(
//                    "lxrdifferencetable/Bundle", locale).
//                        getString("labelWarningNotAnApp"));
            ctagsDir = selectedFile.getParentFile();
            ctagsFile = null;
        }
    }
}
