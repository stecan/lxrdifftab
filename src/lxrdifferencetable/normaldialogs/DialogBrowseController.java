/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.BROWSE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.CTAGS_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.DUMMY_STAGE;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.tools.BashHighlighting;
import lxrdifferencetable.tools.CHighlighting;
import lxrdifferencetable.tools.CsharpHighlighting;
import lxrdifferencetable.tools.CtagsEntry;
import lxrdifferencetable.tools.CtagsFileEntry;
import lxrdifferencetable.tools.JavaHighlighting;
import lxrdifferencetable.tools.NoHighlighting;
import lxrdifferencetable.tools.ProgrammingLanguage;
import lxrdifferencetable.tools.WorkingDirectory;
import org.apache.commons.io.FileUtils;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.StyleSpans;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogBrowseController implements Initializable, ControlledStage {

    static final Logger logger = Logger.getLogger
        (LxrDifferenceTableController.class.getName());
    
    private DialogTagToFileController dialogTagToFileController;
    
    private Scene scene;
    private StagesController myController;

    private final List <String> fileContent;
    
    private HashMap<String, CtagsEntry> ctags;
    private HashMap<String, CtagsFileEntry> ctagsFile;
    File file1;
    String diffFilename1;
    
    private ProgrammingLanguage.Language progLang;
    
    File workingDirectory;
    
    private int lastIndex = 0;
    
    public DialogBrowseController() {
        this.fileContent = new ArrayList<>();
        this.progLang = ProgrammingLanguage.Language.JAVA;
    }
    
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
        scene= stageParent.getStage(BROWSE_STAGE).getScene();
        scene.getStylesheets().add(DialogBrowseController.class.
                getResource("java-keywords.css").toExternalForm());
        WorkingDirectory dir = new WorkingDirectory(myController);
        workingDirectory = dir.getWorkingDir();    
    }
  
    // Task for syntax highlighting and coloring of differences for CodeArea 
    // content
    Task TaskHighlightContent() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                StyleSpans<Collection<String>> computeHighlighting;
                List<String> tagnames;
                switch (progLang) {
                    case BASH:
                        if (ctagsFile == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile.get(diffFilename1) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile.get(diffFilename1).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                BashHighlighting.computeHighlighting(content.getText(),
                                        tagnames);
                        break;
                    case C:
                        if (ctagsFile == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile.get(diffFilename1) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile.get(diffFilename1).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                CHighlighting.computeHighlighting(content.getText(),
                                        tagnames);
                        break;
                    case JAVA:
                        if (ctagsFile == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile.get(diffFilename1) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile.get(diffFilename1).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                JavaHighlighting.computeHighlighting(content.getText(), 
                                        tagnames);
                        break;

                    case CSHARP:
                        if (ctagsFile == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile.get(diffFilename1) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile.get(diffFilename1).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                CsharpHighlighting.computeHighlighting(content.getText(),
                                        tagnames);
                        break;
                    default:
                        // only for initialization purposes
                        computeHighlighting = 
                               NoHighlighting.computeHighlighting(content.getText());
                        break;

                }
                Platform.runLater(() -> {
                    switch (progLang) {
                        case C:
                        case JAVA:
                        case CSHARP:
                            content.setStyleSpans(0, computeHighlighting);
                            break;
                        default:
                            break;
                    }
                }); 
                return true;
            }
        };
    }


    @FXML
    CodeArea content;
    
    @FXML
    VirtualizedScrollPane contentScrollPane;

    @FXML
    Label labelVersion;
    
    @FXML
    Label labelFile;

    @FXML
    Button searchButton;

    @FXML
    Button forwaredSearchButton;

    @FXML
    Button backwardSearchButton;

    @FXML
    TextField searchField;

    
    @FXML
    public void searchHandler(ActionEvent event) {
        forwardSearch();
    }
    
    @FXML
    public void forwardSearchHandler(ActionEvent event) {
        forwardSearch();
    }
    
    @FXML
    public void backwardSearchHandler(ActionEvent event) {
        backwardSearch();
    }
    
    @FXML
    public void searchItem1(MouseEvent event) {
        String toString = event.getTarget().toString();
        String[] split = toString.split("\"");
        String tagname;
        if (split.length > 1) {
            tagname = split[1];
            if (ctagsFile.get(diffFilename1) == null) {
                // do nothing
            } else {
                if (ctagsFile.get(diffFilename1).getTagnames().contains(tagname)) {
                    List<String> tagfiles = ctags.get(tagname).getTagfiles();
                    List<String> tagaddresses = ctags.get(tagname).getTagaddresses();
                    
                    FXMLLoader loader;
                    loader = myController.getLoader(CTAGS_STAGE);
                    dialogTagToFileController = loader.
                            <DialogTagToFileController>getController();

                    Platform.runLater(() -> {
                        dialogTagToFileController.setTable(ctags, ctagsFile, 
                                diffFilename1, tagname, tagfiles,tagaddresses);
                        myController.showStage(CTAGS_STAGE);
                    });                    
                    
                } else {
                    // do nothing
                }
            }
        }
    }

    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    

   
    public void setFile(HashMap<String, CtagsEntry> ctags,
            HashMap<String, CtagsFileEntry> ctagsFile,
            String diffFilename, String version, String position) {

        this.ctags = ctags;
        this.ctagsFile = ctagsFile;
        diffFilename1 = diffFilename;
        String pathName = workingDirectory + File.separator + diffFilename;
        file1 = new File(pathName);
        progLang = ProgrammingLanguage.registerProgrammingLanguage(file1);
        List<String> lines = new ArrayList<>();

        try {
            lines = FileUtils.readLines(file1, "UTF-8");
        } catch (IOException ex) {
            // if a file is deleted, then an exception is thrown. In this case,
            // it is a legal state.
        }

        labelVersion.setText("Base Version = " + version);
        labelFile.setText("File = " + shortenFile(diffFilename));

        // clear old contents
        fileContent.clear();
        content.clear();
        
        for (String line : lines) {
            fileContent.add(line + "\n");
        }
        
        // bind scrollpanes to contents
        StringBuilder sb1 = new StringBuilder();
        fileContent.forEach(obj -> {
            sb1.append(obj);
        });
        
        content.setParagraphGraphicFactory(LineNumberFactory.get(content));
        content.appendText(sb1.toString());

        Task task1 = TaskHighlightContent();
        
        Thread thread1 = new Thread(task1);
        thread1.start();
        
        while (thread1.getState().equals(Thread.State.RUNNABLE)) {
            try {
                Thread.sleep(1000L,10);
            } catch (InterruptedException ex) {
                logger.warning("Thread1:" + ex.getMessage() );
            }
        }

        // set scroll pane to position
        int pos = Integer.parseInt(position) - 1;
        content.moveTo(pos,0);
        content.requestFollowCaret();  // see: https://github.com/FXMisc/RichTextFX/issues/414
        content.setEditable(false);
    }
    
    private String shortenFile(String file) {
        String newfile = "";
        String[] split = file.split("/");
        if (split.length >= 2) {
            for (int i = 2; i < split.length; i++) {
                newfile = newfile + "/" + split[i];
            }
        } else {
            newfile = file;
        }
        return newfile;
    }
    
    private void forwardSearch() {
        String searchedText = searchField.getText();
        if (searchedText != null && !searchedText.isEmpty()) {
            int index = content.getText().indexOf(searchedText,lastIndex); 
            if (index == -1) {
                lastIndex = 0;
            } else {
                lastIndex = index+1;
                content.moveTo(index);
                content.requestFollowCaret();  // see: https://github.com/FXMisc/RichTextFX/issues/414
                content.selectRange(index, index + searchField.getLength());
            }       
        } else {
            // do nothing
        }
    }

    private void backwardSearch() {
        String searchedText = searchField.getText();
        if (searchedText != null && !searchedText.isEmpty()) {
            int index = content.getText().lastIndexOf(searchedText,lastIndex); 
            if (index == -1) {
                lastIndex = content.getText().length();
            } else {
                if (index == 0) {
                    lastIndex = content.getText().length();
                } else {
                    lastIndex = index-1;
                }
                content.moveTo(index);
                content.requestFollowCaret();  // see: https://github.com/FXMisc/RichTextFX/issues/414
                content.selectRange(index, index + searchField.getLength());
            }       
        } else {
            // do noting
        }
    }
}
