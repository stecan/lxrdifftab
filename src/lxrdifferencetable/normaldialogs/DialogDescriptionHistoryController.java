/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.util.Callback;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.DESC_HISTORY_STAGE;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.data.DescDataModel;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.tablecells.DescriptionEditingCell;
import lxrdifferencetable.tools.ConvertLxrDiffTabStrings;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.TableUtils;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogDescriptionHistoryController implements Initializable, ControlledStage {

    private StagesController myController;
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;                        
    private ConvertLxrDiffTabStrings conv;
    private boolean initialized;
    
    private TableColumn<DescDataModel, String> columnWithShortDescription;
    private TableColumn<DescDataModel, String> columnWithAuthor;
    private TableColumn<DescDataModel, String> columnWithDate;
    
    
    public DialogDescriptionHistoryController() {
        this.props = new Properties();
        initialized = false;
    }
    
    @FXML
    private TableView<DescDataModel> descriptionTable;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    private void handleExit(ActionEvent event) {
	myController.hideStage(DESC_HISTORY_STAGE);
    }
    
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    resource bundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
        conv = new ConvertLxrDiffTabStrings(locale);
    }

    /**
     * This method sets the values in the TableView "table".
     * 
     * @param entry         associtated data
     * @param pos           associated position in entry
     * @param reviewState   true -> review descriptions are shown
     *                      false -> test descriptions are shown
     */
    public void setTable(TableEntry entry, int pos, boolean reviewState) {
        // clear table
        descriptionTable.getItems().clear();      // clear content
        
        if (!initialized) {
            initialized = true;
            initTable();                    // init table
        }
        
        if (reviewState) {
            String title;
            title = java.util.ResourceBundle
                    .getBundle("lxrdifferencetable/Bundle", locale)
                    .getString("programName") +
                    " - " + 
                    java.util.ResourceBundle
                            .getBundle("lxrdifferencetable/Bundle", locale)
                            .getString("title.ReviewDescriptionHistory");
            myController.getStage(DESC_HISTORY_STAGE).setTitle(title);        
            
            List<String> listReviewShortDescription = entry.getReviewShortDescription(pos);
            for (int i = listReviewShortDescription.size()-1; i >= 0; i--) {
                addNewLineInTable(conv.convertDescriptionString(
                        entry.getReviewShortDescription(pos).get(i)), 
                        entry.getReviewDescAuthor(pos).get(i), 
                        entry.getReviewDescDate(pos).get(i));
            }
        } else {
            String title;
            title = java.util.ResourceBundle
                    .getBundle("lxrdifferencetable/Bundle", locale)
                    .getString("programName") +
                    " - " + 
                    java.util.ResourceBundle
                            .getBundle("lxrdifferencetable/Bundle", locale)
                            .getString("title.TestDescriptionHistory");
            myController.getStage(DESC_HISTORY_STAGE).setTitle(title);        

            List<String> listTestShortDescription = entry.getTestShortDescription(pos);
            for (int i = listTestShortDescription.size()-1; i >= 0; i--) {
                addNewLineInTable(conv.convertDescriptionString(
                        entry.getTestShortDescription(pos).get(i)), 
                        entry.getTestDescAuthor(pos).get(i), 
                        entry.getTestDescDate(pos).get(i));
            }
        }
    }
    
    private void initTable() {
        //Create a customer cell factory so that cells can support editing.
        Callback<TableColumn<DescDataModel, String>, TableCell<DescDataModel, String>> cellFactory = 
                new Callback<TableColumn<DescDataModel, String>, TableCell<DescDataModel, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                DescriptionEditingCell cell = new DescriptionEditingCell();
                cell.widthProperty()
                        .addListener((ObservableValue<? extends Number> observableValue, 
                                Number oldSceneWidth, Number newSceneWidth) -> {
                    Text text = new Text();
                    cell.setGraphic(text);
                    text.wrappingWidthProperty().bind(cell.widthProperty());
                    text.textProperty().bind(cell.itemProperty());
                });
                return cell;
            }
        };
        
        columnWithShortDescription = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemShortDescription"));
        columnWithShortDescription.setCellValueFactory(new PropertyValueFactory<>("shortDescription"));
        columnWithShortDescription.setCellFactory(cellFactory);
        columnWithAuthor = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemAuthor"));
        columnWithAuthor.setCellValueFactory(new PropertyValueFactory<>("author"));
        columnWithAuthor.setCellFactory(cellFactory);
        columnWithDate = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemDate"));
        columnWithDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnWithDate.setCellFactory(cellFactory);

        descriptionTable.getColumns().addAll(
                columnWithShortDescription, 
                columnWithAuthor, columnWithDate);
	// enable multi-selection
	descriptionTable.getSelectionModel().setCellSelectionEnabled(true);
	descriptionTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        // resize listener
        myController.getStage(DESC_HISTORY_STAGE).getScene()
                .widthProperty()
                .addListener((ObservableValue<? extends Number> observableValue, 
                        Number oldSceneWidth, Number newSceneWidth) -> {
                    double width = myController.getStage(DESC_HISTORY_STAGE).getScene()
                            .getWidth();
                    if (width < 400.0) {
                        columnWithShortDescription.setPrefWidth(100.0);
                        columnWithAuthor.setPrefWidth(80.0);
                        columnWithDate.setPrefWidth(100.0);
                    } else if (width < 500.0) {
                        columnWithShortDescription.setPrefWidth(150.0);
                        columnWithAuthor.setPrefWidth(100.0);
                        columnWithDate.setPrefWidth(150.0);
                    } else if (width < 700.0) {
                        columnWithShortDescription.setPrefWidth(200.0);
                        columnWithAuthor.setPrefWidth(150.0);
                        columnWithDate.setPrefWidth(200.0);
                    } else if (width < 900.0) {
                        columnWithShortDescription.setPrefWidth(300.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1100.0) {
                        columnWithShortDescription.setPrefWidth(500.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1300.0) {
                        columnWithShortDescription.setPrefWidth(600.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1500.0) {
                        columnWithShortDescription.setPrefWidth(800.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1700.0) {
                        columnWithShortDescription.setPrefWidth(1000.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1900.0) {
                        columnWithShortDescription.setPrefWidth(1200.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 2100.0) {
                        columnWithShortDescription.setPrefWidth(1200.0);
                        columnWithAuthor.setPrefWidth(400.0);
                        columnWithDate.setPrefWidth(400.0);
                    } else if (width < 2300.0) {
                        columnWithShortDescription.setPrefWidth(1400.0);
                        columnWithAuthor.setPrefWidth(400.0);
                        columnWithDate.setPrefWidth(400.0);
                    } else {
                        columnWithShortDescription.setPrefWidth(1600.0);
                        columnWithAuthor.setPrefWidth(400.0);
                        columnWithDate.setPrefWidth(400.0);
                    }
        });
    }
    
    private void addNewLineInTable(String shortDescription,
            String author, String date)
    {
        DescDataModel descDataModel = new DescDataModel();
        descDataModel.setShortDescription(shortDescription);
        descDataModel.setAuthor(author);
        descDataModel.setDate(date);
        descriptionTable.getItems().add(descDataModel);
        descriptionTable.setEditable(true);
        // enable copy/paste
        TableUtils tableUtils = new TableUtils();
        tableUtils.installCopyPasteHandler(descriptionTable);
    
        // set context menu for copy/paste
        ContextMenu contextMenu = new ContextMenu();
        MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem.setOnAction((ActionEvent event) -> {
            tableUtils.copySelectionToClipboard(descriptionTable);
        });
        markAllMenuItem.setOnAction((ActionEvent event) -> {
            descriptionTable.getSelectionModel().selectAll();
        });
        contextMenu.getItems().addAll(copyMenuItem, markAllMenuItem);
        descriptionTable.setContextMenu(contextMenu);
    }
}
