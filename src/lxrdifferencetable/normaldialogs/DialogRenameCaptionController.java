/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.io.IOException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.CAPTION_RENAME_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.LOCKED_DESCRIPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_2;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.LxrTestTableController;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.data.ConfigEntry;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.XmlHandling;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogRenameCaptionController implements Initializable, ControlledStage {

    static final Logger logger = Logger.getLogger
        (LxrDifferenceTableController.class.getName());

    private StagesController myController;
    private LxrDifferenceTableController lxrDifferenceTableController;
    private LxrTestTableController lxrTestTableController;

    private Locale locale;                      // locale for i18n
    private String lang;
    private String country;
    private final Properties props;

    private ConfigEntry entry;
    private boolean editable;

    public DialogRenameCaptionController() {
        this.props = new Properties();
        this.editable = true;
    }
    
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }

    @FXML
    Label labelAuthor;
    
    @FXML
    Label labelDate;
    
    @FXML
    Label labelLockedByUser;

    @FXML
    Label labelUser;

    @FXML
    Label labelAccessability;

    @FXML
    Button buttonYes;
    
    @FXML
    Button buttonNo;
    
    @FXML
    TextField textField;
    
    @FXML
    private void handleYes(ActionEvent event) {
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        XmlHandling.LockType locked = xml.isCaptionLocked();
        if (locked != XmlHandling.LockType.locked) {
            if (xml.writeCaptionLock(entry)) {
                String selectedItem;
                selectedItem = textField.getText();
                if (selectedItem.equals("")) {
                    selectedItem = ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("labelCaption");
                }
                lxrTestTableController.renameCaption(selectedItem);     // set label
                entry.putCaption(selectedItem);                         // save

                // save changed user and date
                String user = System.getProperty("user.name");  // get user name
                String date = ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME);
                entry.putCaptionAuthor(user);
                entry.putCaptionDate(date);
                try {
                    if (locked == XmlHandling.LockType.notLocked) {
                        xml.writeCaption(entry);
                    } else if (locked == XmlHandling.LockType.ownLocked) {
                        xml.writeCaption(entry);
                    }
                } catch (IOException ex) {
                    logger.info("DialogRenameCaptionController: " + 
                            "An exception occured during writing "
                            + "the caption to a file");
                }

                myController.hideStage(CAPTION_RENAME_STAGE);
            } else {
                // Lock is not gotten!
                logger.info("DialogRenameCaptionController: " + 
                        "Lock is not gotten!");
            } 
        } else {
            // entry is locked!
            myController.showStage(LOCKED_DESCRIPTION_STAGE);
        }
    }
    
    @FXML
    private void handleNo(ActionEvent event) {
        myController.hideStage(CAPTION_RENAME_STAGE);
    }

    /**
     * Sets the new name of the caption.
     * 
     * @param name
     */
    public void setCaptionName (String name) {
        textField.setText(name);

        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.<LxrTestTableController>getController();
        loader = myController.getLoader(MAIN_STAGE_1);
        lxrDifferenceTableController = loader.
                <LxrDifferenceTableController>getController();
    }
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
    }    

    public void setTableEntry(ConfigEntry entry) {
        // save references
        this.entry = entry;

        labelAuthor.setText(entry.getCaptionAuthorLast());
        labelDate.setText(entry.getCaptionDateLast());
    }
    
    public void setEditable(boolean editable) {
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_1);
        lxrDifferenceTableController = loader.
                <LxrDifferenceTableController>getController();
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();

        this.editable = editable;
        if (editable) {
            buttonYes.setDisable(false);
            labelLockedByUser.setVisible(false);
            labelUser.setVisible(false);
            textField.setEditable(true);
        } else {
            buttonYes.setDisable(true);
            labelLockedByUser.setVisible(true);
            labelLockedByUser.setStyle("-fx-background-color: yellow");
            labelUser.setVisible(true);
            labelUser.setStyle("-fx-background-color: yellow");
            textField.setEditable(false);
        }

        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml.isReadOnly()) {
            labelAccessability.setText(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("noAccess"));
            labelAccessability.setStyle("-fx-background-color: yellow");

	    // Overwrite settings!
            buttonYes.setDisable(true);
            textField.setEditable(false);
        } else {
            labelAccessability.setText(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("access"));
            labelAccessability.setStyle("");
	    // Do not overwrite settings!
        }
    }
    
    public void setLockedByUser(String user) {
        labelUser.setText(user);
    }
}
