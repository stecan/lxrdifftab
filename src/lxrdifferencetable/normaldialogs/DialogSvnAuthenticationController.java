/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import static lxrdifferencetable.LxrDifferenceTable.SVN_AUTH_STAGE;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.comp.Generate;
import lxrdifferencetable.tools.Properties;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogSvnAuthenticationController implements Initializable, ControlledStage {

    private StagesController myController;
    private final Properties props;
    private String user;
    private String password;
    private String pathToRepo;
    private Generate.State state;
    
    public DialogSvnAuthenticationController () {
        this.props = new Properties();
        user = "";
        password = "";
        pathToRepo = "";
        state = Generate.State.PASSED;
    }
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }

    @FXML
    Label labelRepo;

    @FXML
    TextField textFieldAccount;

    @FXML
    PasswordField passwordField;

    
    @FXML
    private void handleOK(ActionEvent event) {
        state = Generate.State.PASSED;
        myController.hideStage(SVN_AUTH_STAGE);
        
        // return to "handleGenerate" 
        FXMLLoader loader1 = myController.getLoader(MAIN_STAGE_1);
        LxrDifferenceTableController lxrDifferenceTableController = 
                loader1.<LxrDifferenceTableController>getController();
        lxrDifferenceTableController.handleGenerate(null);
    }
    
    @FXML
    private void handleAbort(ActionEvent event) {
        state = Generate.State.ABORTED_DURING_REPO_SCANNING;
        myController.hideStage(SVN_AUTH_STAGE);

        // return to "handleGenerate" 
        FXMLLoader loader1 = myController.getLoader(MAIN_STAGE_1);
        LxrDifferenceTableController lxrDifferenceTableController = 
                loader1.<LxrDifferenceTableController>getController();
        lxrDifferenceTableController.handleGenerate(null);
    }

    @FXML
    private void handleAccount(ActionEvent event) {
        user = textFieldAccount.getText();
        props.putProperty(props.SVNUSER, user);
    }

    @FXML
    private void handlePassword(ActionEvent event) {
        password = passwordField.getText();
        state = Generate.State.PASSED;
        
        myController.hideStage(SVN_AUTH_STAGE);

        // return to "handleGenerate" 
        FXMLLoader loader1 = myController.getLoader(MAIN_STAGE_1);
        LxrDifferenceTableController lxrDifferenceTableController = 
                loader1.<LxrDifferenceTableController>getController();
        lxrDifferenceTableController.handleGenerate(null);
        
    }

    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        user = props.getProperty(props.SVNUSER);
        if (user.equals("")) {
            user = System.getProperty("user.name");
            textFieldAccount.setText(user);
        } else {
            textFieldAccount.setText(user);
        }
    }    
    
    public Generate.State getState() {
        return state;
    }
    
    public void setPathToRepo(String pathToRepo) {
        this.pathToRepo = pathToRepo;
        labelRepo.setText(pathToRepo);
    }
    
    public String getAccount () {
        return user;
    }
    
    public String getPassword () {
        return password;
    }
}