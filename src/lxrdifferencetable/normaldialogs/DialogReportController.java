/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.JR_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.NULL_POINTER_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.REPORT_OK_STAGE;
import lxrdifferencetable.StagesController;
import static lxrdifferencetable.LxrDifferenceTable.REPORT_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SAVE_STAGE;
import lxrdifferencetable.SaveController;
import lxrdifferencetable.data.ConfigEntry;
import lxrdifferencetable.data.StatisticalData;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.exceptiondialogs.DialogJRExceptionController;
import lxrdifferencetable.jrxml.AnchorJrxml;
import lxrdifferencetable.reports.DataBean;
import lxrdifferencetable.reports.DataBeanMaker;
import lxrdifferencetable.tools.Properties;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.ReportExportConfiguration;
import net.sf.jasperreports.export.SimpleDocxReportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOdsReportConfiguration;
import net.sf.jasperreports.export.SimpleOdtReportConfiguration;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

/**
 * FXML controller class to handle the help menu "Language".
 *
 * @author Stefan Canali
 */
public class DialogReportController implements Initializable, ControlledStage {

    private static final int STATEREADY = 0;
    private static final int STATENULLPOINTEREXCEPTION = -1;
    private static final int STATEEXCEPTION = -2;
    private static final int STATEJREXCEPTION = -3;

    private enum ReportType {html, xlsx, ods, docx, odt, pdf};
    private ReportType reportType;
    private File reportFile;
    private StatisticalData statistics;

    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;                        
    
    private final Properties props;
    private StagesController myController;
    
    private boolean titlePage;
    private boolean summaryPage;
    private boolean svnAvailable;
    
    volatile String text;

    @FXML
    ProgressBar progressBar;

    @FXML
    Button buttonReportFile;
    
    @FXML
    RadioButton radioButtonHtml;
    
    @FXML
    RadioButton radioButtonExcel;
    
    @FXML
    RadioButton radioButtonXlsx;
    
    @FXML
    RadioButton radioButtonOds;
    
    @FXML
    RadioButton radioButtonWord;
    
    @FXML
    RadioButton radioButtonDocx;
    
    @FXML
    RadioButton radioButtonOdt;
    
    @FXML
    RadioButton radioButtonPDF;

    @FXML
    CheckBox checkboxTitlePage;

    @FXML
    CheckBox checkboxSummaryPage;

    @FXML
    TabPane tabPane;

    @FXML
    Tab tabReportLayout;

    @FXML
    Tab tabReportOptions;


    public DialogReportController() {
        this.summaryPage = false;
        this.titlePage = false;
        this.text = "";
        this.reportType = ReportType.html;
        this.props = new Properties();
        this.svnAvailable = true;
    }

    private List<TableEntry> changedFileList;   // list of changed files
    private ConfigEntry configEntry;


    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }

    
    @FXML
    private void handleAbort (ActionEvent event) {
        myController.hideStage(REPORT_STAGE);
    }
    
    @FXML
    private void handleGenerate (ActionEvent event) {
        handleReport();
    }

    @FXML
    private void handleHtml (ActionEvent event) {
        radioButtonHtml.setSelected(true);
        radioButtonExcel.setSelected(false);
        radioButtonXlsx.setSelected(false);
        radioButtonOds.setSelected(false);
        radioButtonWord.setSelected(false);
        radioButtonDocx.setSelected(false);
        radioButtonOdt.setSelected(false);
        radioButtonPDF.setSelected(false);
        reportType = ReportType.html;
        props.putProperty(props.REPORT, reportType.toString());
    }

    @FXML
    private void handleExcel (ActionEvent event) {
        radioButtonHtml.setSelected(false);
        radioButtonExcel.setSelected(true);
        radioButtonXlsx.setSelected(true);
        radioButtonOds.setSelected(false);
        radioButtonWord.setSelected(false);
        radioButtonDocx.setSelected(false);
        radioButtonOdt.setSelected(false);
        radioButtonPDF.setSelected(false);
        reportType = ReportType.xlsx;
        props.putProperty(props.REPORT, reportType.toString());
    }

    @FXML
    private void handleXlsx (ActionEvent event) {
        radioButtonHtml.setSelected(false);
        radioButtonExcel.setSelected(true);
        radioButtonXlsx.setSelected(true);
        radioButtonOds.setSelected(false);
        radioButtonWord.setSelected(false);
        radioButtonDocx.setSelected(false);
        radioButtonOdt.setSelected(false);
        radioButtonPDF.setSelected(false);
        reportType = ReportType.xlsx;
        props.putProperty(props.REPORT, reportType.toString());
    }

    @FXML
    private void handleOds (ActionEvent event) {
        radioButtonHtml.setSelected(false);
        radioButtonExcel.setSelected(true);
        radioButtonXlsx.setSelected(false);
        radioButtonOds.setSelected(true);
        radioButtonWord.setSelected(false);
        radioButtonDocx.setSelected(false);
        radioButtonOdt.setSelected(false);
        radioButtonPDF.setSelected(false);
        reportType = ReportType.ods;
        props.putProperty(props.REPORT, reportType.toString());
    }
    
    @FXML
    private void handleWord (ActionEvent event) {
        radioButtonHtml.setSelected(false);
        radioButtonExcel.setSelected(false);
        radioButtonXlsx.setSelected(false);
        radioButtonOds.setSelected(false);
        radioButtonWord.setSelected(true);
        radioButtonDocx.setSelected(true);
        radioButtonOdt.setSelected(false);
        radioButtonPDF.setSelected(false);
        reportType = ReportType.docx;
        props.putProperty(props.REPORT, reportType.toString());
    }

    @FXML
    private void handleDocx (ActionEvent event) {
        radioButtonHtml.setSelected(false);
        radioButtonExcel.setSelected(false);
        radioButtonXlsx.setSelected(false);
        radioButtonOds.setSelected(false);
        radioButtonWord.setSelected(true);
        radioButtonDocx.setSelected(true);
        radioButtonOdt.setSelected(false);
        radioButtonPDF.setSelected(false);
        reportType = ReportType.docx;
        props.putProperty(props.REPORT, reportType.toString());
    }

    @FXML
    private void handleOdt (ActionEvent event) {
        radioButtonHtml.setSelected(false);
        radioButtonExcel.setSelected(false);
        radioButtonXlsx.setSelected(false);
        radioButtonOds.setSelected(false);
        radioButtonWord.setSelected(true);
        radioButtonDocx.setSelected(false);
        radioButtonOdt.setSelected(true);
        radioButtonPDF.setSelected(false);
        reportType = ReportType.odt;
        props.putProperty(props.REPORT, reportType.toString());
    }

    @FXML
    private void handlePDF (ActionEvent event) {
        radioButtonHtml.setSelected(false);
        radioButtonExcel.setSelected(false);
        radioButtonXlsx.setSelected(false);
        radioButtonOds.setSelected(false);
        radioButtonWord.setSelected(false);
        radioButtonDocx.setSelected(false);
        radioButtonOdt.setSelected(false);
        radioButtonPDF.setSelected(true);
        reportType = ReportType.pdf;
        props.putProperty(props.REPORT, reportType.toString());
    }

    @FXML
    private void handleReportFile (ActionEvent event) {
        Stage stage;
        stage = myController.getStage(REPORT_STAGE);
        
        FXMLLoader loader;
        final SaveController saveController;

        loader = myController.getLoader(SAVE_STAGE);
        saveController = loader.<SaveController>getController();

        saveController.setMode(SaveController.Mode.REPORT_MODE);
        saveController.initialize(null, null);
        myController.getStage(SAVE_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("selectFile"));
        
        myController.showStage(SAVE_STAGE);
    }
    
    @FXML
    private void handleTitlePage (ActionEvent event) {
        if (checkboxTitlePage.isSelected()) {
            titlePage = true;
            props.putProperty(props.REPORT_TITLEPAGE, "true");
        } else {
            titlePage = false;
            props.putProperty(props.REPORT_TITLEPAGE, "false");
        }
    }
    
    @FXML
    private void handleSummaryPage (ActionEvent event) {
        if (checkboxSummaryPage.isSelected()) {
            summaryPage = true;
            props.putProperty(props.REPORT_SUMMARYPAGE, "true");
        } else {
            summaryPage = false;
            props.putProperty(props.REPORT_SUMMARYPAGE, "false");
        }
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        titlePage = Boolean.parseBoolean (props.getProperty(props.REPORT_TITLEPAGE));
        checkboxTitlePage.setSelected(titlePage);
        summaryPage = Boolean.parseBoolean (props.getProperty(props.REPORT_SUMMARYPAGE));
        checkboxSummaryPage.setSelected(summaryPage);

        // set locale and resource file for report
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
        switch (props.getProperty(props.REPORT)) {
            case "html":
                reportType = ReportType.html;
                radioButtonHtml.setSelected(true);
                radioButtonExcel.setSelected(false);
                radioButtonXlsx.setSelected(false);
                radioButtonOds.setSelected(false);
                radioButtonWord.setSelected(false);
                radioButtonDocx.setSelected(false);
                radioButtonOdt.setSelected(false);
                radioButtonPDF.setSelected(false);
                break;
            case "xlsx":
                reportType = ReportType.xlsx;
                radioButtonHtml.setSelected(false);
                radioButtonExcel.setSelected(true);
                radioButtonXlsx.setSelected(true);
                radioButtonOds.setSelected(false);
                radioButtonWord.setSelected(false);
                radioButtonDocx.setSelected(false);
                radioButtonOdt.setSelected(false);
                radioButtonPDF.setSelected(false);
                break;
            case "ods":
                reportType = ReportType.ods;
                radioButtonHtml.setSelected(false);
                radioButtonExcel.setSelected(true);
                radioButtonXlsx.setSelected(false);
                radioButtonOds.setSelected(true);
                radioButtonWord.setSelected(false);
                radioButtonDocx.setSelected(false);
                radioButtonOdt.setSelected(false);
                radioButtonPDF.setSelected(false);
                break;
            case "docx":
                reportType = ReportType.docx;
                radioButtonHtml.setSelected(false);
                radioButtonExcel.setSelected(false);
                radioButtonXlsx.setSelected(false);
                radioButtonOds.setSelected(false);
                radioButtonWord.setSelected(true);
                radioButtonDocx.setSelected(true);
                radioButtonOdt.setSelected(false);
                radioButtonPDF.setSelected(false);
                break;
            case "odt":
                reportType = ReportType.odt;
                radioButtonHtml.setSelected(false);
                radioButtonExcel.setSelected(false);
                radioButtonXlsx.setSelected(false);
                radioButtonOds.setSelected(false);
                radioButtonWord.setSelected(true);
                radioButtonDocx.setSelected(false);
                radioButtonOdt.setSelected(true);
                radioButtonPDF.setSelected(false);
                break;
            case "pdf":
                reportType = ReportType.pdf;
                radioButtonHtml.setSelected(false);
                radioButtonExcel.setSelected(false);
                radioButtonXlsx.setSelected(false);
                radioButtonOds.setSelected(false);
                radioButtonWord.setSelected(false);
                radioButtonDocx.setSelected(false);
                radioButtonOdt.setSelected(false);
                radioButtonPDF.setSelected(true);
                break;
            default:
                reportType = ReportType.html;
                radioButtonHtml.setSelected(true);
                radioButtonExcel.setSelected(false);
                radioButtonXlsx.setSelected(false);
                radioButtonOds.setSelected(false);
                radioButtonWord.setSelected(false);
                radioButtonDocx.setSelected(false);
                radioButtonOdt.setSelected(false);
                radioButtonPDF.setSelected(false);
                break;
        }

        String file = props.getProperty(props.LASTREPORTFILE);
        if ((file != null) && !(file.isEmpty())) {
            reportFile = new File(file);
            buttonReportFile.setText(reportFile.toString());
        }
        
        progressBar.setVisible(false);
        // tabPane.getTabs().remove(tabReportLayout);
        tabPane.getTabs().remove(tabReportOptions);
    }    

    public void setTable(List<TableEntry> changedFileList,
            ConfigEntry configEntry) {
        this.changedFileList = changedFileList;
        this.configEntry = configEntry;
    }

    public int generateReport() {
        ClassLoader cl;
        InputStream inputStream;
        int state = STATEREADY;
        try {
            Platform.runLater(() -> progressBar.setProgress(0.1));

            cl = AnchorJrxml.class.getClassLoader();
            switch (reportType) {
                case xlsx:
                case ods:
                    if (svnAvailable) {
                        inputStream = cl.
                                getResourceAsStream(
                                        "lxrdifferencetable/jrxml/excelReport.jrxml");                            
                    } else {
                        inputStream = cl.
                                getResourceAsStream(
                                        "lxrdifferencetable/jrxml/excelReportWithoutSvn.jrxml");                            
                    }
                    break;
                case pdf:
                case html:
                    if (svnAvailable) {
                        inputStream = cl.
                                getResourceAsStream(
                                        "lxrdifferencetable/jrxml/dina4Pdf.jrxml");
                    } else {
                        inputStream = cl.
                                getResourceAsStream(
                                        "lxrdifferencetable/jrxml/dina4PdfWithoutSvn.jrxml");
                    }
                    break;
                default:
                    if (svnAvailable) {
                        inputStream = cl.
                                getResourceAsStream(
                                        "lxrdifferencetable/jrxml/dina4Doc.jrxml");
                    } else {
                        inputStream = cl.
                                getResourceAsStream(
                                        "lxrdifferencetable/jrxml/dina4DocWithoutSvn.jrxml");
                    }
                    break;
            }
            DataBeanMaker dataBeanMaker = new DataBeanMaker(changedFileList,
                    configEntry, reportFile, statistics,
                    titlePage, summaryPage);
            switch (reportType) {
                case xlsx:
                case ods:
                    dataBeanMaker.setExcel(true);
                    break;
                default:
                    dataBeanMaker.setExcel(false);
                    break;
            }
            ArrayList<DataBean> dataBeanList = dataBeanMaker.getDataBeanList();
            Map parameters = new HashMap();

            parameters.put("REPORT_LOCALE", locale);
            parameters.put("REPORT_RESOURCE_BUNDLE", 
                    java.util.ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale));                    
            Platform.runLater(() -> progressBar.setProgress(0.2));

            JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
            Platform.runLater(() -> progressBar.setProgress(0.3));

            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
            Platform.runLater(() -> progressBar.setProgress(0.4));

            JRBeanCollectionDataSource beanColDataSource = new
                JRBeanCollectionDataSource(dataBeanList);
            Platform.runLater(() -> progressBar.setProgress(0.5));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, 
                    parameters, beanColDataSource);
            Platform.runLater(() -> progressBar.setProgress(0.8));
            switch (reportType) {
                case html:
                    JasperExportManager.exportReportToHtmlFile(jasperPrint, 
                            reportFile.toString()); 
                    break;
                case xlsx:
                    JRXlsxExporter exporterXlsx = new JRXlsxExporter();
                    exporterXlsx.setExporterInput(
                            new SimpleExporterInput(jasperPrint));
                    exporterXlsx.setExporterOutput(
                            new SimpleOutputStreamExporterOutput(reportFile));

                    SimpleXlsxReportConfiguration configurationXlsx = 
                            new SimpleXlsxReportConfiguration();
                    configurationXlsx.setOnePagePerSheet(true);
                    exporterXlsx.setConfiguration(configurationXlsx);

                    exporterXlsx.exportReport();
                    break;
                case ods:
                    JROdsExporter exporterOds = new JROdsExporter();
                    exporterOds.setExporterInput(
                            new SimpleExporterInput(jasperPrint));
                    exporterOds.setExporterOutput(
                            new SimpleOutputStreamExporterOutput(reportFile));

                    SimpleOdsReportConfiguration configurationOds = 
                            new SimpleOdsReportConfiguration();
                    configurationOds.setOnePagePerSheet(true);
                    exporterOds.setConfiguration(configurationOds);

                    exporterOds.exportReport();
                    break;
                case docx:
                    JRDocxExporter exporterDocx = new JRDocxExporter();
                    exporterDocx.setExporterInput(
                            new SimpleExporterInput(jasperPrint));
                    exporterDocx.setExporterOutput(
                            new SimpleOutputStreamExporterOutput(reportFile));

                    SimpleDocxReportConfiguration configurationDocx = 
                            new SimpleDocxReportConfiguration();
                    exporterDocx.setConfiguration(configurationDocx);

                    exporterDocx.exportReport();
                    break;
                case odt:
                    JROdtExporter exporterOdt = new JROdtExporter();
                    exporterOdt.setExporterInput(
                            new SimpleExporterInput(jasperPrint));
                    exporterOdt.setExporterOutput(
                            new SimpleOutputStreamExporterOutput(reportFile));

                    SimpleOdtReportConfiguration configurationOdt = 
                            new SimpleOdtReportConfiguration();
                    exporterOdt.setConfiguration(configurationOdt);

                    exporterOdt.exportReport();
                    break;
                case pdf:
                    JasperExportManager.exportReportToPdfFile(jasperPrint, 
                            reportFile.toString()); 
                    break;
                default:
                    break;
            }
            Platform.runLater(() -> progressBar.setProgress(1.0));
        } catch (NullPointerException ex) {
            text = ex.getMessage();
            state = STATENULLPOINTEREXCEPTION;
        } catch (JRException ex) {
            text = ex.getMessage();
            state = STATEJREXCEPTION;
        } catch (JRRuntimeException ex)  {
            text = ex.getMessage();
            state = STATEJREXCEPTION;
        } catch (Exception ex) {
            text = ex.getMessage();
            state = STATEEXCEPTION;
        } 
        return state;
    }
    
    public void handleReport () {

        Task<Integer> task;

        // Task
        task = new Task<Integer>() {
            @Override
            protected Integer call() {
                int state;
                state = generateReport();
                return state;
            }
        };

        // Event handler to wait for completion of worker task.
        // 
        // If the worker task completes, then the PROGRESSSTAGE is hidden.
        task.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED,
                (WorkerStateEvent t) -> {
                    // wait on completion of worker task
                    int state;

                    progressBar.setVisible(false);

                    state = task.getValue();
                    // ask for state of completion
                    switch (state) {
                        case STATEREADY:
                            // it's all right!
                            myController.showStage(REPORT_OK_STAGE);
                            break;
                        case STATEEXCEPTION:
                            // An exception occured!
                            // Show the approviate message!
                            myController.showStage(EXCEPTION_STAGE);
                            break;
                        case STATEJREXCEPTION:
                            // An exception occured!
                            // Show the approviate message!
                            FXMLLoader loader;
                            loader = myController.getLoader(JR_EXCEPTION_STAGE);
                            DialogJRExceptionController ctr = loader
                                    .<DialogJRExceptionController>getController();
                            ctr.setText(text);
                            myController.showStage(JR_EXCEPTION_STAGE);
                            break;
                        case STATENULLPOINTEREXCEPTION:
                            // A null pointer exception occured!
                            // Show the approviate message!
                            myController.showStage(NULL_POINTER_EXCEPTION_STAGE);
                            break;
                    }
                    progressBar.setVisible(false);
                }
        );
        
        progressBar.setVisible(true);                           // init progressBar
        progressBar.setProgress(0.0);
        myController.hideStage(EXCEPTION_STAGE);                // hide (previous shown) stage
        myController.hideStage(JR_EXCEPTION_STAGE);             // hide (previous shown) stage
        myController.hideStage(NULL_POINTER_EXCEPTION_STAGE);   // hide (previous shown) stage

        Thread th = new Thread(task);               // define new task
        th.setDaemon(true);                         // set task to daemon state
        th.setName("HandleReport");
        th.start();                                 // start task as daemon
    }

    public void setStatistics (StatisticalData statistics) {
        this.statistics = statistics;
    }
    
    public void setReportFile(File reportFile) {
        this.reportFile = reportFile;
        buttonReportFile.setText(reportFile.toString());
        // save to properties
        props.putProperty(props.LASTREPORTFILE, reportFile.toString());
    }
    
    public void setSvnAvailable(boolean svnAvailable) {
        this.svnAvailable = svnAvailable;
    }
}
