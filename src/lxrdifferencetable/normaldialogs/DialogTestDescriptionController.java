/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.io.IOException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_2;
import static lxrdifferencetable.LxrDifferenceTable.OBSOLETE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.TEST_DESCRIPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.UNSAVED_MODIFICATIONS_STAGE;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.LxrTestTableController;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.tools.ConvertLxrDiffTabStrings;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.XmlHandling;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogTestDescriptionController implements Initializable, ControlledStage {

    static final Logger logger = Logger.getLogger
        (LxrDifferenceTableController.class.getName());
    
    private StagesController myController;
    private LxrDifferenceTableController lxrDifferenceTableController;
    private LxrTestTableController lxrTestTableController;
    
    private Locale locale;                      // locale for i18n
    private String lang;
    private String country;
    private final Properties props;
    private ConvertLxrDiffTabStrings conv;

    private String shortDescriptionFieldOld;
    private String descriptionFieldOld;
    private String descriptionFieldNew;

    private WebView webView;
    private WebEngine webEngine;
    
    private TableEntry entry;
    private int pos;
    private boolean editable;
    
    // reference to text field "testedDescriptionCol" in main stage 
    // "LxrTestTableController"
    private TextField refShortDescriptionField;
    
    
    public DialogTestDescriptionController() {
        this.props = new Properties();
        this.descriptionFieldOld = "";
        this.descriptionFieldNew = "";
        this.shortDescriptionFieldOld = "";
        this.editable = true;
    }
    
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    TextField shortDescriptionField;

    @FXML
    HTMLEditor descriptionField;
    
    @FXML
    Label labelAuthor;
    
    @FXML
    Label labelDate;
    
    @FXML
    Label labelUser;

    @FXML
    Label labelLockedByUser;
    
    @FXML
    Label labelAccessability;

    @FXML
    Button saveButton;
    
    @FXML
    private void handleSave(ActionEvent event) {
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_1);
        lxrDifferenceTableController = loader.
                <LxrDifferenceTableController>getController();
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();
        
        if ((shortDescriptionField.getText().equals(shortDescriptionFieldOld)) && 
                (descriptionField.getHtmlText().equals(descriptionFieldOld))){
            // No modifications are done! 
            //
            // Leave stage!
            myController.hideStage(TEST_DESCRIPTION_STAGE);
            lxrTestTableController.setActiveTestDescriptionController(-1);
        } else {
            if ((shortDescriptionFieldOld.equals(conv.convertDescriptionString(
                    entry.getTestShortDescriptionLast(pos)))) 
                    && ((descriptionFieldOld.equals(entry.getTestDescriptionLast(pos))) ||
                    (entry.getTestDescriptionLast(pos).equals("")))) {
                // Modifications are done. Save them!
                save();
            } else {
                // Modifications are done by another user. The old description is 
                // obsolete.
                //
                // Show a message to indicate the user, that there is a 
                // problem with his/her content.
                myController.showStage(OBSOLETE_STAGE);
            }
        }
    }
    
    @FXML
    private void handleAbort(ActionEvent event) {
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();
        
        if (!shortDescriptionField.getText().equals(shortDescriptionFieldOld)) {
            myController.showStage(UNSAVED_MODIFICATIONS_STAGE);
        } else if (!descriptionField.getHtmlText().equals(descriptionFieldOld)) {
            myController.showStage(UNSAVED_MODIFICATIONS_STAGE);
        } else {
            // No modifications are done. Leave stage!
            myController.hideStage(TEST_DESCRIPTION_STAGE);
            lxrTestTableController.setActiveTestDescriptionController(-1);
            
        }
    }

    @FXML
    private void handleForwardButton(ActionEvent event) {
        int currentIndex;
        int size;
        
        currentIndex = webEngine.getHistory().getCurrentIndex();
        size = webEngine.getHistory().getEntries().size();
        if (currentIndex == size - 1) {
            // do nothing!
        } else {
            webEngine.getHistory().go(1);
        }
    }
    
    @FXML
    private void handleBackwardButton(ActionEvent event) {
        int currentIndex;
        int size;
        
        size = webEngine.getHistory().getEntries().size();
        currentIndex = webEngine.getHistory().getCurrentIndex();
        if (currentIndex == 0) {
            descriptionField.setHtmlText(descriptionFieldNew);
        } else {
            webEngine.getHistory().go(-1);
        }
    }
    
    
    @FXML
    private void handleOnMouseClicked(MouseEvent event) {
        int currentIndex;
        int size;
        
        size = webEngine.getHistory().getEntries().size();
        currentIndex = webEngine.getHistory().getCurrentIndex();
        if ((currentIndex == 0) && (size > 0)) {
            descriptionFieldNew = descriptionField.getHtmlText();
        } 
    }
    
    public void handleSave() {
        save();
        myController.hideStage(TEST_DESCRIPTION_STAGE);
    }
    
    public void handleAbort() {
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();
        
        // Modifications are done. Discard them and leave stage!
        myController.hideStage(TEST_DESCRIPTION_STAGE);
        lxrTestTableController.setActiveTestDescriptionController(-1);
    }

    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
        conv = new ConvertLxrDiffTabStrings(locale);

        // addition of static layout grow constraints for the htmleditor 
        // embedded webview.
        //
        // see:
        // https://stackoverflow.com/questions/11213079/javafx-2-borderpane-use-full-space
        webView = (WebView) descriptionField.lookup("WebView");
        webEngine = webView.getEngine();
        GridPane.setHgrow(webView, Priority.ALWAYS);  // allow the webview to grow beyond it's preferred width of 800.
        GridPane.setVgrow(webView, Priority.ALWAYS);  // allow the webview to grow beyond it's preferred height of 600.
        
    }    


    /**
     * Sets the text field "element" to the object of the 
     * calling controller.
     * 
     * Note:
     * This routine is called by the main controller
     * "LxrTestTableController".
     * 
     * 
     * @param element   TextField object of calling controller
     */
    public void setShortDescriptionField(TextField element) {
        String title;
        title = java.util.ResourceBundle
                .getBundle("lxrdifferencetable/Bundle", locale)
                .getString("programName") +
                " - " + 
                java.util.ResourceBundle
                        .getBundle("lxrdifferencetable/Bundle", locale)
                        .getString("title.TestDescription");
        myController.getStage(TEST_DESCRIPTION_STAGE).setTitle(title);        
        
        refShortDescriptionField = element;                     // save reference
        shortDescriptionField.setText(element.getText());       // local copy
    }


    public void setTableEntry(TableEntry entry, int pos) {
        // save references
        this.entry = entry;
        this.pos = pos;
        
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_1);
        lxrDifferenceTableController = loader.
                <LxrDifferenceTableController>getController();
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();
        
        // save old values first
        shortDescriptionFieldOld = conv.convertDescriptionString(
                entry.getTestShortDescriptionLast(pos));
        descriptionFieldOld = entry.getTestDescriptionLast(pos);
        if (descriptionFieldOld.equals("")) {
            // It's not a good solution to copy the empty HTML string
            // to the old description (it's dependent on content and
            // local preferences). But for now, it's the only solution I see!
            descriptionFieldOld = "<html dir=\"ltr\"><head></head><body contenteditable=\"true\"></body></html>";
        }
        descriptionFieldNew = descriptionFieldOld;
        
        // does not function with history!
        // webEngine.load(descriptionFieldNew); 
        
        webEngine.load("about:Blank");  // for history only!

        // set table fields
        shortDescriptionField.setText(shortDescriptionFieldOld);
        descriptionField.setHtmlText(descriptionFieldOld);
        labelAuthor.setText(entry.getTestDescAuthorLast(pos));
        labelDate.setText(entry.getTestDescDateLast(pos));
        
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml.isReadOnly()) {
            labelAccessability.setText(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("noAccess"));
            // Overwrite settings!
            labelAccessability.setStyle("-fx-background-color: yellow");
            saveButton.setDisable(true);
            shortDescriptionField.setEditable(false);
            descriptionField.setDisable(true);
            
        } else {
            labelAccessability.setText(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("access"));
            labelAccessability.setStyle("");
            // Do not overwrite settings!
        }
    }
    
    public void setEditable(boolean editable) {
        this.editable = editable;
        if (editable) {
            saveButton.setDisable(false);
            labelLockedByUser.setVisible(false);
            labelUser.setVisible(false);
            shortDescriptionField.setEditable(true);
            descriptionField.setDisable(false);
        } else {
            saveButton.setDisable(true);
            labelLockedByUser.setVisible(true);
            labelUser.setVisible(true);
            labelLockedByUser.setStyle("-fx-background-color: yellow");
            labelUser.setStyle("-fx-background-color: yellow");
            shortDescriptionField.setEditable(false);
            descriptionField.setDisable(true);
        }
    }
    
    public void setLockedByUser(String user) {
        labelUser.setText(user);
    }

    private void save() {
        // Modifications are done. Save them!
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        XmlHandling.LockType locked = xml.isLocked(Integer.parseInt(entry.getFileCount())-1);
        if (locked != XmlHandling.LockType.locked) {
            if (xml.writeTestShortDescriptionLock(entry)) {
                // Lock is gotten! We can proceed!
                // save changes!
                refShortDescriptionField.setText(shortDescriptionField.getText());

                String user = System.getProperty("user.name");  // get user name
                entry.putTestDescAuthor(pos, user);
                String date = ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME);
                entry.putTestDescDate(pos, date);

                entry.putTestShortDescription(pos, 
                        conv.toInternalString(shortDescriptionField.getText()));
                entry.putTestDescription(pos, descriptionField.getHtmlText());

                try {
                    if (locked == XmlHandling.LockType.notLocked) {
                        xml.writeTestShortDescription(pos, entry);
                    } else if (locked == XmlHandling.LockType.ownLocked) {
                        xml.writeTestShortDescription(pos, entry);
                    }
                } catch (IOException ex) {
                    logger.info("DialogTestDescriptionController: " + 
                            "An exception occured during writing "
                            + "the test description to a file");
                }
                myController.hideStage(TEST_DESCRIPTION_STAGE);
                lxrTestTableController.setActiveTestDescriptionController(-1);
            } else {
                // Lock is not gotten!
                logger.info("DialogTestDescriptionController: " + 
                        "Lock is not gotten!");
            }
        } else {
            // entry is locked!
            logger.info("DialogTestDescriptionController: " + 
                    "Entry is locked!");
        }
        
    }
}

