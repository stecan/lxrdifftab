/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_2;
import static lxrdifferencetable.LxrDifferenceTable.SEARCH_STAGE;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.LxrTestTableController;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.data.SearchDataModel;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.tools.ConvertLxrDiffTabStrings;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.TableUtils;

/**
 * FXML Controller class
 * 
 * Controller class to search the table "changedFileList".
 *
 * @author Stefan Canali
 */
public class DialogSearchController implements Initializable, ControlledStage {

    static final Logger logger = Logger.getLogger
    (LxrDifferenceTableController.class.getName());

    private StagesController myController;
    private LxrTestTableController lxrTestTableController;
    
    private List<TableEntry> changedFileList;   // list of changed files
    
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;
    private String country;
    private ConvertLxrDiffTabStrings conv;
    private int actualFileCount;
    private boolean initialized;
    
    private boolean isFileCount;
    private boolean isLink;
    private boolean isLineNr;
    private boolean isSvnVersion;
    private boolean isSvnDesc;
    private boolean isSvnAuthor;
    private boolean isSvnDate;
    private boolean isReviewState;
    private boolean isReviewDesc;
    private boolean isTestState;
    private boolean isTestDesc;
    private boolean isReviewAuthor;
    private boolean isReviewDate;
    private boolean isTestAuthor;
    private boolean isTestDate;
    
    private boolean svnAvailable;
    
    private volatile boolean abort;
    
    private boolean running;       // marker for search task
    
    private TableColumn<String, String> columnWithFileCount;
    private TableColumn<String, String> columnWithLink;
    private TableColumn<String, String> columnWithLineNumber;
    private TableColumn<String, String> columnWithSvnVersion;
    private TableColumn<String, String> columnWithSvnDesc;
    private TableColumn<String, String> columnWithSvnAuthor;
    private TableColumn<String, String> columnWithSvnDate;
    private TableColumn<String, String> columnWithReviewState;
    private TableColumn<String, String> columnWithTestState;
    private TableColumn<String, String> columnWithReviewShortDescription;
    private TableColumn<String, String> columnWithTestShortDescription;
    
    public DialogSearchController() {
        this.abort = false;
        this.running = false;       // search task is initally not running
        this.props = new Properties();
        this.svnAvailable = true;
        this.actualFileCount = 0;
        initialized = false;
    }
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    Label labelFound;

    @FXML
    Label labelFoundResult;
    
    @FXML
    CheckBox checkBoxFileCount;

    @FXML
    CheckBox checkBoxLink;

    @FXML
    CheckBox checkBoxLineNumber;

    @FXML
    CheckBox checkBoxSvnVersion;
    
    @FXML
    CheckBox checkBoxSvnDescription;
    
    @FXML
    CheckBox checkBoxSvnAuthor;
    
    @FXML
    CheckBox checkBoxSvnDate;
    
    @FXML
    CheckBox checkBoxReviewState;
    
    @FXML
    CheckBox checkBoxReviewDesc;
    
    @FXML
    CheckBox checkBoxTestState;
    
    @FXML
    CheckBox checkBoxTestDesc;

    @FXML
    CheckBox checkBoxAll;

    @FXML
    CheckBox checkBoxReviewAuthor;

    @FXML
    CheckBox checkBoxReviewDate;

    @FXML
    CheckBox checkBoxTestAuthor;

    @FXML
    CheckBox checkBoxTestDate;

    @FXML
    TableView tableField;
    
    @FXML
    GridPane gridPane;
    
    @FXML
    TextField textfieldSearchText;
    
    @FXML
    ProgressIndicator progress;

    @FXML
    private void handleClose(ActionEvent event) {
        abort = true;
	myController.hideStage(SEARCH_STAGE);
    }
    
    @FXML
    private void handleSearch(ActionEvent event) {
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();
        actualFileCount = lxrTestTableController.getActualFileCount() - 1;

        // hide progress indicator
        progress.setDisable(false);
        progress.setVisible(true);
        progress.setProgress(-1.0);
        
        // clear old values
        labelFoundResult.setText("");
        if (!tableField.getColumns().isEmpty()) {
            tableField.getItems().clear();      // clear content
        } 
        
        abort = false;      // set abort to false to enable search
        
        Task<Integer> task;
        
        // Worker task to handle the long running searches 
        task = new Task<Integer>() {
            @Override
            protected Integer call() {
                long time1;
                long time2;
                int count;
                
                time1 = System.currentTimeMillis();
                int searchCount = 0;
                String searchTextLowerCase;
                searchTextLowerCase = (textfieldSearchText.getText()).toLowerCase(locale);
                for (int j=actualFileCount; j<changedFileList.size(); j++) {
                    if (abort) {
                        break;      // synchronously exit loop!
                    }
                    TableEntry entry = changedFileList.get(j);
                    for (int i = 0; i < entry.getHrefText().size(); i++) {
                        if (abort) {
                            break;      // synchronously exit loop!
                        }
                        if (!lxrTestTableController.isRowHidden(entry, i)) {
                            boolean found;
                            found = searchLine (entry, i,  searchTextLowerCase);
                            if (found) {
                                searchCount++;

                                if (i==0) {
                                    String svnVersion = "";
                                    count = 0;
                                    for (String str : entry.getSvnVersion()) {
                                        count++;
                                        if (count == entry.getSvnVersion().size()) {
                                            svnVersion = svnVersion + str;
                                        } else {
                                            svnVersion = svnVersion + str + "\n";
                                        }
                                    }
                                    String svnDesc = "";
                                    count = 0;
                                    for (String str : entry.getSvnDesc()) {
                                        count++;
                                        if (count == entry.getSvnDesc().size()) {
                                            svnDesc = svnDesc + str;
                                        } else {
                                            svnDesc = svnDesc + str + "\n";
                                        }
                                    }
                                    String svnAuthor = "";
                                    count = 0;
                                    for (String str : entry.getSvnAuthor()) {
                                        count++;
                                        if (count == entry.getSvnAuthor().size()) {
                                            svnAuthor = svnAuthor + str;
                                        } else {
                                            svnAuthor = svnAuthor + str + "\n";
                                        }
                                    }
                                    String svnDate = "";
                                    count = 0;
                                    for (String str : entry.getSvnDate()) {
                                        count++;
                                        if (count == entry.getSvnDate().size()) {
                                            svnDate = svnDate + str;
                                        } else {
                                            svnDate = svnDate + str + "\n";
                                        }
                                    }
                                    addNewLineInTable(tableField, 
                                            entry.getFileCount(),
                                            entry.getHrefTextLast(i),
                                            entry.getLineNumberLast(i),
                                            svnVersion,
                                            svnDesc,
                                            svnAuthor,
                                            svnDate,
                                            true,
                                            entry.getReviewStateLast(i),
                                            conv.convertDescriptionString(entry
                                                    .getReviewShortDescriptionLast(i)),
                                            entry.getTestStateLast(i),
                                            conv.convertDescriptionString(entry
                                                    .getTestShortDescriptionLast(i)));
                                } else {
                                    addNewLineInTable(tableField, 
                                            entry.getFileCount(),
                                            entry.getHrefTextLast(i),
                                            entry.getLineNumberLast(i),
                                            "",
                                            "",
                                            "",
                                            "",
                                            true,
                                            entry.getReviewStateLast(i),
                                            conv.convertDescriptionString(entry
                                                    .getReviewShortDescriptionLast(i)),
                                            entry.getTestStateLast(i),
                                            conv.convertDescriptionString(entry
                                                    .getTestShortDescriptionLast(i)));
                                }
                            }
                        }
                    }
                }
                for (int j=0; j<actualFileCount; j++) {
                    if (abort) {
                        break;      // synchronously exit loop!
                    }
                    TableEntry entry = changedFileList.get(j);
                    for (int i = 0; i < entry.getHrefText().size(); i++) {
                        if (abort) {
                            break;      // synchronously exit loop!
                        }
                        if (!lxrTestTableController.isRowHidden(entry, i)) {
                            boolean found;
                            found = searchLine (entry, i,  searchTextLowerCase);
                            if (found) {
                                searchCount++;
    
                                if (i==0) {
                                    String svnVersion = "";
                                    count = 0;
                                    for (String str : entry.getSvnVersion()) {
                                        count++;
                                        if (count == entry.getSvnVersion().size()) {
                                            svnVersion = svnVersion + str;
                                        } else {
                                            svnVersion = svnVersion + str + "\n";
                                        }
                                    }
                                    String svnDesc = "";
                                    count = 0;
                                    for (String str : entry.getSvnDesc()) {
                                        count++;
                                        if (count == entry.getSvnDesc().size()) {
                                            svnDesc = svnDesc + str;
                                        } else {
                                            svnDesc = svnDesc + str + "\n";
                                        }
                                    }
                                    String svnAuthor = "";
                                    count = 0;
                                    for (String str : entry.getSvnAuthor()) {
                                        count++;
                                        if (count == entry.getSvnAuthor().size()) {
                                            svnAuthor = svnAuthor + str;
                                        } else {
                                            svnAuthor = svnAuthor + str + "\n";
                                        }
                                    }
                                    String svnDate = "";
                                    count = 0;
                                    for (String str : entry.getSvnDate()) {
                                        count++;
                                        if (count == entry.getSvnDate().size()) {
                                            svnDate = svnDate + str;
                                        } else {
                                            svnDate = svnDate + str + "\n";
                                        }
                                    }
                                    addNewLineInTable(tableField, 
                                            entry.getFileCount(),
                                            entry.getHrefTextLast(i),
                                            entry.getLineNumberLast(i),
                                            svnVersion,
                                            svnDesc,
                                            svnAuthor,
                                            svnDate,
                                            true,
                                            entry.getReviewStateLast(i),
                                            conv.convertDescriptionString(entry
                                                    .getReviewShortDescriptionLast(i)),
                                            entry.getTestStateLast(i),
                                            conv.convertDescriptionString(entry
                                                    .getTestShortDescriptionLast(i)));
                                } else {
                                    addNewLineInTable(tableField, 
                                            entry.getFileCount(),
                                            entry.getHrefTextLast(i),
                                            entry.getLineNumberLast(i),
                                            "",
                                            "",
                                            "",
                                            "",
                                            true,
                                            entry.getReviewStateLast(i),
                                            conv.convertDescriptionString(entry
                                                    .getReviewShortDescriptionLast(i)),
                                            entry.getTestStateLast(i),
                                            conv.convertDescriptionString(entry
                                                    .getTestShortDescriptionLast(i)));
                                }
                            }
                        }
                    }
                }
                time2 = System.currentTimeMillis();
                long delta = time2 - time1;
                logger.info("DialogSearchController: Search Time = {0}ms" + 
                        delta);
                
                return searchCount;
            }
            
        };
        
        // Event handler to wait for completion of worker task.
        task.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, 
                (WorkerStateEvent t) -> {
                    
            // wait on completion of worker task
            Integer searchCount;
            searchCount = task.getValue();
            labelFoundResult.setText("" + searchCount);

            // Do not delete the following line! It's important to refresh!
            tableField.refresh();

            // hide progress indicator
            progress.setProgress(0);
            progress.setDisable(true);
            progress.setVisible(false);
            running = false;
            
        });
        if (!running) {
            running = true;
            Thread th = new Thread(task);           // define new task
            th.setDaemon(true);                     // set task to daemon state
            th.setName("Search Task");              // set name of task
            th.start();                             // start task as daemon
        }
    }
    
    @FXML
    private void handleCheckBoxFileCount(ActionEvent event) {
        if (checkBoxFileCount.isSelected()) {
            isFileCount = true;
            props.putProperty(props.SEARCH_FILE_COUNT,"true");
        } else {
            isFileCount = false;
            props.putProperty(props.SEARCH_FILE_COUNT,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxLink(ActionEvent event) {
        if (checkBoxLink.isSelected()) {
            isLink = true;
            props.putProperty(props.SEARCH_LINK,"true");
        } else {
            isLink = false;
            props.putProperty(props.SEARCH_LINK,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxLineNumber(ActionEvent event) {
        if (checkBoxLineNumber.isSelected()) {
            isLineNr = true;
            props.putProperty(props.SEARCH_LINE_NR,"true");
        } else {
            isLineNr = false;
            props.putProperty(props.SEARCH_LINE_NR,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxSvnVersion(ActionEvent event) {
        if (checkBoxSvnVersion.isSelected()) {
            isSvnVersion = true;
            props.putProperty(props.SEARCH_SVN_VERSION,"true");
        } else {
            isSvnVersion = false;
            props.putProperty(props.SEARCH_SVN_VERSION,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxSvnDescription(ActionEvent event) {
        if (checkBoxSvnDescription.isSelected()) {
            isSvnDesc = true;
            props.putProperty(props.SEARCH_SVN_DESC,"true");
        } else {
            isSvnDesc = false;
            props.putProperty(props.SEARCH_SVN_DESC,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxSvnAuthor(ActionEvent event) {
        if (checkBoxSvnAuthor.isSelected()) {
            isSvnAuthor = true;
            props.putProperty(props.SEARCH_SVN_AUTHOR,"true");
        } else {
            isSvnAuthor = false;
            props.putProperty(props.SEARCH_SVN_AUTHOR,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxSvnDate(ActionEvent event) {
        if (checkBoxSvnDate.isSelected()) {
            isSvnDate = true;
            props.putProperty(props.SEARCH_SVN_DATE,"true");
        } else {
            isSvnDate = false;
            props.putProperty(props.SEARCH_SVN_DATE,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxReviewState(ActionEvent event) {
        if (checkBoxReviewState.isSelected()) {
            isReviewState = true;
            props.putProperty(props.SEARCH_REVIEW_STATE,"true");
        } else {
            isReviewState = false;
            props.putProperty(props.SEARCH_REVIEW_STATE,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxReviewDesc(ActionEvent event) {
        if (checkBoxReviewDesc.isSelected()) {
            isReviewDesc = true;
            props.putProperty(props.SEARCH_REVIEW_DESC,"true");
        } else {
            isReviewDesc = false;
            props.putProperty(props.SEARCH_REVIEW_DESC,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxTestState(ActionEvent event) {
        if (checkBoxTestState.isSelected()) {
            isTestState = true;
            props.putProperty(props.SEARCH_TEST_STATE,"true");
        } else {
            isTestState = false;
            props.putProperty(props.SEARCH_TEST_STATE,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxTestDesc(ActionEvent event) {
        if (checkBoxTestDesc.isSelected()) {
            isTestDesc = true;
            props.putProperty(props.SEARCH_TEST_DESC,"true");
        } else {
            isTestDesc = false;
            props.putProperty(props.SEARCH_TEST_DESC,"false");
        }
        updateCheckboxAll();
    }
    
    @FXML
    private void handleCheckBoxReviewAuthor(ActionEvent event) {
        if (checkBoxReviewAuthor.isSelected()) {
            isReviewAuthor = true;
            props.putProperty(props.SEARCH_REVIEW_AUTHOR,"true");
        } else {
            isReviewAuthor = false;
            props.putProperty(props.SEARCH_REVIEW_AUTHOR,"false");
        }
        updateCheckboxAll();
    }

    @FXML
    private void handleCheckBoxReviewDate(ActionEvent event) {
        if (checkBoxReviewDate.isSelected()) {
            isReviewDate = true;
            props.putProperty(props.SEARCH_REVIEW_DATE,"true");
        } else {
            isReviewDate = false;
            props.putProperty(props.SEARCH_REVIEW_DATE,"false");
        }
        updateCheckboxAll();
    }

    @FXML
    private void handleCheckBoxTestAuthor(ActionEvent event) {
        if (checkBoxTestAuthor.isSelected()) {
            isTestAuthor = true;
            props.putProperty(props.SEARCH_TEST_AUTHOR,"true");
        } else {
            isTestAuthor = false;
            props.putProperty(props.SEARCH_TEST_AUTHOR,"false");
        }
        updateCheckboxAll();
    }

    @FXML
    private void handleCheckBoxTestDate(ActionEvent event) {
        if (checkBoxTestDate.isSelected()) {
            isTestDate = true;
            props.putProperty(props.SEARCH_TEST_DATE,"true");
        } else {
            isTestDate = false;
            props.putProperty(props.SEARCH_TEST_DATE,"false");
        }
        updateCheckboxAll();
    }

    @FXML
    private void handleCheckBoxAll(ActionEvent event) {
        if (checkBoxAll.isSelected()) {
            checkBoxFileCount.setSelected(true);
            isFileCount = true;
            props.putProperty(props.SEARCH_FILE_COUNT,"true");
            
            checkBoxLink.setSelected(true);
            isLink = true;
            props.putProperty(props.SEARCH_LINK,"true");
            
            checkBoxLineNumber.setSelected(true);
            isLineNr = true;
            props.putProperty(props.SEARCH_LINE_NR,"true");
            
            if (svnAvailable) {
                checkBoxSvnVersion.setSelected(true);
                isSvnVersion = true;
                props.putProperty(props.SEARCH_SVN_VERSION,"true");

                checkBoxSvnDescription.setSelected(true);
                isSvnDesc = true;
                props.putProperty(props.SEARCH_SVN_DESC,"true");

                checkBoxSvnAuthor.setSelected(true);
                isSvnAuthor = true;
                props.putProperty(props.SEARCH_SVN_AUTHOR,"true");

                checkBoxSvnDate.setSelected(true);
                isSvnDate = true;
                props.putProperty(props.SEARCH_SVN_DATE,"true");
            }
            
            checkBoxReviewState.setSelected(true);
            isReviewState = true;
            props.putProperty(props.SEARCH_REVIEW_STATE,"true");
            
            checkBoxReviewDesc.setSelected(true);
            isReviewDesc = true;
            props.putProperty(props.SEARCH_REVIEW_DESC,"true");
            
            checkBoxTestState.setSelected(true);
            isTestState = true;
            props.putProperty(props.SEARCH_TEST_STATE,"true");

            checkBoxTestDesc.setSelected(true);
            isTestDesc = true;
            props.putProperty(props.SEARCH_TEST_DESC,"true");
            
            checkBoxReviewAuthor.setSelected(true);
            isReviewAuthor = true;
            props.putProperty(props.SEARCH_REVIEW_AUTHOR, "true");

            checkBoxReviewDate.setSelected(true);
            isReviewDate = true;
            props.putProperty(props.SEARCH_REVIEW_DATE, "true");

            checkBoxTestAuthor.setSelected(true);
            isTestAuthor = true;
            props.putProperty(props.SEARCH_TEST_AUTHOR, "true");

            checkBoxTestDate.setSelected(true);
            isTestDate = true;
            props.putProperty(props.SEARCH_TEST_DATE, "true");
        } else {
            checkBoxFileCount.setSelected(false);
            isFileCount = false;
            props.putProperty(props.SEARCH_FILE_COUNT,"false");
            
            checkBoxLink.setSelected(false);
            isLink = false;
            props.putProperty(props.SEARCH_LINK,"false");
            
            checkBoxLineNumber.setSelected(false);
            isLineNr = false;
            props.putProperty(props.SEARCH_LINE_NR,"false");
            
            if (svnAvailable) {
                checkBoxSvnVersion.setSelected(false);
                isSvnVersion = false;
                props.putProperty(props.SEARCH_SVN_VERSION,"false");

                checkBoxSvnDescription.setSelected(false);
                isSvnDesc = false;
                props.putProperty(props.SEARCH_SVN_DESC,"false");

                checkBoxSvnAuthor.setSelected(false);
                isSvnAuthor = false;
                props.putProperty(props.SEARCH_SVN_AUTHOR,"false");

                checkBoxSvnDate.setSelected(false);
                isSvnDate = false;
                props.putProperty(props.SEARCH_SVN_DATE,"false");
            }
            
            checkBoxReviewState.setSelected(false);
            isReviewState = false;
            props.putProperty(props.SEARCH_REVIEW_STATE,"false");
            
            checkBoxReviewDesc.setSelected(false);
            isReviewDesc = false;
            props.putProperty(props.SEARCH_REVIEW_DESC,"false");
            
            checkBoxTestState.setSelected(false);
            isTestState = false;
            props.putProperty(props.SEARCH_TEST_STATE,"false");

            checkBoxTestDesc.setSelected(false);
            isTestDesc = false;
            props.putProperty(props.SEARCH_TEST_DESC,"false");
            
            checkBoxReviewAuthor.setSelected(false);
            isReviewAuthor = false;
            props.putProperty(props.SEARCH_REVIEW_AUTHOR, "false");

            checkBoxReviewDate.setSelected(false);
            isReviewDate = false;
            props.putProperty(props.SEARCH_REVIEW_DATE, "false");

            checkBoxTestAuthor.setSelected(false);
            isTestAuthor = false;
            props.putProperty(props.SEARCH_TEST_AUTHOR, "false");

            checkBoxTestDate.setSelected(false);
            isTestDate = false;
            props.putProperty(props.SEARCH_TEST_DATE, "false");
        }
    }

    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // Set focus to textfield
        setFocusOnSearchTextbox();
        
        // locale settings
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
        conv = new ConvertLxrDiffTabStrings(locale);
        
        progress.setDisable(true);
        progress.setVisible(false);
        labelFoundResult.setText("");
        
        // SEARCH_FILE_COUNT
        switch (props.getProperty(props.SEARCH_FILE_COUNT)) {
            case "true":
                isFileCount = true;
                break;
            case "false":
                isFileCount = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_FILE_COUNT,"true");
                isFileCount = true;
                break;
        }
        checkBoxFileCount.setSelected(isFileCount);
        
        // SEARCH_LINK
        switch (props.getProperty(props.SEARCH_LINK)) {
            case "true":
                isLink = true;
                break;
            case "false":
                isLink = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_LINK,"true");
                isLink = true;
                break;
        }
        checkBoxLink.setSelected(isLink);
        
        // SEARCH_LINE_NR
        switch (props.getProperty(props.SEARCH_LINE_NR)) {
            case "true":
                isLineNr = true;
                break;
            case "false":
                isLineNr = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_LINE_NR,"true");
                isLineNr = true;
                break;
        }
        checkBoxLineNumber.setSelected(isLineNr);
        
        // SEARCH_SVN_VERSION
        switch (props.getProperty(props.SEARCH_SVN_VERSION)) {
            case "true":
                isSvnVersion = true;
                break;
            case "false":
                isSvnVersion = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_SVN_VERSION,"true");
                isSvnVersion = true;
                break;
        }
        checkBoxSvnVersion.setSelected(isSvnVersion);
        
        // SEARCH_SVN_DESC
        switch (props.getProperty(props.SEARCH_SVN_DESC)) {
            case "true":
                isSvnDesc = true;
                break;
            case "false":
                isSvnDesc = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_SVN_DESC,"true");
                isSvnDesc = true;
                break;
        }
        checkBoxSvnDescription.setSelected(isSvnDesc);
        
        // SEARCH_SVN_AUTHOR
        switch (props.getProperty(props.SEARCH_SVN_AUTHOR)) {
            case "true":
                isSvnAuthor = true;
                break;
            case "false":
                isSvnAuthor = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_SVN_AUTHOR,"true");
                isSvnAuthor = true;
                break;
        }
        checkBoxSvnAuthor.setSelected(isSvnAuthor);
        
        // SEARCH_SVN_DATE
        switch (props.getProperty(props.SEARCH_SVN_DATE)) {
            case "true":
                isSvnDate = true;
                break;
            case "false":
                isSvnDate = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_SVN_DATE,"true");
                isSvnDate = true;
                break;
        }
        checkBoxSvnDate.setSelected(isSvnDate);
        
        // SEARCH_REVIEW_STATE
        switch (props.getProperty(props.SEARCH_REVIEW_STATE)) {
            case "true":
                isReviewState = true;
                break;
            case "false":
                isReviewState = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_REVIEW_STATE,"true");
                isReviewState = true;
                break;
        }
        checkBoxReviewState.setSelected(isReviewState);
        
        // SEARCH_REVIEW_DESC
        switch (props.getProperty(props.SEARCH_REVIEW_DESC)) {
            case "true":
                isReviewDesc = true;
                break;
            case "false":
                isReviewDesc = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_REVIEW_DESC,"true");
                isReviewDesc = true;
                break;
        }
        checkBoxReviewDesc.setSelected(isReviewDesc);
        
        // SEARCH_TEST_STATE
        switch (props.getProperty(props.SEARCH_TEST_STATE)) {
            case "true":
                isTestState = true;
                break;
            case "false":
                isTestState = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_TEST_STATE,"true");
                isTestState = true;
                break;
        }
        checkBoxTestState.setSelected(isTestState);
        
        // SEARCH_TEST_DESC
        switch (props.getProperty(props.SEARCH_TEST_DESC)) {
            case "true":
                isTestDesc = true;
                break;
            case "false":
                isTestDesc = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_TEST_DESC,"true");
                isTestDesc = true;
                break;
        }
        checkBoxTestDesc.setSelected(isTestDesc);
        
        // SEARCH_REVIEW_AUTHOR
        switch (props.getProperty(props.SEARCH_REVIEW_AUTHOR)) {
            case "true":
                isReviewAuthor = true;
                break;
            case "false":
                isReviewAuthor = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_REVIEW_AUTHOR,"true");
                isReviewAuthor = true;
                break;
        }
        checkBoxReviewAuthor.setSelected(isReviewAuthor);

        // SEARCH_REVIEW_DATE
        switch (props.getProperty(props.SEARCH_REVIEW_DATE)) {
            case "true":
                isReviewDate = true;
                break;
            case "false":
                isReviewDate = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_REVIEW_DATE,"true");
                isReviewDate = true;
                break;
        }
        checkBoxReviewDate.setSelected(isReviewDate);

        // SEARCH_TEST_AUTHOR
        switch (props.getProperty(props.SEARCH_TEST_AUTHOR)) {
            case "true":
                isTestAuthor = true;
                break;
            case "false":
                isTestAuthor = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_TEST_AUTHOR,"true");
                isTestAuthor = true;
                break;
        }
        checkBoxTestAuthor.setSelected(isTestAuthor);

        // SEARCH_TEST_DATE
        switch (props.getProperty(props.SEARCH_TEST_DATE)) {
            case "true":
                isTestDate = true;
                break;
            case "false":
                isTestDate = false;
                break;
            default:
                // It seems to be, that the property is not set. 
                props.putProperty(props.SEARCH_TEST_DATE,"true");
                isTestDate = true;
                break;
        }
        checkBoxTestDate.setSelected(isTestDate);

        updateCheckboxAll();

        tableField.getItems().clear();      // clear content
        tableField.getColumns().clear();    // clear columns
        initSearchTable();                  // init table
    }    

    /**
     * Method to set the table, that is to be searched.
     * 
     * 
     * @param changedFileList   - table, that is to be searched
     */
    public void setTable(List<TableEntry> changedFileList) {
        this.changedFileList = changedFileList;
        labelFoundResult.setText("");       // reset label (because of new table)!
    }
    
    /**
     * Method to set the boolean variable "svnAvailable".
     * 
     * If the variable is set to "true", then SVN is available. Otherwise,
     * SVN is not available and all "svn"-columns are hidden.
     * 
     * 
     * @param svnAvailable
     */
    public void setSvn(boolean svnAvailable) {
        this.svnAvailable = svnAvailable;
        
        if (svnAvailable) {
            checkBoxSvnVersion.setVisible(true);
            checkBoxSvnDescription.setVisible(true);
            checkBoxSvnAuthor.setVisible(true);
            checkBoxSvnDate.setVisible(true);
        } else {
            checkBoxSvnVersion.setVisible(false);
            checkBoxSvnDescription.setVisible(false);
            checkBoxSvnAuthor.setVisible(false);
            checkBoxSvnDate.setVisible(false);
        }
        tableField.getItems().clear();      // clear content
        tableField.getColumns().clear();    // clear columns
        initSearchTable();              // init table
    }
    
    /**
     * Method to set the focus on the search TextBox.
     * 
     */
    public void setFocusOnSearchTextbox() {
        // Set focus to textfield
        Platform.runLater(() -> {
            textfieldSearchText.requestFocus();
        });        
    }
    
    /**
     * Method to set the actual start position for the search operation.
     * 
     * Example:
     * If the table has 5000 lines and the user has selected the actual 
     * line 2300, then the search of "index" is beginning at line 2300.
     * 
     * 
     * @param fileCount     - actual starting position for search operation
     * 
     */
    public void setActualFileCount(int fileCount) {
        this.actualFileCount = fileCount;
    }
    
    /**
     * Build TextFlow with selected text marked in color ORANGE. 
     * 
     * The search is case independent but the returned TextFlow is 
     * case dependent.
     * 
     * @param text      - string with text
     * @param filter    - string to select in text
     * @return          - TextFlow with colored text
     */
    private TextFlow buildTextFlow(String text, String filter) {
        TextFlow textFlow = new TextFlow();
        String[] split = text.split("\n");
        int i = 0;
        for (String str : split) {
            i++;
            boolean end = false;
            String str1 = str;
            while (!end) {
                int filterIndex = str1.toLowerCase().indexOf(filter.toLowerCase());
                String strBefore;

                if (filterIndex == -1) {
                    if (i == split.length) {
                        if (!str1.equals("")) {
                            Text textAll = new Text(str1);
                            textFlow.getChildren().add(textAll);
                        } else {
                            // do nothing
                        }
                    } else {
                        if (!str1.equals("")) {
                            Text textAll = new Text(str1 + "\n");
                            textFlow.getChildren().add(textAll);
                        } else {
                            Text textAll = new Text("\n");
                            textFlow.getChildren().add(textAll);
                        }
                    }
                    end = true;
                } else {
                    strBefore = str1.substring(0, filterIndex);
                    Text textBefore = new Text(strBefore);
                    Text textFilter = new Text(str1.substring(filterIndex, 
                            filterIndex + 
                                    filter.length())); //instead of "filter" to keep "case"
                    textFilter.setFill(Color.ORANGE);
                    TextFlow textFlow1 = new TextFlow(textBefore); 
                    TextFlow textFlow2 = new TextFlow(textFilter);
                    textFlow.getChildren().add(textFlow1);
                    textFlow.getChildren().add(textFlow2);
                    str1 = str1.substring(filterIndex + filter.length());
                }
            }
        }
        
        return textFlow;
    }        
    
    private void initSearchTable() {
//        if (!initialized) {
//            initialized = true;
            // Avoid change of the height of the row in graphics mode with textflow!
            tableField.setFixedCellSize(60);

            // init tableview
            columnWithFileCount = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemFileCount"));
            columnWithFileCount.setCellValueFactory(
                    new PropertyValueFactory<>("fileCount"));
            columnWithFileCount.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isFileCount && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithLink = new TableColumn<>("Link");
            columnWithLink.setCellValueFactory(
                    new PropertyValueFactory<>("hrefText"));
            columnWithLink.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                if (isLink && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithLineNumber = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemLineNumber"));
            columnWithLineNumber.setCellValueFactory(
                    new PropertyValueFactory<>("lineNumber"));
            columnWithLineNumber.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isLineNr && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithSvnVersion = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemSvnVersion"));
            columnWithSvnVersion.setCellValueFactory(
                    new PropertyValueFactory<>("svnVersion"));
            columnWithSvnVersion.setCellFactory((TableColumn<String, String> column) -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isSvnVersion && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithSvnDesc = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemSvnDescription"));
            columnWithSvnDesc.setCellValueFactory(
                    new PropertyValueFactory<>("svnDesc"));
            columnWithSvnDesc.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isSvnDesc && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithSvnAuthor = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemSvnAuthor"));
            columnWithSvnAuthor.setCellValueFactory(
                    new PropertyValueFactory<>("svnAuthor"));
            columnWithSvnAuthor.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isSvnAuthor && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithSvnDate = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemSvnDate"));
            columnWithSvnDate.setCellValueFactory(
                    new PropertyValueFactory<>("svnDate"));
            columnWithSvnDate.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isSvnDate && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithReviewState = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemReviewState"));
            columnWithReviewState.setCellValueFactory(
                    new PropertyValueFactory<>("reviewState"));
            columnWithReviewState.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isReviewState && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithReviewShortDescription = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemReviewShortDescription"));
            columnWithReviewShortDescription.setCellValueFactory(
                    new PropertyValueFactory<>("reviewShortDescription"));
            columnWithReviewShortDescription.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isReviewDesc && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithTestState = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemTestState"));
            columnWithTestState.setCellValueFactory(
                    new PropertyValueFactory<>("testState"));
            columnWithTestState.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isTestState && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });

            columnWithTestShortDescription = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemTestShortDescription"));
            columnWithTestShortDescription.setCellValueFactory(
                    new PropertyValueFactory<>("testShortDescription"));
            columnWithTestShortDescription.setCellFactory(column -> {
                    return new TableCell<String, String>() {
                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);
                            if (item == null || empty) {
                                setGraphic(null);
                                setText(null);
                                setStyle("");
                            } else {        
                                String filter = textfieldSearchText.getText();
                                setGraphic(null);
                                if (isTestDesc && !filter.isEmpty() && 
                                        item.toLowerCase().contains(filter.toLowerCase())) {
                                    String[] split = item.split("\n");
                                    String result = "";
                                    int i = 0;
                                    for (String str : split) {
                                        i++;
                                        if (str.toLowerCase().contains(filter.toLowerCase())) {
                                            if (i == split.length) {
                                                result = result + str;
                                            } else {
                                                result = result + str + "\n";
                                            }
                                        }
                                    }
                                    setGraphic(buildTextFlow(result, filter));
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }
                                else {
                                    setGraphic(null);

                                    Text textAll = new Text(item);
                                    textAll.setFill(Color.DARKGRAY);
                                    TextFlow textFlow = new TextFlow();
                                    textFlow.getChildren().add(textAll);

                                    setGraphic(textFlow);
                                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                }   
                            }
                        }
                    };
            });
//        }

        if (svnAvailable) {
            tableField.getColumns().addAll(columnWithFileCount,
                    columnWithLink, columnWithLineNumber, 
                    columnWithSvnVersion,
                    columnWithSvnDesc, columnWithSvnAuthor, 
                    columnWithSvnDate,
                    columnWithReviewState, 
                    columnWithReviewShortDescription,
                    columnWithTestState, columnWithTestShortDescription);
        } else {
            tableField.getColumns().addAll(columnWithFileCount,
                    columnWithLink, columnWithLineNumber, 
                    columnWithReviewState, 
                    columnWithReviewShortDescription,
                    columnWithTestState, columnWithTestShortDescription);
        }
	// enable multi-selection
	tableField.getSelectionModel().setCellSelectionEnabled(true);
	tableField.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        Platform.runLater(() -> {
            // resize listener
            myController.getStage(SEARCH_STAGE).getScene()
                    .widthProperty()
                    .addListener((ObservableValue<? extends Number> observableValue, 
                            Number oldSceneWidth, Number newSceneWidth) -> {
                        double width = myController.getStage(SEARCH_STAGE).getScene()
                                .getWidth();
                        if (svnAvailable) {
                            if (width < 400.0) {
                                columnWithFileCount.setPrefWidth(30.0);
                                columnWithLink.setPrefWidth(60.0);
                                columnWithLineNumber.setPrefWidth(30.0);
                                columnWithSvnVersion.setPrefWidth(30.0);
                                columnWithSvnDesc.setPrefWidth(60.0);
                                columnWithSvnAuthor.setPrefWidth(30.0);
                                columnWithSvnDate.setPrefWidth(30.0);
                                columnWithReviewState.setPrefWidth(30.0);
                                columnWithTestState.setPrefWidth(30.0);
                                columnWithReviewShortDescription.setPrefWidth(30.0);
                                columnWithTestShortDescription.setPrefWidth(30.0);
                            } else if (width < 500.0) {
                                columnWithFileCount.setPrefWidth(30.0);
                                columnWithLink.setPrefWidth(60.0);
                                columnWithLineNumber.setPrefWidth(30.0);
                                columnWithSvnVersion.setPrefWidth(60.0);
                                columnWithSvnDesc.setPrefWidth(60.0);
                                columnWithSvnAuthor.setPrefWidth(60.0);
                                columnWithSvnDate.setPrefWidth(30.0);
                                columnWithReviewState.setPrefWidth(30.0);
                                columnWithTestState.setPrefWidth(30.0);
                                columnWithReviewShortDescription.setPrefWidth(30.0);
                                columnWithTestShortDescription.setPrefWidth(30.0);
                            } else if (width < 700.0) {
                                columnWithFileCount.setPrefWidth(60.0);
                                columnWithLink.setPrefWidth(60.0);
                                columnWithLineNumber.setPrefWidth(60.0);
                                columnWithSvnVersion.setPrefWidth(60.0);
                                columnWithSvnDesc.setPrefWidth(60.0);
                                columnWithSvnAuthor.setPrefWidth(60.0);
                                columnWithSvnDate.setPrefWidth(60.0);
                                columnWithReviewState.setPrefWidth(60.0);
                                columnWithTestState.setPrefWidth(60.0);
                                columnWithReviewShortDescription.setPrefWidth(60.0);
                                columnWithTestShortDescription.setPrefWidth(60.0);
                            } else if (width < 900.0) {
                                columnWithFileCount.setPrefWidth(60.0);
                                columnWithLink.setPrefWidth(100.0);
                                columnWithLineNumber.setPrefWidth(60.0);
                                columnWithSvnVersion.setPrefWidth(100.0);
                                columnWithSvnDesc.setPrefWidth(100.0);
                                columnWithSvnAuthor.setPrefWidth(100.0);
                                columnWithSvnDate.setPrefWidth(100.0);
                                columnWithReviewState.setPrefWidth(60.0);
                                columnWithTestState.setPrefWidth(60.0);
                                columnWithReviewShortDescription.setPrefWidth(60.0);
                                columnWithTestShortDescription.setPrefWidth(60.0);
                            } else if (width < 1100.0) {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(100.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithSvnVersion.setPrefWidth(100.0);
                                columnWithSvnDesc.setPrefWidth(100.0);
                                columnWithSvnAuthor.setPrefWidth(100.0);
                                columnWithSvnDate.setPrefWidth(100.0);
                                columnWithReviewState.setPrefWidth(100.0);
                                columnWithTestState.setPrefWidth(100.0);
                                columnWithReviewShortDescription.setPrefWidth(100.0);
                                columnWithTestShortDescription.setPrefWidth(100.0);
                            } else if (width < 1500.0) {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(200.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithSvnVersion.setPrefWidth(100.0);
                                columnWithSvnDesc.setPrefWidth(200.0);
                                columnWithSvnAuthor.setPrefWidth(100.0);
                                columnWithSvnDate.setPrefWidth(100.0);
                                columnWithReviewState.setPrefWidth(150.0);
                                columnWithTestState.setPrefWidth(150.0);
                                columnWithReviewShortDescription.setPrefWidth(100.0);
                                columnWithTestShortDescription.setPrefWidth(100.0);
                            } else if (width < 1700.0) {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(200.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithSvnVersion.setPrefWidth(100.0);
                                columnWithSvnDesc.setPrefWidth(200.0);
                                columnWithSvnAuthor.setPrefWidth(100.0);
                                columnWithSvnDate.setPrefWidth(200.0);
                                columnWithReviewState.setPrefWidth(150.0);
                                columnWithTestState.setPrefWidth(150.0);
                                columnWithReviewShortDescription.setPrefWidth(150.0);
                                columnWithTestShortDescription.setPrefWidth(150.0);
                            } else if (width < 1900.0) {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(200.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithSvnVersion.setPrefWidth(100.0);
                                columnWithSvnDesc.setPrefWidth(200.0);
                                columnWithSvnAuthor.setPrefWidth(100.0);
                                columnWithSvnDate.setPrefWidth(200.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(200.0);
                                columnWithReviewShortDescription.setPrefWidth(200.0);
                                columnWithTestShortDescription.setPrefWidth(200.0);
                            } else if (width < 2100.0) {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(300.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithSvnVersion.setPrefWidth(100.0);
                                columnWithSvnDesc.setPrefWidth(200.0);
                                columnWithSvnAuthor.setPrefWidth(200.0);
                                columnWithSvnDate.setPrefWidth(200.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(200.0);
                                columnWithReviewShortDescription.setPrefWidth(200.0);
                                columnWithTestShortDescription.setPrefWidth(200.0);
                            } else if (width < 2300.0) {
                                columnWithFileCount.setPrefWidth(150.0);
                                columnWithLink.setPrefWidth(400.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithSvnVersion.setPrefWidth(150.0);
                                columnWithSvnDesc.setPrefWidth(300.0);
                                columnWithSvnAuthor.setPrefWidth(200.0);
                                columnWithSvnDate.setPrefWidth(200.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(250.0);
                                columnWithReviewShortDescription.setPrefWidth(200.0);
                                columnWithTestShortDescription.setPrefWidth(250.0);
                            } else {
                                columnWithFileCount.setPrefWidth(150.0);
                                columnWithLink.setPrefWidth(500.0);
                                columnWithLineNumber.setPrefWidth(150.0);
                                columnWithSvnVersion.setPrefWidth(150.0);
                                columnWithSvnDesc.setPrefWidth(500.0);
                                columnWithSvnAuthor.setPrefWidth(200.0);
                                columnWithSvnDate.setPrefWidth(300.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(300.0);
                                columnWithReviewShortDescription.setPrefWidth(200.0);
                                columnWithTestShortDescription.setPrefWidth(300.0);
                            }
                        } else {
                            if (width < 400.0) {
                                columnWithFileCount.setPrefWidth(60.0);
                                columnWithLink.setPrefWidth(60.0);
                                columnWithLineNumber.setPrefWidth(60.0);
                                columnWithReviewState.setPrefWidth(60.0);
                                columnWithTestState.setPrefWidth(60.0);
                                columnWithReviewShortDescription.setPrefWidth(30.0);
                                columnWithTestShortDescription.setPrefWidth(30.0);
                            } else if (width < 500.0) {
                                columnWithFileCount.setPrefWidth(60.0);
                                columnWithLink.setPrefWidth(60.0);
                                columnWithLineNumber.setPrefWidth(60.0);
                                columnWithReviewState.setPrefWidth(60.0);
                                columnWithTestState.setPrefWidth(60.0);
                                columnWithReviewShortDescription.setPrefWidth(60.0);
                                columnWithTestShortDescription.setPrefWidth(60.0);
                            } else if (width < 700.0) {
                                columnWithFileCount.setPrefWidth(60.0);
                                columnWithLink.setPrefWidth(100.0);
                                columnWithLineNumber.setPrefWidth(60.0);
                                columnWithReviewState.setPrefWidth(100.0);
                                columnWithTestState.setPrefWidth(100.0);
                                columnWithReviewShortDescription.setPrefWidth(100.0);
                                columnWithTestShortDescription.setPrefWidth(100.0);
                            } else if (width < 900.0) {
                                columnWithFileCount.setPrefWidth(80.0);
                                columnWithLink.setPrefWidth(300.0);
                                columnWithLineNumber.setPrefWidth(80.0);
                                columnWithReviewState.setPrefWidth(150.0);
                                columnWithTestState.setPrefWidth(150.0);
                                columnWithReviewShortDescription.setPrefWidth(100.0);
                                columnWithTestShortDescription.setPrefWidth(100.0);
                            } else if (width < 1100.0) {
                                columnWithFileCount.setPrefWidth(80.0);
                                columnWithLink.setPrefWidth(300.0);
                                columnWithLineNumber.setPrefWidth(80.0);
                                columnWithReviewState.setPrefWidth(150.0);
                                columnWithTestState.setPrefWidth(150.0);
                                columnWithReviewShortDescription.setPrefWidth(200.0);
                                columnWithTestShortDescription.setPrefWidth(200.0);
                            } else if (width < 1500.0) {
                                columnWithFileCount.setPrefWidth(80.0);
                                columnWithLink.setPrefWidth(400.0);
                                columnWithLineNumber.setPrefWidth(80.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(200.0);
                                columnWithReviewShortDescription.setPrefWidth(200.0);
                                columnWithTestShortDescription.setPrefWidth(200.0);
                            } else if (width < 1700.0) {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(500.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(200.0);
                                columnWithReviewShortDescription.setPrefWidth(200.0);
                                columnWithTestShortDescription.setPrefWidth(200.0);
                            } else if (width < 1900.0) {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(600.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(200.0);
                                columnWithReviewShortDescription.setPrefWidth(100.0);
                                columnWithTestShortDescription.setPrefWidth(100.0);
                            } else if (width < 2100.0) {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(700.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(200.0);
                                columnWithReviewShortDescription.setPrefWidth(200.0);
                                columnWithTestShortDescription.setPrefWidth(200.0);
                            } else if (width < 2300.0) {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(700.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(200.0);
                                columnWithReviewShortDescription.setPrefWidth(300.0);
                                columnWithTestShortDescription.setPrefWidth(300.0);
                            } else {
                                columnWithFileCount.setPrefWidth(100.0);
                                columnWithLink.setPrefWidth(800.0);
                                columnWithLineNumber.setPrefWidth(100.0);
                                columnWithReviewState.setPrefWidth(200.0);
                                columnWithTestState.setPrefWidth(200.0);
                                columnWithReviewShortDescription.setPrefWidth(400.0);
                                columnWithTestShortDescription.setPrefWidth(400.0);
                            }
                        }
            });
        });
    }

    private void addNewLineInTable(TableView<SearchDataModel> tableField, 
            String fileCount,
            String hrefText,
            String lineNumber, String svnVersion, String svnDesc,
            String svnAuthor, String svnDate, boolean svn,
            TableEntry.State reviewState,
            String reviewShortDescription,
            TableEntry.State testState,
            String testShortDescription) {
        SearchDataModel testDataModel = new SearchDataModel();

        testDataModel.setFileCount(fileCount);
        testDataModel.setHrefText(hrefText);
        testDataModel.setLineNumber(lineNumber, locale);
        if (svn) {
            testDataModel.setSvnVersion(svnVersion);
            testDataModel.setSvnDesc(svnDesc);
            testDataModel.setSvnAuthor(svnAuthor);
            testDataModel.setSvnDate(svnDate);
        } else {
            // no SVN!
        }
        testDataModel.setReviewState(reviewState.getName());
        testDataModel.setReviewShortDescription(reviewShortDescription, locale);
        testDataModel.setTestState(testState.getName());
        testDataModel.setTestShortDescription(testShortDescription, locale);

        tableField.getItems().add(testDataModel);
        tableField.setRowFactory( tv -> {
            TableRow<SearchDataModel> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                final int f1 = Integer.parseInt(row.getItem().getFileCount());
                final int index = f1-1;
                final int size = changedFileList.get(index).getLineNumber().size();
                int l2 = 0;
                for (int i = 0; i < size; i++) {
                    String vgl = changedFileList.get(index).getLineNumberLast(i);
                    if (vgl.equals(row.getItem().getLineNumber())) {
                        l2 = i;
                        break;
                    }
                }
                // jump to the clicked result an select the appropriate line
                lxrTestTableController.jumpToLine(f1, l2); 
            });
            return row ;
        });
        
        // enable copy/paste
        TableUtils tableUtils = new TableUtils();
        tableUtils.installCopyPasteHandler(tableField);
        
        // set context menu for copy/paste
        ContextMenu contextMenu = new ContextMenu();
        MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem.setOnAction((ActionEvent event) -> {
            tableUtils.copySelectionToClipboard(tableField);
        });
        markAllMenuItem.setOnAction((ActionEvent event) -> {
            tableField.getSelectionModel().selectAll();
        });
        contextMenu.getItems().addAll(copyMenuItem, markAllMenuItem);
        tableField.setContextMenu(contextMenu);
    }

    private boolean isAllSelected() {
        boolean all = true;
        
        if (!isFileCount) {
            all = false;
        }
        if (!isLink) {
            all = false;
        }
        if (!isLineNr) {
            all = false;
        }
        if (svnAvailable) {
            if (!isSvnVersion) {
                all = false;
            }
            if (!isSvnDesc) {
                all = false;
            }
            if (!isSvnAuthor) {
                all = false;
            }
            if (!isSvnDate) {
                all = false;
            }
        }
        if (!isReviewState) {
            all = false;
        }
        if (!isReviewDesc) {
            all = false;
        }
        if (!isTestState) {
            all = false;
        }
        if (!isTestDesc) {
            all = false;
        }
        if (!isReviewAuthor) {
            all = false;
        }
        if (!isReviewDate) {
            all = false;
        }
        if (!isTestAuthor) {
            all = false;
        }
        if (!isTestDate) {
            all = false;
        }

        return all;
    }
    
    private boolean isAllDeselected() {
        boolean deselected = true;
        
        if (isFileCount) {
            deselected = false;
        }
        if (isLink) {
            deselected = false;
        }
        if (isLineNr) {
            deselected = false;
        }
        if (svnAvailable) {
            if (isSvnVersion) {
                deselected = false;
            }
            if (isSvnDesc) {
                deselected = false;
            }
            if (isSvnAuthor) {
                deselected = false;
            }
            if (isSvnDate) {
                deselected = false;
            }
        }
        if (isReviewState) {
            deselected = false;
        }
        if (isReviewDesc) {
            deselected = false;
        }
        if (isTestState) {
            deselected = false;
        }
        if (isTestDesc) {
            deselected = false;
        }
        if (isReviewAuthor) {
            deselected = false;
        }
        if (isReviewDate) {
            deselected = false;
        }
        if (isTestAuthor) {
            deselected = false;
        }
        if (isTestDate) {
            deselected = false;
        }

        return deselected;
    }

    private void updateCheckboxAll() {
        if (isAllSelected()) {
            checkBoxAll.setIndeterminate(false);
            checkBoxAll.setSelected(true);
        } else if (isAllDeselected()) {
            checkBoxAll.setIndeterminate(false);
            checkBoxAll.setSelected(false);
        } else {
            checkBoxAll.setIndeterminate(true);
        }
    }
    
    private boolean searchLine (TableEntry entry, int i, 
            String searchTextLowerCase) {
        boolean found = false;
        if (isFileCount && (i == 0)) {
            String text = entry.getFileCount().toLowerCase(locale);
            if (text.contains(searchTextLowerCase)) {
                found = true;
            }
        }
        if (isLink) {
            String text = entry.getHrefTextLast(i).toLowerCase(locale);
            if (text.contains(searchTextLowerCase)) {
                found = true;
            }
        }
        if (isLineNr) {
            String text = entry.getLineNumberLast(i);
            text = conv.convertLineNumberString(text);
            text = text.toLowerCase(locale);
            if (text.contains(searchTextLowerCase)) {
                found = true;
            }
        }
        if (svnAvailable) {
            if (isSvnVersion && (i==0)) {
                for (String str : entry.getSvnVersion()) {
                    String text = str.toLowerCase(locale);
                    if (text.contains(searchTextLowerCase)) {
                        found = true;
                    }
                }
            }
            if (isSvnDesc && (i==0)) {
                for (String str : entry.getSvnDesc()) {
                    String text = str.toLowerCase(locale);
                    if (text.contains(searchTextLowerCase)) {
                        found = true;
                    }
                }
            }
            if (isSvnAuthor && (i==0)) {
                for (String str : entry.getSvnAuthor()) {
                    String text = str.toLowerCase(locale);
                    if (text.contains(searchTextLowerCase)) {
                        found = true;
                    }
                }
            }
            if (isSvnDate && (i==0)) {
                for (String str : entry.getSvnDate()) {
                    String text = str.toLowerCase(locale);
                    if (text.contains(searchTextLowerCase)) {
                        found = true;
                    }
                }
            }
        }
        if (isReviewState) {
            String text = entry.getReviewStateLast(i).getName()
                    .toLowerCase(locale);
            if (text.contains(searchTextLowerCase)) {
                found = true;
            }
        }
        if (isReviewDesc) {
            String text = conv.convertDescriptionString(entry.getReviewShortDescriptionLast(i));
            text = conv.convertDescriptionString(text);
            text = text.toLowerCase(locale);
            String htmlText = entry.getReviewDescriptionLast(i)
                    .toLowerCase(locale);
            // replace html tags
            htmlText = htmlText.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ");
            if (text.contains(searchTextLowerCase)) {
                found = true;
            } else if (htmlText.contains(searchTextLowerCase)) {
                found = true;
            }
        }
        if (isReviewAuthor) {
            String text = entry.getReviewDescAuthorLast(i).toLowerCase(locale);
            if (text.contains(searchTextLowerCase)) {
                found = true;
            }
            text = entry.getReviewResAuthorLast(i).toLowerCase(locale);
            if (text.contains(searchTextLowerCase)) {
                found = true;
            }
        }
        if (isTestState) {
            String text = entry.getTestStateLast(i).getName()
                    .toLowerCase(locale);
            if (text.contains(searchTextLowerCase)) {
                found = true;
            }
        }
        if (isTestDesc) {
            String text = conv.convertDescriptionString(
                    entry.getTestShortDescriptionLast(i));
            text = conv.convertDescriptionString(text);
            text = text.toLowerCase(locale);
            String htmlText = entry.getTestDescriptionLast(i)
                    .toLowerCase(locale);
            // replace html tags
            htmlText = htmlText.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ");
            if (text.contains(searchTextLowerCase)) {
                found = true;
            } else if (htmlText.contains(searchTextLowerCase)) {
                found = true;
            }
        }
        if (isTestAuthor) {
            String text = entry.getTestDescAuthorLast(i).toLowerCase(locale);
            if (text.contains(searchTextLowerCase)) {
                found = true;
            }
            text = entry.getTestResAuthorLast(i).toLowerCase(locale);
            if (text.contains(searchTextLowerCase)) {
                found = true;
            }
        }
        
        return found;
    }
}
