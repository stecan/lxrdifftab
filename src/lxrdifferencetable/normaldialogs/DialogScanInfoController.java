/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.util.Callback;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.SCAN_INFO_STAGE;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.data.LogEntry;
import lxrdifferencetable.data.ScanInfoDataModel;
import lxrdifferencetable.tablecells.StateInfoEditingCell;
import lxrdifferencetable.tools.Properties;

/**
 * FXML Controller class
 *
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class DialogScanInfoController implements Initializable, ControlledStage {

    private StagesController myController;
    
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;
    private String country;
    private boolean initialized;
    
    private TableColumn<ScanInfoDataModel, String> columnDescription;
    private TableColumn<ScanInfoDataModel, String> columnMessage1;
    
        
    public DialogScanInfoController() {
        this.props = new Properties();
        initialized = false;
    }
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    TableView tableField;
    
    @FXML
    private void handleClose(ActionEvent event) {
        myController.hideStage(SCAN_INFO_STAGE);
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
    }    
    
    private void initTestTable() {
        //Create a customer cell factory so that cells can support editing.
        Callback<TableColumn<ScanInfoDataModel, String>, 
                TableCell<ScanInfoDataModel, String>> cellFactory;
        cellFactory = new Callback<TableColumn<ScanInfoDataModel, String>, 
                TableCell<ScanInfoDataModel, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                StateInfoEditingCell cell = new StateInfoEditingCell();
                cell.widthProperty()
                        .addListener((ObservableValue<? extends Number> observableValue, 
                                Number oldSceneWidth, Number newSceneWidth) -> {
                    Text text = new Text();
                    cell.setGraphic(text);
                    text.wrappingWidthProperty().bind(cell.widthProperty());
                    text.textProperty().bind(cell.itemProperty());
                });
                return cell;
            }
        };
        
        // init tableview
        columnDescription = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemDescription"));
        columnDescription.setCellValueFactory(
                new PropertyValueFactory<>("description"));
        columnDescription.setCellFactory(cellFactory);
        columnMessage1 = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemMessage"));
        columnMessage1.setCellValueFactory(
                new PropertyValueFactory<>("message1"));
        columnMessage1.setCellFactory(cellFactory);

        tableField.getColumns().addAll(columnDescription,
                columnMessage1);

	// enable multi-selection
	tableField.getSelectionModel().setCellSelectionEnabled(true);
	tableField.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // resize listener
        myController.getStage(SCAN_INFO_STAGE).getScene()
                .widthProperty()
                .addListener((ObservableValue<? extends Number> observableValue, 
                        Number oldSceneWidth, Number newSceneWidth) -> {
                    double width = myController.getStage(SCAN_INFO_STAGE).getScene()
                            .getWidth();
                    if (width < 400.0) {
                        columnDescription.setPrefWidth(250.0);
                        columnMessage1.setPrefWidth(100.0);
                    } else if (width < 500.0) {
                        columnDescription.setPrefWidth(300.0);
                        columnMessage1.setPrefWidth(100.0);
                    } else if (width < 700.0) {
                        columnDescription.setPrefWidth(350.0);
                        columnMessage1.setPrefWidth(250.0);
                    } else if (width < 900.0) {
                        columnDescription.setPrefWidth(350.0);
                        columnMessage1.setPrefWidth(350.0);
                    } else if (width < 1100.0) {
                        columnDescription.setPrefWidth(400.0);
                        columnMessage1.setPrefWidth(500.0);
                    } else if (width < 1300.0) {
                        columnDescription.setPrefWidth(400.0);
                        columnMessage1.setPrefWidth(600.0);
                    } else if (width < 1500.0) {
                        columnDescription.setPrefWidth(400.0);
                        columnMessage1.setPrefWidth(700.0);
                    } else if (width < 1700.0) {
                        columnDescription.setPrefWidth(400.0);
                        columnMessage1.setPrefWidth(800.0);
                    } else if (width < 1900.0) {
                        columnDescription.setPrefWidth(400.0);
                        columnMessage1.setPrefWidth(900.0);
                    } else if (width < 2100.0) {
                        columnDescription.setPrefWidth(400.0);
                        columnMessage1.setPrefWidth(1000.0);
                    } else if (width < 2300.0) {
                        columnDescription.setPrefWidth(400.0);
                        columnMessage1.setPrefWidth(1200.0);
                    } else {
                        columnDescription.setPrefWidth(400.0);
                        columnMessage1.setPrefWidth(1400.0);
                    }
        });
    }
    
    private void addNewLineInTable(String description, String message1, 
            String message2, String message3, String message4) {
        ScanInfoDataModel scanDataModel = new ScanInfoDataModel();
        scanDataModel.setDescription(description);
        String message = message1;
        if ("".equals(message2)) {
            // do nothing
        } else {
            message = message + "\n" + message2;
        }
        if ("".equals(message3)) {
            // do nothing
        } else {
            message = message + "\n" + message3;
        }
        if ("".equals(message4)) {
            // do nothing
        } else {
            message = message + "\n" + message4;
        }
        scanDataModel.setMessage1(message);
        tableField.getItems().add(scanDataModel);
        tableField.setEditable(true);
        // enable copy/paste
        lxrdifferencetable.tools.TableUtils tableUtils = 
                new lxrdifferencetable.tools.TableUtils();
        tableUtils.installCopyPasteHandler(tableField);
        
        // set context menu for copy/paste
        ContextMenu contextMenu = new ContextMenu();
        MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem.setOnAction((ActionEvent event) -> {
            tableUtils.copySelectionToClipboard(tableField);
        });
        markAllMenuItem.setOnAction((ActionEvent event) -> {
            tableField.getSelectionModel().selectAll();
        });
        contextMenu.getItems().addAll(copyMenuItem, markAllMenuItem);
        tableField.setContextMenu(contextMenu);
    }
    
    public void setRepoLog(List<LogEntry> repoLog) {
        // clear table
        tableField.getItems().clear();      // clear content
        if (!initialized) {
            initialized = true;
            initTestTable();

            String title;
            title = java.util.ResourceBundle
                    .getBundle("lxrdifferencetable/Bundle", locale)
                    .getString("programName") +
                    " - " + 
                    java.util.ResourceBundle
                            .getBundle("lxrdifferencetable/Bundle", locale)
                            .getString("title.ScanInfo");
            myController.getStage(SCAN_INFO_STAGE).setTitle(title);        
        }
        // it's time to generate the log table
        int repoCount = 0;
        int blacklistCount = 0;
        for (LogEntry log : repoLog) {
            try {
                switch (log.getDescription()) {
                    case "log.repositoryMismatch":
                        repoCount++;
                        addNewLineInTable(
                                ResourceBundle.getBundle("lxrdifferencetable/Bundle", 
                                        locale).getString(log.getDescription()) + 
                                        " " + repoCount,
                                log.getMessage1(), log.getMessage2(), 
                                log.getMessage3(), log.getMessage4());
                        break;
                    case "log.repositoryName":
                        repoCount++;
                        addNewLineInTable(
                                ResourceBundle.getBundle("lxrdifferencetable/Bundle", 
                                        locale).getString(log.getDescription()) + 
                                        " " + repoCount,
                                log.getMessage1(), log.getMessage2(), 
                                log.getMessage3(), log.getMessage4());
                        break;
                    case "log.repositoryRevisions":
                        addNewLineInTable(
                                ResourceBundle.getBundle("lxrdifferencetable/Bundle", 
                                        locale).getString(log.getDescription()) + 
                                        " " + repoCount,
                                log.getMessage1(), log.getMessage2(), 
                                log.getMessage3(), log.getMessage4());
                        break;
                    case "log.blacklistFiles":
                        blacklistCount++;
                        addNewLineInTable(
                                ResourceBundle.getBundle("lxrdifferencetable/Bundle", 
                                        locale).getString(log.getDescription()) + 
                                        " " + blacklistCount,
                                log.getMessage1(), log.getMessage2(), 
                                log.getMessage3(), log.getMessage4());
                        break;
                    default:
                        addNewLineInTable(
                                ResourceBundle.getBundle("lxrdifferencetable/Bundle", 
                                        locale).getString(log.getDescription()),
                                log.getMessage1(), log.getMessage2(), 
                                log.getMessage3(), log.getMessage4());
                        break;
                }
            } catch (Exception ex) {
                // Do nothing:
                // Be quiet. Only relevant if resource entries are changed.
            }
        }
    }
}
