/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.BROWSE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.CTAGS_STAGE;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.data.CtagsDataModel;
import lxrdifferencetable.tools.CtagsEntry;
import lxrdifferencetable.tools.CtagsFileEntry;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.TableUtils;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogTagToFileController implements Initializable, ControlledStage {

    private StagesController myController;
    private DialogBrowseController dialogBrowseController;
    
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;       
    private boolean initialized;
    private String filename;
    private String version;
    private String prepath;
    
    private HashMap<String, CtagsEntry> ctags;
    private HashMap<String, CtagsFileEntry> ctagsFile;
    
    
    private TableColumn<CtagsDataModel, String> columnWithFile;
    private TableColumn<CtagsDataModel, String> columnWithLine;
    
    
    public DialogTagToFileController() {
        props = new Properties();
        initialized = false;
        filename = "";
        prepath = "";
    }
    
    @FXML
    Label baseFile;
    
    @FXML
    Label baseDirectory;
    
    @FXML
    Label referenceCtag;
    
    @FXML
    private TableView<CtagsDataModel> ctagsTable;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    private void handleExit(ActionEvent event) {
	myController.hideStage(CTAGS_STAGE);
    }
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
    }    

    /**
     * This method sets the values in the TableView "table".
     * 
     * @param entry     associtated data
     */
    public void setTable(HashMap<String, CtagsEntry> ctags, 
            HashMap<String, CtagsFileEntry> ctagsFile, 
            String filename, String tagname, List<String> tagfiles, 
            List<String> tagaddresses) {
        this.ctags = ctags;
        this.ctagsFile = ctagsFile;
        // clear table
        ctagsTable.getItems().clear();        // clear content
        this.filename = filename;
        String[] splitFilename = filename.split("/");
        if (splitFilename.length > 1) {
            this.version = splitFilename[1];
        } else {
            this.version = "";
        }
        
        if (!initialized) {
            initialized = true;
            initTable();                    // init table
            
            String title;
            title = java.util.ResourceBundle
                    .getBundle("lxrdifferencetable/Bundle", locale)
                    .getString("programName") +
                    " - " + 
                    java.util.ResourceBundle
                            .getBundle("lxrdifferencetable/Bundle", locale)
                            .getString("title.Ctags");
            myController.getStage(CTAGS_STAGE).setTitle(title);        
        }

        referenceCtag.setText(tagname);
        baseFile.setText(shortenFile(filename));
        
        String[] split = tagfiles.get(0).split("/");
        if (split.length >= 2) {
            baseDirectory.setText(split[1]);
        }
        
        int position = 0;
        for (String file : tagfiles) {
            if (file.equals(filename)) {
                String addresses = tagaddresses.get(position);
                addNewLineInTable(shortenFile(file), addresses);
                break;
            } else {
                position++;
            }
        }
        int i = 0;
        for (String file : tagfiles) {
            if (i == position) {
                // Skip
            } else {
                String addresses = tagaddresses.get(i);
                addNewLineInTable(shortenFile(file), addresses);
            }
            i++;
        }
    }
    
    private void initTable() {
        columnWithFile = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemFile"));
        columnWithFile.setCellValueFactory(new PropertyValueFactory<>("file"));
        columnWithLine = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemLine"));
        columnWithLine.setCellValueFactory(new PropertyValueFactory<>("line"));

        ctagsTable.getColumns().addAll(
                columnWithFile, 
                columnWithLine);
	// enable multi-selection
	ctagsTable.getSelectionModel().setCellSelectionEnabled(true);
	ctagsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        // resize listener
        myController.getStage(CTAGS_STAGE).getScene()
                .widthProperty()
                .addListener((ObservableValue<? extends Number> observableValue, 
                        Number oldSceneWidth, Number newSceneWidth) -> {
                    double width = myController.getStage(CTAGS_STAGE).getScene()
                            .getWidth();
                    if (width < 400.0) {
                        columnWithFile.setPrefWidth(250.0);
                        columnWithLine.setPrefWidth(100.0);
                    } else if (width < 500.0) {
                        columnWithFile.setPrefWidth(350.0);
                        columnWithLine.setPrefWidth(100.0);
                    } else if (width < 700.0) {
                        columnWithFile.setPrefWidth(400.0);
                        columnWithLine.setPrefWidth(200.0);
                    } else if (width < 900.0) {
                        columnWithFile.setPrefWidth(500.0);
                        columnWithLine.setPrefWidth(200.0);
                    } else if (width < 1100.0) {
                        columnWithFile.setPrefWidth(600.0);
                        columnWithLine.setPrefWidth(200.0);
                    } else if (width < 1500.0) {
                        columnWithFile.setPrefWidth(700.0);
                        columnWithLine.setPrefWidth(300.0);
                    } else if (width < 1700.0) {
                        columnWithFile.setPrefWidth(800.0);
                        columnWithLine.setPrefWidth(400.0);
                    } else if (width < 1900.0) {
                        columnWithFile.setPrefWidth(900.0);
                        columnWithLine.setPrefWidth(500.0);
                    } else if (width < 2100.0) {
                        columnWithFile.setPrefWidth(1000.0);
                        columnWithLine.setPrefWidth(600.0);
                    } else if (width < 2300.0) {
                        columnWithFile.setPrefWidth(1100.0);
                        columnWithLine.setPrefWidth(700.0);
                    } else {
                        columnWithFile.setPrefWidth(1200.0);
                        columnWithLine.setPrefWidth(800.0);
                    }
        });
    }
    
    private void addNewLineInTable(String file, String line)
    {
        String description;
        description = line;
        description = description.replaceAll("<br />", "\n");
        CtagsDataModel ctagsDataModel = new CtagsDataModel();
        ctagsDataModel.setFile(file);

        Text textLine = new Text(line);
        ctagsDataModel.setLine(textLine);
        textLine.setFill(Color.BLUE);
        textLine.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                new Thread(() -> {
                    FXMLLoader loader;
                    loader = myController.getLoader(BROWSE_STAGE);
                    dialogBrowseController = loader.
                            <DialogBrowseController>getController();
                   
                    final String filename = expandFile(ctagsDataModel.getFile());
                    final String version1 = version;
                    final Text pos = ctagsDataModel.getLine();
                    final String pos1 = pos.getText();
                    Platform.runLater(() -> {
                        dialogBrowseController.setFile(ctags, ctagsFile, 
                                filename, version1, pos1);
                        myController.showStage(BROWSE_STAGE);
                    });                    
                    
                }).start();
                // select line
                // ctagsTable.getSelectionModel().select(tableNr);
                textLine.setUnderline(true);
            }
        });

        ctagsTable.getItems().add(ctagsDataModel);
        ctagsTable.setEditable(true);
        
        // enable copy/paste
        TableUtils tableUtils = new TableUtils();
        tableUtils.installCopyPasteHandler(ctagsTable);
        
        // set context menu for copy/paste
        ContextMenu contextMenu = new ContextMenu();
        MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem.setOnAction((ActionEvent event) -> {
            tableUtils.copySelectionToClipboard(ctagsTable);
        });
        markAllMenuItem.setOnAction((ActionEvent event) -> {
            ctagsTable.getSelectionModel().selectAll();
        });
        contextMenu.getItems().addAll(copyMenuItem, markAllMenuItem);
        ctagsTable.setContextMenu(contextMenu);
    }
    
    private String shortenFile(String file) {
        String newfile = "";
        String[] split = file.split("/");
        if (split.length >= 2) {
            prepath = split[0] + "/" + split[1];
            for (int i = 2; i < split.length; i++) {
                newfile = newfile + "/" + split[i];
            }
        } else {
            newfile = file;
        }
        return newfile;
    }
    
    private String expandFile(String file) {
        return prepath + file;
    }
}
