/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.READ_ONLY_MODE_STAGE;
import lxrdifferencetable.StagesController;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogReadOnlyModeController implements Initializable, ControlledStage {

    private StagesController myController;

    @FXML
    Button buttonOK;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }

    @FXML
    private void handleOK(ActionEvent event) throws IOException {
        myController.hideStage(READ_ONLY_MODE_STAGE);
    }
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
