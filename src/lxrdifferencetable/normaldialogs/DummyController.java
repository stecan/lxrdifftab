/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.DUMMY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.comp.Generate;
import lxrdifferencetable.data.ConfigEntry;
import lxrdifferencetable.data.LogEntry;
import lxrdifferencetable.tools.CtagsEntry;
import lxrdifferencetable.tools.CtagsFileEntry;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DummyController implements Initializable, ControlledStage {

    private StagesController myController;
    private Generate gen;
    private ConfigEntry configEntry;
    private HashMap<String, CtagsEntry> ctags1;
    private HashMap<String, CtagsEntry> ctags2;
    private HashMap<String, CtagsFileEntry> ctagsFile1;
    private HashMap<String, CtagsFileEntry> ctagsFile2;
    private String activeStage;
    private List<LogEntry> repoLog;
    private File distribDir;

    public DummyController () {
        gen = null;
        activeStage = MAIN_STAGE_1;
        distribDir = null;
    }
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    private void handleOK(ActionEvent event) {
        myController.hideStage(DUMMY_STAGE);
    }
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public Generate getGen() {
	return gen;
    }

    public void setGen(Generate gen) {
	this.gen = gen;
    }
    
    public ConfigEntry getConfigEntry() {
	return configEntry;
    }

    public void setConfigEntry(ConfigEntry configEntry) {
	this.configEntry = configEntry;
    }

    public HashMap<String, CtagsEntry> getCtags1() {
	return ctags1;
    }

    public void setCtags1(HashMap<String, CtagsEntry> ctags1) {
	this.ctags1 = ctags1;
    }

    public HashMap<String, CtagsEntry> getCtags2() {
	return ctags2;
    }

    public void setCtags2(HashMap<String, CtagsEntry> ctags2) {
	this.ctags2 = ctags2;
    }

    public HashMap<String, CtagsFileEntry> getCtagsFile1() {
	return ctagsFile1;
    }

    public void setCtagsFile1(HashMap<String, CtagsFileEntry> ctagsFile1) {
	this.ctagsFile1 = ctagsFile1;
    }

    public HashMap<String, CtagsFileEntry> getCtagsFile2() {
	return ctagsFile2;
    }

    public void setCtagsFile2(HashMap<String, CtagsFileEntry> ctagsFile2) {
	this.ctagsFile2 = ctagsFile2;
    }

    public List<LogEntry> getRepoLog() {
        return repoLog;
    }
    
    public void setRepoLog(List<LogEntry> repoLog) {
        this.repoLog = repoLog;
    }
    
    public String getActiveStage () {
        return activeStage;
    }
    
    public void setActiveStage (String activeStage) {
        this.activeStage = activeStage;
    }
    
    public void setDistribDir (File distribDir) {
        this.distribDir = distribDir;
    }
    
    public File getDistribDir () {
        return distribDir;
    }
}
