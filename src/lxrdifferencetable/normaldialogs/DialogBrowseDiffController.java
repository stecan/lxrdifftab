/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.BROWSE_DIFF_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.BROWSE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.CTAGS_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.DUMMY_STAGE;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.tools.BashHighlighting;
import lxrdifferencetable.tools.CHighlighting;
import lxrdifferencetable.tools.CsharpHighlighting;
import lxrdifferencetable.tools.CtagsEntry;
import lxrdifferencetable.tools.CtagsFileEntry;
import lxrdifferencetable.tools.HighlightEntry;
import lxrdifferencetable.tools.JavaHighlighting;
import lxrdifferencetable.tools.NoHighlighting;
import lxrdifferencetable.tools.ProgrammingLanguage;
import lxrdifferencetable.tools.WorkingDirectory;
import org.apache.commons.io.FileUtils;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Diff;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.model.StyleSpans;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogBrowseDiffController implements Initializable, ControlledStage {

    static final Logger logger = Logger.getLogger
        (LxrDifferenceTableController.class.getName());
    
    private DummyController dummyController;
    private DialogTagToFileController dialogTagToFileController;
    private DialogBrowseController dialogBrowseController;
    
    private Scene scene;
    private StagesController myController;

    private final List <String> fileContent1;
    private final List <String> fileContent2;
    
    private final Collection<String> INSERTED;
    private final Collection<String> DELETED;
    private final Collection<String> MODIFIED;
    
    private final int LINEOFFSET;
    
    private final List<Integer> inserted1;
    private final List<Integer> deleted1;
    private final List<Integer> modified1;
    private final List<Integer> inserted2;
    private final List<Integer> deleted2;
    private final List<Integer> modified2;

    private final List<HighlightEntry> partiallyDeleted1;
    private final List<HighlightEntry> partiallyInserted2;
    
    private HashMap<String, CtagsEntry> ctags1;
    private HashMap<String, CtagsEntry> ctags2;
    private HashMap<String, CtagsFileEntry> ctagsFile1;
    private HashMap<String, CtagsFileEntry> ctagsFile2;
    File file1;
    File file2;
    String diffFilename1;
    String diffFilename2;
    String lastFilename;
    String version1;
    String version2;
    String position;                                // string position for lxr server
    int pos;                                        // iteger position for lxr server
    int markerPos1;
    int markerPos2;
    int markerPos;
    
    private ProgrammingLanguage.Language progLang;
    
    File workingDirectory;
    
    private int lastIndex1 = 0;
    private int lastIndex2 = 0;
    
    public DialogBrowseDiffController() {
        this.INSERTED = new ArrayList<String>() {{ add("inserted"); }};
        this.DELETED = new ArrayList<String>() {{ add("deleted"); }};
        this.MODIFIED = new ArrayList<String>() {{ add("modified"); }};
        this.inserted1 = new ArrayList<>();
        this.deleted1 = new ArrayList<>();
        this.modified1 = new ArrayList<>();
        this.inserted2 = new ArrayList<>();
        this.deleted2 = new ArrayList<>();
        this.modified2 = new ArrayList<>();
        this.fileContent1 = new ArrayList<>();
        this.fileContent2 = new ArrayList<>();
        this.partiallyDeleted1 = new ArrayList<>();
        this.partiallyInserted2 = new ArrayList<>();
        this.LINEOFFSET = 6;        // the first six characters are the line number
        this.progLang = ProgrammingLanguage.Language.JAVA;
        this.version1 = "";
        this.version2 = "";
        this.position = "";
        this.lastFilename = "";
    }
    
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
        scene= stageParent.getStage(BROWSE_DIFF_STAGE).getScene();
        scene.getStylesheets().add(DialogBrowseDiffController.class.
                getResource("java-keywords.css").toExternalForm());
        WorkingDirectory dir = new WorkingDirectory(myController);
        workingDirectory = dir.getWorkingDir();    
    }
  
    // Task for syntax highlighting and coloring of differences for CodeArea 
    // content1
    Task TaskHighlightContent1() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                StyleSpans<Collection<String>> computeHighlighting;
                List<String> tagnames;
                switch (progLang) {
                    case BASH:
                        if (ctagsFile1 == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile1.get(diffFilename1) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile1.get(diffFilename1).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                BashHighlighting.computeHighlighting(content1.getText(),
                                        tagnames);
                        break;
                    case C:
                        if (ctagsFile1 == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile1.get(diffFilename1) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile1.get(diffFilename1).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                CHighlighting.computeHighlighting(content1.getText(),
                                        tagnames);
                        break;
                    case JAVA:
                        if (ctagsFile1 == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile1.get(diffFilename1) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile1.get(diffFilename1).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                JavaHighlighting.computeHighlighting(content1.getText(), 
                                        tagnames);
                        break;

                    case CSHARP:
                        if (ctagsFile1 == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile1.get(diffFilename1) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile1.get(diffFilename1).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                CsharpHighlighting.computeHighlighting(content1.getText(),
                                        tagnames);
                        break;
                    default:
                        // only for initialization purposes
                        computeHighlighting = 
                               NoHighlighting.computeHighlighting(content1.getText());
                        break;

                }
                Platform.runLater(() -> {
                    switch (progLang) {
                        case C:
                        case JAVA:
                        case CSHARP:
                            content1.setStyleSpans(0, computeHighlighting);
                            break;
                        default:
                            break;
                    }
                    
                    inserted1.forEach(pos -> {
                        content1.setStyle(pos, INSERTED);
                    });
                    deleted1.forEach(pos -> {
                        content1.setStyle(pos, DELETED);
                    });
                    modified1.forEach(pos -> {
                        content1.setStyle(pos, MODIFIED);
                    });
                    partiallyDeleted1.forEach(pos -> {
                        content1.setStyle(pos.getLine(),pos.getBegin(),pos.getEnd(),DELETED);
                    });
                }); 
                return true;
            }
        };
    }

    // Task for syntax highlighting and coloring of differences for CodeArea 
    // content2
    Task TaskHighlightContent2() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                StyleSpans<Collection<String>> computeHighlighting;
                List<String> tagnames;
                switch (progLang) {
                    case BASH:
                        if (ctagsFile2 == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile2.get(diffFilename2) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile2.get(diffFilename2).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                BashHighlighting.computeHighlighting(content2.getText(),
                                        tagnames);
                        break;
                    case C:
                        if (ctagsFile2 == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile2.get(diffFilename2) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile2.get(diffFilename2).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                CHighlighting.computeHighlighting(content2.getText(),
                                        tagnames);
                        break;
                    case JAVA:
                        if (ctagsFile2 == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile2.get(diffFilename2) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile2.get(diffFilename2).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                JavaHighlighting.computeHighlighting(content2.getText(), 
                                        tagnames);
                        break;
                    case CSHARP:
                        if (ctagsFile2 == null) {
                            tagnames = null;
                        } else {
                            if (ctagsFile2.get(diffFilename2) == null) {
                                tagnames = null;
                            } else {
                                tagnames = ctagsFile2.get(diffFilename2).getTagnames();
                            }
                        }
                        computeHighlighting = 
                                CsharpHighlighting.computeHighlighting(content2.getText(),
                                        tagnames);
                        break;
                    default:
                        // only for initialization purposes
                        computeHighlighting = 
                               NoHighlighting.computeHighlighting(content2.getText());
                        break;
                }
                Platform.runLater(() -> {
                    switch (progLang) {
                        case C:
                        case JAVA:
                        case CSHARP:
                            content2.setStyleSpans(0, computeHighlighting);
                            break;
                        default:
                            break;
                    }
                    
                    inserted2.forEach(pos -> {
                        content2.setStyle(pos, INSERTED);
                    });
                    deleted2.forEach(pos -> {
                        content2.setStyle(pos, DELETED);
                    });
                    modified2.forEach(pos -> {
                        content2.setStyle(pos, MODIFIED);
                    });
                    partiallyInserted2.forEach(pos -> {
                        content2.setStyle(pos.getLine(),pos.getBegin(),pos.getEnd(),INSERTED);
                    });
                    
                }); 
                return true;
            }
        };
    }


    @FXML
    CodeArea content1;
    
    @FXML
    VirtualizedScrollPane content1ScrollPane;

    @FXML
    CodeArea content2;
    
    @FXML
    VirtualizedScrollPane content2ScrollPane;

    @FXML
    Label labelVersion1;
    
    @FXML
    Label labelVersion2;

    @FXML
    Label labelPosition1;
    
    @FXML
    Label labelPosition2;

    @FXML
    Label labelFile;
    
    @FXML
    Button searchButton1;

    @FXML
    Button forwaredSearchButton1;

    @FXML
    Button backwardSearchButton1;

    @FXML
    TextField searchField1;
    
    @FXML
    Button searchButton2;

    @FXML
    Button forwaredSearchButton2;

    @FXML
    Button backwardSearchButton2;

    @FXML
    TextField searchField2;
    
    @FXML
    public void searchHandler1(ActionEvent event) {
        forwardSearch1();
    }
    
    @FXML
    public void forwardSearchHandler1(ActionEvent event) {
        forwardSearch1();
    }
    
    @FXML
    public void backwardSearchHandler1(ActionEvent event) {
        backwardSearch1();
    }

    @FXML
    public void searchHandler2(ActionEvent event) {
        forwardSearch2();
    }
    
    @FXML
    public void forwardSearchHandler2(ActionEvent event) {
        forwardSearch2();
    }
    
    @FXML
    public void backwardSearchHandler2(ActionEvent event) {
        backwardSearch2();
    }

    @FXML
    public void searchItem1(MouseEvent event) {
        String toString = event.getTarget().toString();
        String[] split = toString.split("\"");
        String tagname;
        if (split.length > 1) {
            tagname = split[1];
            if (ctagsFile1 == null) {
                // do nothing
            } else {
                if (ctagsFile1.get(diffFilename1) == null) {
                    // do nothing
                } else {
                    if (ctagsFile1.get(diffFilename1).getTagnames().contains(tagname)) {
                        List<String> tagfiles = ctags1.get(tagname).getTagfiles();
                        List<String> tagaddresses = ctags1.get(tagname).getTagaddresses();

                        FXMLLoader loader;
                        loader = myController.getLoader(CTAGS_STAGE);
                        dialogTagToFileController = loader.
                                <DialogTagToFileController>getController();

                        Platform.runLater(() -> {
                            dialogTagToFileController.setTable(ctags1, 
                                    ctagsFile1, diffFilename1, tagname, tagfiles,tagaddresses);
                            myController.showStage(CTAGS_STAGE);
                        });                    

                    } else {
                        // do nothing
                    }
                }
            }
        }
    }

    @FXML
    public void searchItem2(MouseEvent event) {
        String toString = event.getTarget().toString();
        String[] split = toString.split("\"");
        String tagname;
        if (split.length > 1) {
            tagname = split[1];
            if (ctagsFile2 == null) {
                // do nothing
            } else {
                if (ctagsFile2.get(diffFilename2) == null) {
                    // do nothing
                } else {
                    if (ctagsFile2.get(diffFilename2).getTagnames().contains(tagname)) {
                        List<String> tagfiles = ctags2.get(tagname).getTagfiles();
                        List<String> tagaddresses = ctags2.get(tagname).getTagaddresses();

                        FXMLLoader loader;
                        loader = myController.getLoader(CTAGS_STAGE);
                        dialogTagToFileController = loader.
                                <DialogTagToFileController>getController();

                        Platform.runLater(() -> {
                            dialogTagToFileController.setTable(ctags2, 
                                    ctagsFile2, diffFilename2, tagname, tagfiles,tagaddresses);
                            myController.showStage(CTAGS_STAGE);
                        });                    

                    } else {
                        // do nothing
                    }
                }
            }
        }
    }
    
    @FXML
    public void gotoOriginal1(ActionEvent event) {
        FXMLLoader loader;
        loader = myController.getLoader(BROWSE_STAGE);
        dialogBrowseController = loader.
                <DialogBrowseController>getController();

        Platform.runLater(() -> {
            try {
                String pos = Integer.toString(markerPos1);
                dialogBrowseController.setFile(ctags1, ctagsFile1, 
                        diffFilename1, version1, pos);
            } catch (Exception ex) {
                dialogBrowseController.setFile(ctags1, ctagsFile1, 
                        diffFilename1, version1, position);
            }
            myController.showStage(BROWSE_STAGE);
        });                    
    }
    
    @FXML
    public void gotoOriginal2(ActionEvent event) {
        FXMLLoader loader;
        loader = myController.getLoader(BROWSE_STAGE);
        dialogBrowseController = loader.
                <DialogBrowseController>getController();

        Platform.runLater(() -> {
            try {
                String pos = Integer.toString(markerPos2);
                dialogBrowseController.setFile(ctags2, ctagsFile2, 
                        diffFilename2, version2, pos);
            } catch (Exception ex) {
                dialogBrowseController.setFile(ctags2, ctagsFile2, 
                        diffFilename2, version2, position);
            }
            myController.showStage(BROWSE_STAGE);
        });                    
    }
    
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    

   
    public void setDiffFile(String filename, String version1, String version2, String position) {
        this.version1 = version1;
        this.version2 = version2;
        this.position = position;
        this.pos = Integer.parseInt(position)-1;

        diffFilename1 = "ReviewedSources/" + version1 + filename;
        diffFilename2 = "ReviewedSources/" + version2 + filename;
        String pathName1 = workingDirectory + File.separator + diffFilename1;
        String pathName2 = workingDirectory + File.separator + diffFilename2;
        file1 = new File(pathName1);
        file2 = new File(pathName2);
        progLang = ProgrammingLanguage.registerProgrammingLanguage(file1);
        List<String> lines1 = new ArrayList<>();
        List<String> lines2 = new ArrayList<>();

        try {
            lines1 = FileUtils.readLines(file1, "UTF-8");
        } catch (IOException ex) {
            // if a file is deleted, then an exception is thrown. In this case,
            // it is a legal state.
        }
        try {
            lines2 = FileUtils.readLines(file2, "UTF-8");
        } catch (IOException ex) {
            // if a file is deleted, then an exception is thrown. In this case,
            // it is a legal state.
        }

        labelVersion1.setText("Base Version = " + version1);
        labelVersion2.setText("Diff Version = " + version2);
        labelFile.setText("File = " + filename);

        Patch patch = DiffUtils.diff(lines1, lines2);
        List<Delta> deltas = patch.getDeltas();

        List<Integer> linePositionOriginal = new ArrayList<>();
        List<Integer> sizeOfChangeOriginal = new ArrayList<>();
        List<Integer> linePositionRevised = new ArrayList<>();
        List<Integer> sizeOfChangeRevised = new ArrayList<>();
        for (Delta delta : deltas) {
            linePositionOriginal.add(delta.getOriginal().getPosition());
            linePositionRevised.add(delta.getRevised().getPosition());
            sizeOfChangeOriginal.add(delta.getOriginal().getLines().size());
            sizeOfChangeRevised.add(delta.getRevised().getLines().size());
        }
        
        // clear old contents
        fileContent1.clear();
        fileContent2.clear();
        inserted1.clear();
        inserted2.clear();
        deleted1.clear();
        deleted2.clear();
        modified1.clear();
        modified2.clear();
        partiallyDeleted1.clear();
        partiallyInserted2.clear();
        
        int linesMax = Integer.max(lines1.size(), lines2.size());
            
        int posToList = 0;
        int lineCount1 = 0;
        int lineCount2 = 0;
        int i = 0;
        while ((lineCount1 < lines1.size()) || (lineCount2 < lines2.size())) {
            if (pos == lineCount1) {
                markerPos1 = lineCount1;
                markerPos2 = lineCount2;
                markerPos = i;
            }
            if ((posToList < linePositionOriginal.size()) && 
                    (linePositionOriginal.get(posToList).equals(lineCount1) || 
                    linePositionRevised.get(posToList).equals(lineCount2))) {
                
                int max = Integer.max(sizeOfChangeRevised.get(posToList), 
                        sizeOfChangeOriginal.get(posToList));
                for (int j = 0; j < max; j++) {
                    String line1;
                    String line2;
                    if (j < deltas.get(posToList).getOriginal().size() && 
                            j < deltas.get(posToList).getRevised().size()) {
                        String lineno1 = String.format("%04d  ", lineCount1+1);
                        modified1.add(i);
                        String lineno2 = String.format("%04d  ", lineCount2+1);
                        modified2.add(i);
                        
                        fileContent1.add(lineno1);
                        fileContent2.add(lineno2);
                        
                        line1 = deltas.get(posToList).getOriginal().getLines().
                                get(j).toString();
                        line2 = deltas.get(posToList).getRevised().getLines().
                                get(j).toString();

                        DiffMatchPatch dmp = new DiffMatchPatch();
                        LinkedList<Diff> diff = dmp.diffMain(line1, line2, false);

                        // colorize modified line for content1
                        int begin = LINEOFFSET;
                        int end = LINEOFFSET;
                        for (Diff d : diff) {
                            switch (d.operation) {
                                case EQUAL:
                                    end = end + d.text.length();
                                    fileContent1.add(d.text);
                                    break;
                                case DELETE:
                                    String substr1 = d.text;
                                    end = end + substr1.length();
                                    HighlightEntry entry1 = new HighlightEntry(i,begin,end);
                                    partiallyDeleted1.add(entry1);
                                    fileContent1.add(substr1);
                                    break;
                                case INSERT:
                                    break;
                                default:
                                    break;
                            }
                            begin = end;
                            end = begin;
                        }
                        // colorize modified line for content2
                        begin = LINEOFFSET;
                        end = LINEOFFSET;
                        for (Diff d : diff) {
                            switch (d.operation) {
                                case EQUAL:
                                    end = end + d.text.length();
                                    fileContent2.add(d.text);
                                    break;
                                case DELETE:
                                    break;
                                case INSERT:
                                    String substr2 = d.text;
                                    end = end + substr2.length();
                                    HighlightEntry entry2 = new HighlightEntry(i,begin,end);
                                    partiallyInserted2.add(entry2);
                                    fileContent2.add(substr2);
                                    break;
                                default:
                                    break;
                            }
                            begin = end;
                            end = begin;
                        }
                        fileContent1.add("\n");
                        fileContent2.add("\n");
                        lineCount1++;
                        lineCount2++;

                    } else if (j < deltas.get(posToList).getOriginal().size()) {
                        line1 = deltas.get(posToList).getOriginal().getLines().get(j).toString();
                        String line = String.format("%04d  ", lineCount1+1) + line1 + "\n";
                        deleted1.add(i);
                        fileContent1.add(line);
                        lineCount1++;

                        String linenumber = String.format("%04d  ", lineCount2+1) + "\n";
                        inserted2.add(i);
                        fileContent2.add(linenumber);
                        
                    } else if (j < deltas.get(posToList).getRevised().size()) {
                        line2 = deltas.get(posToList).getRevised().getLines().get(j).toString();
                        String line = String.format("%04d  ", lineCount2+1) + line2 + "\n";
                        inserted2.add(i);
                        fileContent2.add(line);
                        lineCount2++;
                        
                        String linenumber = String.format("%04d  ", lineCount1+1) + "\n";
                        deleted1.add(i);
                        fileContent1.add(linenumber);
                    } else {
                        // do nothing
                    }
                    if (j == max-1) {
                        // do not increment i!
                    } else {
                        i++;
                    }
                }
                posToList++;
            } else {
                if (lineCount1 < lines1.size()) {
                    String line1 = lines1.get(lineCount1);
                    if (line1 != null) {
                        fileContent1.add(String.format("%04d  ", lineCount1+1) + line1 + "\n");
                        lineCount1++;
                    }
                }
                if (lineCount2 < lines2.size()) {
                    String line2 = lines2.get(lineCount2);
                    if (line2 != null) {
                        fileContent2.add(String.format("%04d  ", lineCount2+1) + line2 + "\n");
                        lineCount2++;
                    }
                }
            }
            i++;
        }
        
        if (!filename.equals(lastFilename)) {

            lastFilename = filename;
            
            content1.clear();
            content2.clear();

            // bind scrollpanes to contents
            StringBuilder sb1 = new StringBuilder();
            fileContent1.forEach(obj -> {
                sb1.append(obj);
            });
            content1.appendText(sb1.toString());

            StringBuilder sb2 = new StringBuilder();
            fileContent2.forEach(obj -> {
                sb2.append(obj);
            });
            content2.appendText(sb2.toString());

            // load references to all needed controllers
            FXMLLoader loader;
            loader = myController.getLoader(DUMMY_STAGE);
            dummyController = loader.<DummyController>getController();
            ctags1 = dummyController.getCtags1();
            ctags2 = dummyController.getCtags2();
            ctagsFile1 = dummyController.getCtagsFile1();
            ctagsFile2 = dummyController.getCtagsFile2();

            Task task1 = TaskHighlightContent1();
            Task task2 = TaskHighlightContent2();

            Thread thread1 = new Thread(task1);
            Thread thread2 = new Thread(task2);
            thread1.start();
            thread2.start();

            while (thread1.getState().equals(Thread.State.RUNNABLE)) {
                try {
                    Thread.sleep(1000L,10);
                } catch (InterruptedException ex) {
                    logger.warning("Thread1:" + ex.getMessage() );
                }
            }
            while (thread2.getState().equals(Thread.State.RUNNABLE)) {
                try {
                    Thread.sleep(1000L,10);
                } catch (InterruptedException ex) {
                    logger.warning("Thread1:" + ex.getMessage() );
                }
            }
        }

        content1.moveTo(markerPos,0);
        content1.requestFollowCaret();  // see: https://github.com/FXMisc/RichTextFX/issues/414
        content2.moveTo(markerPos,0);
        content2.requestFollowCaret();  // see: https://github.com/FXMisc/RichTextFX/issues/414

        labelPosition1.setText("Position Base File = " + (markerPos1 + 1));
        labelPosition2.setText("Position Diff File = " + (markerPos2 + 1));
        
        // Bind scrollbars of two code areas to each other:
        // Not working!
//        content1.prefHeightProperty().bindBidirectional(content2.prefHeightProperty());
//        content1ScrollPane.vbarPolicyProperty().bindBidirectional(content2ScrollPane.vbarPolicyProperty());
        content1.setEditable(false);
        content2.setEditable(false);
        
    }

    private void forwardSearch1() {
        String searchedText = searchField1.getText();
        if (searchedText != null && !searchedText.isEmpty()) {
            int index = content1.getText().indexOf(searchedText,lastIndex1); 
            if (index == -1) {
                lastIndex1 = 0;
            } else {
                lastIndex1 = index+1;
                content1.moveTo(index);
                content1.requestFollowCaret();  // see: https://github.com/FXMisc/RichTextFX/issues/414
                content1.selectRange(index, index + searchField1.getLength());
            }       
        } else {
            // do nothing
        }
    }

    private void backwardSearch1() {
        String searchedText = searchField1.getText();
        if (searchedText != null && !searchedText.isEmpty()) {
            int index = content1.getText().lastIndexOf(searchedText,lastIndex1); 
            if (index == -1) {
                lastIndex1 = content1.getText().length();
            } else {
                if (index == 0) {
                    lastIndex1 = content1.getText().length();
                } else {
                    lastIndex1 = index-1;
                }
                content1.moveTo(index);
                content1.requestFollowCaret();  // see: https://github.com/FXMisc/RichTextFX/issues/414
                content1.selectRange(index, index + searchField1.getLength());
            }       
        } else {
            // do noting
        }
    }
    
    private void forwardSearch2() {
        String searchedText = searchField2.getText();
        if (searchedText != null && !searchedText.isEmpty()) {
            int index = content2.getText().indexOf(searchedText,lastIndex2); 
            if (index == -1) {
                lastIndex2 = 0;
            } else {
                lastIndex2 = index+1;
                content2.moveTo(index);
                content2.requestFollowCaret();  // see: https://github.com/FXMisc/RichTextFX/issues/414
                content2.selectRange(index, index + searchField2.getLength());
            }       
        } else {
            // do nothing
        }
    }

    private void backwardSearch2() {
        String searchedText = searchField2.getText();
        if (searchedText != null && !searchedText.isEmpty()) {
            int index = content2.getText().lastIndexOf(searchedText,lastIndex2); 
            if (index == -1) {
                lastIndex2 = content2.getText().length();
            } else {
                if (index == 0) {
                    lastIndex2 = content2.getText().length();
                } else {
                    lastIndex2 = index-1;
                }
                content2.moveTo(index);
                content2.requestFollowCaret();  // see: https://github.com/FXMisc/RichTextFX/issues/414
                content2.selectRange(index, index + searchField2.getLength());
            }       
        } else {
            // do noting
        }
    }
    
}
