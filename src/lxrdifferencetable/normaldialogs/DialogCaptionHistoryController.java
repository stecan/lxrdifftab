/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.util.Callback;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.CAPTION_HISTORY_STAGE;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.data.ConfigEntry;
import lxrdifferencetable.data.CaptionDataModel;
import lxrdifferencetable.tablecells.CaptionEditingCell;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.TableUtils;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogCaptionHistoryController implements Initializable, ControlledStage {

    private StagesController myController;
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;                        
    private boolean initialized;
    
    private TableColumn<CaptionDataModel, String> columnWithCaption;
    private TableColumn<CaptionDataModel, String> columnWithAuthor;
    private TableColumn<CaptionDataModel, String> columnWithDate;
    
    
    public DialogCaptionHistoryController() {
        this.props = new Properties();
        initialized = false;
    }
    
    @FXML
    private TableView captionTable;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    private void handleExit(ActionEvent event) {
	myController.hideStage(CAPTION_HISTORY_STAGE);
    }
    
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
    }    

    /**
     * This method sets the values in the TableView "table".
     * 
     * @param entry     associtated data
     */
    public void setTable(ConfigEntry entry) {
        // clear table
        captionTable.getItems().clear();      // clear content

        if (!initialized) {
            initialized = true;
            initTable();                    // init table
            
            String title;
            title = java.util.ResourceBundle
                    .getBundle("lxrdifferencetable/Bundle", locale)
                    .getString("programName") +
                    " - " + 
                    java.util.ResourceBundle
                            .getBundle("lxrdifferencetable/Bundle", locale)
                            .getString("title.CaptionHistory");
            myController.getStage(CAPTION_HISTORY_STAGE).setTitle(title);        
        }
        
        List<String> listCaption = entry.getCaption();
        for (int i = listCaption.size()-1; i >= 0; i--) {
            addNewLineInTable(entry.getCaption(i), 
                    entry.getCaptionAuthor(i),
                    entry.getCaptionDate(i));
        }
    }
    
    private void initTable() {
        //Create a customer cell factory so that cells can support editing.
        Callback<TableColumn<CaptionDataModel, String>, 
                TableCell<CaptionDataModel, String>> cellFactory;
        cellFactory = new Callback<TableColumn<CaptionDataModel, String>, 
                TableCell<CaptionDataModel, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                CaptionEditingCell cell = new CaptionEditingCell();
                cell.widthProperty()
                        .addListener((ObservableValue<? extends Number> observableValue, 
                                Number oldSceneWidth, Number newSceneWidth) -> {
                    Text text = new Text();
                    cell.setGraphic(text);
                    text.wrappingWidthProperty().bind(cell.widthProperty());
                    text.textProperty().bind(cell.itemProperty());
                });
                return cell;
            }
        };
        
        columnWithCaption = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemCaption"));
        columnWithCaption.setCellValueFactory(new PropertyValueFactory<>("caption"));
        columnWithCaption.setCellFactory(cellFactory);
        columnWithAuthor = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemAuthor"));
        columnWithAuthor.setCellValueFactory(new PropertyValueFactory<>("author"));
        columnWithAuthor.setCellFactory(cellFactory);
        columnWithDate = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemDate"));
        columnWithDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnWithDate.setCellFactory(cellFactory);

        captionTable.getColumns().addAll(
                columnWithCaption, 
                columnWithAuthor, columnWithDate);
	// enable multi-selection
	captionTable.getSelectionModel().setCellSelectionEnabled(true);
	captionTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        // resize listener
        myController.getStage(CAPTION_HISTORY_STAGE).getScene()
                .widthProperty()
                .addListener((ObservableValue<? extends Number> observableValue, 
                        Number oldSceneWidth, Number newSceneWidth) -> {
                    double width = myController.getStage(CAPTION_HISTORY_STAGE).getScene()
                            .getWidth();
                    if (width < 400.0) {
                        columnWithCaption.setPrefWidth(100.0);
                        columnWithAuthor.setPrefWidth(80.0);
                        columnWithDate.setPrefWidth(100.0);
                    } else if (width < 500.0) {
                        columnWithCaption.setPrefWidth(150.0);
                        columnWithAuthor.setPrefWidth(100.0);
                        columnWithDate.setPrefWidth(150.0);
                    } else if (width < 700.0) {
                        columnWithCaption.setPrefWidth(200.0);
                        columnWithAuthor.setPrefWidth(150.0);
                        columnWithDate.setPrefWidth(200.0);
                    } else if (width < 900.0) {
                        columnWithCaption.setPrefWidth(300.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1100.0) {
                        columnWithCaption.setPrefWidth(500.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1300.0) {
                        columnWithCaption.setPrefWidth(600.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1500.0) {
                        columnWithCaption.setPrefWidth(800.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1700.0) {
                        columnWithCaption.setPrefWidth(1000.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1900.0) {
                        columnWithCaption.setPrefWidth(1200.0);
                        columnWithAuthor.setPrefWidth(200.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 2100.0) {
                        columnWithCaption.setPrefWidth(1200.0);
                        columnWithAuthor.setPrefWidth(400.0);
                        columnWithDate.setPrefWidth(400.0);
                    } else if (width < 2300.0) {
                        columnWithCaption.setPrefWidth(1400.0);
                        columnWithAuthor.setPrefWidth(400.0);
                        columnWithDate.setPrefWidth(400.0);
                    } else {
                        columnWithCaption.setPrefWidth(1600.0);
                        columnWithAuthor.setPrefWidth(400.0);
                        columnWithDate.setPrefWidth(400.0);
                    }
        });
    }
    
    private void addNewLineInTable(String caption, String author, String date)
    {
        CaptionDataModel captionDataModel = new CaptionDataModel();
        captionDataModel.setCaption(caption);
        captionDataModel.setAuthor(author);
        captionDataModel.setDate(date);
        captionTable.getItems().add(captionDataModel);
        captionTable.setEditable(true);
        // enable copy/paste
        TableUtils tableUtils = new TableUtils();
        tableUtils.installCopyPasteHandler(captionTable);
        
        // set context menu for copy/paste
        ContextMenu contextMenu = new ContextMenu();
        MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem.setOnAction((ActionEvent event) -> {
            tableUtils.copySelectionToClipboard(captionTable);
        });
        markAllMenuItem.setOnAction((ActionEvent event) -> {
            captionTable.getSelectionModel().selectAll();
        });
        contextMenu.getItems().addAll(copyMenuItem, markAllMenuItem);
        captionTable.setContextMenu(contextMenu);
    }
}
