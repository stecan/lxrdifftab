/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.util.Callback;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.STATE_HISTORY_STAGE;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.data.StateDataModel;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.data.TableEntry.State;
import lxrdifferencetable.tablecells.StateEditingCell;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.TableUtils;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogStateHistoryController implements Initializable, ControlledStage {

    private StagesController myController;
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;                        
    private boolean initialized;
    
    private TableColumn<StateDataModel, ComboBox<State>> columnWithState;
    private TableColumn<StateDataModel, String> columnWithAuthor;
    private TableColumn<StateDataModel, String> columnWithDate;
    
    
    public DialogStateHistoryController() {
        this.props = new Properties();
        initialized = false;
    }
    
    @FXML
    private TableView<StateDataModel> stateTable;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    private void handleExit(ActionEvent event) {
	myController.hideStage(STATE_HISTORY_STAGE);
    }
    
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
    }    

    /**
     * This method sets the values in the TableView "table".
     * 
     * @param entry     associtated data
     * @param pos       associated position in entry
     */
    public void setTable(TableEntry entry, int pos, boolean reviewState) {
        // clear table
        stateTable.getItems().clear();      // clear content
        if (!initialized) {
            initialized = true;
            initTable();                    // init table
        }

        if (reviewState) {
            String title;
            title = java.util.ResourceBundle
                    .getBundle("lxrdifferencetable/Bundle", locale)
                    .getString("programName") +
                    " - " + 
                    java.util.ResourceBundle
                            .getBundle("lxrdifferencetable/Bundle", locale)
                            .getString("title.ReviewStateHistory");
            myController.getStage(STATE_HISTORY_STAGE).setTitle(title);        
            
            List<State> listReviewResult = entry.getReviewState(pos);
            for (int i = listReviewResult.size()-1; i >= 0; i--) {
                addNewLineInTable(entry.getReviewState(pos).get(i), 
                        entry.getReviewResAuthor(pos).get(i),
                        entry.getReviewResDate(pos).get(i));
            }
        } else {
            String title;
            title = java.util.ResourceBundle
                    .getBundle("lxrdifferencetable/Bundle", locale)
                    .getString("programName") +
                    " - " + 
                    java.util.ResourceBundle
                            .getBundle("lxrdifferencetable/Bundle", locale)
                            .getString("title.TestStateHistory");
            myController.getStage(STATE_HISTORY_STAGE).setTitle(title);        
            
            List<State> listTestState = entry.getTestState(pos);
            for (int i = listTestState.size()-1; i >= 0; i--) {
                addNewLineInTable(entry.getTestState(pos).get(i), 
                        entry.getTestResAuthor(pos).get(i),
                        entry.getTestResDate(pos).get(i));
            }
        }
    }
    
    private void initTable() {
        //Create a customer cell factory so that cells can support editing.
        Callback<TableColumn<StateDataModel, String>, TableCell<StateDataModel, String>> cellFactory = 
                new Callback<TableColumn<StateDataModel, String>, 
                        TableCell<StateDataModel, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                StateEditingCell cell = new StateEditingCell();
                cell.widthProperty()
                        .addListener((ObservableValue<? extends Number> observableValue, 
                                Number oldSceneWidth, Number newSceneWidth) -> {
                    Text text = new Text();
                    cell.setGraphic(text);
                    text.wrappingWidthProperty().bind(cell.widthProperty());
                    text.textProperty().bind(cell.itemProperty());
                });
                return cell;
            }
        };
        
        columnWithState = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemState"));
        columnWithState.setCellValueFactory(new PropertyValueFactory<>("state"));
        columnWithAuthor = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemAuthor"));
        columnWithAuthor.setCellValueFactory(new PropertyValueFactory<>("author"));
        columnWithAuthor.setCellFactory(cellFactory);
        columnWithDate = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemDate"));
        columnWithDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        columnWithDate.setCellFactory(cellFactory);

        stateTable.getColumns().addAll(
                columnWithState, 
                columnWithAuthor, columnWithDate);
	// enable multi-selection
	stateTable.getSelectionModel().setCellSelectionEnabled(true);
	stateTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        // resize listener
        myController.getStage(STATE_HISTORY_STAGE).getScene()
                .widthProperty()
                .addListener((ObservableValue<? extends Number> observableValue, 
                        Number oldSceneWidth, Number newSceneWidth) -> {
                    double width = myController.getStage(STATE_HISTORY_STAGE).getScene()
                            .getWidth();
                    if (width < 400.0) {
                        columnWithState.setPrefWidth(100.0);
                        columnWithAuthor.setPrefWidth(80.0);
                        columnWithDate.setPrefWidth(100.0);
                    } else if (width < 500.0) {
                        columnWithState.setPrefWidth(150.0);
                        columnWithAuthor.setPrefWidth(150.0);
                        columnWithDate.setPrefWidth(150.0);
                    } else if (width < 700.0) {
                        columnWithState.setPrefWidth(200.0);
                        columnWithAuthor.setPrefWidth(150.0);
                        columnWithDate.setPrefWidth(200.0);
                    } else if (width < 900.0) {
                        columnWithState.setPrefWidth(200.0);
                        columnWithAuthor.setPrefWidth(250.0);
                        columnWithDate.setPrefWidth(250.0);
                    } else if (width < 1100.0) {
                        columnWithState.setPrefWidth(200.0);
                        columnWithAuthor.setPrefWidth(250.0);
                        columnWithDate.setPrefWidth(300.0);
                    } else if (width < 1300.0) {
                        columnWithState.setPrefWidth(300.0);
                        columnWithAuthor.setPrefWidth(300.0);
                        columnWithDate.setPrefWidth(300.0);
                    } else if (width < 1500.0) {
                        columnWithState.setPrefWidth(300.0);
                        columnWithAuthor.setPrefWidth(300.0);
                        columnWithDate.setPrefWidth(400.0);
                    } else if (width < 1700.0) {
                        columnWithState.setPrefWidth(300.0);
                        columnWithAuthor.setPrefWidth(400.0);
                        columnWithDate.setPrefWidth(500.0);
                    } else if (width < 1900.0) {
                        columnWithState.setPrefWidth(300.0);
                        columnWithAuthor.setPrefWidth(400.0);
                        columnWithDate.setPrefWidth(600.0);
                    } else if (width < 2100.0) {
                        columnWithState.setPrefWidth(300.0);
                        columnWithAuthor.setPrefWidth(400.0);
                        columnWithDate.setPrefWidth(400.0);
                    } else if (width < 2300.0) {
                        columnWithState.setPrefWidth(300.0);
                        columnWithAuthor.setPrefWidth(600.0);
                        columnWithDate.setPrefWidth(700.0);
                    } else {
                        columnWithState.setPrefWidth(300.0);
                        columnWithAuthor.setPrefWidth(700.0);
                        columnWithDate.setPrefWidth(800.0);
                    }
        });
    }
    
    private void addNewLineInTable(State state, String author, String date)
    {
        
        ComboBox<State>stateCol = new ComboBox<>();
        stateCol.setCellFactory(new Callback<ListView<State>, 
                ListCell<State>>() {
          @Override
          public ListCell<State> call(ListView<State> p) {
            return new DialogStateHistoryController.StateListCell();
          }
        });
        
        stateCol.setButtonCell(new DialogStateHistoryController.StateListCell());
        stateCol.getItems().addAll(state);
        stateCol.getSelectionModel().select(state);

        // Do not delete or comment the followin line! It's important to show
        // all combobox elements with the same size!
        stateCol.setPrefWidth(200);
        
        StateDataModel stateDataModel = new StateDataModel();
        stateDataModel.setState(stateCol);
        stateDataModel.setAuthor(author);
        stateDataModel.setDate(date);

        stateTable.getItems().add(stateDataModel);
        stateTable.setEditable(true);
        // enable copy/paste
        TableUtils tableUtils = new TableUtils();
        tableUtils.installCopyPasteHandler(stateTable);
        
        // set context menu for copy/paste
        ContextMenu contextMenu = new ContextMenu();
        MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem.setOnAction((ActionEvent event) -> {
            tableUtils.copySelectionToClipboard(stateTable);
        });
        markAllMenuItem.setOnAction((ActionEvent event) -> {
            stateTable.getSelectionModel().selectAll();
        });
        contextMenu.getItems().addAll(copyMenuItem, markAllMenuItem);
        stateTable.setContextMenu(contextMenu);
    }
    
    private static class StateListCell extends ListCell<State> {
        
        private final ImageView view;
        StateListCell() {
            view = new ImageView();
        }

        @Override
        protected void updateItem(State item, boolean empty) {
            super.updateItem(item, empty);

            if (item == null || empty) {
                setGraphic(null);
                setText(null);
            } else {
                view.setImage(item.getIcon());
                setGraphic(view);
                setText(item.getName());
            }
        }
    }
}
