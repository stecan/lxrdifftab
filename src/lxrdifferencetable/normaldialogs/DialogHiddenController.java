/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.HIDDEN_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_2;
import lxrdifferencetable.LxrTestTableController;
import lxrdifferencetable.ProgressController;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.SetProgress;

/**
 * FXML controller class to handle the dialog to hide some information
 *
 * @author Stefan Canali
 */
public class DialogHiddenController implements Initializable, ControlledStage {

    private StagesController myController;
    private LxrTestTableController lxrTestTableController;
    
    private boolean hideDeletedFiles;
    private boolean hideNewFiles;
    private boolean hideDublicatedFiles;
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;
    private String country;
    
    private ProgressController progressController;
    private SetProgress setProgress;

    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    public DialogHiddenController() {
        this.props = new Properties();
        this.hideDeletedFiles = false;
        this.hideNewFiles = false;
        this.hideDublicatedFiles = false;
    }
    
    @FXML
    Label labelNoInformationToHide;
    
    @FXML
    Button buttonClose;

    @FXML
    public CheckBox checkboxHideDeletedFiles;

    @FXML
    public CheckBox checkboxHideNewFiles;

    @FXML
    public CheckBox checkboxHideDublicatedFiles;
    
    
    @FXML
    private void handleHideDeletedFiles(ActionEvent event) {
        if (checkboxHideDeletedFiles.isDisabled()) {
            // do nothing
        } else {
            if (checkboxHideDeletedFiles.isSelected()) {
                hideDeletedFiles = true;
                props.putProperty(props.HIDEDELETEDFILES, "true");
            } else {
                hideDeletedFiles = false;
                props.putProperty(props.HIDEDELETEDFILES, "false");
            }
        }
        checkboxHideDeletedFiles.setDisable(true);
        checkboxHideNewFiles.setDisable(true);
        checkboxHideDublicatedFiles.setDisable(true);
        
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();
        
        lxrTestTableController.setHideDeletedFiles(hideDeletedFiles);
        lxrTestTableController.buildTables(null, progressController, 
                setProgress, null, true);
    }

    @FXML
    private void handleHideNewFiles(ActionEvent event) {
        if (checkboxHideNewFiles.isDisabled()) {
            // do nothing
        } else {
            if (checkboxHideNewFiles.isSelected()) {
                hideNewFiles = true;
                props.putProperty(props.HIDENEWFILES, "true");
            } else {
                hideNewFiles = false;
                props.putProperty(props.HIDENEWFILES, "false");
            }
        }
        checkboxHideDeletedFiles.setDisable(true);
        checkboxHideNewFiles.setDisable(true);
        checkboxHideDublicatedFiles.setDisable(true);
        
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();
        
        lxrTestTableController.setHideNewFiles(hideNewFiles);
        lxrTestTableController.buildTables(null, progressController, 
                setProgress, null, true);
    }

    @FXML
    private void handleHideDublicatedFiles(ActionEvent event) {
        if (checkboxHideDublicatedFiles.isDisabled()) {
            // do nothing
        } else {
            if (checkboxHideDublicatedFiles.isSelected()) {
                hideDublicatedFiles = true;
                props.putProperty(props.HIDEDUBLICATEDFILES, "true");
            } else {
                hideDublicatedFiles = false;
                props.putProperty(props.HIDEDUBLICATEDFILES, "false");
            }
        }
        checkboxHideDeletedFiles.setDisable(true);
        checkboxHideNewFiles.setDisable(true);
        checkboxHideDublicatedFiles.setDisable(true);
        
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_2);
        lxrTestTableController = loader.
                <LxrTestTableController>getController();
        
        lxrTestTableController.setHideDublicatedFiles(hideDublicatedFiles);
        lxrTestTableController.buildTables(null, progressController, 
                setProgress, null, true);
    }
    
    @FXML
    private void handleClose(ActionEvent event) {
        myController.hideStage(HIDDEN_STAGE);
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // locale settings
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);

        // initialize checkboxes for hiding information
        String hide = props.getProperty(props.HIDEDUBLICATEDFILES);
        switch (hide) {
            case "false":
                hideDublicatedFiles = false;
                break;
            case "true":
                hideDublicatedFiles = true;
                checkboxHideDublicatedFiles.setSelected(true);
                break;
            default:
                hideDublicatedFiles = false;
                props.putProperty(props.HIDEDUBLICATEDFILES, "false");
                break;
        }
        
        hide = props.getProperty(props.HIDEDELETEDFILES);
        switch (hide) {
            case "false":
                hideDeletedFiles = false;
                break;
            case "true":
                hideDeletedFiles = true;
                checkboxHideDeletedFiles.setSelected(true);
                break;
            default:
                hideDeletedFiles = false;
                props.putProperty(props.HIDEDELETEDFILES, "false");
                break;
        }
        hide = props.getProperty(props.HIDENEWFILES);
        switch (hide) {
            case "false":
                hideNewFiles = false;
                break;
            case "true":
                hideNewFiles = true;
                checkboxHideNewFiles.setSelected(true);
                break;
            default:
                hideNewFiles = false;
                props.putProperty(props.HIDENEWFILES, "false");
                break;
        }
        
        if (checkboxHideDeletedFiles.isDisabled() && 
                checkboxHideNewFiles.isDisabled() &&
                checkboxHideDublicatedFiles.isDisabled()) {
            // If no information is available to hide, then shows it as a hint 
            // to the user.
            labelNoInformationToHide.setText(ResourceBundle.getBundle(
                        "lxrdifferencetable/Bundle", locale).
                        getString("labelNoInformationToHide"));
        } else {
            // There is information available to hide. Shows this as a hint to 
            // the user.
            labelNoInformationToHide.setText(ResourceBundle.getBundle(
                        "lxrdifferencetable/Bundle", locale).
                        getString("labelInformationToHide"));
        }
        
    }    

    public void setProgressController (ProgressController progressController) {
        this.progressController = progressController;
    }
    
    public void setProgress (SetProgress setProgress) {
        this.setProgress = setProgress;
    }
}
