/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import lxrdifferencetable.ControlledStage;
import lxrdifferencetable.SaveController;
import lxrdifferencetable.StagesController;
import static lxrdifferencetable.LxrDifferenceTable.FILE_RENAME_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SAVE_STAGE;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogRenameFileController implements Initializable, ControlledStage {

    private StagesController myController;
    private SaveController saveController;
    
    private String fileName;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;

        FXMLLoader loader;
        loader = myController.getLoader(SAVE_STAGE);
        saveController = loader.<SaveController>getController();
    }

    @FXML
    Button buttonYes;
    
    @FXML
    Button buttonNo;
    
    @FXML
    TextField textField;
    
    @FXML
    private void handleYes(ActionEvent event) {
        String selectedItem;
        selectedItem = textField.getText();
        saveController.renameFile(selectedItem);
        myController.hideStage(FILE_RENAME_STAGE);
    }
    
    @FXML
    private void handleNo(ActionEvent event) {
        myController.hideStage(FILE_RENAME_STAGE);
    }

    /**
     * Sets the name of the file, that is to be renamed.
     * 
     * @param name
     */
    public void setFileName (String name) {
        fileName = name;
        textField.setText(name);
    }
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
