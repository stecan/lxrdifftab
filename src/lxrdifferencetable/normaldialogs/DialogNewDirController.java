/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lxrdifferencetable.ControlledStage;
import lxrdifferencetable.SaveController;
import lxrdifferencetable.StagesController;
import static lxrdifferencetable.LxrDifferenceTable.NEW_DIR_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SAVE_STAGE;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogNewDirController implements Initializable, ControlledStage {

    private StagesController myController;
    private SaveController saveController;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;

        FXMLLoader loader;
        loader = myController.getLoader(SAVE_STAGE);
        saveController = loader.<SaveController>getController();
    }
    
    @FXML
    Button buttonYes;
    
    @FXML
    Button buttonNo;
    
    @FXML
    TextField textField;
    
    @FXML
    private void handleYes(ActionEvent event) {
        String selectedItem;
        selectedItem = textField.getText();
        saveController.CreateDirectory(selectedItem);
        myController.hideStage(NEW_DIR_STAGE);
    }
    
    @FXML
    private void handleKey(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {        
            handleYes(null);
        }
    }

    @FXML
    private void handleNo(ActionEvent event) {
        myController.hideStage(NEW_DIR_STAGE);
    }

    /**
     * Sets the name of the file, that is to be renamed.
     * 
     * @param name
     */
    public void setDirName (String name) {
        textField.setText(name);
        textField.requestFocus();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
