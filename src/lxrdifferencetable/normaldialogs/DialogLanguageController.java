/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import lxrdifferencetable.ControlledStage;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.StagesController;
import static lxrdifferencetable.LxrDifferenceTable.LANGUAGE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_2;
import lxrdifferencetable.LxrTestTableController;
import lxrdifferencetable.tools.Properties;

/**
 * FXML controller class to handle the help menu "Language".
 *
 * @author Stefan Canali
 */
public class DialogLanguageController implements Initializable, ControlledStage {

    private final Properties props;
    private String lang;
    private String langs;
    private String langsDE;
    private String langsEN;
    private String country;
    private String countries;

    private StagesController myController;
    
    public DialogLanguageController() {
        this.props = new Properties();
    }

    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }

    @FXML
    ComboBox comboBoxLanguage;
    
    @FXML
    private void handleAbort (ActionEvent event) {
        myController.hideStage(LANGUAGE_STAGE);

        // if changes are made, then reset to saved language.
        switch (lang) {
            case "de":
                comboBoxLanguage.getSelectionModel().select(0);
                break;
            case "en":
                comboBoxLanguage.getSelectionModel().select(1);
                break;
            // default
            default:
                comboBoxLanguage.getSelectionModel().select(0);
                break;
        }
        
    }
    
    @FXML
    private void handleSave (ActionEvent event) {
        // Do not exchange the following two lines!
        //
        // Reason:
        // To reload all stages with the new language,
        // all controllers have to be inactivated!
        String currentLang = lang;
        myController.hideStage(LANGUAGE_STAGE);     // deactivate controller!
        save();                                     // save new settings
        
        if (currentLang.equals(lang)) {
            // do nothing, because the selected language is the old one.
        } else {
            // Close watcher thread of test table controller
            FXMLLoader loader1 = myController.getLoader(MAIN_STAGE_2);
            LxrTestTableController lxrTestTableController = loader1.<LxrTestTableController>getController();
            lxrTestTableController.closeWatcherThread();

            // Reload all stages with the new language!
            FXMLLoader loader = myController.getLoader(MAIN_STAGE_1);
            LxrDifferenceTableController lxrDifferenceTableController = loader.<LxrDifferenceTableController>getController();
            lxrDifferenceTableController.reloadStages();
        }
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> items;
        String[] langList;

        lang = props.getProperty(props.LANG);
        langs = props.getProperty(props.LANGS);
        langsDE = props.getProperty(props.LANGSDE);
        langsEN = props.getProperty(props.LANGSEN);
        country = props.getProperty(props.COUNTRY);
        countries = props.getProperty(props.COUNTRIES);
        
        if ((langs.equals("")) || (countries.equals("")) || 
                (langsDE.equals("")) || (langsEN.equals("")) ||
                (lang.equals("")) ) {
            
            // if one language parameter is empty, then all
            // language parameters are initialized.
            lang = "de";
            props.putProperty(props.LANG, lang);
            langs = "de,en";
            props.putProperty(props.LANGS, langs);
            langsDE = "Deutsch,Englisch";
            props.putProperty(props.LANGSDE, langsDE);
            langsEN = "German,English";
            props.putProperty(props.LANGSEN, langsEN);

            country = "DE";
            props.putProperty(props.COUNTRIES, country);
            countries = "DE,GB";
            props.putProperty(props.COUNTRIES, countries);
        }

        switch (lang) {
            case "de":
                langList = langsDE.split(",");

                items = FXCollections.observableArrayList ();
                items.addAll(Arrays.asList(langList));
                comboBoxLanguage.setItems(items);  
                comboBoxLanguage.getSelectionModel().
                        select(items.get(0));
                break;
            case "en":
                langList = langsEN.split(",");

                items = FXCollections.observableArrayList ();
                items.addAll(Arrays.asList(langList));
                comboBoxLanguage.setItems(items);    
                comboBoxLanguage.getSelectionModel().
                        select(items.get(1));
                break;
        // default
            default:
                langList = langsDE.split(",");

                items = FXCollections.observableArrayList ();
                items.addAll(Arrays.asList(langList));
                comboBoxLanguage.setItems(items);    
                comboBoxLanguage.getSelectionModel().
                        select(items.get(0));
                break;
        }

    }    
    
    private void save () {
        int selectedIndex;
        String[] langList;
        String[] countryList;
        
        selectedIndex = comboBoxLanguage.getSelectionModel().getSelectedIndex();
        
        langList = langs.split(",");
        countryList = countries.split(",");
        
        lang = langList[selectedIndex];
        country = countryList[selectedIndex];
        
        props.putProperty(props.LANG, lang);
        props.putProperty(props.COUNTRY, country);
    }
    
}
