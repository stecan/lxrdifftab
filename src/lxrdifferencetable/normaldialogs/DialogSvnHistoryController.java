/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.normaldialogs;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.util.Callback;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.SVN_HISTORY_STAGE;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.data.SvnDataModel;
import lxrdifferencetable.tablecells.SvnEditingCell;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.TableUtils;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogSvnHistoryController implements Initializable, ControlledStage {

    private StagesController myController;
    
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;                           
    private String country;       
    private boolean initialized;
    
    private TableColumn<SvnDataModel, String> columnWithSvnVersion;
    private TableColumn<SvnDataModel, String> columnWithSvnDesc;
    private TableColumn<SvnDataModel, String> columnWithSvnAuthor;
    private TableColumn<SvnDataModel, String> columnWithSvnDate;
    
    
    public DialogSvnHistoryController() {
        props = new Properties();
        initialized = false;
    }
    
    @FXML
    private TableView svnTable;
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    private void handleExit(ActionEvent event) {
	myController.hideStage(SVN_HISTORY_STAGE);
    }
    
    
    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
    }    

    /**
     * This method sets the values in the TableView "table".
     * 
     * @param entry     associtated data
     */
    public void setTable(TableEntry entry) {
        // clear table
        svnTable.getItems().clear();        // clear content
        
        if (!initialized) {
            initialized = true;
            initTable();                    // init table
            
            String title;
            title = java.util.ResourceBundle
                    .getBundle("lxrdifferencetable/Bundle", locale)
                    .getString("programName") +
                    " - " + 
                    java.util.ResourceBundle
                            .getBundle("lxrdifferencetable/Bundle", locale)
                            .getString("title.SvnHistory");
            myController.getStage(SVN_HISTORY_STAGE).setTitle(title);        
        }

        int i = 0;
        for (String version : entry.getSvnVersion()) {
            addNewLineInTable(version, entry.getSvnDesc(i),
                    entry.getSvnAuthor(i), entry.getSvnDate(i));
            i++;
        }
    }
    
    private void initTable() {
        //Create a customer cell factory so that cells can support editing.
        Callback<TableColumn<SvnDataModel, String>, TableCell<SvnDataModel, String>> cellFactory = 
                new Callback<TableColumn<SvnDataModel, String>, 
                        TableCell<SvnDataModel, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                SvnEditingCell cell = new SvnEditingCell();
                cell.widthProperty()
                        .addListener((ObservableValue<? extends Number> observableValue, 
                                Number oldSceneWidth, Number newSceneWidth) -> {
                    Text text = new Text();
                    cell.setGraphic(text);
                    text.wrappingWidthProperty().bind(cell.widthProperty());
                    text.textProperty().bind(cell.itemProperty());
                });
                return cell;
            }
        };
        
        columnWithSvnVersion = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemSvnVersion"));
        columnWithSvnVersion.setCellValueFactory(new PropertyValueFactory<>("svnVersion"));
        columnWithSvnVersion.setCellFactory(cellFactory);
        columnWithSvnDesc = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemSvnDescription"));
        columnWithSvnDesc.setCellValueFactory(new PropertyValueFactory<>("svnDesc"));
        columnWithSvnDesc.setCellFactory(cellFactory);
        columnWithSvnAuthor = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemSvnAuthor"));
        columnWithSvnAuthor.setCellValueFactory(new PropertyValueFactory<>("svnAuthor"));
        columnWithSvnAuthor.setCellFactory(cellFactory);
        columnWithSvnDate = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemSvnDate"));
        columnWithSvnDate.setCellValueFactory(new PropertyValueFactory<>("svnDate"));
        columnWithSvnDate.setCellFactory(cellFactory);

        svnTable.getColumns().addAll(
                columnWithSvnVersion, 
                columnWithSvnDesc, columnWithSvnAuthor, columnWithSvnDate);
	// enable multi-selection
	svnTable.getSelectionModel().setCellSelectionEnabled(true);
	svnTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        // resize listener
        myController.getStage(SVN_HISTORY_STAGE).getScene()
                .widthProperty()
                .addListener((ObservableValue<? extends Number> observableValue, 
                        Number oldSceneWidth, Number newSceneWidth) -> {
                    double width = myController.getStage(SVN_HISTORY_STAGE).getScene()
                            .getWidth();
                    if (width < 400.0) {
                        columnWithSvnVersion.setPrefWidth(60.0);
                        columnWithSvnDesc.setPrefWidth(100.0);
                        columnWithSvnAuthor.setPrefWidth(80.0);
                        columnWithSvnDate.setPrefWidth(100.0);
                    } else if (width < 500.0) {
                        columnWithSvnVersion.setPrefWidth(80.0);
                        columnWithSvnDesc.setPrefWidth(150.0);
                        columnWithSvnAuthor.setPrefWidth(80.0);
                        columnWithSvnDate.setPrefWidth(150.0);
                    } else if (width < 700.0) {
                        columnWithSvnVersion.setPrefWidth(100.0);
                        columnWithSvnDesc.setPrefWidth(200.0);
                        columnWithSvnAuthor.setPrefWidth(100.0);
                        columnWithSvnDate.setPrefWidth(200.0);
                    } else if (width < 900.0) {
                        columnWithSvnVersion.setPrefWidth(100.0);
                        columnWithSvnDesc.setPrefWidth(300.0);
                        columnWithSvnAuthor.setPrefWidth(100.0);
                        columnWithSvnDate.setPrefWidth(250.0);
                    } else if (width < 1100.0) {
                        columnWithSvnVersion.setPrefWidth(100.0);
                        columnWithSvnDesc.setPrefWidth(400.0);
                        columnWithSvnAuthor.setPrefWidth(100);
                        columnWithSvnDate.setPrefWidth(200.0);
                    } else if (width < 1500.0) {
                        columnWithSvnVersion.setPrefWidth(100.0);
                        columnWithSvnDesc.setPrefWidth(800.0);
                        columnWithSvnAuthor.setPrefWidth(100.0);
                        columnWithSvnDate.setPrefWidth(250.0);
                    } else if (width < 1700.0) {
                        columnWithSvnVersion.setPrefWidth(100.0);
                        columnWithSvnDesc.setPrefWidth(1000.0);
                        columnWithSvnAuthor.setPrefWidth(100.0);
                        columnWithSvnDate.setPrefWidth(250.0);
                    } else if (width < 1900.0) {
                        columnWithSvnVersion.setPrefWidth(100.0);
                        columnWithSvnDesc.setPrefWidth(1200.0);
                        columnWithSvnAuthor.setPrefWidth(100.0);
                        columnWithSvnDate.setPrefWidth(250.0);
                    } else if (width < 2100.0) {
                        columnWithSvnVersion.setPrefWidth(120.0);
                        columnWithSvnDesc.setPrefWidth(1400.0);
                        columnWithSvnAuthor.setPrefWidth(120.0);
                        columnWithSvnDate.setPrefWidth(300.0);
                    } else if (width < 2300.0) {
                        columnWithSvnVersion.setPrefWidth(120.0);
                        columnWithSvnDesc.setPrefWidth(1500.0);
                        columnWithSvnAuthor.setPrefWidth(200.0);
                        columnWithSvnDate.setPrefWidth(300.0);
                    } else {
                        columnWithSvnVersion.setPrefWidth(120.0);
                        columnWithSvnDesc.setPrefWidth(1600.0);
                        columnWithSvnAuthor.setPrefWidth(300.0);
                        columnWithSvnDate.setPrefWidth(300.0);
                    }
        });
    }
    
    private void addNewLineInTable(String svnVersion, String svnDesc,
            String svnAuthor, String svnDate)
    {
        String description;
        description = svnDesc;
        description = description.replaceAll("<br />", "\n");
        SvnDataModel svnDataModel = new SvnDataModel();
        svnDataModel.setSvnVersion(svnVersion);
        svnDataModel.setSvnDesc(description);
        svnDataModel.setSvnAuthor(svnAuthor);
        svnDataModel.setSvnDate(svnDate);
        svnTable.getItems().add(svnDataModel);
        svnTable.setEditable(true);
        
        // enable copy/paste
        TableUtils tableUtils = new TableUtils();
        tableUtils.installCopyPasteHandler(svnTable);
        
        // set context menu for copy/paste
        ContextMenu contextMenu = new ContextMenu();
        MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem.setOnAction((ActionEvent event) -> {
            tableUtils.copySelectionToClipboard(svnTable);
        });
        markAllMenuItem.setOnAction((ActionEvent event) -> {
            svnTable.getSelectionModel().selectAll();
        });
        contextMenu.getItems().addAll(copyMenuItem, markAllMenuItem);
        svnTable.setContextMenu(contextMenu);
    }
}
