/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lxrdifferencetable.errordialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import lxrdifferencetable.ControlledStage;
import lxrdifferencetable.StagesController;
import static lxrdifferencetable.LxrDifferenceTable.COULD_NOT_RENAME_FILE_STAGE;

/**
 * FXML Controller class
 *
 * @author stecan
 */
public class DialogCouldNotRenameFileController implements Initializable, ControlledStage {

    private StagesController myController;

    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    Button buttonOK;
    
    @FXML
    private void handleOK(ActionEvent event) {
        myController.hideStage(COULD_NOT_RENAME_FILE_STAGE);
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
