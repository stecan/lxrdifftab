/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.errordialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import lxrdifferencetable.ControlledStage;
import lxrdifferencetable.StagesController;
import static lxrdifferencetable.LxrDifferenceTable.REVIEW_DESCRIPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.TEST_DESCRIPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.UNSAVED_MODIFICATIONS_STAGE;
import lxrdifferencetable.normaldialogs.DialogReviewDescriptionController;
import lxrdifferencetable.normaldialogs.DialogTestDescriptionController;

/**
 * FXML Controller class
 *
 * @author Stefan Canali
 */
public class DialogUnsavedModificationsController implements Initializable, ControlledStage {

    private StagesController myController;
    private DialogReviewDescriptionController dialogReviewDescriptionController;
    private DialogTestDescriptionController dialogTestDescriptionController;

    public DialogUnsavedModificationsController() {
    }
    
    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }
    
    @FXML
    Button buttonYes;
    
    @FXML
    Button buttonNo;
    
    @FXML
    private void handleYes(ActionEvent event) {
        FXMLLoader loader;
        loader = myController.getLoader(REVIEW_DESCRIPTION_STAGE);
        dialogReviewDescriptionController = loader.<DialogReviewDescriptionController>getController();
        loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
        dialogTestDescriptionController = loader.<DialogTestDescriptionController>getController();

        // Modification are done. Discard them and leave stages
        dialogReviewDescriptionController.handleAbort();             // leave calling stage
        dialogTestDescriptionController.handleAbort();               // leave calling stage
        myController.hideStage(UNSAVED_MODIFICATIONS_STAGE);    // leave this stage
    }
    
    @FXML
    private void handleNo(ActionEvent event) {
        myController.hideStage(UNSAVED_MODIFICATIONS_STAGE);    // leave this stage
    }

    /**
     * Initializes the controller class.
     * 
     * @param url   not used!
     * @param rb    not used!
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
}
