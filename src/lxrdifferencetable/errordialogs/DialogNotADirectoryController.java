/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package lxrdifferencetable.errordialogs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import lxrdifferencetable.ControlledStage;
import static lxrdifferencetable.LxrDifferenceTable.NOT_A_DIRECTORY_STAGE;
import lxrdifferencetable.StagesController;

/**
 * FXML Controller class
 *
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class DialogNotADirectoryController implements Initializable, ControlledStage {

    private StagesController myController;

    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
    }

    @FXML
    Button buttonOK;
    
    @FXML
    private void handleOK(ActionEvent event) {
        myController.hideStage(NOT_A_DIRECTORY_STAGE);
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
