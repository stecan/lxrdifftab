/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.config;

/**
 *
 * @author Stefan Canali
 */
public class AnchorConfig {

    /**
     * The AnchorConfig class is just here to identify the Java archive
     * that holds the resources.
     *
     * This trick is to retrieve resources from the jar file.
     *
     * For more information see the Java Web Start Resource Loading Tutorial on
     * http://rachel.sourceforge.net/tutorial.html
     */
    public AnchorConfig() {
    }
}
