/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import lxrdifferencetable.comp.Generate;
import lxrdifferencetable.tools.Properties;
import java.net.URL;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.web.WebView;
import static lxrdifferencetable.LxrDifferenceTable.ABOUT_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.CONFIG_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.FAQ_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.IO_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.JDOM_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.NULL_POINTER_EXCEPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.REVIEW_DESCRIPTION_STAGE;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.data.TestDataModel;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableRow;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Callback;
import static lxrdifferencetable.LxrDifferenceTable.BROWSE_DIFF_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.CAPTION_HISTORY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.CAPTION_RENAME_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.DESC_HISTORY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.DESC_HISTORY_STAGE1;
import static lxrdifferencetable.LxrDifferenceTable.EXTERNAL_TOOLS_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.HIDDEN_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.LANGUAGE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.LOCKED_DESCRIPTION_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.LOCKED_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_1;
import static lxrdifferencetable.LxrDifferenceTable.MAIN_STAGE_2;
import static lxrdifferencetable.LxrDifferenceTable.PROGRESS_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.READ_ONLY_MODE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.REPORT_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SAVE_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SCAN_INFO_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SEARCH_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.STATE_HISTORY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.SVN_HISTORY_STAGE;
import static lxrdifferencetable.LxrDifferenceTable.TEST_DESCRIPTION_STAGE;
import lxrdifferencetable.data.ConfigEntry;
import lxrdifferencetable.data.EntryPosition;
import lxrdifferencetable.data.LogEntry;
import lxrdifferencetable.data.StatisticalData;
import lxrdifferencetable.data.StatisticsDataModel;
import lxrdifferencetable.data.TableEntry.State;
import lxrdifferencetable.normaldialogs.DialogBrowseDiffController;
import lxrdifferencetable.normaldialogs.DialogCaptionHistoryController;
import lxrdifferencetable.normaldialogs.DialogDescriptionHistory1Controller;
import lxrdifferencetable.normaldialogs.DialogDescriptionHistoryController;
import lxrdifferencetable.normaldialogs.DialogHiddenController;
import lxrdifferencetable.normaldialogs.DialogRenameCaptionController;
import lxrdifferencetable.normaldialogs.DialogReportController;
import lxrdifferencetable.normaldialogs.DialogReviewDescriptionController;
import lxrdifferencetable.normaldialogs.DialogScanInfoController;
import lxrdifferencetable.normaldialogs.DialogSearchController;
import lxrdifferencetable.normaldialogs.DialogStateHistoryController;
import lxrdifferencetable.normaldialogs.DialogSvnHistoryController;
import lxrdifferencetable.normaldialogs.DialogTestDescriptionController;
import lxrdifferencetable.tablecells.StatisticsEditingCell;
import lxrdifferencetable.tablecells.TestDataEditingCell;
import lxrdifferencetable.tools.ConvertLxrDiffTabStrings;
import lxrdifferencetable.tools.SetProgress;
import lxrdifferencetable.tools.WorkingDirectory;
import lxrdifferencetable.tools.XmlHandling;
import lxrdifferencetable.tools.XmlHandling.LockType;

/**
 * Main controller for program LxrDifferenceTable
 *
 * @author Stefan Canali
 */
public class LxrTestTableController implements Initializable, ControlledStage {

    static final Logger logger = Logger.getLogger
        (LxrDifferenceTableController.class.getName());

    private final Integer MAX = 100;            // maximum number of table entries

    private static final int STATEREADY = 0;
    private static final int STATENULLPOINTEREXCEPTION = -1;
    private static final int STATEEXCEPTION = -2;
    private static final int STATEIOEXCEPTION = -3;
    private static final int STATEJDOMEXCEPTION = -4;
    private static final int STATEABORT = -4;

    private enum SelectedTab {tab0, tab1, tab2, tab3, tab4, tabStatistics};
    
    private List<TableEntry> changedFileList;   // list of changed files
    private List<List<TableEntry>> partedFileList;
    private List<LogEntry> repoLog;
    private final StatisticalData statistics;
    private final StatisticalData svnData;

    private final HashMap<String, Integer> userMap;
    private final HashMap<Integer, Integer> commitMap;
    private int userNumber;
    
    // The pageTable has the starting fileCounts for each page
    private final List<Integer> pageTable;

    private boolean lineNumberAvailable;        // marker for column
    private boolean svnAvailable;               // marker for svn
    private boolean enableMaximizedScreen;      // do only after init
    private boolean maximizedScreen;
    
    private boolean hideDeletedFiles;
    private boolean hideNewFiles;
    private boolean hideDublicatedFiles;
    private boolean areFilesDeleted;
    private boolean areFilesNew;
    private boolean areFilesDublicated;
    
    private boolean ownBrowing;                 // REQ550
    
    private final Properties props;
    private Locale locale;                      // locale for i18n
    private String lang;
    private String country;
    private ConvertLxrDiffTabStrings conv;
    private File xmlFile;
    private File workingDir;
    private String version1 = "";
    private String version2 = "";
    private String position = "0000";

    private StagesController myController;
    private LxrDifferenceTableController lxrDifferenceTableController;
    private DialogRenameCaptionController dialogRenameCaptionController;
    private DialogHiddenController dialogHiddenController;
    private DialogScanInfoController dialogScanInfoController;
    private DialogBrowseDiffController dialogBrowseDiffController;
    private ConfigEntry configEntry;
    
    // holds the position of active controller
    private int activeReviewDescriptionController;
    private boolean activeReviewDescriptionControllerShown;
    private int activeTestDescriptionController;
    private boolean activeTestDescriptionControllerShown;
    
    private ProgressController progressController;
    private SetProgress setProgress;
    
    private volatile boolean finishedRunLater;
    
    public LxrTestTableController() {
        this.selectedTab = SelectedTab.tab0;
        this.buildTablesIsRunning = false;
        this.hideDeletedFiles = false;
        this.hideNewFiles = false;
        this.hideDublicatedFiles = false;
        this.processingBuildTables = false;
        this.tasksFinished = true;
        this.taskField = new boolean[5];
        this.titleField = new String [5];
        this.isJump = false;
        this.lastFileNr = -1;
        this.activeRegion = 0;
        this.lastActiveRegion = 0;
        this.activeReviewDescriptionControllerShown = false;
        this.activeReviewDescriptionController = -1;
        this.activeTestDescriptionControllerShown = false;
        this.activeTestDescriptionController = -1;
        this.lineNumberAvailable = true;
        this.svnAvailable = true;
        this.maximizedScreen = false;
        this.enableMaximizedScreen = false;
        this.props = new Properties();
        this.changedFileList = new ArrayList<>();
        this.partedFileList = new ArrayList<>();
        this.repoLog = new ArrayList<>();
        this.reviewStateColList = new ArrayList<>();
        this.reviewStateColBooleanList = new ArrayList<>();
        this.testStateColList = new ArrayList<>();
        this.testStateColBooleanList = new ArrayList<>();
        this.reviewShortDescriptionColList = new ArrayList<>();
        this.reviewShortDescriptionColBooleanList = new ArrayList<>();
        this.testShortDescriptionColList = new ArrayList<>();
        this.testShortDescriptionColBooleanList = new ArrayList<>();
        this.statistics = new StatisticalData();
        this.svnData = new StatisticalData();
        this.xmlFile = null;
        this.workingDir = null;
        this.areFilesDeleted = false;
        this.areFilesNew = false;
        this.areFilesDublicated = false;
        this.ownBrowing = false;
        this.pageTable = new ArrayList();
        this.jumpFileCount = 0;
        this.jumpLineNr = 0;
        userMap = new HashMap<>();
        commitMap = new HashMap<>();
        userNumber = 0;
    }

    @Override
    public void setStageParent(StagesController stageParent, boolean reloaded){
        myController = stageParent;
        
        WorkingDirectory dir = new WorkingDirectory(myController);
        workingDir = dir.getWorkingDir();
        
        ownBrowing();
        
        File lockDir = new File(workingDir + File.separator + ".lock");
        if (lockDir.isDirectory()) {
            menuItemSave.setVisible(false);
        } else {
            menuItemSave.setVisible(true);
        }
        
    }
    
    private void ownBrowing () {
        File reviewedSources = new File(workingDir.getPath() + File.separator + "ReviewedSources");
        if (reviewedSources.exists()) {
            ownBrowing = true;
        } else {
            ownBrowing = false;
        }
    }

    private TableColumn<TestDataModel, Integer> columnWithFileCount;
    private TableColumn<TestDataModel, Text> columnWithLink;
    private TableColumn<TestDataModel, String> columnWithLineNumber;
    private TableColumn<TestDataModel, String> columnWithSvnVersion;
    private TableColumn<TestDataModel, String> columnWithSvnDesc;
    private TableColumn<TestDataModel, String> columnWithSvnAuthor;
    private TableColumn<TestDataModel, String> columnWithSvnDate;
    private TableColumn<TestDataModel, ComboBox<State>> columnWithReviewState;
    private TableColumn<TestDataModel, ComboBox<State>> columnWithTestState;
    private TableColumn<TestDataModel, TextField> columnWithReviewShortDescription;
    private TableColumn<TestDataModel, TextField> columnWithTestShortDescription;

    // helper lists
    private final List<List<ComboBox>> reviewStateColList;
    private final List<List<Boolean>> reviewStateColBooleanList;
    private final List<List<ComboBox>> testStateColList;
    private final List<List<Boolean>> testStateColBooleanList;
    private final List<List<TextField>> reviewShortDescriptionColList;
    private final List<List<Boolean>> reviewShortDescriptionColBooleanList;
    private final List<List<TextField>> testShortDescriptionColList;
    private final List<List<Boolean>> testShortDescriptionColBooleanList;
    
    private TableColumn<StatisticsDataModel, String> columnWithStatisticsTableData;
    private TableColumn<StatisticsDataModel, String> columnWithStatisticsTableCount;
    private TableColumn<StatisticsDataModel, String> columnWithStatisticsReviewData;
    private TableColumn<StatisticsDataModel, String> columnWithStatisticsReviewCount;
    private TableColumn<StatisticsDataModel, String> columnWithStatisticsTestData;
    private TableColumn<StatisticsDataModel, String> columnWithStatisticsTestCount;

    private TableColumn<StatisticsDataModel, String> columnWithSvnUsers;
    private TableColumn<StatisticsDataModel, String> columnWithSvnCommits;
    
    private int lastFileNr;     
    private int lastActiveRegion;           // pointer to partedFileList
    private boolean processingBuildTables;  // variable to block parallel access
    private boolean buildTablesIsRunning;   // Is method "buildTables" running?
    
    // share variables/fields
    private volatile boolean[] taskField;
    private volatile String[] titleField;
    private volatile SelectedTab selectedTab;
    private volatile boolean isJump;        // flag for running method 
                                            // jumpToLine

    private volatile int activeRegion;      // pointer to partedFileList
    private volatile boolean tasksFinished;
    
    private int jumpFileCount;
    private int jumpLineNr;
    
    Runnable watcherTask;
    Thread watcherThread; 
    WatchService watcher;
    
    @FXML
    Label labelCaption;

    @FXML
    Hyperlink hyperlinkUrl;

    @FXML
    WebView webView;
    
    @FXML
    Label labelAccessability;

    @FXML
    Label labelNrOfAuthors;

    @FXML
    Label labelNrOfCommits;
    
    @FXML
    TabPane tabPane;

    @FXML
    Tab tabTestPrevious;

    @FXML
    ContextMenu tabTestPreviousContextMenu;
    
    @FXML
    Tooltip tabTestPreviousTooltip;
    
    @FXML
    Tab tabTest0;
    
    @FXML
    Tooltip tabTestTooltip0;

    @FXML
    Tab tabTest1;

    @FXML
    Tooltip tabTestTooltip1;

    @FXML
    Tab tabTest2;

    @FXML
    Tooltip tabTestTooltip2;

    @FXML
    Tab tabTest3;

    @FXML
    Tooltip tabTestTooltip3;

    @FXML
    Tab tabTest4;
    
    @FXML
    Tooltip tabTestTooltip4;

    @FXML
    Tab tabTestNext;

    @FXML
    Tab tabStatistics;

    @FXML
    Tooltip tabTestNextTooltip;
    
    @FXML
    ContextMenu tabTestNextContextMenu;
    
    @FXML
    ProgressBar progressBar;

    @FXML
    MenuItem menuItemSearch;

    @FXML
    MenuItem menuItemSave;

    @FXML
    private TableView<TestDataModel> tableField;
    
    @FXML
    private TableView<TestDataModel> tableField1;
    
    @FXML
    private TableView<TestDataModel> tableField2;
    
    @FXML
    private TableView<TestDataModel> tableField3;
    
    @FXML
    private TableView<TestDataModel> tableField4;
    
    @FXML
    private TableView<TestDataModel> tableFieldEnd;
    
    @FXML
    private TableView<StatisticsDataModel> tableStatisticsField;

    @FXML
    private TableView<StatisticsDataModel> reviewStatisticsField;

    @FXML
    private TableView<StatisticsDataModel> testStatisticsField;

    @FXML
    private TableView<StatisticsDataModel> svnStatisticsField;

    @FXML
    private void handleAbout(ActionEvent event) {
        myController.getStage(ABOUT_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemAbout"));
        myController.showStage(ABOUT_STAGE);
    }

    @FXML
    private void handleFaq(ActionEvent event) {
        myController.getStage(FAQ_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemFaq"));
        myController.showStage(FAQ_STAGE);
    }

    @FXML
    private void handleExternalTools(ActionEvent event) {
        myController.getStage(EXTERNAL_TOOLS_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemExternalTools"));
        myController.showStage(EXTERNAL_TOOLS_STAGE);
    }

    @FXML
    private void handleConfig(ActionEvent event) {
        myController.getStage(CONFIG_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemConfig"));
        myController.showStage(CONFIG_STAGE);
    }

    @FXML
    private void handleClose(ActionEvent event) {
        // Init caption to default.
        // Do not remove!
        // The intialization is important to reinvoking this class, because
        // it is set by FXML automatically. In the case of reinvokation, an
        // empty caption results in showing the last caption from the last
        // project!
        labelCaption.setText(ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("labelCaption"));
        myController.hideStage(MAIN_STAGE_2);
        myController.showStage(MAIN_STAGE_1);
        lxrDifferenceTableController.setActiveStage(MAIN_STAGE_1);
        lxrDifferenceTableController.deleteGen();
        if (lxrDifferenceTableController.getXmlHandling() != null) {
            // do only, if XML-File is loaded
            lxrDifferenceTableController.getXmlHandling().releaseLockFile();
            lxrDifferenceTableController.getXmlHandling().clearOldLocks(false);
        }
        lxrDifferenceTableController.resetXmlLoaded();
        
        if (maximizedScreen) {
            props.putProperty(props.MAXIMIZEDSCREEN, "true");
        } else {
            props.putProperty(props.MAXIMIZEDSCREEN, "false");
        }

        // force the garbage collector to clean up the memory
        // System.gc ();
        // System.runFinalization ();
    }

    @FXML
    private void handleLanguage(ActionEvent event) {
        myController.getStage(LANGUAGE_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemLanguage"));
        myController.showStage(LANGUAGE_STAGE);
    }

    @FXML
    private void handleHidden(ActionEvent event) {
        myController.getStage(HIDDEN_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemHidden"));
        myController.showStage(HIDDEN_STAGE);
    }
    
    @FXML
    private void handleScanInfo(ActionEvent event) {
        FXMLLoader loader;
        loader = myController.getLoader(SCAN_INFO_STAGE);
        dialogScanInfoController = loader.
                <DialogScanInfoController>getController();
        dialogScanInfoController.setRepoLog(repoLog);
        
        myController.getStage(SCAN_INFO_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemScanInfo"));
        myController.showStage(SCAN_INFO_STAGE);
    }
    
    /**
     * Method: extractVersions
     * 
     * Description:
     * This method is only called once, because the diffed versions are the same 
     * for all files.
     * 
     */
    private void extractVersions() {
        boolean found = false;
        // 1st: search for a diff file to extract the versions
        for (int i = 0; i < changedFileList.size(); i++) {
            if (changedFileList.get(i).getDiff().equalsIgnoreCase("true")) {
                String href = changedFileList.get(i).getHref(0).get(0);
                version1 = getVersion1(href);
                version2 = getVersion2(href);
                found = true;
                break;
            }
        }
        if (!found) {
            // 2nd: search for deleted or new files to extract the versions
            for (int i = 0; i < changedFileList.size(); i++) {
                if (changedFileList.get(i).getDiff().equalsIgnoreCase("false")) {
                    String href = changedFileList.get(i).getHref(0).get(0);
                    version1 = getVersion1(href);
                    version2 = getVersion2(href);
                    found = true;
                    break;
                }
            }
        }
        if (!found) {
            // normally, this cannot be happend!
            logger.severe("Versions not found: I give up!");
        }
    }
    
    public String getPosition(String href) {
        // e.g. href = 
        // "http://localhost/lxr/.../SaveControll...#0035
        //        firstPart                         - secondPart
        String position = "";
        String[] split = href.split("#");
        if (split.length == 1) {
            position = "0001";                      // in case of an error!
        } else {
            position = split[1];
        }
        return position;
    }
    
    private String getVersion1(String href) {
        // e.g. href = 
        // "http://localhost/lxr/.../SaveController.java?v=20201122&~v=20210411&!v=20201122#1260"
        //        firstPart                             ---       secondPart
        String version = "";
        String[] split = href.split("\\?v=");
        if (split.length == 1) {
            // do nothing
        } else {
            String secondPart = split[1];
            String[] split1 = secondPart.split("&~v=");
            version = split1[0];      // ... part before "%~v="
        }        
        return version;
    }
    
    private String getVersion2(String href) {
        // e.g. href = 
        // "http://localhost/lxr/.../SaveController.java?v=20201122&~v=20210411&!v=20201122#1260"
        //        firstPart                             ---       secondPart
        String version = "";
        String[] split = href.split("&~v=");
        if (split.length == 1) {
            // do nothing
        } else {
            String secondPart = split[1];
            String[] split1 = secondPart.split("&!v=");
            version = split1[0];      // ... part before "%~v="
        }        
        return version;
    }
    
    private String getDiffFileName(String href) {
        // e.g. href = 
        // "http://localhost/lxr/diff/src/lxrdifferencetable/SaveController.java?v=2020..."
        //        firstPart     -----        secondPart
        String filename = "";
        String[] split = href.split("/lxr/diff");
        if (split.length == 1) {
            // do nothing
        } else {
            String secondPart = split[1];
            String[] split1 = secondPart.split("\\?");
            filename = split1[0];      // ... part before question mark ("?")
        }
        return filename;
    }

    private String getSourceFileName(String href) {
        // e.g. href = 
        // "http://localhost/lxr/source/src/lxrdifferencetable/SaveController.java?v=2020..."
        //        firstPart     --------        secondPart
        String filename = "";
        String[] split = href.split("/lxr/source");
        if (split.length == 1) {
            // do nothing
        } else {
            String secondPart = split[1];
            String[] split1 = secondPart.split("\\?");
            filename = split1[0];      // ... part before question mark ("?")
        }
        return filename;
    }
    
    /**
     * If a local scan directory exists, then this method returns a modified
     * address to the local filesystem, else it returns the original address.
     * 
     * Implemented requirements
     * - REQ488
     * - REQ494
     * 
     * @param href
     * @return String   
     */
    private String getModifiedHref(String href) {
        String returnString;
        String[] split = href.split("/");
        
        String protocol = split[0];
        String serverWithPort = split [2];
        
        String[] split1 = serverWithPort.split(":");
        String server = split1[0];
        
        // REQ488
        File dir = new File (workingDir + File.separator + server);
        if (dir.exists()) {
            String r1 = "file:///" + dir + File.separator +
                    href.replace(protocol + "//" + serverWithPort + "/", "");
            String[] split2 = r1.split("#");
            String r2;
            if (split2.length == 2) {
                r2 = split2[0] + ".html#" + split2[1];
            } else if (split2.length == 1) {
                r2 = split2[0] + ".html";
            } else {
                r2 = r1;
            }
            returnString = r2.replace("?", "%3F");
            
        } else {
            returnString = href;
        }
        return returnString;
    }
    
    private boolean isHidden () {
        boolean hidden = false;
        if (hideDeletedFiles || hideNewFiles || hideDublicatedFiles) {
            hidden = true;
        }
        return hidden;
    }
    
    public boolean isRowHidden(TableEntry entry, int changedLineNr) {
        boolean hidden = false;
        String lineNumber;

        try {
            lineNumber = entry.getLineNumberLast(changedLineNr);
            if (lineNumber.equals("fileDeleted")) {
                areFilesDeleted = true;
                if (hideDeletedFiles) {
                    hidden = true;
                }
            }
            if (lineNumber.equals("newFile")) {
                areFilesNew = true;
                if (hideNewFiles) {
                    hidden = true;
                }
            }
            if (lineNumber.equals("dublicatedFile")) {
                areFilesDublicated = true;
                if (hideDublicatedFiles) {
                    hidden = true;
                }
            }

        } catch (Exception ex) {
            // do nothing!
        }
        return hidden;
    }


    // Task container for binding the progress indicator!
    Task TaskForProgressBar() {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                int progress = 0;
                while (progress < 100) {
                    progress = 20;
                    for (int i = 0; i < taskField.length; i++) {
                        if (taskField[i]) {
                            progress = progress + 20;
                        }
                    }
                    switch (progress) {
                        case 20:
                            Platform.runLater(() -> {
                                progressBar.setProgress(0.2);
                            }); 
                            break;
                        case 40:
                            Platform.runLater(() -> {
                                progressBar.setProgress(0.4);
                            }); 
                            break;
                        case 60:
                            Platform.runLater(() -> {
                                progressBar.setProgress(0.6);
                            }); 
                            break;
                        case 80:
                            Platform.runLater(() -> {
                                progressBar.setProgress(0.8);
                            }); 
                            break;
                        case 100:
                            Platform.runLater(() -> {
                                    progressBar.setProgress(1.0);
                            }); 
                            break;
                        default:
                            Platform.runLater(() -> {
                                    progressBar.setProgress(1.0);
                            }); 
                            break;
                    }
                    Thread.sleep(1000);
                }
                while (!tasksFinished) {    // wait for tasks to finish
                    Thread.sleep(500);
                }
                Thread.sleep(2000);     // give progress bar time to complete

                return true;
            }
        };
    }
    
    // Task for building the tables
    Task TaskForBuildTables(boolean statistics) {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                Thread.sleep(100);  // give the progress bar to update the UI
                buildPartedTables(statistics);
                return true;
            }
        };
    }

    @FXML
    private void handleTabTestPrevious() {
        Task taskForProgressBar;    // task to handle progress bar
        Task taskForBuildTables;    // task to handle the parted tables
        
        tabPane.getSelectionModel().select(tabTest0);
        if ((dialogHiddenController == null) || 
                (dialogHiddenController.checkboxHideDeletedFiles == null) && 
                (dialogHiddenController.checkboxHideNewFiles == null) && 
                (dialogHiddenController.checkboxHideDublicatedFiles == null)) {
            // do nothing
        } else if (buildTablesIsRunning) {
            // do nothing
        } else {
            if (processingBuildTables) {
            } else {
                if (partedFileList.size() > 5) {
                    if (activeRegion >= 5) {
                        activeRegion = activeRegion - 5;
                    } else {
                        activeRegion = 0;
                    }

                    if (activeRegion != lastActiveRegion) {
                        processingBuildTables = true;
                        lastActiveRegion = activeRegion;

                        progressBar.setProgress(0);
                        progressBar.setVisible(true);     // show local progress bar

                        taskForProgressBar = TaskForProgressBar();
                        taskForBuildTables = TaskForBuildTables(false);

                        // Initially, all tasks are marked as inactive
                        taskField[0] = false;
                        taskField[1] = false;
                        taskField[2] = false;
                        taskField[3] = false;
                        taskField[4] = false;

                        // Initially, all tab panes are marked empty
                        titleField[0] = "";
                        titleField[1] = "";
                        titleField[2] = "";
                        titleField[3] = "";
                        titleField[4] = "";

                        // The tasks are not finished
                        tasksFinished = false;

                        // start tasks as threads
                        new Thread(taskForProgressBar).start();
                        Thread thread = new Thread(taskForBuildTables);
                        thread.setName("TaskForBuildTables - Previous Tab");
                        thread.start();
                    }
                }
            }
        }
    }

    @FXML
    private void handleTabTest0() {
    }

    @FXML
    private void handleTabTest1() {
    }

    @FXML
    private void handleTabTest2() {
    }

    @FXML
    private void handleTabTest3() {
    }

    @FXML
    private void handleTabTest4() {
    }

    @FXML
    private void handleTabTestNext() {
        Task taskForProgressBar;    // task to handle progress bar
        Task taskForBuildTables;    // task to handle the parted tables
        
        tabPane.getSelectionModel().select(tabTest0);
        
        if ((dialogHiddenController == null) || 
                (dialogHiddenController.checkboxHideDeletedFiles == null) && 
                (dialogHiddenController.checkboxHideNewFiles == null) && 
                (dialogHiddenController.checkboxHideDublicatedFiles == null)) {
            // do nothing
        } else if (buildTablesIsRunning) {
            // do nothing
        } else {
            if (processingBuildTables) {
            } else {
                if (partedFileList.size() > 5) {
                    if (activeRegion + 10 < partedFileList.size()) {
                        activeRegion = activeRegion + 5;
                    } else if (activeRegion + 5 < partedFileList.size()) {
                        activeRegion = partedFileList.size() - 5;
                    }

                    if (activeRegion != lastActiveRegion) {
                        processingBuildTables = true;
                        lastActiveRegion = activeRegion;

                        progressBar.setProgress(0);
                        progressBar.setVisible(true);     // show local progress bar

                        taskForProgressBar = TaskForProgressBar();
                        taskForBuildTables = TaskForBuildTables(false);

                        // Initially, all tasks are marked as inactive
                        taskField[0] = false;
                        taskField[1] = false;
                        taskField[2] = false;
                        taskField[3] = false;
                        taskField[4] = false;

                        // Initially, all tab panes are marked empty
                        titleField[0] = "";
                        titleField[1] = "";
                        titleField[2] = "";
                        titleField[3] = "";
                        titleField[4] = "";

                        // The tasks are not finished
                        tasksFinished = false;

                        // start tasks as threads
                        new Thread(taskForProgressBar).start();
                        Thread thread = new Thread(taskForBuildTables);
                        thread.setName("TaskForBuildTables - Next Tab");
                        thread.start();
                    }
                }
            }
        }
    }

    public void buildTables(Generate gen,
            ProgressController progressController,
            SetProgress setProgress, File xmlFile,
            boolean rebuildTables) {

        Task<Integer> task;
        
        // Worker task to handle the long lasting methodes of generating the
        // list of changed files and of outputting to TextArea
        task = new Task<Integer>() {
            @Override
            protected Integer call() {
                int state;
                Generate.State genState = Generate.State.PASSED;
                String tabTooltip;

                try {
                    // generate new output
                    state = STATEREADY;
                    
                    {   // first: initialize statistics
                        finishedRunLater = false;
                        Platform.runLater(() -> {
                            initStatiticsTable();
                            initSvnStatisticsTable();
                            finishedRunLater = true;
                        });
                        // wait, until statistics are initialized
                        while (!finishedRunLater) {
                            Thread.sleep(100);
                        }
                    }
                    
                    if (!rebuildTables) {
                        changedFileList = gen.getOutList(); // save list
                    }

                    // partedFileList = gen.getPartList();
                    getPartList();

                    if (areFilesDeleted) {
                        dialogHiddenController.checkboxHideDeletedFiles
                                .setDisable(false);
                    } else {
                        dialogHiddenController.checkboxHideDeletedFiles
                                .setDisable(true);
                        dialogHiddenController.checkboxHideDeletedFiles
                                .setSelected(false);
                    }
                    if (areFilesNew) {
                        dialogHiddenController.checkboxHideNewFiles
                                .setDisable(false);
                    } else {
                        dialogHiddenController.checkboxHideNewFiles
                                .setDisable(true);
                        dialogHiddenController.checkboxHideNewFiles
                                .setSelected(false);
                    }
                    if (areFilesDublicated) {
                        dialogHiddenController.checkboxHideDublicatedFiles
                                .setDisable(false);
                    } else {
                        dialogHiddenController.checkboxHideDublicatedFiles
                                .setDisable(true);
                        dialogHiddenController.checkboxHideDublicatedFiles
                                .setSelected(false);
                    }
                    
                    {   // second: initialize table fields and tabs
                        finishedRunLater = false;
                        Platform.runLater(() -> {
                            initCaption();
                            
                            setSelectedTab();
                            switch (partedFileList.size()) {
                                case 0:
                                    // error!
                                    tabPane.getTabs().clear();
                                    tabPane.getTabs().add(tabStatistics);
                                    break;
                                case 1:
                                    tabPane.getTabs().clear();
                                    tabPane.getTabs().add(tabTest0);
                                    tabPane.getTabs().add(tabStatistics);
                                    break;
                                case 2:
                                    tabPane.getTabs().clear();
                                    tabPane.getTabs().add(tabTest0);
                                    tabPane.getTabs().add(tabTest1);
                                    tabPane.getTabs().add(tabStatistics);
                                    break;
                                case 3:
                                    tabPane.getTabs().clear();
                                    tabPane.getTabs().add(tabTest0);
                                    tabPane.getTabs().add(tabTest1);
                                    tabPane.getTabs().add(tabTest2);
                                    tabPane.getTabs().add(tabStatistics);
                                    break;
                                case 4:
                                    tabPane.getTabs().clear();
                                    tabPane.getTabs().add(tabTest0);
                                    tabPane.getTabs().add(tabTest1);
                                    tabPane.getTabs().add(tabTest2);
                                    tabPane.getTabs().add(tabTest3);
                                    tabPane.getTabs().add(tabStatistics);
                                    break;
                                case 5:
                                    tabPane.getTabs().clear();
                                    tabPane.getTabs().add(tabTest0);
                                    tabPane.getTabs().add(tabTest1);
                                    tabPane.getTabs().add(tabTest2);
                                    tabPane.getTabs().add(tabTest3);
                                    tabPane.getTabs().add(tabTest4);
                                    tabPane.getTabs().add(tabStatistics);
                                    break;
                                default:
                                    tabPane.getTabs().clear();
                                    tabPane.getTabs().add(tabTestPrevious);
                                    tabPane.getTabs().add(tabTest0);
                                    tabPane.getTabs().add(tabTest1);
                                    tabPane.getTabs().add(tabTest2);
                                    tabPane.getTabs().add(tabTest3);
                                    tabPane.getTabs().add(tabTest4);
                                    tabPane.getTabs().add(tabTestNext);
                                    tabPane.getTabs().add(tabStatistics);
                                    break;
                            }
                            // delete old test tables
                            initTestTable(tableField);
                            initTestTable(tableField1);
                            initTestTable(tableField2);
                            initTestTable(tableField3);
                            initTestTable(tableField4);
                            finishedRunLater = true;
                        });
                        // wait, until statistics are initialized
                        while (!finishedRunLater) {
                            Thread.sleep(100);
                        }
                    }

                    statistics.setNumberOfPages(partedFileList.size());
                    activeRegion = 0;
                    lastActiveRegion = 0;   // Importannt! Do not change!
                    int begin;
                    int end =  0;
                    for (int i = 0; i < partedFileList.size(); i++) {
                        final String title;
                        begin = end + 1;
                        end = begin + partedFileList.get(i).size()-1;
                        // title = begin + " ... " + end;
                        title = partedFileList.get(i).get(0).getFileCount() + 
                                " ... " + 
                                partedFileList.get(i).get(partedFileList.get(i)
                                        .size()-1).getFileCount();
                        pageTable.add(Integer.parseInt(
                                partedFileList.get(i).get(0).getFileCount()));
                        switch (i) {
                            case 0: 
                                writeTestTable(tableField, i, true,
                                        progressController, setProgress);
                                
                                finishedRunLater = false;
                                Platform.runLater(() -> {
                                    tabTest0.setText(title);
                                    tableField.refresh();
                                    finishedRunLater = true;
                                });  

                                while (!finishedRunLater) {
                                    Thread.sleep(100);
                                }
                                
                                tabTooltip = getTabTooltip(partedFileList
                                        .get(i), end - begin + 1, 
                                        tableField.getItems().size());
                                tabTestTooltip0.setText(tabTooltip);
                                break;
                            case 1:
                                writeTestTable(tableField1, i, true,
                                        progressController, setProgress);

                                finishedRunLater = false;
                                Platform.runLater(() -> {
                                    tabTest1.setText(title);
                                    tableField1.refresh();
                                    finishedRunLater = true;
                                });                                

                                while (!finishedRunLater) {
                                    Thread.sleep(100);
                                }

                                tabTooltip = getTabTooltip(partedFileList
                                        .get(i), end - begin + 1,
                                        tableField1.getItems().size());
                                tabTestTooltip1.setText(tabTooltip);
                                break;
                            case 2:
                                writeTestTable(tableField2, i, true,
                                        progressController, setProgress);

                                finishedRunLater = false;
                                Platform.runLater(() -> {
                                    tabTest2.setText(title);
                                    tableField2.refresh();
                                    finishedRunLater = true;
                                });                                

                                while (!finishedRunLater) {
                                    Thread.sleep(100);
                                }

                                tabTooltip = getTabTooltip(partedFileList
                                        .get(i), end - begin + 1,
                                        tableField2.getItems().size());
                                tabTestTooltip2.setText(tabTooltip);
                                break;
                            case 3:
                                writeTestTable(tableField3, i, true,
                                        progressController, setProgress);

                                finishedRunLater = false;
                                Platform.runLater(() -> {
                                        tabTest3.setText(title);
                                        tableField3.refresh();
                                        finishedRunLater = true;
                                });                                

                                while (!finishedRunLater) {
                                    Thread.sleep(100);
                                }

                                tabTooltip = getTabTooltip(partedFileList
                                        .get(i), end - begin + 1,
                                        tableField3.getItems().size());
                                tabTestTooltip3.setText(tabTooltip);
                                break;
                            case 4:
                                writeTestTable(tableField4, i, true,
                                        progressController, setProgress);

                                finishedRunLater = false;
                                Platform.runLater(() -> {
                                        tabTest4.setText(title);
                                        tableField4.refresh();
                                        finishedRunLater = true;
                                });                                

                                while (!finishedRunLater) {
                                    Thread.sleep(100);
                                }

                                tabTooltip = getTabTooltip(partedFileList
                                        .get(i), end - begin + 1,
                                        tableField4.getItems().size());
                                tabTestTooltip4.setText(tabTooltip);
                                break;
                            default:
                                // create context menu
                                if (i == 5) {
                                    int count = 1;
                                    int i2 = 0;  // second count variable "i"
                                    int to = 0;
                                    for (int j = i2; j < partedFileList.size(); j++) {
                                        to = j;
                                        if (j < 5) {
                                            count = count + partedFileList
                                                    .get(j).size();
                                        } else {
                                            break;
                                        }
                                    }
                                    String str = getTooltipMenuItems(i2, to, 
                                            partedFileList);
                                    // String title2 = 0 + ".." + 
                                    //         (count-1) + "\n" + str;
                                    String title2 = partedFileList
                                            .get(0).get(0).getFileCount() + 
                                            ".." + 
                                            partedFileList.get(4)
                                                    .get(partedFileList.get(4)
                                                            .size()-1).getFileCount() +
                                            "\n" + str;
                                    
                                    EventHandler p = new EventHandler<ActionEvent>() {
                                        public void handle(ActionEvent event) {
                                            // first action handler on tabs
                                            // previous and next

                                            Task taskForProgressBar;    // progress bar
                                            Task taskForBuildTables;    // parted tables

                                            activeRegion = i2;
                                            tabPane.getSelectionModel()
                                                    .select(tabTest0);
                                            if (activeRegion != lastActiveRegion) {
                                                processingBuildTables = true;
                                                lastActiveRegion = activeRegion;

                                                progressBar.setProgress(0);
                                                // show local progress bar
                                                progressBar.setVisible(true);

                                                taskForProgressBar = TaskForProgressBar();
                                                taskForBuildTables = TaskForBuildTables(false);

                                                // Initially, all tasks are marked 
                                                // as inactive
                                                taskField[0] = false;
                                                taskField[1] = false;
                                                taskField[2] = false;
                                                taskField[3] = false;
                                                taskField[4] = false;

                                                // Initially, all tab panes are 
                                                // marked empty
                                                titleField[0] = "";
                                                titleField[1] = "";
                                                titleField[2] = "";
                                                titleField[3] = "";
                                                titleField[4] = "";

                                                // The tasks are not finished
                                                tasksFinished = false;

                                                // start tasks as threads
                                                new Thread(taskForProgressBar).start();
                                                Thread thread = new Thread(taskForBuildTables);
                                                thread.start();
                                            }
                                        }
                                    };
                                    
                                    MenuItem e1 = new MenuItem(title2);
                                    e1.setOnAction(p);
                                    MenuItem e2 = new MenuItem(title2);
                                    e2.setOnAction(p);
                                    tabTestNextContextMenu.getItems().add(e1);
                                    tabTestPreviousContextMenu.getItems().add(e2);
                                }
                                if (i%5 == 0) {
                                    int count = begin;
                                    int i2;     // second count variable "i"
                                    if (i + 5 <= partedFileList.size()) {
                                        i2 = i;
                                    } else {
                                        i2 = partedFileList.size() - 5;
                                        for (int k = i2; k < i; k++) {
                                            // recalculate count
                                            count = count - partedFileList
                                                    .get(k).size();
                                        }
                                    }
                                    int to = 0;
                                    for (int j = i2; j < partedFileList.size(); j++) {
                                        to = j;
                                        if (j < i + 5) {
                                            count = count + partedFileList
                                                    .get(j).size();
                                        } else {
                                            break;
                                        }
                                    }
                                    String str = getTooltipMenuItems(i2, to, 
                                            partedFileList);

                                    // String title2 = begin + ".." + 
                                    //        (count-1) + "\n" + str;
                                    String title2 = partedFileList.get(i).get(0)
                                            .getFileCount() + 
                                            ".." + 
                                            partedFileList.get(i).get(partedFileList
                                                    .get(i).size()-1).getFileCount() +
                                            "\n" + str;
                                    
                                    EventHandler p = new EventHandler<ActionEvent>() {
                                        public void handle(ActionEvent event) {
                                            // first action handler on tabs
                                            // previous and next

                                            Task taskForProgressBar;    // progress bar
                                            Task taskForBuildTables;    // parted tables

                                            activeRegion = i2;
                                            tabPane.getSelectionModel()
                                                    .select(tabTest0);
                                            if (activeRegion != lastActiveRegion) {
                                                processingBuildTables = true;
                                                lastActiveRegion = activeRegion;

                                                progressBar.setProgress(0);
                                                // show local progress bar
                                                progressBar.setVisible(true);

                                                taskForProgressBar = TaskForProgressBar();
                                                taskForBuildTables = TaskForBuildTables(false);

                                                // Initially, all tasks are marked 
                                                // as inactive
                                                taskField[0] = false;
                                                taskField[1] = false;
                                                taskField[2] = false;
                                                taskField[3] = false;
                                                taskField[4] = false;

                                                // Initially, all tab panes are 
                                                // marked empty
                                                titleField[0] = "";
                                                titleField[1] = "";
                                                titleField[2] = "";
                                                titleField[3] = "";
                                                titleField[4] = "";

                                                // The tasks are not finished
                                                tasksFinished = false;

                                                // start tasks as threads
                                                new Thread(taskForProgressBar).start();
                                                Thread thread = new Thread(taskForBuildTables);
                                                thread.start();
                                            }
                                        }
                                    };
                                    
                                    MenuItem e1 = new MenuItem(title2);
                                    e1.setOnAction(p);
                                    MenuItem e2 = new MenuItem(title2);
                                    e2.setOnAction(p);
                                    tabTestNextContextMenu.getItems().add(e1);
                                    tabTestPreviousContextMenu.getItems().add(e2);
                                }
                                
                                // Do for all other parts to refresh 
                                // statistics. 
                                // write dummy test table
                                writeTestTableDummy(tableFieldEnd, i, true,
                                        progressController, setProgress);
                                break;
                        }
                    }
                    writeStatisticsTable();

                    // only for testing purposes!
                    //
                    // throw new Exception("Test");
                    // throw new NullPointerException("Test");

                } catch (NullPointerException ex) {
                    state = STATENULLPOINTEREXCEPTION;
                } catch (Exception ex) {
                    state = STATEEXCEPTION;
                }

                if (genState == Generate.State.ABORTED) {
                    return STATEABORT;
                } else {
                    return state;
                }
            }
        };

        // Event handler to wait for completion of worker task.
        //
        // If the worker task completes, then the PROGRESSSTAGE is hidden.
        task.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, 
                (WorkerStateEvent t) -> {
            // wait on completion of worker task
            Integer state;
            
            state = task.getValue();
            // ask for state of completion
            switch (state) {
                case STATEREADY:
                    // it's all right!
                    break;
                case STATEEXCEPTION:
                    // An exception occured!
                    // Show the approviate message!
                    myController.showStage(EXCEPTION_STAGE);
                    break;
                case STATEIOEXCEPTION:
                    // An exception occured!
                    // Show the approviate message!
                    myController.showStage(IO_EXCEPTION_STAGE);
                    break;
                case STATENULLPOINTEREXCEPTION:
                    // A null pointer exception occured!
                    // Show the approviate message!
                    myController.showStage(NULL_POINTER_EXCEPTION_STAGE);
                    break;
                case STATEJDOMEXCEPTION:
                    // A null pointer exception occured!
                    // Show the approviate message!
                    myController.showStage(JDOM_EXCEPTION_STAGE);
                    break;
            }
            
            // All tables are processed, though we can write the statistics 
            // for svn.
            writeSvnTable();
            
            if (state == STATEABORT) {
                // reset progress and label to intial settings
                progressController.setProgressIndicatorVisible(true);
                progressController.setProgressBarVisible(false);
                progressController.setLabelCosts(true);
                progressController.setLabelProcessing(false);
                // reset progess bar
                progressController.setProgress(0.0);
            } else {
                FXMLLoader loader;
                loader = myController.getLoader(MAIN_STAGE_1);
                lxrDifferenceTableController = loader.
                        <LxrDifferenceTableController>getController();
                
                watchXmlLockFiles();    // run lock directory watcher
                
                myController.showStage(MAIN_STAGE_2);
                lxrDifferenceTableController.setActiveStage(MAIN_STAGE_2);
                myController.hideStage(MAIN_STAGE_1);
                
                // wait, until MAIN_STAGE_2 is shown
                while (!myController.isShowingStage(MAIN_STAGE_2)) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        // do nothing
                    }
                }
                // hide progress indicator after MAIN_STAGE_2 is shown
                myController.hideStage(PROGRESS_STAGE);
                
                // reset progress
                progressController.setProgressIndicatorVisible(true);
                progressController.setProgressBarVisible(false);
                progressController.setLabelCosts(true);
                progressController.setLabelProcessing(false);
                progressController.setProgress(0.0);
                
                if (maximizedScreen) {
                    // the preferences says that the windows 
                    // has to be maximized
                    myController.getStage(MAIN_STAGE_2).setMaximized(true);
                } else {
                    maximizedScreen = false;
                }
                enableMaximizedScreen = true;
            }
            // the checkboxes are freed because all processes are finished
            if (areFilesDeleted) {
                // There are files deleted. Enable the relevant checkbox!
                dialogHiddenController.checkboxHideDeletedFiles.setDisable(false);
            }
            
            if (areFilesNew) {
                // There are new files. Enable the relevant checkbox!
                dialogHiddenController.checkboxHideNewFiles.setDisable(false);
            }

            if (areFilesDublicated) {
                // There are dublicated files. Enable the relevant checkbox!
                dialogHiddenController.checkboxHideDublicatedFiles.setDisable(false);
            }
            
            if (selectedTab.equals(SelectedTab.tabStatistics)) {
                // if statistics tabulator was the last selected tabulator, the 
                // switch to it.
                tabPane.getSelectionModel().select(tabStatistics);
            } else {
                // default: switch to tabulator 0
                tabPane.getSelectionModel().select(tabTest0);
            }
            buildTablesIsRunning = false;
        });

        buildTablesIsRunning = true;
        if (rebuildTables) {
            // do nothing
            progressController.setProgressIndicatorVisible(true);
            progressController.setProgressBarVisible(false);
            progressController.setLabelCosts(false);
            progressController.setLabelProcessing(false);
            
            myController.showStage(PROGRESS_STAGE);     // show progress stage
            progressController.setAbortState(false);
            
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
            }
            
            
        } else {
            this.progressController = progressController;
            this.setProgress = setProgress;

            this.xmlFile = xmlFile;
            if (xmlFile != null) {
                String title = ResourceBundle.getBundle(
                        "lxrdifferencetable/Bundle", locale).
                        getString("programName")  + "  -  " + 
                        xmlFile.toString();
                myController.getStage(MAIN_STAGE_2).setTitle(title);
            }

            enableMaximizedScreen = false;
            svnAvailable = configEntry.getSvn().equals("true");
        }

        FXMLLoader loader;
        loader = myController.getLoader(HIDDEN_STAGE);
        dialogHiddenController = loader.
                <DialogHiddenController>getController();
        
        dialogHiddenController.setProgressController(progressController);
        dialogHiddenController.setProgress(setProgress);
        
        File lockDir = new File(workingDir + File.separator + ".lock");
        if (lockDir.isDirectory()) {
            menuItemSave.setVisible(false);
        } else {
            menuItemSave.setVisible(true);
        }
        
        pageTable.clear();
        progressBar.setVisible(false);
        Thread th = new Thread(task);           // define new task
        th.setDaemon(true);                     // set task to daemon state
        th.setName("BuildTables");
        th.start();                             // start task as daemon
        
        if (rebuildTables) {
            // do nothing
        } else {
            // resize listener
            myController.getStage(MAIN_STAGE_2).getScene()
                    .widthProperty()
                    .addListener((ObservableValue<? extends Number> observableValue, 
                            Number oldSceneWidth, Number newSceneWidth) -> {

                        if (enableMaximizedScreen) {
                            // Do after initialization, only.
                            if (myController.getStage(MAIN_STAGE_2).isMaximized()) {
                                // user has clicked the maximize button!
                                maximizedScreen = true;
                            } else {
                                maximizedScreen = false;
                            }
                        }
                        
                        columnWithSvnUsers.setPrefWidth(80);
                        columnWithSvnCommits.setPrefWidth(200);
                        columnWithStatisticsTableData.setPrefWidth(80);
                        columnWithStatisticsTableCount.setPrefWidth(80);
                        columnWithStatisticsReviewData.setPrefWidth(80);
                        columnWithStatisticsReviewCount.setPrefWidth(80);
                        columnWithStatisticsTestData.setPrefWidth(80);
                        columnWithStatisticsTestCount.setPrefWidth(80);

                        double width = myController.getStage(MAIN_STAGE_2).getScene()
                                .getWidth();
                        if (svnAvailable) {
                            if (width < 400) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(100);
                                tableField1.getColumns().get(1).setPrefWidth(100);
                                tableField2.getColumns().get(1).setPrefWidth(100);
                                tableField3.getColumns().get(1).setPrefWidth(100);
                                tableField4.getColumns().get(1).setPrefWidth(100);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(50);
                                tableField1.getColumns().get(2).setPrefWidth(50);
                                tableField2.getColumns().get(2).setPrefWidth(50);
                                tableField3.getColumns().get(2).setPrefWidth(50);
                                tableField4.getColumns().get(2).setPrefWidth(50);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(100);
                                tableField1.getColumns().get(4).setPrefWidth(100);
                                tableField2.getColumns().get(4).setPrefWidth(100);
                                tableField3.getColumns().get(4).setPrefWidth(100);
                                tableField4.getColumns().get(4).setPrefWidth(100);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(40);
                                tableField1.getColumns().get(5).setPrefWidth(40);
                                tableField2.getColumns().get(5).setPrefWidth(40);
                                tableField3.getColumns().get(5).setPrefWidth(40);
                                tableField4.getColumns().get(5).setPrefWidth(40);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(30);
                                tableField1.getColumns().get(6).setPrefWidth(30);
                                tableField2.getColumns().get(6).setPrefWidth(30);
                                tableField3.getColumns().get(6).setPrefWidth(30);
                                tableField4.getColumns().get(6).setPrefWidth(30);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(70);
                                tableField1.getColumns().get(7).setPrefWidth(70);
                                tableField2.getColumns().get(7).setPrefWidth(70);
                                tableField3.getColumns().get(7).setPrefWidth(70);
                                tableField4.getColumns().get(7).setPrefWidth(70);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(80);
                                tableField1.getColumns().get(8).setPrefWidth(80);
                                tableField2.getColumns().get(8).setPrefWidth(80);
                                tableField3.getColumns().get(8).setPrefWidth(80);
                                tableField4.getColumns().get(8).setPrefWidth(80);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(70);
                                tableField1.getColumns().get(9).setPrefWidth(70);
                                tableField2.getColumns().get(9).setPrefWidth(70);
                                tableField3.getColumns().get(9).setPrefWidth(70);
                                tableField4.getColumns().get(9).setPrefWidth(70);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(80);
                                tableField1.getColumns().get(10).setPrefWidth(80);
                                tableField2.getColumns().get(10).setPrefWidth(80);
                                tableField3.getColumns().get(10).setPrefWidth(80);
                                tableField4.getColumns().get(10).setPrefWidth(80);
                            } else if (width < 500) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(100);
                                tableField1.getColumns().get(1).setPrefWidth(100);
                                tableField2.getColumns().get(1).setPrefWidth(100);
                                tableField3.getColumns().get(1).setPrefWidth(100);
                                tableField4.getColumns().get(1).setPrefWidth(100);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(60);
                                tableField1.getColumns().get(2).setPrefWidth(60);
                                tableField2.getColumns().get(2).setPrefWidth(60);
                                tableField3.getColumns().get(2).setPrefWidth(60);
                                tableField4.getColumns().get(2).setPrefWidth(60);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(100);
                                tableField1.getColumns().get(4).setPrefWidth(100);
                                tableField2.getColumns().get(4).setPrefWidth(100);
                                tableField3.getColumns().get(4).setPrefWidth(100);
                                tableField4.getColumns().get(4).setPrefWidth(100);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(70);
                                tableField1.getColumns().get(6).setPrefWidth(70);
                                tableField2.getColumns().get(6).setPrefWidth(70);
                                tableField3.getColumns().get(6).setPrefWidth(70);
                                tableField4.getColumns().get(6).setPrefWidth(70);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(70);
                                tableField1.getColumns().get(7).setPrefWidth(70);
                                tableField2.getColumns().get(7).setPrefWidth(70);
                                tableField3.getColumns().get(7).setPrefWidth(70);
                                tableField4.getColumns().get(7).setPrefWidth(70);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(80);
                                tableField1.getColumns().get(8).setPrefWidth(80);
                                tableField2.getColumns().get(8).setPrefWidth(80);
                                tableField3.getColumns().get(8).setPrefWidth(80);
                                tableField4.getColumns().get(8).setPrefWidth(80);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(70);
                                tableField1.getColumns().get(9).setPrefWidth(70);
                                tableField2.getColumns().get(9).setPrefWidth(70);
                                tableField3.getColumns().get(9).setPrefWidth(70);
                                tableField4.getColumns().get(9).setPrefWidth(70);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(80);
                                tableField1.getColumns().get(10).setPrefWidth(80);
                                tableField2.getColumns().get(10).setPrefWidth(80);
                                tableField3.getColumns().get(10).setPrefWidth(80);
                                tableField4.getColumns().get(10).setPrefWidth(80);
                            } else if (width < 700) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(100);
                                tableField1.getColumns().get(1).setPrefWidth(100);
                                tableField2.getColumns().get(1).setPrefWidth(100);
                                tableField3.getColumns().get(1).setPrefWidth(100);
                                tableField4.getColumns().get(1).setPrefWidth(100);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(60);
                                tableField1.getColumns().get(2).setPrefWidth(60);
                                tableField2.getColumns().get(2).setPrefWidth(60);
                                tableField3.getColumns().get(2).setPrefWidth(60);
                                tableField4.getColumns().get(2).setPrefWidth(60);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(150);
                                tableField1.getColumns().get(4).setPrefWidth(150);
                                tableField2.getColumns().get(4).setPrefWidth(150);
                                tableField3.getColumns().get(4).setPrefWidth(150);
                                tableField4.getColumns().get(4).setPrefWidth(150);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(70);
                                tableField1.getColumns().get(6).setPrefWidth(70);
                                tableField2.getColumns().get(6).setPrefWidth(70);
                                tableField3.getColumns().get(6).setPrefWidth(70);
                                tableField4.getColumns().get(6).setPrefWidth(70);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(70);
                                tableField1.getColumns().get(7).setPrefWidth(70);
                                tableField2.getColumns().get(7).setPrefWidth(70);
                                tableField3.getColumns().get(7).setPrefWidth(70);
                                tableField4.getColumns().get(7).setPrefWidth(70);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(80);
                                tableField1.getColumns().get(8).setPrefWidth(80);
                                tableField2.getColumns().get(8).setPrefWidth(80);
                                tableField3.getColumns().get(8).setPrefWidth(80);
                                tableField4.getColumns().get(8).setPrefWidth(80);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(70);
                                tableField1.getColumns().get(9).setPrefWidth(70);
                                tableField2.getColumns().get(9).setPrefWidth(70);
                                tableField3.getColumns().get(9).setPrefWidth(70);
                                tableField4.getColumns().get(9).setPrefWidth(70);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(80);
                                tableField1.getColumns().get(10).setPrefWidth(80);
                                tableField2.getColumns().get(10).setPrefWidth(80);
                                tableField3.getColumns().get(10).setPrefWidth(80);
                                tableField4.getColumns().get(10).setPrefWidth(80);
                            } else if (width < 900) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(100);
                                tableField1.getColumns().get(1).setPrefWidth(100);
                                tableField2.getColumns().get(1).setPrefWidth(100);
                                tableField3.getColumns().get(1).setPrefWidth(100);
                                tableField4.getColumns().get(1).setPrefWidth(100);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(60);
                                tableField1.getColumns().get(2).setPrefWidth(60);
                                tableField2.getColumns().get(2).setPrefWidth(60);
                                tableField3.getColumns().get(2).setPrefWidth(60);
                                tableField4.getColumns().get(2).setPrefWidth(60);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(150);
                                tableField1.getColumns().get(4).setPrefWidth(150);
                                tableField2.getColumns().get(4).setPrefWidth(150);
                                tableField3.getColumns().get(4).setPrefWidth(150);
                                tableField4.getColumns().get(4).setPrefWidth(150);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(100);
                                tableField1.getColumns().get(6).setPrefWidth(100);
                                tableField2.getColumns().get(6).setPrefWidth(100);
                                tableField3.getColumns().get(6).setPrefWidth(100);
                                tableField4.getColumns().get(6).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(80);
                                tableField1.getColumns().get(7).setPrefWidth(80);
                                tableField2.getColumns().get(7).setPrefWidth(80);
                                tableField3.getColumns().get(7).setPrefWidth(80);
                                tableField4.getColumns().get(7).setPrefWidth(80);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(100);
                                tableField1.getColumns().get(8).setPrefWidth(100);
                                tableField2.getColumns().get(8).setPrefWidth(100);
                                tableField3.getColumns().get(8).setPrefWidth(100);
                                tableField4.getColumns().get(8).setPrefWidth(100);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(80);
                                tableField1.getColumns().get(9).setPrefWidth(80);
                                tableField2.getColumns().get(9).setPrefWidth(80);
                                tableField3.getColumns().get(9).setPrefWidth(80);
                                tableField4.getColumns().get(9).setPrefWidth(80);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(100);
                                tableField1.getColumns().get(10).setPrefWidth(100);
                                tableField2.getColumns().get(10).setPrefWidth(100);
                                tableField3.getColumns().get(10).setPrefWidth(100);
                                tableField4.getColumns().get(10).setPrefWidth(100);
                            } else if (width < 1100) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(200);
                                tableField1.getColumns().get(1).setPrefWidth(200);
                                tableField2.getColumns().get(1).setPrefWidth(200);
                                tableField3.getColumns().get(1).setPrefWidth(200);
                                tableField4.getColumns().get(1).setPrefWidth(200);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(60);
                                tableField1.getColumns().get(2).setPrefWidth(60);
                                tableField2.getColumns().get(2).setPrefWidth(60);
                                tableField3.getColumns().get(2).setPrefWidth(60);
                                tableField4.getColumns().get(2).setPrefWidth(60);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(150);
                                tableField1.getColumns().get(4).setPrefWidth(150);
                                tableField2.getColumns().get(4).setPrefWidth(150);
                                tableField3.getColumns().get(4).setPrefWidth(150);
                                tableField4.getColumns().get(4).setPrefWidth(150);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(100);
                                tableField1.getColumns().get(6).setPrefWidth(100);
                                tableField2.getColumns().get(6).setPrefWidth(100);
                                tableField3.getColumns().get(6).setPrefWidth(100);
                                tableField4.getColumns().get(6).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(80);
                                tableField1.getColumns().get(7).setPrefWidth(80);
                                tableField2.getColumns().get(7).setPrefWidth(80);
                                tableField3.getColumns().get(7).setPrefWidth(80);
                                tableField4.getColumns().get(7).setPrefWidth(80);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(100);
                                tableField1.getColumns().get(8).setPrefWidth(100);
                                tableField2.getColumns().get(8).setPrefWidth(100);
                                tableField3.getColumns().get(8).setPrefWidth(100);
                                tableField4.getColumns().get(8).setPrefWidth(100);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(80);
                                tableField1.getColumns().get(9).setPrefWidth(80);
                                tableField2.getColumns().get(9).setPrefWidth(80);
                                tableField3.getColumns().get(9).setPrefWidth(80);
                                tableField4.getColumns().get(9).setPrefWidth(80);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(100);
                                tableField1.getColumns().get(10).setPrefWidth(100);
                                tableField2.getColumns().get(10).setPrefWidth(100);
                                tableField3.getColumns().get(10).setPrefWidth(100);
                                tableField4.getColumns().get(10).setPrefWidth(100);
                            } else if (width < 1300) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(200);
                                tableField1.getColumns().get(1).setPrefWidth(200);
                                tableField2.getColumns().get(1).setPrefWidth(200);
                                tableField3.getColumns().get(1).setPrefWidth(200);
                                tableField4.getColumns().get(1).setPrefWidth(200);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(60);
                                tableField1.getColumns().get(2).setPrefWidth(60);
                                tableField2.getColumns().get(2).setPrefWidth(60);
                                tableField3.getColumns().get(2).setPrefWidth(60);
                                tableField4.getColumns().get(2).setPrefWidth(60);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(200);
                                tableField1.getColumns().get(4).setPrefWidth(200);
                                tableField2.getColumns().get(4).setPrefWidth(200);
                                tableField3.getColumns().get(4).setPrefWidth(200);
                                tableField4.getColumns().get(4).setPrefWidth(200);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(150);
                                tableField1.getColumns().get(6).setPrefWidth(150);
                                tableField2.getColumns().get(6).setPrefWidth(150);
                                tableField3.getColumns().get(6).setPrefWidth(150);
                                tableField4.getColumns().get(6).setPrefWidth(150);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(100);
                                tableField1.getColumns().get(7).setPrefWidth(100);
                                tableField2.getColumns().get(7).setPrefWidth(100);
                                tableField3.getColumns().get(7).setPrefWidth(100);
                                tableField4.getColumns().get(7).setPrefWidth(100);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(100);
                                tableField1.getColumns().get(8).setPrefWidth(100);
                                tableField2.getColumns().get(8).setPrefWidth(100);
                                tableField3.getColumns().get(8).setPrefWidth(100);
                                tableField4.getColumns().get(8).setPrefWidth(100);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(100);
                                tableField1.getColumns().get(9).setPrefWidth(100);
                                tableField2.getColumns().get(9).setPrefWidth(100);
                                tableField3.getColumns().get(9).setPrefWidth(100);
                                tableField4.getColumns().get(9).setPrefWidth(100);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(100);
                                tableField1.getColumns().get(10).setPrefWidth(100);
                                tableField2.getColumns().get(10).setPrefWidth(100);
                                tableField3.getColumns().get(10).setPrefWidth(100);
                                tableField4.getColumns().get(10).setPrefWidth(100);
                            } else if (width < 1500) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(300);
                                tableField1.getColumns().get(1).setPrefWidth(300);
                                tableField2.getColumns().get(1).setPrefWidth(300);
                                tableField3.getColumns().get(1).setPrefWidth(300);
                                tableField4.getColumns().get(1).setPrefWidth(300);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(60);
                                tableField1.getColumns().get(2).setPrefWidth(60);
                                tableField2.getColumns().get(2).setPrefWidth(60);
                                tableField3.getColumns().get(2).setPrefWidth(60);
                                tableField4.getColumns().get(2).setPrefWidth(60);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(300);
                                tableField1.getColumns().get(4).setPrefWidth(300);
                                tableField2.getColumns().get(4).setPrefWidth(300);
                                tableField3.getColumns().get(4).setPrefWidth(300);
                                tableField4.getColumns().get(4).setPrefWidth(300);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(150);
                                tableField1.getColumns().get(6).setPrefWidth(150);
                                tableField2.getColumns().get(6).setPrefWidth(150);
                                tableField3.getColumns().get(6).setPrefWidth(150);
                                tableField4.getColumns().get(6).setPrefWidth(150);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(100);
                                tableField1.getColumns().get(7).setPrefWidth(100);
                                tableField2.getColumns().get(7).setPrefWidth(100);
                                tableField3.getColumns().get(7).setPrefWidth(100);
                                tableField4.getColumns().get(7).setPrefWidth(100);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(100);
                                tableField1.getColumns().get(8).setPrefWidth(100);
                                tableField2.getColumns().get(8).setPrefWidth(100);
                                tableField3.getColumns().get(8).setPrefWidth(100);
                                tableField4.getColumns().get(8).setPrefWidth(100);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(100);
                                tableField1.getColumns().get(9).setPrefWidth(100);
                                tableField2.getColumns().get(9).setPrefWidth(100);
                                tableField3.getColumns().get(9).setPrefWidth(100);
                                tableField4.getColumns().get(9).setPrefWidth(100);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(100);
                                tableField1.getColumns().get(10).setPrefWidth(100);
                                tableField2.getColumns().get(10).setPrefWidth(100);
                                tableField3.getColumns().get(10).setPrefWidth(100);
                                tableField4.getColumns().get(10).setPrefWidth(100);
                            } else if (width < 1700) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(300);
                                tableField1.getColumns().get(1).setPrefWidth(300);
                                tableField2.getColumns().get(1).setPrefWidth(300);
                                tableField3.getColumns().get(1).setPrefWidth(300);
                                tableField4.getColumns().get(1).setPrefWidth(300);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(80);
                                tableField1.getColumns().get(2).setPrefWidth(80);
                                tableField2.getColumns().get(2).setPrefWidth(80);
                                tableField3.getColumns().get(2).setPrefWidth(80);
                                tableField4.getColumns().get(2).setPrefWidth(80);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(300);
                                tableField1.getColumns().get(4).setPrefWidth(300);
                                tableField2.getColumns().get(4).setPrefWidth(300);
                                tableField3.getColumns().get(4).setPrefWidth(300);
                                tableField4.getColumns().get(4).setPrefWidth(300);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(150);
                                tableField1.getColumns().get(6).setPrefWidth(150);
                                tableField2.getColumns().get(6).setPrefWidth(150);
                                tableField3.getColumns().get(6).setPrefWidth(150);
                                tableField4.getColumns().get(6).setPrefWidth(150);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(200);
                                tableField1.getColumns().get(7).setPrefWidth(200);
                                tableField2.getColumns().get(7).setPrefWidth(200);
                                tableField3.getColumns().get(7).setPrefWidth(200);
                                tableField4.getColumns().get(7).setPrefWidth(200);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(100);
                                tableField1.getColumns().get(8).setPrefWidth(100);
                                tableField2.getColumns().get(8).setPrefWidth(100);
                                tableField3.getColumns().get(8).setPrefWidth(100);
                                tableField4.getColumns().get(8).setPrefWidth(100);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(200);
                                tableField1.getColumns().get(9).setPrefWidth(200);
                                tableField2.getColumns().get(9).setPrefWidth(200);
                                tableField3.getColumns().get(9).setPrefWidth(200);
                                tableField4.getColumns().get(9).setPrefWidth(200);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(100);
                                tableField1.getColumns().get(10).setPrefWidth(100);
                                tableField2.getColumns().get(10).setPrefWidth(100);
                                tableField3.getColumns().get(10).setPrefWidth(100);
                                tableField4.getColumns().get(10).setPrefWidth(100);
                            } else if (width < 1900) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(200);
                                tableField1.getColumns().get(1).setPrefWidth(200);
                                tableField2.getColumns().get(1).setPrefWidth(200);
                                tableField3.getColumns().get(1).setPrefWidth(200);
                                tableField4.getColumns().get(1).setPrefWidth(200);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(80);
                                tableField1.getColumns().get(2).setPrefWidth(80);
                                tableField2.getColumns().get(2).setPrefWidth(80);
                                tableField3.getColumns().get(2).setPrefWidth(80);
                                tableField4.getColumns().get(2).setPrefWidth(80);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(300);
                                tableField1.getColumns().get(4).setPrefWidth(300);
                                tableField2.getColumns().get(4).setPrefWidth(300);
                                tableField3.getColumns().get(4).setPrefWidth(300);
                                tableField4.getColumns().get(4).setPrefWidth(300);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(200);
                                tableField1.getColumns().get(6).setPrefWidth(200);
                                tableField2.getColumns().get(6).setPrefWidth(200);
                                tableField3.getColumns().get(6).setPrefWidth(200);
                                tableField4.getColumns().get(6).setPrefWidth(200);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(180);
                                tableField1.getColumns().get(7).setPrefWidth(180);
                                tableField2.getColumns().get(7).setPrefWidth(180);
                                tableField3.getColumns().get(7).setPrefWidth(180);
                                tableField4.getColumns().get(7).setPrefWidth(180);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(160);
                                tableField1.getColumns().get(8).setPrefWidth(160);
                                tableField2.getColumns().get(8).setPrefWidth(160);
                                tableField3.getColumns().get(8).setPrefWidth(160);
                                tableField4.getColumns().get(8).setPrefWidth(160);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(180);
                                tableField1.getColumns().get(9).setPrefWidth(180);
                                tableField2.getColumns().get(9).setPrefWidth(180);
                                tableField3.getColumns().get(9).setPrefWidth(180);
                                tableField4.getColumns().get(9).setPrefWidth(180);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(160);
                                tableField1.getColumns().get(10).setPrefWidth(160);
                                tableField2.getColumns().get(10).setPrefWidth(160);
                                tableField3.getColumns().get(10).setPrefWidth(160);
                                tableField4.getColumns().get(10).setPrefWidth(160);
                            } else if (width < 2100) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(300);
                                tableField1.getColumns().get(1).setPrefWidth(300);
                                tableField2.getColumns().get(1).setPrefWidth(300);
                                tableField3.getColumns().get(1).setPrefWidth(300);
                                tableField4.getColumns().get(1).setPrefWidth(300);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(80);
                                tableField1.getColumns().get(2).setPrefWidth(80);
                                tableField2.getColumns().get(2).setPrefWidth(80);
                                tableField3.getColumns().get(2).setPrefWidth(80);
                                tableField4.getColumns().get(2).setPrefWidth(80);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(400);
                                tableField1.getColumns().get(4).setPrefWidth(400);
                                tableField2.getColumns().get(4).setPrefWidth(400);
                                tableField3.getColumns().get(4).setPrefWidth(400);
                                tableField4.getColumns().get(4).setPrefWidth(400);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(200);
                                tableField1.getColumns().get(6).setPrefWidth(200);
                                tableField2.getColumns().get(6).setPrefWidth(200);
                                tableField3.getColumns().get(6).setPrefWidth(200);
                                tableField4.getColumns().get(6).setPrefWidth(200);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(180);
                                tableField1.getColumns().get(7).setPrefWidth(180);
                                tableField2.getColumns().get(7).setPrefWidth(180);
                                tableField3.getColumns().get(7).setPrefWidth(180);
                                tableField4.getColumns().get(7).setPrefWidth(180);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(160);
                                tableField1.getColumns().get(8).setPrefWidth(160);
                                tableField2.getColumns().get(8).setPrefWidth(160);
                                tableField3.getColumns().get(8).setPrefWidth(160);
                                tableField4.getColumns().get(8).setPrefWidth(160);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(180);
                                tableField1.getColumns().get(9).setPrefWidth(180);
                                tableField2.getColumns().get(9).setPrefWidth(180);
                                tableField3.getColumns().get(9).setPrefWidth(180);
                                tableField4.getColumns().get(9).setPrefWidth(180);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(160);
                                tableField1.getColumns().get(10).setPrefWidth(160);
                                tableField2.getColumns().get(10).setPrefWidth(160);
                                tableField3.getColumns().get(10).setPrefWidth(160);
                                tableField4.getColumns().get(10).setPrefWidth(160);
                            } else if (width < 2300) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(400);
                                tableField1.getColumns().get(1).setPrefWidth(400);
                                tableField2.getColumns().get(1).setPrefWidth(400);
                                tableField3.getColumns().get(1).setPrefWidth(400);
                                tableField4.getColumns().get(1).setPrefWidth(400);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(80);
                                tableField1.getColumns().get(2).setPrefWidth(80);
                                tableField2.getColumns().get(2).setPrefWidth(80);
                                tableField3.getColumns().get(2).setPrefWidth(80);
                                tableField4.getColumns().get(2).setPrefWidth(80);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(500);
                                tableField1.getColumns().get(4).setPrefWidth(500);
                                tableField2.getColumns().get(4).setPrefWidth(500);
                                tableField3.getColumns().get(4).setPrefWidth(500);
                                tableField4.getColumns().get(4).setPrefWidth(500);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(200);
                                tableField1.getColumns().get(6).setPrefWidth(200);
                                tableField2.getColumns().get(6).setPrefWidth(200);
                                tableField3.getColumns().get(6).setPrefWidth(200);
                                tableField4.getColumns().get(6).setPrefWidth(200);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(180);
                                tableField1.getColumns().get(7).setPrefWidth(180);
                                tableField2.getColumns().get(7).setPrefWidth(180);
                                tableField3.getColumns().get(7).setPrefWidth(180);
                                tableField4.getColumns().get(7).setPrefWidth(180);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(160);
                                tableField1.getColumns().get(8).setPrefWidth(160);
                                tableField2.getColumns().get(8).setPrefWidth(160);
                                tableField3.getColumns().get(8).setPrefWidth(160);
                                tableField4.getColumns().get(8).setPrefWidth(160);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(180);
                                tableField1.getColumns().get(9).setPrefWidth(180);
                                tableField2.getColumns().get(9).setPrefWidth(180);
                                tableField3.getColumns().get(9).setPrefWidth(180);
                                tableField4.getColumns().get(9).setPrefWidth(180);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(160);
                                tableField1.getColumns().get(10).setPrefWidth(160);
                                tableField2.getColumns().get(10).setPrefWidth(160);
                                tableField3.getColumns().get(10).setPrefWidth(160);
                                tableField4.getColumns().get(10).setPrefWidth(160);
                            } else {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(500);
                                tableField1.getColumns().get(1).setPrefWidth(500);
                                tableField2.getColumns().get(1).setPrefWidth(500);
                                tableField3.getColumns().get(1).setPrefWidth(500);
                                tableField4.getColumns().get(1).setPrefWidth(500);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(80);
                                tableField1.getColumns().get(2).setPrefWidth(80);
                                tableField2.getColumns().get(2).setPrefWidth(80);
                                tableField3.getColumns().get(2).setPrefWidth(80);
                                tableField4.getColumns().get(2).setPrefWidth(80);
                                // SVN Revision NUmber
                                tableField.getColumns().get(3).setPrefWidth(90);
                                tableField1.getColumns().get(3).setPrefWidth(90);
                                tableField2.getColumns().get(3).setPrefWidth(90);
                                tableField3.getColumns().get(3).setPrefWidth(90);
                                tableField4.getColumns().get(3).setPrefWidth(90);
                                // SVN-Description
                                tableField.getColumns().get(4).setPrefWidth(500);
                                tableField1.getColumns().get(4).setPrefWidth(500);
                                tableField2.getColumns().get(4).setPrefWidth(500);
                                tableField3.getColumns().get(4).setPrefWidth(500);
                                tableField4.getColumns().get(4).setPrefWidth(500);
                                // SVN Author
                                tableField.getColumns().get(5).setPrefWidth(90);
                                tableField1.getColumns().get(5).setPrefWidth(90);
                                tableField2.getColumns().get(5).setPrefWidth(90);
                                tableField3.getColumns().get(5).setPrefWidth(90);
                                tableField4.getColumns().get(5).setPrefWidth(90);
                                // SVN-Date
                                tableField.getColumns().get(6).setPrefWidth(200);
                                tableField1.getColumns().get(6).setPrefWidth(200);
                                tableField2.getColumns().get(6).setPrefWidth(200);
                                tableField3.getColumns().get(6).setPrefWidth(200);
                                tableField4.getColumns().get(6).setPrefWidth(200);
                                // Review State
                                tableField.getColumns().get(7).setPrefWidth(200);
                                tableField1.getColumns().get(7).setPrefWidth(200);
                                tableField2.getColumns().get(7).setPrefWidth(200);
                                tableField3.getColumns().get(7).setPrefWidth(200);
                                tableField4.getColumns().get(7).setPrefWidth(200);
                                // Review Short Description
                                tableField.getColumns().get(8).setPrefWidth(180);
                                tableField1.getColumns().get(8).setPrefWidth(180);
                                tableField2.getColumns().get(8).setPrefWidth(180);
                                tableField3.getColumns().get(8).setPrefWidth(180);
                                tableField4.getColumns().get(8).setPrefWidth(180);
                                // Test State
                                tableField.getColumns().get(9).setPrefWidth(200);
                                tableField1.getColumns().get(9).setPrefWidth(200);
                                tableField2.getColumns().get(9).setPrefWidth(200);
                                tableField3.getColumns().get(9).setPrefWidth(200);
                                tableField4.getColumns().get(9).setPrefWidth(200);
                                // Test Short Description
                                tableField.getColumns().get(10).setPrefWidth(180);
                                tableField1.getColumns().get(10).setPrefWidth(180);
                                tableField2.getColumns().get(10).setPrefWidth(180);
                                tableField3.getColumns().get(10).setPrefWidth(180);
                                tableField4.getColumns().get(10).setPrefWidth(180);
                            }
                        } else {    // svn not available!
                            if (width < 400) {
                                // ID
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(100);
                                tableField1.getColumns().get(1).setPrefWidth(100);
                                tableField2.getColumns().get(1).setPrefWidth(100);
                                tableField3.getColumns().get(1).setPrefWidth(100);
                                tableField4.getColumns().get(1).setPrefWidth(100);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(50);
                                tableField1.getColumns().get(2).setPrefWidth(50);
                                tableField2.getColumns().get(2).setPrefWidth(50);
                                tableField3.getColumns().get(2).setPrefWidth(50);
                                tableField4.getColumns().get(2).setPrefWidth(50);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(70);
                                tableField1.getColumns().get(3).setPrefWidth(70);
                                tableField2.getColumns().get(3).setPrefWidth(70);
                                tableField3.getColumns().get(3).setPrefWidth(70);
                                tableField4.getColumns().get(3).setPrefWidth(70);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(40);
                                tableField1.getColumns().get(4).setPrefWidth(40);
                                tableField2.getColumns().get(4).setPrefWidth(40);
                                tableField3.getColumns().get(4).setPrefWidth(40);
                                tableField4.getColumns().get(4).setPrefWidth(40);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(70);
                                tableField1.getColumns().get(5).setPrefWidth(70);
                                tableField2.getColumns().get(5).setPrefWidth(70);
                                tableField3.getColumns().get(5).setPrefWidth(70);
                                tableField4.getColumns().get(5).setPrefWidth(70);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(40);
                                tableField1.getColumns().get(6).setPrefWidth(40);
                                tableField2.getColumns().get(6).setPrefWidth(40);
                                tableField3.getColumns().get(6).setPrefWidth(40);
                                tableField4.getColumns().get(6).setPrefWidth(40);
                            } else if (width < 500) {
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(100);
                                tableField1.getColumns().get(1).setPrefWidth(100);
                                tableField2.getColumns().get(1).setPrefWidth(100);
                                tableField3.getColumns().get(1).setPrefWidth(100);
                                tableField4.getColumns().get(1).setPrefWidth(100);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(50);
                                tableField1.getColumns().get(2).setPrefWidth(50);
                                tableField2.getColumns().get(2).setPrefWidth(50);
                                tableField3.getColumns().get(2).setPrefWidth(50);
                                tableField4.getColumns().get(2).setPrefWidth(50);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(70);
                                tableField1.getColumns().get(3).setPrefWidth(70);
                                tableField2.getColumns().get(3).setPrefWidth(70);
                                tableField3.getColumns().get(3).setPrefWidth(70);
                                tableField4.getColumns().get(3).setPrefWidth(70);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(80);
                                tableField1.getColumns().get(4).setPrefWidth(80);
                                tableField2.getColumns().get(4).setPrefWidth(80);
                                tableField3.getColumns().get(4).setPrefWidth(80);
                                tableField4.getColumns().get(4).setPrefWidth(80);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(70);
                                tableField1.getColumns().get(5).setPrefWidth(70);
                                tableField2.getColumns().get(5).setPrefWidth(70);
                                tableField3.getColumns().get(5).setPrefWidth(70);
                                tableField4.getColumns().get(5).setPrefWidth(70);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(80);
                                tableField1.getColumns().get(6).setPrefWidth(80);
                                tableField2.getColumns().get(6).setPrefWidth(80);
                                tableField3.getColumns().get(6).setPrefWidth(80);
                                tableField4.getColumns().get(6).setPrefWidth(80);
                            } else if (width < 700) {
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(150);
                                tableField1.getColumns().get(1).setPrefWidth(150);
                                tableField2.getColumns().get(1).setPrefWidth(150);
                                tableField3.getColumns().get(1).setPrefWidth(150);
                                tableField4.getColumns().get(1).setPrefWidth(150);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(50);
                                tableField1.getColumns().get(2).setPrefWidth(50);
                                tableField2.getColumns().get(2).setPrefWidth(50);
                                tableField3.getColumns().get(2).setPrefWidth(50);
                                tableField4.getColumns().get(2).setPrefWidth(50);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(100);
                                tableField1.getColumns().get(3).setPrefWidth(100);
                                tableField2.getColumns().get(3).setPrefWidth(100);
                                tableField3.getColumns().get(3).setPrefWidth(100);
                                tableField4.getColumns().get(3).setPrefWidth(100);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(80);
                                tableField1.getColumns().get(4).setPrefWidth(80);
                                tableField2.getColumns().get(4).setPrefWidth(80);
                                tableField3.getColumns().get(4).setPrefWidth(80);
                                tableField4.getColumns().get(4).setPrefWidth(80);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(100);
                                tableField1.getColumns().get(5).setPrefWidth(100);
                                tableField2.getColumns().get(5).setPrefWidth(100);
                                tableField3.getColumns().get(5).setPrefWidth(100);
                                tableField4.getColumns().get(5).setPrefWidth(100);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(80);
                                tableField1.getColumns().get(6).setPrefWidth(80);
                                tableField2.getColumns().get(6).setPrefWidth(80);
                                tableField3.getColumns().get(6).setPrefWidth(80);
                                tableField4.getColumns().get(6).setPrefWidth(80);
                            } else if (width < 900) {
                                tableField.getColumns().get(0).setPrefWidth(40);
                                tableField1.getColumns().get(0).setPrefWidth(40);
                                tableField2.getColumns().get(0).setPrefWidth(40);
                                tableField3.getColumns().get(0).setPrefWidth(40);
                                tableField4.getColumns().get(0).setPrefWidth(40);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(200);
                                tableField1.getColumns().get(1).setPrefWidth(200);
                                tableField2.getColumns().get(1).setPrefWidth(200);
                                tableField3.getColumns().get(1).setPrefWidth(200);
                                tableField4.getColumns().get(1).setPrefWidth(200);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(70);
                                tableField1.getColumns().get(2).setPrefWidth(70);
                                tableField2.getColumns().get(2).setPrefWidth(70);
                                tableField3.getColumns().get(2).setPrefWidth(70);
                                tableField4.getColumns().get(2).setPrefWidth(70);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(100);
                                tableField1.getColumns().get(3).setPrefWidth(100);
                                tableField2.getColumns().get(3).setPrefWidth(100);
                                tableField3.getColumns().get(3).setPrefWidth(100);
                                tableField4.getColumns().get(3).setPrefWidth(100);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(100);
                                tableField1.getColumns().get(4).setPrefWidth(100);
                                tableField2.getColumns().get(4).setPrefWidth(100);
                                tableField3.getColumns().get(4).setPrefWidth(100);
                                tableField4.getColumns().get(4).setPrefWidth(100);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(100);
                                tableField1.getColumns().get(5).setPrefWidth(100);
                                tableField2.getColumns().get(5).setPrefWidth(100);
                                tableField3.getColumns().get(5).setPrefWidth(100);
                                tableField4.getColumns().get(5).setPrefWidth(100);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(100);
                                tableField1.getColumns().get(6).setPrefWidth(100);
                                tableField2.getColumns().get(6).setPrefWidth(100);
                                tableField3.getColumns().get(6).setPrefWidth(100);
                                tableField4.getColumns().get(6).setPrefWidth(100);
                            } else if (width < 1100) {
                                tableField.getColumns().get(0).setPrefWidth(80);
                                tableField1.getColumns().get(0).setPrefWidth(80);
                                tableField2.getColumns().get(0).setPrefWidth(80);
                                tableField3.getColumns().get(0).setPrefWidth(80);
                                tableField4.getColumns().get(0).setPrefWidth(80);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(300);
                                tableField1.getColumns().get(1).setPrefWidth(300);
                                tableField2.getColumns().get(1).setPrefWidth(300);
                                tableField3.getColumns().get(1).setPrefWidth(300);
                                tableField4.getColumns().get(1).setPrefWidth(300);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(100);
                                tableField1.getColumns().get(2).setPrefWidth(100);
                                tableField2.getColumns().get(2).setPrefWidth(100);
                                tableField3.getColumns().get(2).setPrefWidth(100);
                                tableField4.getColumns().get(2).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(150);
                                tableField1.getColumns().get(3).setPrefWidth(150);
                                tableField2.getColumns().get(3).setPrefWidth(150);
                                tableField3.getColumns().get(3).setPrefWidth(150);
                                tableField4.getColumns().get(3).setPrefWidth(150);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(150);
                                tableField1.getColumns().get(4).setPrefWidth(150);
                                tableField2.getColumns().get(4).setPrefWidth(150);
                                tableField3.getColumns().get(4).setPrefWidth(150);
                                tableField4.getColumns().get(4).setPrefWidth(150);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(150);
                                tableField1.getColumns().get(5).setPrefWidth(150);
                                tableField2.getColumns().get(5).setPrefWidth(150);
                                tableField3.getColumns().get(5).setPrefWidth(150);
                                tableField4.getColumns().get(5).setPrefWidth(150);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(150);
                                tableField1.getColumns().get(6).setPrefWidth(150);
                                tableField2.getColumns().get(6).setPrefWidth(150);
                                tableField3.getColumns().get(6).setPrefWidth(150);
                                tableField4.getColumns().get(6).setPrefWidth(150);
                            } else if (width < 1300) {
                                tableField.getColumns().get(0).setPrefWidth(100);
                                tableField1.getColumns().get(0).setPrefWidth(100);
                                tableField2.getColumns().get(0).setPrefWidth(100);
                                tableField3.getColumns().get(0).setPrefWidth(100);
                                tableField4.getColumns().get(0).setPrefWidth(100);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(400);
                                tableField1.getColumns().get(1).setPrefWidth(400);
                                tableField2.getColumns().get(1).setPrefWidth(400);
                                tableField3.getColumns().get(1).setPrefWidth(400);
                                tableField4.getColumns().get(1).setPrefWidth(400);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(100);
                                tableField1.getColumns().get(2).setPrefWidth(100);
                                tableField2.getColumns().get(2).setPrefWidth(100);
                                tableField3.getColumns().get(2).setPrefWidth(100);
                                tableField4.getColumns().get(2).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(150);
                                tableField1.getColumns().get(3).setPrefWidth(150);
                                tableField2.getColumns().get(3).setPrefWidth(150);
                                tableField3.getColumns().get(3).setPrefWidth(150);
                                tableField4.getColumns().get(3).setPrefWidth(150);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(200);
                                tableField1.getColumns().get(4).setPrefWidth(200);
                                tableField2.getColumns().get(4).setPrefWidth(200);
                                tableField3.getColumns().get(4).setPrefWidth(200);
                                tableField4.getColumns().get(4).setPrefWidth(200);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(150);
                                tableField1.getColumns().get(5).setPrefWidth(150);
                                tableField2.getColumns().get(5).setPrefWidth(150);
                                tableField3.getColumns().get(5).setPrefWidth(150);
                                tableField4.getColumns().get(5).setPrefWidth(150);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(200);
                                tableField1.getColumns().get(6).setPrefWidth(200);
                                tableField2.getColumns().get(6).setPrefWidth(200);
                                tableField3.getColumns().get(6).setPrefWidth(200);
                                tableField4.getColumns().get(6).setPrefWidth(200);
                            } else if (width < 1500) {
                                tableField.getColumns().get(0).setPrefWidth(100);
                                tableField1.getColumns().get(0).setPrefWidth(100);
                                tableField2.getColumns().get(0).setPrefWidth(100);
                                tableField3.getColumns().get(0).setPrefWidth(100);
                                tableField4.getColumns().get(0).setPrefWidth(100);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(400);
                                tableField1.getColumns().get(1).setPrefWidth(400);
                                tableField2.getColumns().get(1).setPrefWidth(400);
                                tableField3.getColumns().get(1).setPrefWidth(400);
                                tableField4.getColumns().get(1).setPrefWidth(400);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(100);
                                tableField1.getColumns().get(2).setPrefWidth(100);
                                tableField2.getColumns().get(2).setPrefWidth(100);
                                tableField3.getColumns().get(2).setPrefWidth(100);
                                tableField4.getColumns().get(2).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(200);
                                tableField1.getColumns().get(3).setPrefWidth(200);
                                tableField2.getColumns().get(3).setPrefWidth(200);
                                tableField3.getColumns().get(3).setPrefWidth(200);
                                tableField4.getColumns().get(3).setPrefWidth(200);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(200);
                                tableField1.getColumns().get(4).setPrefWidth(200);
                                tableField2.getColumns().get(4).setPrefWidth(200);
                                tableField3.getColumns().get(4).setPrefWidth(200);
                                tableField4.getColumns().get(4).setPrefWidth(200);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(200);
                                tableField1.getColumns().get(5).setPrefWidth(200);
                                tableField2.getColumns().get(5).setPrefWidth(200);
                                tableField3.getColumns().get(5).setPrefWidth(200);
                                tableField4.getColumns().get(5).setPrefWidth(200);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(200);
                                tableField1.getColumns().get(6).setPrefWidth(200);
                                tableField2.getColumns().get(6).setPrefWidth(200);
                                tableField3.getColumns().get(6).setPrefWidth(200);
                                tableField4.getColumns().get(6).setPrefWidth(200);
                            } else if (width < 1700) {
                                tableField.getColumns().get(0).setPrefWidth(100);
                                tableField1.getColumns().get(0).setPrefWidth(100);
                                tableField2.getColumns().get(0).setPrefWidth(100);
                                tableField3.getColumns().get(0).setPrefWidth(100);
                                tableField4.getColumns().get(0).setPrefWidth(100);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(400);
                                tableField1.getColumns().get(1).setPrefWidth(400);
                                tableField2.getColumns().get(1).setPrefWidth(400);
                                tableField3.getColumns().get(1).setPrefWidth(400);
                                tableField4.getColumns().get(1).setPrefWidth(400);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(100);
                                tableField1.getColumns().get(2).setPrefWidth(100);
                                tableField2.getColumns().get(2).setPrefWidth(100);
                                tableField3.getColumns().get(2).setPrefWidth(100);
                                tableField4.getColumns().get(2).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(200);
                                tableField1.getColumns().get(3).setPrefWidth(200);
                                tableField2.getColumns().get(3).setPrefWidth(200);
                                tableField3.getColumns().get(3).setPrefWidth(200);
                                tableField4.getColumns().get(3).setPrefWidth(200);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(300);
                                tableField1.getColumns().get(4).setPrefWidth(300);
                                tableField2.getColumns().get(4).setPrefWidth(300);
                                tableField3.getColumns().get(4).setPrefWidth(300);
                                tableField4.getColumns().get(4).setPrefWidth(300);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(200);
                                tableField1.getColumns().get(5).setPrefWidth(200);
                                tableField2.getColumns().get(5).setPrefWidth(200);
                                tableField3.getColumns().get(5).setPrefWidth(200);
                                tableField4.getColumns().get(5).setPrefWidth(200);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(300);
                                tableField1.getColumns().get(6).setPrefWidth(300);
                                tableField2.getColumns().get(6).setPrefWidth(300);
                                tableField3.getColumns().get(6).setPrefWidth(300);
                                tableField4.getColumns().get(6).setPrefWidth(300);
                            } else if (width < 1900) {
                                tableField.getColumns().get(0).setPrefWidth(100);
                                tableField1.getColumns().get(0).setPrefWidth(100);
                                tableField2.getColumns().get(0).setPrefWidth(100);
                                tableField3.getColumns().get(0).setPrefWidth(100);
                                tableField4.getColumns().get(0).setPrefWidth(100);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(500);
                                tableField1.getColumns().get(1).setPrefWidth(500);
                                tableField2.getColumns().get(1).setPrefWidth(500);
                                tableField3.getColumns().get(1).setPrefWidth(500);
                                tableField4.getColumns().get(1).setPrefWidth(500);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(100);
                                tableField1.getColumns().get(2).setPrefWidth(100);
                                tableField2.getColumns().get(2).setPrefWidth(100);
                                tableField3.getColumns().get(2).setPrefWidth(100);
                                tableField4.getColumns().get(2).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(200);
                                tableField1.getColumns().get(3).setPrefWidth(200);
                                tableField2.getColumns().get(3).setPrefWidth(200);
                                tableField3.getColumns().get(3).setPrefWidth(200);
                                tableField4.getColumns().get(3).setPrefWidth(200);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(300);
                                tableField1.getColumns().get(4).setPrefWidth(300);
                                tableField2.getColumns().get(4).setPrefWidth(300);
                                tableField3.getColumns().get(4).setPrefWidth(300);
                                tableField4.getColumns().get(4).setPrefWidth(300);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(200);
                                tableField1.getColumns().get(5).setPrefWidth(200);
                                tableField2.getColumns().get(5).setPrefWidth(200);
                                tableField3.getColumns().get(5).setPrefWidth(200);
                                tableField4.getColumns().get(5).setPrefWidth(200);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(300);
                                tableField1.getColumns().get(6).setPrefWidth(300);
                                tableField2.getColumns().get(6).setPrefWidth(300);
                                tableField3.getColumns().get(6).setPrefWidth(300);
                                tableField4.getColumns().get(6).setPrefWidth(300);
                            } else if (width < 2100) {
                                tableField.getColumns().get(0).setPrefWidth(100);
                                tableField1.getColumns().get(0).setPrefWidth(100);
                                tableField2.getColumns().get(0).setPrefWidth(100);
                                tableField3.getColumns().get(0).setPrefWidth(100);
                                tableField4.getColumns().get(0).setPrefWidth(100);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(500);
                                tableField1.getColumns().get(1).setPrefWidth(500);
                                tableField2.getColumns().get(1).setPrefWidth(500);
                                tableField3.getColumns().get(1).setPrefWidth(500);
                                tableField4.getColumns().get(1).setPrefWidth(500);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(100);
                                tableField1.getColumns().get(2).setPrefWidth(100);
                                tableField2.getColumns().get(2).setPrefWidth(100);
                                tableField3.getColumns().get(2).setPrefWidth(100);
                                tableField4.getColumns().get(2).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(200);
                                tableField1.getColumns().get(3).setPrefWidth(200);
                                tableField2.getColumns().get(3).setPrefWidth(200);
                                tableField3.getColumns().get(3).setPrefWidth(200);
                                tableField4.getColumns().get(3).setPrefWidth(200);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(400);
                                tableField1.getColumns().get(4).setPrefWidth(400);
                                tableField2.getColumns().get(4).setPrefWidth(400);
                                tableField3.getColumns().get(4).setPrefWidth(400);
                                tableField4.getColumns().get(4).setPrefWidth(400);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(200);
                                tableField1.getColumns().get(5).setPrefWidth(200);
                                tableField2.getColumns().get(5).setPrefWidth(200);
                                tableField3.getColumns().get(5).setPrefWidth(200);
                                tableField4.getColumns().get(5).setPrefWidth(200);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(400);
                                tableField1.getColumns().get(6).setPrefWidth(400);
                                tableField2.getColumns().get(6).setPrefWidth(400);
                                tableField3.getColumns().get(6).setPrefWidth(400);
                                tableField4.getColumns().get(6).setPrefWidth(400);
                            } else if (width < 2300) {
                                tableField.getColumns().get(0).setPrefWidth(100);
                                tableField1.getColumns().get(0).setPrefWidth(100);
                                tableField2.getColumns().get(0).setPrefWidth(100);
                                tableField3.getColumns().get(0).setPrefWidth(100);
                                tableField4.getColumns().get(0).setPrefWidth(100);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(500);
                                tableField1.getColumns().get(1).setPrefWidth(500);
                                tableField2.getColumns().get(1).setPrefWidth(500);
                                tableField3.getColumns().get(1).setPrefWidth(500);
                                tableField4.getColumns().get(1).setPrefWidth(500);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(100);
                                tableField1.getColumns().get(2).setPrefWidth(100);
                                tableField2.getColumns().get(2).setPrefWidth(100);
                                tableField3.getColumns().get(2).setPrefWidth(100);
                                tableField4.getColumns().get(2).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(200);
                                tableField1.getColumns().get(3).setPrefWidth(200);
                                tableField2.getColumns().get(3).setPrefWidth(200);
                                tableField3.getColumns().get(3).setPrefWidth(200);
                                tableField4.getColumns().get(3).setPrefWidth(200);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(500);
                                tableField1.getColumns().get(4).setPrefWidth(500);
                                tableField2.getColumns().get(4).setPrefWidth(500);
                                tableField3.getColumns().get(4).setPrefWidth(500);
                                tableField4.getColumns().get(4).setPrefWidth(500);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(200);
                                tableField1.getColumns().get(5).setPrefWidth(200);
                                tableField2.getColumns().get(5).setPrefWidth(200);
                                tableField3.getColumns().get(5).setPrefWidth(200);
                                tableField4.getColumns().get(5).setPrefWidth(200);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(500);
                                tableField1.getColumns().get(6).setPrefWidth(500);
                                tableField2.getColumns().get(6).setPrefWidth(500);
                                tableField3.getColumns().get(6).setPrefWidth(500);
                                tableField4.getColumns().get(6).setPrefWidth(500);
                            } else {
                                tableField.getColumns().get(0).setPrefWidth(100);
                                tableField1.getColumns().get(0).setPrefWidth(100);
                                tableField2.getColumns().get(0).setPrefWidth(100);
                                tableField3.getColumns().get(0).setPrefWidth(100);
                                tableField4.getColumns().get(0).setPrefWidth(100);
                                // Link
                                tableField.getColumns().get(1).setPrefWidth(600);
                                tableField1.getColumns().get(1).setPrefWidth(600);
                                tableField2.getColumns().get(1).setPrefWidth(600);
                                tableField3.getColumns().get(1).setPrefWidth(600);
                                tableField4.getColumns().get(1).setPrefWidth(600);
                                // Line-Number
                                tableField.getColumns().get(2).setPrefWidth(100);
                                tableField1.getColumns().get(2).setPrefWidth(100);
                                tableField2.getColumns().get(2).setPrefWidth(100);
                                tableField3.getColumns().get(2).setPrefWidth(100);
                                tableField4.getColumns().get(2).setPrefWidth(100);
                                // Review State
                                tableField.getColumns().get(3).setPrefWidth(200);
                                tableField1.getColumns().get(3).setPrefWidth(200);
                                tableField2.getColumns().get(3).setPrefWidth(200);
                                tableField3.getColumns().get(3).setPrefWidth(200);
                                tableField4.getColumns().get(3).setPrefWidth(200);
                                // Review Short Description
                                tableField.getColumns().get(4).setPrefWidth(600);
                                tableField1.getColumns().get(4).setPrefWidth(600);
                                tableField2.getColumns().get(4).setPrefWidth(600);
                                tableField3.getColumns().get(4).setPrefWidth(600);
                                tableField4.getColumns().get(4).setPrefWidth(600);
                                // Test State
                                tableField.getColumns().get(5).setPrefWidth(200);
                                tableField1.getColumns().get(5).setPrefWidth(200);
                                tableField2.getColumns().get(5).setPrefWidth(200);
                                tableField3.getColumns().get(5).setPrefWidth(200);
                                tableField4.getColumns().get(5).setPrefWidth(200);
                                // Test Short Description
                                tableField.getColumns().get(6).setPrefWidth(600);
                                tableField1.getColumns().get(6).setPrefWidth(600);
                                tableField2.getColumns().get(6).setPrefWidth(600);
                                tableField3.getColumns().get(6).setPrefWidth(600);
                                tableField4.getColumns().get(6).setPrefWidth(600);
                            }
                        }
            });
        }
    }

    // Task for building the tables
    Task Controller() {
        return new Task() {
            @Override
            protected Object call() {
                try {
                    // wait until all tasks are finished
                    boolean finished = false;
                    while (!finished) {
                        finished = true;
                        for (int i = 0; i < taskField.length; i++) {
                            if (!taskField[i]) {
                                finished = false;
                                break;
                            }
                        }
                        if (!finished) {
                            Thread.sleep(1000);
                        }
                    }
                    // wait for tab panes are written
                    while (!tabTest0.getText().equals(titleField[0])) {
                        Thread.sleep(500);
                    }
                    while (!tabTest1.getText().equals(titleField[1])) {
                        Thread.sleep(500);
                    }
                    while (!tabTest2.getText().equals(titleField[2])) {
                        Thread.sleep(500);
                    }
                    while (!tabTest3.getText().equals(titleField[3])) {
                        Thread.sleep(500);
                    }
                    while (!tabTest4.getText().equals(titleField[4])) {
                        Thread.sleep(500);
                    }

                    tasksFinished = true;
                    processingBuildTables = false;

                    // hide progress indicator
                    progressBar.setVisible(false);
                    logger.info("LxrTestTableController: Ending buildPartedTables");
                    
                } catch (Exception ex) {
                    tasksFinished = true;
                    processingBuildTables = false;

                    // hide progress indicator
                    progressBar.setVisible(false);

                    logger.info("LxrTestTableController: Ending buildPartedTables with an exception!");
                }

                return true;
            }
        };
    }
    
    private int isInRegion(int fileCount) {
        int i = 0;
        for (int page : pageTable) {
            if (fileCount >= page) {
                // do nothing, continue!
            } else {
                break;
            }
            i++; 
        }
        int region = ((i-1)/5)*5;
        if (region + 5 > partedFileList.size()) {
            region = partedFileList.size() - 5;
        }
        
        return region;
    }

    private int switchToTab(int region, int page) {
        int diff = activeRegion %5;
        if (diff == 0) {
            return page%5;  // normal operation
        } else {
            return page-diff;  // operation at the end of pages
        }
    }
    
    private void jump(int fileCount, int lineNr) {
            int i = 0;
            int page = -1;
            int line;
            for (int p : pageTable) { // e.g pageTable = {1,51}
                if (fileCount >= p) {
                    // e.g. 
                    // fileCount = 51, p = 1
                    // fileCount = 1, p = 1
                    i++;
                } else {
                    // e.g. fileCount = 51, p = 51
                    // page found
                    page = i-1;
                    break;
                }
            }
            if (page == -1) {
                page = pageTable.size()-1;
            }
            // There are less or equal than 5 pages
            switch (switchToTab(activeRegion, page)) {
                case 0:
                    line = computeLine(page, fileCount, lineNr);
                    
                    Platform.runLater(new Runnable() {
                        @Override public void run() {
                            tabPane.getSelectionModel().select(tabTest0);

                            tableField.requestFocus();
                            tableField.scrollTo(line);
                            tableField.getSelectionModel().clearSelection();
                            tableField.getSelectionModel().select(line);
                        }
                    });                                

                    break;
                case 1:
                    line = computeLine(page, fileCount, lineNr);
                    
                    Platform.runLater(new Runnable() {
                        @Override public void run() {
                            tabPane.getSelectionModel().select(tabTest1);

                            tableField1.requestFocus();
                            tableField1.scrollTo(line);
                            tableField1.getSelectionModel().clearSelection();
                            tableField1.getSelectionModel().select(line);
                        }
                    });                                

                    break;
                case 2:
                    line = computeLine(page, fileCount, lineNr);
                    
                    Platform.runLater(new Runnable() {
                        @Override public void run() {
                            tabPane.getSelectionModel().select(tabTest2);

                            tableField2.requestFocus();
                            tableField2.scrollTo(line);
                            tableField2.getSelectionModel().clearSelection();
                            tableField2.getSelectionModel().select(line);
                        }
                    });                                

                    break;
                case 3:
                    line = computeLine(page, fileCount, lineNr);
                    
                    Platform.runLater(new Runnable() {
                        @Override public void run() {
                            tabPane.getSelectionModel().select(tabTest3);

                            tableField3.requestFocus();
                            tableField3.scrollTo(line);
                            tableField3.getSelectionModel().clearSelection();
                            tableField3.getSelectionModel().select(line);
                        }
                    });                                

                    break;
                case 4:
                    line = computeLine(page, fileCount, lineNr);
                    
                    Platform.runLater(new Runnable() {
                        @Override public void run() {
                            tabPane.getSelectionModel().select(tabTest4);

                            tableField4.requestFocus();
                            tableField4.scrollTo(line);
                            tableField4.getSelectionModel().clearSelection();
                            tableField4.getSelectionModel().select(line);
                        }
                    });                                

                    break;
                default:
                    break;
            }
    }
    
    public void jumpToLine(int fileCount, int lineNr) {
        jumpFileCount = fileCount;
        jumpLineNr = lineNr;
        if (pageTable.size() > 5) {
            Task taskForProgressBar;    // progress bar
            Task taskForBuildTables;    // parted tables

            activeRegion = isInRegion(fileCount);
            tabPane.getSelectionModel()
                    .select(tabTest0);
            if (activeRegion != lastActiveRegion) {
                processingBuildTables = true;
                lastActiveRegion = activeRegion;

                progressBar.setProgress(0);
                // show local progress bar
                progressBar.setVisible(true);

                taskForProgressBar = TaskForProgressBar();
                taskForBuildTables = TaskForBuildTables(false);

                // Initially, all tasks are marked 
                // as inactive
                taskField[0] = false;
                taskField[1] = false;
                taskField[2] = false;
                taskField[3] = false;
                taskField[4] = false;

                // Initially, all tab panes are 
                // marked empty
                titleField[0] = "";
                titleField[1] = "";
                titleField[2] = "";
                titleField[3] = "";
                titleField[4] = "";

                // The tasks are not finished
                tasksFinished = false;
                
                // marker to call jumpToLine
                isJump = true;

                // start tasks as threads
                new Thread(taskForProgressBar).start();
                Thread thread = new Thread(taskForBuildTables);
                thread.start();
            } else {
                // The specified line is available in the active region. 
                // Thus, we can jump directly to that line.
                jump(fileCount, lineNr);
            }
            
        } else {
            // There are less than 5 pages available. Thus, we can jump 
            // directly to the specified line.
            jump(fileCount, lineNr);
        }
    }
    
    private int computeLine(int page, int fileCount, int lineNr) {
        int line = 0;

        int firstFileOnPage = pageTable.get(page);
        int end = fileCount - firstFileOnPage + 1;
        for (int i = 0; i < end; i++) {
            if (Integer.parseInt(partedFileList.get(page).get(i).getFileCount()) < fileCount) {
                int len = partedFileList.get(page).get(i).getHref().size();
                line = line + len;
            } else {
                // exit loop
                break;
            }
        }
        line = line + lineNr;
        
        return line;
    }
    
    public void buildPartedTables(boolean statistics) {
        logger.info("LxrTestTableController: Starting buildPartedTables");
        
        PartedTableTask pth0 = new PartedTableTask(0, statistics);
        PartedTableTask pth1 = new PartedTableTask(1, statistics);
        PartedTableTask pth2 = new PartedTableTask(2, statistics);
        PartedTableTask pth3 = new PartedTableTask(3, statistics);
        PartedTableTask pth4 = new PartedTableTask(4, statistics);
        
        Thread controller = new Thread(Controller());
        controller.setName("BuildPartedTables - Controller");
        controller.start();
        
        // runs in parallel
        Thread th0 = new Thread(pth0);
        th0.setName("BuildPartedTable - Thread 0");
        th0.start();
        Thread th1 = new Thread(pth1);
        th1.setName("BuildPartedTable - Thread 1");
        th1.start();
        Thread th2 = new Thread(pth2);
        th2.setName("BuildPartedTable - Thread 2");
        th2.start();
        Thread th3 = new Thread(pth3);
        th3.setName("BuildPartedTable - Thread 3");
        th3.start();
        Thread th4 = new Thread(pth4);
        th4.setName("BuildPartedTable - Thread 4");
        th4.start();
    }

    @FXML
    private void handleSaveFile(ActionEvent event) {
        FXMLLoader loader;
        final SaveController saveController;

        loader = myController.getLoader(SAVE_STAGE);
        saveController = loader.<SaveController>getController();

        saveController.setOutputList(changedFileList);
        saveController.setMode(SaveController.Mode.XML_MODE);
        saveController.initialize(null, null);
        
        myController.getStage(SAVE_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemSaveTable"));
        myController.showStage(SAVE_STAGE);
    }

    @FXML
    private void handleDistribute(ActionEvent event) {
        FXMLLoader loader;
        final SaveController saveController;

        loader = myController.getLoader(SAVE_STAGE);
        saveController = loader.<SaveController>getController();

        saveController.setOutputList(changedFileList);
        saveController.setMode(SaveController.Mode.DISTRIBUTION_MODE);
        saveController.initialize(null, null);
        
        myController.getStage(SAVE_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemDistribute"));
        myController.showStage(SAVE_STAGE);
    }
    
    @FXML
    private void handleReport(ActionEvent event) {
        FXMLLoader loader;
        DialogReportController dialogReportController;

        loader = myController.getLoader(REPORT_STAGE);
        dialogReportController = loader.<DialogReportController>getController();
        dialogReportController.setTable(changedFileList, configEntry);
        dialogReportController.setStatistics(statistics);
        dialogReportController.setSvnAvailable(svnAvailable);

        myController.getStage(REPORT_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemReport"));
        myController.showStage(REPORT_STAGE);
    }
    
    public int getActualFileCount() {
        int page;
        int index;
        int fileCount;
        page = tabPane.getSelectionModel().getSelectedIndex();
        switch (page) {
            case 0:
                index = tableField.getSelectionModel().getFocusedIndex();
                if (index == -1){
                    // noting is focused. Select element 0!
                    index = 0;
                }
                fileCount = tableField.getItems().get(index).getFileCount();
                break;
            case 1:
                index = tableField1.getSelectionModel().getFocusedIndex();
                if (index == -1){
                    // noting is focused. Select element 0!
                    index = 0;
                }
                fileCount = tableField1.getItems().get(index).getFileCount();
                break;
            case 2:
                index = tableField2.getSelectionModel().getFocusedIndex();
                if (index == -1){
                    // noting is focused. Select element 0!
                    index = 0;
                }
                fileCount = tableField2.getItems().get(index).getFileCount();
                break;
            case 3:
                index = tableField3.getSelectionModel().getFocusedIndex();
                if (index == -1){
                    // noting is focused. Select element 0!
                    index = 0;
                }
                fileCount = tableField3.getItems().get(index).getFileCount();
                break;
            case 4:
                index = tableField4.getSelectionModel().getFocusedIndex();
                if (index == -1){
                    // noting is focused. Select element 0!
                    index = 0;
                }
                fileCount = tableField4.getItems().get(index).getFileCount();
                break;
            default:
                index = tableField.getSelectionModel().getFocusedIndex();
                if (index == -1){
                    // noting is focused. Select element 0!
                    index = 0;
                }
                fileCount = tableField.getItems().get(index).getFileCount();
                break;
        }
        return fileCount;
    }

    @FXML
    private void handleSearch(ActionEvent event) {
        FXMLLoader loader;
        DialogSearchController dialogSearchController;
        

        loader = myController.getLoader(SEARCH_STAGE);
        dialogSearchController = loader.<DialogSearchController>getController();
        dialogSearchController.setTable(changedFileList);
        dialogSearchController.setSvn(svnAvailable);
        dialogSearchController.setFocusOnSearchTextbox();
        myController.getStage(SEARCH_STAGE).setTitle(
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("programName") + " - " + 
                ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("menuItemSearch"));
        myController.showStage(SEARCH_STAGE);
    }

    @FXML
    private void handleSave(ActionEvent event) {
        FXMLLoader loader;
        final SaveController saveController;

        loader = myController.getLoader(SAVE_STAGE);
        saveController = loader.<SaveController>getController();

        saveController.setOutputList(changedFileList);
        saveController.setMode(SaveController.Mode.XML_MODE);
        saveController.initialize(null, null);
          
        String name = workingDir.getName();
        name = workingDir.getAbsolutePath() + File.separator + name + ".xml";
        File xmlFile = new File(name);
        saveController.writeXmlFile(xmlFile);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // The search functionality is not implemented yet!
        // menuItemSearch.setDisable(true);

        // locale settings
        lang = props.getProperty(props.LANG);
        country = props.getProperty(props.COUNTRY);
        locale = new Locale(lang, country);
        conv = new ConvertLxrDiffTabStrings(locale);
        String fs = props.getProperty(props.MAXIMIZEDSCREEN);
        if (fs.equals("")) {
            // initially, maximized screen is disabled
            maximizedScreen = false;
            props.putProperty(props.MAXIMIZEDSCREEN, "false");
        } else {
            maximizedScreen = fs.equals("true");
        }
        
       switch (props.getProperty(props.HIDEDUBLICATEDFILES)) {
            case "false":
                hideDublicatedFiles = false;
                break;
            case "true":
                hideDublicatedFiles = true;
                break;
            default:
                hideDublicatedFiles = false;
                props.putProperty(props.HIDEDUBLICATEDFILES, "false");
                break;
        }
        switch (props.getProperty(props.HIDEDELETEDFILES)) {
            case "false":
                hideDeletedFiles = false;
                break;
            case "true":
                hideDeletedFiles = true;
                break;
            default:
                hideDeletedFiles = false;
                props.putProperty(props.HIDEDELETEDFILES, "false");
                break;
        }
        switch (props.getProperty(props.HIDENEWFILES)) {
            case "false":
                hideNewFiles = false;
                break;
            case "true":
                hideNewFiles = true;
                break;
            default:
                hideNewFiles = false;
                props.putProperty(props.HIDENEWFILES, "false");
                break;
        }
    }

    public void renameCaption(String name) {
        labelCaption.setText(name);
    }

    /**
     * Sets the member varialbe "lineNumberAvailable" to "true".
     *
     */
    public void setLineNumberAvailable() {
        lineNumberAvailable = true;
    }

    /**
     * Sets the member varialbe "lineNumberAvailable" to "false".
     *
     */
    public void resetLineNumberAvailable() {
        lineNumberAvailable = false;
    }

    public void setConfigEntry(ConfigEntry entry) {
        this.configEntry = entry;
    }

    public ConfigEntry getConfigEntry() {
        return this.configEntry;
    }
    
    public void setRepoLog(List<LogEntry> repoLog) {
        this.repoLog = repoLog;
    }
            
    public List<LogEntry> getRepoLog() {
        return repoLog;
    }

    /**
     * This method runs a watcher for the "lockDirectory".
     * 
     * Note:
     * The lock directory is created in XmlHandling during reading of xml
     * file.
     * 
     */
    public void watchXmlLockFiles() {

        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml != null) {
            File lockDir = xml.getLockDir();
            watcherTask = () -> 
            {
                boolean running = true;
                logger.info("LxrTestTableController: Watcher Thread: Starting...");
                while (running) {
                    try {
                        if (lockDir.listFiles() == null) {
                            // No lock directory available. Wait until it is 
                            // created. Then proceed.
                            labelAccessability.setText(ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("noAccess"));
                            labelAccessability
                                    .setStyle("-fx-background-color: yellow");
                            while (lockDir.listFiles() == null) {
                                Thread.sleep(500);        // wait 2 seconds
                            }
                            // Lock directory is created. Proceed!
                            File lockFile = new File (lockDir.getAbsoluteFile() + 
                                    "/.lock");
                            if (xml.isReadOnly(lockFile)) {
                                // Do nothing, because the right state is set.
                            } else {
                                labelAccessability.setText(ResourceBundle.
                                    getBundle("lxrdifferencetable/Bundle", locale).
                                    getString("access"));
                                labelAccessability.setStyle("");
                            }
                        } else {
                            File lockFile = new File (lockDir.getAbsoluteFile() + 
                                    "/.lock");
                            if (xml.isReadOnly(lockFile)) {
                                labelAccessability.setText(ResourceBundle.
                                    getBundle("lxrdifferencetable/Bundle", locale).
                                    getString("noAccess"));
                                labelAccessability
                                        .setStyle("-fx-background-color: yellow");
                            } else {
                                labelAccessability.setText(ResourceBundle.
                                    getBundle("lxrdifferencetable/Bundle", locale).
                                    getString("access"));
                                labelAccessability.setStyle("");
                            }
                        }

                        watcher = FileSystems.getDefault().newWatchService();
                        Paths.get(lockDir.toString()).register( watcher,
                                StandardWatchEventKinds.ENTRY_CREATE, 
                                StandardWatchEventKinds.ENTRY_DELETE);

                        String str;
                        File f;
                        while ( true ) {
                            try {
                                WatchKey key = watcher.take();
                                for ( WatchEvent<?> event : key.pollEvents() ) {
                                    String name = event.kind().name();
                                    switch (name) {
                                        case "ENTRY_CREATE":
                                            str = event.context().toString();
                                            f = new File (lockDir + "/" + str);
                                            LockType lockType;
                                            String lockedName;
                                            lockedName = xml.extractLoginName(
                                                    new File (str));
                                            xml.addLogin(lockedName);
                                            lockType = xml.isLocked(f);
                                            if (lockType == LockType.locked) {
                                                EntryPosition pos;
                                                // read xml file
                                                pos = xml.readLockedXml(f, false);

                                                XmlHandling.RowType rowType;
                                                rowType = xml.getRowType(f);
                                                if (null != rowType) switch (rowType) {
                                                    case reviewState:
                                                        handleReviewState(pos);
                                                        break;
                                                    case reviewShortDesc:
                                                        handleReviewShortDescription(pos);
                                                        break;
                                                    case testState:
                                                        handleTestState(pos);
                                                        break;
                                                    case testDesc:
                                                        handleTestShortDescription(pos);
                                                        break;
                                                    case caption:
                                                        handleCaption();
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                            break;
                                        case "ENTRY_DELETE":
                                            str = event.context().toString();
                                            lockedName = xml.extractLoginName(
                                                    new File (str));
                                            xml.deleteLogin(lockedName);
                                            handleDeleted(lockedName);
                                            break;
                                        case "OVERFLOW":
                                            logger.info("LxrTestTableController: " + 
                                                    "Watcher Thread: " + 
                                                    "Overflow!");
                                            break;
                                        default:
                                            logger.info("LxrTestTableController: " + 
                                                    "Watcher Thread: " + 
                                                    "Unknown name!");
                                            // Do nothing!
                                            break;
                                    }
                                }
                                key.reset();
                            } catch (InterruptedException ex) {
                                logger.info("LxrTestTableController: " + ""
                                        + "Watcher Thread: An interruppted " + 
                                        "exception occured! Exception Text = " + 
                                        ex.getMessage());
                            }

                        }
                    } catch (IOException | InterruptedException ex) {
                        logger.info("LxrTestTableController: " + 
                                "An exception occured during watching "
                                + "the xml file locks. Aborting...");
                    } catch (ClosedWatchServiceException ex) {
                        running = false;
                    }    
                }
            };
            if (watcherThread == null) {
                watcherThread = new Thread(watcherTask);
                watcherThread.setName("FileWatcher");
                watcherThread.start();
            }
        }
    }
    
    public void closeWatcherThread() {
        try {
            if (watcher != null) {
                // Close watcher if watcher is available!
                watcher.close();
            }
        } catch (IOException ex) {
            logger.info("LxrTestTableController: " + 
                    "An exception occured during close operation " + 
                    "of watcher thread.");
        }
    }
    
    /**
     * This method updates the new "review state", marks the relevant rows 
     * as locked and calls the handler to mark the locked lines yellow.
     * 
     * @param pos   
     */
    private void handleReviewState(EntryPosition pos) {
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml != null) {
            Platform.runLater(() -> {
                // Note:
                // An "IndexOutOfBoundsException" would occur in
                // "ReadOnlyUnbackedObservableList.java", if the
                // "runLater" is not used!
                
                TestDataModel e;
                int fileNumber;
                int j;
                int size;
                
                
                // force the whole table to refresh by changing column
                // visibility on and off
                //
                // see: http://jaakkola.net/juhani/blog/?p=264
                //
                // refresh "tableField"
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField1"
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField2"
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField3"
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField4"
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(true);
                
                fileNumber = pos.getFileNumber();
                size = changedFileList.get(fileNumber).getLineNumber().size();
                
                for (int i = 0; i < size; i++) {
                    j = changedFileList.get(fileNumber).getTableViewLineNumber(i);
                    
                    State reviewStateLast;
                    reviewStateLast = changedFileList.get(fileNumber)
                            .getReviewStateLast(i);
                    
                    xml.putLockedRowsList(
                            changedFileList.get(fileNumber).getLoginName(),
                            j, fileNumber);
                    
                    if (pos.getChangedLineNumber() == i) {
                        // update statistics
                        List<State> reviewState = changedFileList.get(fileNumber)
                                .getReviewState(i);
                        int s = reviewState.size();
                        if (s == 1) {
                            // the inital state is empty
                            statistics.updateReview(reviewStateLast,
                                    State.EMPTY);
                        } else {
                            statistics.updateReview(reviewStateLast,
                                    reviewState.get(s-2));
                        }
                        writeReviewStatisticsField();
                        
                        // do not exchange the following
                        // two lines!
                        reviewStateColBooleanList.get(fileNumber).set(i, true);
                        
                        // call handler
                        if (reviewStateColList.get(fileNumber).get(i) != null) {
                            reviewStateColList.get(fileNumber).get(i)
                                    .getSelectionModel().select(reviewStateLast);
                        }
                    }
                    if (activeReviewDescriptionController == pos.getFileNumber()) {
                        // The stage "REVIEW_DESCRIPTION_STAGE" is active.
                        // Do a transition from editable to non-editable,
                        // because the line is locked by another user.
                        FXMLLoader loader;
                        DialogReviewDescriptionController dialogReviewDescriptionController;
                        
                        loader = myController.getLoader(REVIEW_DESCRIPTION_STAGE);
                        dialogReviewDescriptionController = loader.
                                <DialogReviewDescriptionController>getController();
                        
                        dialogReviewDescriptionController.setEditable(false);
                        String loginName = changedFileList.get(fileNumber)
                                .getLoginName();
                        String[] split = loginName.split("-");
                        dialogReviewDescriptionController.setLockedByUser(split[0]);
                        if (!activeReviewDescriptionControllerShown) {
                            activeReviewDescriptionControllerShown = true;
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_DESCRIPTION_STAGE);
                            }
                        }
                    }
                    if (activeTestDescriptionController == pos.getFileNumber()) {
                        // The stage "TEST_DESCRIPTION_STAGE" is active.
                        // Do a transition from editable to non-editable,
                        // because the line is locked by another user.
                        FXMLLoader loader;
                        DialogTestDescriptionController dialogTestDescriptionController;
                        
                        loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
                        dialogTestDescriptionController = loader.
                                <DialogTestDescriptionController>getController();
                        
                        dialogTestDescriptionController.setEditable(false);
                        String loginName = changedFileList.get(fileNumber)
                                .getLoginName();
                        String[] split = loginName.split("-");
                        dialogTestDescriptionController.setLockedByUser(split[0]);
                        if (!activeTestDescriptionControllerShown) {
                            activeTestDescriptionControllerShown = true;
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_DESCRIPTION_STAGE);
                            }
                        }
                    }
                }
            });                    
        }
    }
    
    /**
     * This method marks the relevant lines as "not locked" and updates it 
     * 
     * @param lockedName
     */
    private void handleDeleted(String lockedName) {
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml != null) {
            Platform.runLater(() -> {
                // Note:
                // An "IndexOutOfBoundsException" would occur in
                // "ReadOnlyUnbackedObservableList.java", if the
                // "runLater" is not used!
                
                List<Integer> rowsList = xml.getLockedRows(lockedName);
                List<Integer> filesList = xml.getLockedFiles(lockedName);
                int fileNumber;
                int lastFileNumber = -1;
                int index = -1;
                int i = 0;
                
                // force the whole table to refresh by changing column
                // visibility on and off
                //
                // see: http://jaakkola.net/juhani/blog/?p=264
                // refresh "tableField"
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField1"
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField2"
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField3"
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField4"
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(true);
                
                for (int j : rowsList) {
                    index++;
                    fileNumber = filesList.get(index);
                    changedFileList.get(fileNumber).getLineNumber().size();
                    
                    if (fileNumber == lastFileNumber) {
                        i++;
                    } else {
                        i = 0;
                    }
                    lastFileNumber = fileNumber;
                    
                }
                
                if (configEntry.getLoginName().equals(lockedName)) {
                    configEntry.putXmlLockFileNumber(-1);
                    labelCaption.setStyle("");
                    labelCaption.setText(configEntry.getCaptionLast());
                    
                    FXMLLoader loader;
                    loader = myController.getLoader(CAPTION_RENAME_STAGE);
                    dialogRenameCaptionController = loader.
                            <DialogRenameCaptionController>getController();
                    dialogRenameCaptionController.setCaptionName(labelCaption
                            .getText());
                    dialogRenameCaptionController.setTableEntry(configEntry);
                    dialogRenameCaptionController.setLockedByUser(
                            configEntry.getCaptionAuthorLast());
                    dialogRenameCaptionController.setEditable(true);
                }
            });                    
        }
    }
    
    /**
     * This method updates the new "test state", marks the relevant rows 
     * as locked and calls the handler to mark the locked lines yellow.
     * 
     * @param pos   
     */
    private void handleTestState(EntryPosition pos) {
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml != null) {
            Platform.runLater(() -> {
                // Note:
                // An "IndexOutOfBoundsException" would occur in
                // "ReadOnlyUnbackedObservableList.java", if the
                // "runLater" is not used!
                
                TestDataModel e;
                int fileNumber;
                int j;
                int size;
                
                // force the whole table to refresh by changing column
                // visibility on and off
                //
                // see: http://jaakkola.net/juhani/blog/?p=264
                // refresh "tableField"
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField1"
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField2"
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField3"
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField4"
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(true);
                
                fileNumber = pos.getFileNumber();
                size = changedFileList.get(fileNumber).getLineNumber().size();
                
                for (int i = 0; i < size; i++) {
                    j = changedFileList.get(fileNumber).getTableViewLineNumber(i);
                    
                    State testStateLast;
                    testStateLast = changedFileList.get(fileNumber)
                            .getTestStateLast(i);
                    
                    xml.putLockedRowsList(
                            changedFileList.get(fileNumber).getLoginName(),
                            j, fileNumber);
                    
                    if (pos.getChangedLineNumber() == i) {
                        // update statistics
                        List<State> testState = changedFileList.get(fileNumber)
                                .getTestState(i);
                        int s = testState.size();
                        if (s == 1) {
                            // the inital state is empty
                            statistics.updateTest(testStateLast,
                                    State.EMPTY);
                        } else {
                            statistics.updateTest(testStateLast,
                                    testState.get(s-2));
                        }
                        writeTestStatisticsField();
                        // do not exchange the following
                        // two lines!
                        testStateColBooleanList.get(fileNumber).set(i, true);
                        
                        // call handler
                        if (testStateColList.get(fileNumber).get(i) != null) {
                            testStateColList.get(fileNumber).get(i)
                                    .getSelectionModel().select(testStateLast);
                        }
                        
                    }
                    if (activeReviewDescriptionController == pos.getFileNumber()) {
                        // The stage "REVIEW_DESCRIPTION_STAGE" is active.
                        // Do a transition from editable to non-editable,
                        // because the line is locked by another user.
                        FXMLLoader loader;
                        DialogReviewDescriptionController dialogReviewDescriptionController;
                        
                        loader = myController.getLoader(REVIEW_DESCRIPTION_STAGE);
                        dialogReviewDescriptionController = loader.
                                <DialogReviewDescriptionController>getController();
                        
                        dialogReviewDescriptionController.setEditable(false);
                        String loginName = changedFileList.get(fileNumber)
                                .getLoginName();
                        String[] split = loginName.split("-");
                        dialogReviewDescriptionController.setLockedByUser(split[0]);
                        if (!activeReviewDescriptionControllerShown) {
                            activeReviewDescriptionControllerShown = true;
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_DESCRIPTION_STAGE);
                            }
                        }
                    }
                    if (activeTestDescriptionController == pos.getFileNumber()) {
                        // The stage "TEST_DESCRIPTION_STAGE" is active.
                        // Do a transition from editable to non-editable,
                        // because the line is locked by another user.
                        FXMLLoader loader;
                        DialogTestDescriptionController dialogTestDescriptionController;
                        
                        loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
                        dialogTestDescriptionController = loader.
                                <DialogTestDescriptionController>getController();
                        
                        dialogTestDescriptionController.setEditable(false);
                        String loginName = changedFileList.get(fileNumber)
                                .getLoginName();
                        String[] split = loginName.split("-");
                        dialogTestDescriptionController.setLockedByUser(split[0]);
                        if (!activeTestDescriptionControllerShown) {
                            activeTestDescriptionControllerShown = true;
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_DESCRIPTION_STAGE);
                            }
                        }
                    }
                }
            });                    
        }
    }

    /**
     * This method updates the new "review description", marks the relevant rows 
     * as locked and calls the handler to mark the locked lines yellow.
     * 
     * @param pos   
     */
    private void handleReviewShortDescription(EntryPosition pos) {
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml != null) {
            Platform.runLater(() -> {
                // Note:
                // An "IndexOutOfBoundsException" would occur in
                // "ReadOnlyUnbackedObservableList.java", if the
                // "runLater" is not used!
                
                TestDataModel e;
                int fileNumber;
                int j;
                int size;
                
                // force the whole table to refresh by changing column
                // visibility on and off
                //
                // see: http://jaakkola.net/juhani/blog/?p=264
                // refresh "tableField"
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField1"
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField2"
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField3"
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField4"
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(true);
                
                fileNumber = pos.getFileNumber();
                size = changedFileList.get(fileNumber).getLineNumber().size();
                
                for (int i = 0; i < size; i++) {
                    j = changedFileList.get(fileNumber).getTableViewLineNumber(i);
                    
                    String reviewShortDescriptionLast;
                    reviewShortDescriptionLast = conv.convertDescriptionString(
                            changedFileList.get(fileNumber)
                            .getReviewShortDescriptionLast(i));
                    
                    xml.putLockedRowsList(
                            changedFileList.get(fileNumber).getLoginName(),
                            j, fileNumber);
                    
                    if (pos.getChangedLineNumber() == i) {
                        // do not exchange the following
                        // two lines!
                        reviewShortDescriptionColBooleanList.get(fileNumber)
                                .set(i, true);
                        
                        // call handler
                        reviewShortDescriptionColList.get(fileNumber).get(i)
                                .setText(reviewShortDescriptionLast);
                    }
                    if (activeReviewDescriptionController == pos.getFileNumber()) {
                        // The stage "REVIEW_DESCRIPTION_STAGE" is active.
                        // Do a transition from editable to non-editable,
                        // because the line is locked by another user.
                        FXMLLoader loader;
                        DialogReviewDescriptionController dialogReviewDescriptionController;
                        
                        loader = myController.getLoader(REVIEW_DESCRIPTION_STAGE);
                        dialogReviewDescriptionController = loader.
                                <DialogReviewDescriptionController>getController();
                        
                        dialogReviewDescriptionController.setEditable(false);
                        String loginName = changedFileList.get(fileNumber)
                                .getLoginName();
                        String[] split = loginName.split("-");
                        dialogReviewDescriptionController.setLockedByUser(split[0]);
                        if (!activeReviewDescriptionControllerShown) {
                            activeReviewDescriptionControllerShown = true;
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_DESCRIPTION_STAGE);
                            }
                        }
                    }
                    if (activeTestDescriptionController == pos.getFileNumber()) {
                        // The stage "TEST_DESCRIPTION_STAGE" is active.
                        // Do a transition from editable to non-editable,
                        // because the line is locked by another user.
                        FXMLLoader loader;
                        DialogTestDescriptionController dialogTestDescriptionController;
                        
                        loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
                        dialogTestDescriptionController = loader.
                                <DialogTestDescriptionController>getController();
                        
                        dialogTestDescriptionController.setEditable(false);
                        String loginName = changedFileList.get(fileNumber)
                                .getLoginName();
                        String[] split = loginName.split("-");
                        dialogTestDescriptionController.setLockedByUser(split[0]);
                        if (!activeTestDescriptionControllerShown) {
                            activeTestDescriptionControllerShown = true;
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_DESCRIPTION_STAGE);
                            }
                        }
                    }
                }
            });                    
        }
    }
    
    /**
     * This method updates the new "test description", marks the relevant rows 
     * as locked and calls the handler to mark the locked lines yellow.
     * 
     * @param pos   
     */
    private void handleTestShortDescription(EntryPosition pos) {
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml != null) {
            Platform.runLater(() -> {
                // Note:
                // An "IndexOutOfBoundsException" would occur in
                // "ReadOnlyUnbackedObservableList.java", if the
                // "runLater" is not used!
                
                TestDataModel e;
                int fileNumber;
                int j;
                int size;
                
                // force the whole table to refresh by changing column
                // visibility on and off
                //
                // see: http://jaakkola.net/juhani/blog/?p=264
                // refresh "tableField"
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField1"
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField1.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField2"
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField2.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField3"
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField3.getColumns().get(0))
                        .setVisible(true);
                // refresh "tableField4"
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(false);
                ((TableColumn) tableField4.getColumns().get(0))
                        .setVisible(true);
                
                fileNumber = pos.getFileNumber();
                size = changedFileList.get(fileNumber).getLineNumber().size();
                
                for (int i = 0; i < size; i++) {
                    j = changedFileList.get(fileNumber).getTableViewLineNumber(i);
                    
                    String testShortDescriptionLast;
                    testShortDescriptionLast = conv.convertDescriptionString(
                            changedFileList.get(fileNumber)
                            .getTestShortDescriptionLast(i));
                    
                    xml.putLockedRowsList(
                            changedFileList.get(fileNumber).getLoginName(),
                            j, fileNumber);
                    
                    if (pos.getChangedLineNumber() == i) {
                        // do not exchange the following
                        // two lines!
                        testShortDescriptionColBooleanList.get(fileNumber)
                                .set(i, true);
                        
                        // call handler
                        testShortDescriptionColList.get(fileNumber).get(i)
                                .setText(testShortDescriptionLast);
                        
                    }
                    if (activeTestDescriptionController == pos.getFileNumber()) {
                        // The stage "TEST_DESCRIPTION_STAGE" is active.
                        // Do a transition from editable to non-editable,
                        // because the line is locked by another user.
                        FXMLLoader loader;
                        DialogTestDescriptionController dialogTestDescriptionController;
                        
                        loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
                        dialogTestDescriptionController = loader.
                                <DialogTestDescriptionController>getController();
                        
                        dialogTestDescriptionController.setEditable(false);
                        String loginName = changedFileList.get(fileNumber)
                                .getLoginName();
                        String[] split = loginName.split("-");
                        dialogTestDescriptionController.setLockedByUser(split[0]);
                        if (!activeTestDescriptionControllerShown) {
                            activeTestDescriptionControllerShown = true;
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_DESCRIPTION_STAGE);
                            }
                        }
                    }
                }
            });                    
        }
    }

    /**
     * This method updates the new "caption" and marks it yellow (locked).
     * 
     */
    private void handleCaption() {
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml != null) {
            Platform.runLater(() -> {
                // Note:
                // An "IndexOutOfBoundsException" would occur in
                // "ReadOnlyUnbackedObservableList.java", if the
                // "runLater" is not used!
                FXMLLoader loader;
                loader = myController.getLoader(CAPTION_RENAME_STAGE);
                dialogRenameCaptionController = loader.
                        <DialogRenameCaptionController>getController();
                
                if (xml.writeCaptionLock(configEntry)) {
                    labelCaption.setStyle("-fx-background-color: yellow");
                    labelCaption.setText(configEntry.getCaptionLast());
                    
                    dialogRenameCaptionController.setCaptionName(labelCaption
                            .getText());
                    dialogRenameCaptionController.setTableEntry(configEntry);
                    dialogRenameCaptionController.setLockedByUser(
                            configEntry.getCaptionAuthorLast());
                    dialogRenameCaptionController.setEditable(false);
                } else {
                    labelCaption.setStyle("");
                    labelCaption.setText(configEntry.getCaptionLast());
                    
                    dialogRenameCaptionController.setCaptionName(labelCaption
                            .getText());
                    dialogRenameCaptionController.setTableEntry(configEntry);
                    dialogRenameCaptionController.setLockedByUser(
                            configEntry.getCaptionAuthorLast());
                    dialogRenameCaptionController.setEditable(true);
                }
            });                    
        }
    }

    public void initCaption() {
        // context menu for caption
        ContextMenu captionContextMenu = new ContextMenu();
        MenuItem captionEditMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("contextMenuItemEdit"));
        captionEditMenuItem.setOnAction((ActionEvent event) -> {
            dialogCaption();
        });
        MenuItem captionHistoryMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("contextMenuItemHistory"));
        captionHistoryMenuItem.setOnAction((ActionEvent event) -> {
            FXMLLoader loader;
            DialogCaptionHistoryController dialogCaptionHistoryController;

            loader = myController.getLoader(CAPTION_HISTORY_STAGE);
            dialogCaptionHistoryController = loader.
                    <DialogCaptionHistoryController>getController();
            dialogCaptionHistoryController.setTable(configEntry);
            myController.showStage(CAPTION_HISTORY_STAGE);
        });
        captionContextMenu.getItems().add(captionEditMenuItem);
        captionContextMenu.getItems().add(captionHistoryMenuItem);
        labelCaption.setContextMenu(captionContextMenu);
        labelCaption.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    if(mouseEvent.getClickCount() == 2){
                        // Mouse double clicked
                        dialogCaption();
                    }
                }
            }
        });        
        
        FXMLLoader loader;
        loader = myController.getLoader(MAIN_STAGE_1);
        lxrDifferenceTableController = loader.
                <LxrDifferenceTableController>getController();
        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml != null) {
            if (xml.isCaptionLocked() == LockType.locked) {
                labelCaption.setStyle("-fx-background-color: yellow");
            }
        }
    }
    
    public void initTestTable(TableView<TestDataModel> tableField) {
        //Create a customer cell factory so that cells can support editing.
        Callback<TableColumn<TestDataModel, String>, TableCell<TestDataModel, String>> cellFactory = 
                new Callback<TableColumn<TestDataModel, String>, TableCell<TestDataModel, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                TestDataEditingCell cell = new TestDataEditingCell();
                cell.widthProperty()
                        .addListener((ObservableValue<? extends Number> observableValue, 
                                Number oldSceneWidth, Number newSceneWidth) -> {
                    Text text = new Text();
                    cell.setGraphic(text);
                    text.wrappingWidthProperty().bind(cell.widthProperty());
                    text.textProperty().bind(cell.itemProperty());
                });
                return cell;
            }
        };

        // clear old values
        if (!tableField.getColumns().isEmpty()) {
            // clear helper lists
            reviewStateColList.clear();
            reviewStateColBooleanList.clear();
            testStateColList.clear();
            testStateColBooleanList.clear();
            reviewShortDescriptionColList.clear();
            reviewShortDescriptionColBooleanList.clear();
            testShortDescriptionColList.clear();
            testShortDescriptionColBooleanList.clear();
            tableField.getItems().clear();      // clear content
            tableField.getColumns().clear();    // clear rows
        } 
        
        // init tableview
        columnWithFileCount = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemFileCount"));
        columnWithFileCount.setCellValueFactory(
                new PropertyValueFactory<>("fileCount"));
        columnWithLink = new TableColumn<>("Link");
        columnWithLink.setCellValueFactory(
                new PropertyValueFactory<>("hrefText"));
        columnWithLink.setCellFactory(column -> {
                return new TableCell<TestDataModel, Text>() {
                    @Override
                    protected void updateItem(Text item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                            setText(null);
                            setStyle("");
                        } else {      
                            item.setFill(Color.BLUE);
                            setGraphic(item);
                            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        }
                    }
                };
        });
        columnWithLineNumber = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemLineNumber"));
        columnWithLineNumber.setCellValueFactory(
                new PropertyValueFactory<>("lineNumber"));
        columnWithLineNumber.setCellFactory(cellFactory);
        columnWithLineNumber.setComparator(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                // Note:
                // This column has the following values:
                //          0000 .. 9999 .. 
                //          deleted
                //          new
                //          unchanged
                int i1 = 0;
                boolean i1Value;
                try {
                    i1 = Integer.parseInt(o1);
                    i1Value = true;
                } catch (Exception ex) {
                    i1Value = false;
                }
                int i2 = 0;
                boolean i2Value;
                try {
                    i2 = Integer.parseInt(o2);
                    i2Value = true;
                } catch (Exception ex) {
                    i2Value = false;
                }
                if (i1Value && i2Value) {
                    if(i1<i2)return -11;
                    if(i1>i2)return 1;
                    return 0;                
                } else {
                    if (i1Value) {
                        return 1;
                    } else if (i2Value) {
                        return -1;
                    } else {
                        return o1.compareTo(o2);
                    }
                }
            }
        });
        if (svnAvailable) {
            columnWithSvnVersion = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemSvnVersion"));
            columnWithSvnVersion.setCellValueFactory(
                    new PropertyValueFactory<>("svnVersion"));
            columnWithSvnVersion.setCellFactory(cellFactory);
            columnWithSvnDesc = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemSvnDescription"));
            columnWithSvnDesc.setCellValueFactory(
                    new PropertyValueFactory<>("svnDesc"));
            columnWithSvnDesc.setCellFactory(cellFactory);
            columnWithSvnAuthor = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemSvnAuthor"));
            columnWithSvnAuthor.setCellValueFactory(
                    new PropertyValueFactory<>("svnAuthor"));
            columnWithSvnAuthor.setCellFactory(cellFactory);
            columnWithSvnDate = new TableColumn<>(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("tableItemSvnDate"));
            columnWithSvnDate.setCellValueFactory(
                    new PropertyValueFactory<>("svnDate"));
            columnWithSvnDate.setCellFactory(cellFactory);
        }
        columnWithReviewState = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemReviewState"));
        columnWithReviewState.setCellValueFactory(
                new PropertyValueFactory<>("reviewState"));
        columnWithReviewState.setComparator(new Comparator<ComboBox<State>>() {
            @Override
            public int compare(ComboBox<State> o1, ComboBox<State> o2) {
                State value1 = o1.getValue();
                State value2 = o2.getValue();
                int compareTo = value1.compareTo(value2);
                return compareTo;
            }
        });
        columnWithReviewShortDescription = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemReviewShortDescription"));
        columnWithReviewShortDescription.setCellValueFactory(
                new PropertyValueFactory<>("reviewShortDescription"));
        columnWithReviewShortDescription.setComparator(
                new Comparator<TextField>() {
            @Override
            public int compare(TextField o1, TextField o2) {
                String str1 = o1.getText();
                String str2 = o2.getText();
                return str1.compareTo(str2);
            }
        });
        columnWithTestState = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemTestState"));
        columnWithTestState.setCellValueFactory(
                new PropertyValueFactory<>("testState"));
        columnWithTestState.setComparator(new Comparator<ComboBox<State>>() {
            @Override
            public int compare(ComboBox<State> o1, ComboBox<State> o2) {
                State value1 = o1.getValue();
                State value2 = o2.getValue();
                int compareTo = value1.compareTo(value2);
                return compareTo;
            }
        });
        columnWithTestShortDescription = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("tableItemTestShortDescription"));
        columnWithTestShortDescription.setCellValueFactory(
                new PropertyValueFactory<>("testShortDescription"));
        columnWithTestShortDescription.setComparator(
                new Comparator<TextField>() {
            @Override
            public int compare(TextField o1, TextField o2) {
                String str1 = o1.getText();
                String str2 = o2.getText();
                return str1.compareTo(str2);
            }
        });

        if (svnAvailable) {
            if (lineNumberAvailable) {
                tableField.getColumns().addAll(columnWithFileCount,
                        columnWithLink, 
                        columnWithLineNumber, 
                        columnWithSvnVersion,
                        columnWithSvnDesc, columnWithSvnAuthor, 
                        columnWithSvnDate,
                        columnWithReviewState, 
                        columnWithReviewShortDescription,
                        columnWithTestState, 
                        columnWithTestShortDescription);
            } else {
                tableField.getColumns().addAll(columnWithFileCount,
                        columnWithLink, 
                        columnWithSvnVersion,
                        columnWithSvnDesc, columnWithSvnAuthor, 
                        columnWithSvnDate,
                        columnWithReviewState, 
                        columnWithReviewShortDescription,
                        columnWithTestState, 
                        columnWithTestShortDescription);
            }
        } else {
            // Do not show the following fields:
            // - columnWithSvnVersion
            // - columnWithSvnDesc
            // - columnWithSvnAuthor
            // - columnWithSvnDate
            if (lineNumberAvailable) {
                tableField.getColumns().addAll(columnWithFileCount,
                        columnWithLink, 
                        columnWithLineNumber,
                        columnWithReviewState, 
                        columnWithReviewShortDescription,
                        columnWithTestState, 
                        columnWithTestShortDescription);
            } else {
                tableField.getColumns().addAll(columnWithFileCount,
                        columnWithLink,
                        columnWithReviewState, 
                        columnWithReviewShortDescription,
                        columnWithTestState, 
                        columnWithTestShortDescription);
            }
        }
        tableField.getSortOrder().addAll(columnWithFileCount, columnWithLineNumber);
        tableField.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // tableField.setFixedCellSize(70);
    }

    private void addNewLineInTable(TableView<TestDataModel> tableField, 
            TableEntry entry, 
            int tableNr,
            int fileNr,
            int changedLineNr,
            boolean computeStatistics,
            String fileCount,
            String href, String hrefText,
            String lineNumber, String svnVersion, String svnDesc,
            String svnAuthor, String svnDate, boolean svn,
            State reviewState,
            String reviewShortDescription,
            State testState,
            String testShortDescription)
    {
        if (!isRowHidden(entry, changedLineNr)) {
            // compute statistical data
            if (computeStatistics) {
                String[] split = svnAuthor.split("\n");
                for (String svnUser : split) {
                    if (svnUser.equals("")) {
                        // do nothing (empty line)
                    } else if (userMap.get(svnUser) == null) {
                        // save new svn user to hash map and 
                        // initialize the number of commits to 1
                        userMap.put(svnUser, userNumber);
                        commitMap.put(userNumber, 1);
                        userNumber++;
                    } else {
                        // Increment the number of commits to
                        // the given user
                        Integer count = commitMap.get(userMap.get(svnUser));
                        count++;
                        commitMap.put(userMap.get(svnUser), count);
                    }
                }
                statistics.incrementByOneNumberOfEntries();
                int fileCountInt = Integer.parseInt(fileCount); 
                try {
                    Integer.parseInt(lineNumber);
                    statistics.incrementByOneNumberOfChanges();
                    if (lastFileNr != fileCountInt) {
                        // It's a changed file and it's the first change.
                        statistics.incrementByOneNumberOfChangedFiles();
                    }
                } catch (Exception ex) {
                    switch (lineNumber) {
                        case "fileDeleted":
                            statistics.incrementByOneNumberOfDeletedFiles();
                            break;
                        case "directoryDeleted":
                            statistics.incrementByOneNumberOfDeletedFiles();
                            break;
                        case "newDirectory":
                            statistics.incrementByOneNumberOfNewFiles();
                            break;
                        case "newFile":
                            statistics.incrementByOneNumberOfNewFiles();
                            break;
                        case "notChanged":
                            statistics.incrementByOneNumberOfUnmodifiedFiles();
                            break;
                        case "dublicatedFile":
                            statistics.incrementByOneNumberOfDublicatedFiles();
                            break;
                        default:
                            break;
                    }
                }
                switch (reviewState) {
                    case PASSED:
                        statistics.incrementByOneReviewPassed();
                        break;
                    case FAILED:
                        statistics.incrementByOneReviewFailed();
                        break;
                    case COMMENT:
                        statistics.incrementByOneReviewComment();
                        break;
                    case INCONCLUSIVE:
                        statistics.incrementByOneReviewInconclusive();
                        break;
                    case OPEN:
                        statistics.incrementByOneReviewOpen();
                        break;
                    case NOTAPPLICABLE:
                        statistics.incrementByOneReviewNotApplicable();
                        break;
                    default:
                        statistics.incrementByOneReviewEmpty();
                        break;
                }
                switch (testState) {
                    case PASSED:
                        statistics.incrementByOneTestPassed();
                        break;
                    case FAILED:
                        statistics.incrementByOneTestFailed();
                        break;
                    case COMMENT:
                        statistics.incrementByOneTestComment();
                        break;
                    case INCONCLUSIVE:
                        statistics.incrementByOneTestInconclusive();
                        break;
                    case OPEN:
                        statistics.incrementByOneTestOpen();
                        break;
                    case NOTAPPLICABLE:
                        statistics.incrementByOneTestNotApplicable();
                        break;
                    default:
                        statistics.incrementByOneTestEmpty();
                        break;
                }
                lastFileNr = fileCountInt;                    // update value
            }

            String fileCountCol = fileCount;
            
            Hyperlink hrefCol = new Hyperlink(href);        // full link (not shown)!
            
            // split file and path for better representation purposes!
            String fname = hrefText.replace("\\", "/");     // convert to linux path names
            File f = new File (fname);
            String name = f.getName();
            String path = f.getParent();
            if (path == null) {
                path = "";
            }
            Text hrefTextCol = new Text(name + "\n" + path);
            hrefTextCol.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    new Thread(() -> {
                        try {
                            // REQ493
                            String modifiedHref = getModifiedHref(hrefCol.getText());
                            String filename = getDiffFileName(hrefCol.getText());
                            if (filename == "") {
                                filename = getSourceFileName(hrefCol.getText());
                            }
                            final String diffFile = filename;
                            if (version1.equals("") || version2.equals("")) {
                                extractVersions();      // called only once
                            }
                            ownBrowing();
                            if (ownBrowing) {
                                FXMLLoader loader;
                                loader = myController.getLoader(BROWSE_DIFF_STAGE);
                                dialogBrowseDiffController = loader.
                                        <DialogBrowseDiffController>getController();
                                position = getPosition(hrefCol.getText());

                                Platform.runLater(() -> {
                                    dialogBrowseDiffController.setDiffFile(diffFile, version1, version2, position);
                                    myController.showStage(BROWSE_DIFF_STAGE);
                                });                    
                            } else {
                                // REQ147 - Func/Table - Link - Open Default Browser on Click
                                Desktop.getDesktop().browse(new URI(modifiedHref));
                            }
                            
                            
                        } catch (IOException | URISyntaxException ex) {
                            // REQ76 - Error Handling - No Error Window
                            // 
                            // An exception occured while opening the browser. 
                            // Probably, the link is not shown in a browser! 
                            // 
                            // Error handling:
                            // Do not show or report an exception, because
                            //    1. the user is aware of the problem (he/she doesn't see
                            //       the content of the clicked link in a browser and
                            //    2. the problem cannot be resolved by the user
                            logger.severe("Browser Window not opened!");
                        }
                    }).start();
                    // select line
                    tableField.getSelectionModel().select(tableNr);
                    hrefTextCol.setUnderline(true);
                }
            });
            
            ContextMenu reviewStateHistoryContextMenu = new ContextMenu();
            MenuItem reviewStateHistoryMenuItem = new MenuItem(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("menuItemHistory"));
            reviewStateHistoryMenuItem.setOnAction((ActionEvent event) -> {
                // select line
                tableField.getSelectionModel().select(tableNr);

                FXMLLoader loader;
                DialogStateHistoryController dialogStateHistoryController;

                loader = myController.getLoader(STATE_HISTORY_STAGE);
                dialogStateHistoryController = loader.
                        <DialogStateHistoryController>getController();
                dialogStateHistoryController.setTable(entry, changedLineNr, true);
                myController.showStage(STATE_HISTORY_STAGE);
            });
            reviewStateHistoryContextMenu.getItems().add(reviewStateHistoryMenuItem);

            ContextMenu testStateHistoryContextMenu = new ContextMenu();
            MenuItem testStateHistoryMenuItem = new MenuItem(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("menuItemHistory"));
            testStateHistoryMenuItem.setOnAction((ActionEvent event) -> {
                // select line
                tableField.getSelectionModel().select(tableNr);

                FXMLLoader loader;
                DialogStateHistoryController dialogStateHistoryController;

                loader = myController.getLoader(STATE_HISTORY_STAGE);
                dialogStateHistoryController = loader.
                        <DialogStateHistoryController>getController();
                dialogStateHistoryController.setTable(entry, changedLineNr, false);
                myController.showStage(STATE_HISTORY_STAGE);
            });
            testStateHistoryContextMenu.getItems().add(testStateHistoryMenuItem);

            ContextMenu reviewDescriptionHistoryContextMenu = new ContextMenu();
            MenuItem reviewDescriptionHistoryMenuItem = new MenuItem(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("menuItemHistory"));
            reviewDescriptionHistoryMenuItem.setOnAction((ActionEvent event) -> {
                // select line
                tableField.getSelectionModel().select(tableNr);

                int size = entry.getReviewDescription(changedLineNr).size();
                if ((size == 0) || (entry.getReviewDescriptionLast(changedLineNr)
                        .equals(""))) {
                    FXMLLoader loader;
                    DialogDescriptionHistoryController dialogDescriptionHistoryController;
                    loader = myController.getLoader(DESC_HISTORY_STAGE);
                    dialogDescriptionHistoryController = loader.
                            <DialogDescriptionHistoryController>getController();
                    dialogDescriptionHistoryController.setTable(entry, changedLineNr, 
                            true);
                    myController.showStage(DESC_HISTORY_STAGE);
                } else {
                    FXMLLoader loader;
                    DialogDescriptionHistory1Controller dialogDescriptionHistory1Controller;
                    loader = myController.getLoader(DESC_HISTORY_STAGE1);
                    dialogDescriptionHistory1Controller = loader.
                            <DialogDescriptionHistory1Controller>getController();
                    dialogDescriptionHistory1Controller.setTable(entry, changedLineNr, 
                            true);
                    myController.showStage(DESC_HISTORY_STAGE1);
                }
            });
            reviewDescriptionHistoryContextMenu.getItems().add(
                    reviewDescriptionHistoryMenuItem);

            ContextMenu testDescriptionHistoryContextMenu = new ContextMenu();
            MenuItem testDescriptionHistoryMenuItem = new MenuItem(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("menuItemHistory"));
            testDescriptionHistoryMenuItem.setOnAction((ActionEvent event) -> {
                // select line
                tableField.getSelectionModel().select(tableNr);

                int size = entry.getReviewDescription(changedLineNr).size();
                if ((size == 0) || (entry.getReviewDescriptionLast(changedLineNr)
                        .equals(""))) {
                    FXMLLoader loader;
                    DialogDescriptionHistoryController dialogDescriptionHistoryController;

                    loader = myController.getLoader(DESC_HISTORY_STAGE);
                    dialogDescriptionHistoryController = loader.
                            <DialogDescriptionHistoryController>getController();
                    dialogDescriptionHistoryController.setTable(entry, changedLineNr, 
                            false);
                    myController.showStage(DESC_HISTORY_STAGE);
                } else {
                    FXMLLoader loader;
                    DialogDescriptionHistory1Controller dialogDescriptionHistory1Controller;

                    loader = myController.getLoader(DESC_HISTORY_STAGE1);
                    dialogDescriptionHistory1Controller = loader.
                            <DialogDescriptionHistory1Controller>getController();
                    dialogDescriptionHistory1Controller.setTable(entry, changedLineNr, 
                            false);
                    myController.showStage(DESC_HISTORY_STAGE1);
                }
            });
            testDescriptionHistoryContextMenu.getItems().add(
                    testDescriptionHistoryMenuItem);

            String svnVersionCol = truncateLine(svnVersion, 20);
            String svnDescCol = truncateLine(svnDesc, 20);
            String svnAuthorCol = truncateLine(svnAuthor, 20);
            String svnDateCol = truncateLine(svnDate, 20);
            
            // svnVersionCol.setContextMenu(svnContextMenu);    // does not function on String!
            // svnDescCol.setContextMenu(svnContextMenu);       // does not function on String!
            // svnDateCol.setContextMenu(svnContextMenu);       // does not function on String!
            // svnAuthorCol.setContextMenu(svnContextMenu);     // does not function on String!

            ComboBox<State> reviewStateCol = new ComboBox<>();
            helperReviewState(fileNr, changedLineNr, reviewStateCol);
            
            reviewStateCol.setContextMenu(reviewStateHistoryContextMenu);
            reviewStateCol.getItems().addAll(State.values());
            reviewStateCol.getSelectionModel().select(reviewState);
            reviewStateCol.setPrefHeight(38.0);
            reviewStateCol.setCellFactory(new Callback<ListView<State>, 
                    ListCell<State>>() {
              @Override
              public ListCell<State> call(ListView<State> p) {
                return new StateListCell();
              }
            });
            reviewStateCol.setButtonCell(new StateListCell());
            reviewStateCol.getSelectionModel().selectedItemProperty()
                    .addListener(new ChangeListener<State>() {
              @Override
              public void changed(ObservableValue<? extends State> ov, State t, 
                      State t1) {
                String stateName = t1.getName();
                Image stateIcon = t1.getIcon();
                // do whatever you need with the stateName and icon...
              }
            });


            reviewStateCol.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent t) {
                    // select line
                    tableField.getSelectionModel().select(tableNr);

                    XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
                    LockType locked = xml.isLocked(Integer.parseInt(
                            entry.getFileCount())-1);
                    if ((locked == LockType.locked) && 
                            (!reviewStateColBooleanList.get(fileNr-1)
                                    .get(changedLineNr))) {
                        // Restore original state!
                        Platform.runLater(() -> {
                            // Note:
                            // An "IndexOutOfBoundsException" would occur in
                            // "ReadOnlyUnbackedObservableList.java", if the
                            // "runLater" is not used!
                            State reviewStateLast = entry
                                    .getReviewStateLast(changedLineNr);
                            reviewStateCol.getSelectionModel()
                                    .select(reviewStateLast);
                        });                    

                        // Lock is active. Access is denied. A message is
                        // populated to the user.
                        if (xml.isReadOnly()) {
                            myController.showStage(READ_ONLY_MODE_STAGE);
                        } else {
                            myController.showStage(LOCKED_STAGE);
                        }
                    } else if (reviewStateColBooleanList.get(fileNr-1)
                            .get(changedLineNr)) {
                        // do nothing, because the result is already written to
                        // the local table entry

                        // reset to default state
                        reviewStateColBooleanList.get(fileNr-1).set(changedLineNr, 
                                false);
                    } else {
                        if (xml.writeReviewResultLock(entry)) {
                            // Lock is gotten! We can proceed!

                            // save changed user and date
                            State selectedItem = reviewStateCol.getSelectionModel().
                                    getSelectedItem();

                            // update statistics
                            State lastItem = entry.getReviewStateLast(changedLineNr);
                            statistics.updateReview(selectedItem, lastItem);
                            writeReviewStatisticsField();

                            String user = System.getProperty("user.name");
                            String date = ZonedDateTime.now().
                                    format(DateTimeFormatter.RFC_1123_DATE_TIME);


                            entry.putReviewState(changedLineNr, 
                                    State.getEnum(selectedItem.toString()));
                            entry.putReviewResAuthor(changedLineNr, user);
                            entry.putReviewResDate(changedLineNr, date);

                            try {
                                if (locked == LockType.notLocked) {
                                    xml.writeReviewResult(changedLineNr, entry);
                                } else if (locked == LockType.ownLocked) {
                                    xml.writeReviewResult(changedLineNr, entry);
                                }
                            } catch (IOException ex) {
                                logger.info("LxrTestTableController: " + 
                                        "An exception occured during writing "
                                        + "the review result to a file");
                            }
                        } else {
                            // Lock is not gotten!
                            //
                            // Restore original state!
                            Platform.runLater(() -> {
                                // Note:
                                // An "IndexOutOfBoundsException" would occur in
                                // "ReadOnlyUnbackedObservableList.java", if the
                                // "runLater" is not used!
                                State reviewStateLast = entry
                                        .getReviewStateLast(changedLineNr);
                                reviewStateCol.getSelectionModel()
                                        .select(reviewStateLast);
                                logger.info("LxrTestTableController: " + 
                                        "reviewStateLast = " + reviewStateLast);
                            });                    

                            // Lock is active. Access is denied. A message is
                            // populated to the user.
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_STAGE);
                            }
                        }
                    }
                }
            });

            ComboBox<State>testStateCol = new ComboBox<>();
            helperTestState(fileNr, changedLineNr, testStateCol);
            testStateCol.setContextMenu(testStateHistoryContextMenu);
            testStateCol.getItems().addAll(State.values());
            testStateCol.getSelectionModel().select(testState);
            testStateCol.setPrefHeight(32.0);
            testStateCol.setCellFactory(new Callback<ListView<State>, 
                    ListCell<State>>() {
              @Override
              public ListCell<State> call(ListView<State> p) {
                return new StateListCell();
              }
            });
            testStateCol.setButtonCell(new StateListCell());
            testStateCol.getSelectionModel().selectedItemProperty().addListener(
                    new ChangeListener<State>() {
              @Override
              public void changed(ObservableValue<? extends State> ov, State t, 
                      State t1) {
                String stateName = t1.getName();
                Image stateIcon = t1.getIcon();
                // do whatever you need with the stateName and icon...
              }
            });
            testStateCol.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent t) {
                    // select line
                    tableField.getSelectionModel().select(tableNr);

                    XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
                    LockType locked = xml.isLocked(Integer.parseInt(entry
                            .getFileCount())-1);
                    if ((locked == LockType.locked) && 
                            (!testStateColBooleanList.get(fileNr-1)
                                    .get(changedLineNr))) {
                        // Restore original state!
                        Platform.runLater(() -> {
                            // Note:
                            // An "IndexOutOfBoundsException" would occur in
                            // "ReadOnlyUnbackedObservableList.java", if the
                            // "runLater" is not used!
                            State testStateLast = entry.getTestStateLast(
                                    changedLineNr);
                            testStateCol.getSelectionModel().select(testStateLast);
                        });                    

                        // Lock is active. Access is denied. A message is
                        // populated to the user.
                        if (xml.isReadOnly()) {
                            myController.showStage(READ_ONLY_MODE_STAGE);
                        } else {
                            myController.showStage(LOCKED_STAGE);
                        }
                    } else if (testStateColBooleanList.get(fileNr-1)
                            .get(changedLineNr)) {
                        // do nothing, because the result is already written to
                        // the local table entry

                        // reset to default state
                        testStateColBooleanList.get(fileNr-1).set(changedLineNr, 
                                false);
                    } else {
                        if (xml.writeTestResultLock(entry)) {
                            // Lock is gotten! We can proceed!

                            // save changed user and date
                            State selectedItem = testStateCol.getSelectionModel()
                                    .getSelectedItem();

                            // update statistics
                            State lastItem = entry.getTestStateLast(changedLineNr);
                            statistics.updateTest(selectedItem, lastItem);
                            writeTestStatisticsField();

                            String user = System.getProperty("user.name");
                            String date = ZonedDateTime.now().
                                    format(DateTimeFormatter.RFC_1123_DATE_TIME);


                            entry.putTestState(changedLineNr, selectedItem);
                            entry.putTestResAuthor(changedLineNr, user);
                            entry.putTestResDate(changedLineNr, date);

                            try {
                                if (locked == LockType.notLocked) {
                                    xml.writeTestResult(changedLineNr, entry);
                                } else if (locked == LockType.ownLocked) {
                                    xml.writeTestResult(changedLineNr, entry);
                                }
                            } catch (IOException ex) {
                                logger.info("LxrTestTableController: " + 
                                        "An exception occured during writing "
                                        + "the test result to a file");
                            }
                        } else {
                            // Lock is not gotten!
                            //
                            // Restore original state!
                            Platform.runLater(() -> {
                                // Note:
                                // An "IndexOutOfBoundsException" would occur in
                                // "ReadOnlyUnbackedObservableList.java", if the
                                // "runLater" is not used!
                                State testStateLast = entry.getTestStateLast(
                                        changedLineNr);
                                testStateCol.getSelectionModel()
                                        .select(testStateLast);
                                logger.info("LxrTestTableController: " + 
                                        "testStateLast = " + testStateLast);
                            });                    

                            // Lock is active. Access is denied. A message is
                            // populated to the user.
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_STAGE);
                            }
                        }
                    }
                }
            });

            TextField reviewShortDescriptionCol = new TextField(
                    reviewShortDescription);
            helperReviewShortDescription(fileNr, changedLineNr,
                    reviewShortDescriptionCol);
            reviewShortDescriptionCol.setContextMenu(
                    reviewDescriptionHistoryContextMenu);
            Tooltip tooltipLongDescription = new Tooltip(ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("tooltipLongDescription"));
            reviewShortDescriptionCol.setTooltip(tooltipLongDescription);
            reviewShortDescriptionCol.setOnMouseClicked(
                    new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getButton().equals(MouseButton.PRIMARY)){
                        if(event.getClickCount() == 2){ // on double-click
                            // select line
                            tableField.getSelectionModel().select(tableNr);

                            XmlHandling xml = lxrDifferenceTableController
                                    .getXmlHandling();
                            LockType locked = xml.isLocked(Integer.parseInt(
                                    entry.getFileCount())-1);
                            FXMLLoader loader;
                            DialogReviewDescriptionController dialogReviewDescriptionController;

                            loader = myController.getLoader(REVIEW_DESCRIPTION_STAGE);
                            dialogReviewDescriptionController = loader.
                                    <DialogReviewDescriptionController>getController();
                            if (locked == LockType.locked) {
                                dialogReviewDescriptionController
                                        .setShortDescriptionField(
                                                reviewShortDescriptionCol);
                                dialogReviewDescriptionController
                                        .setTableEntry(entry, changedLineNr);
                                dialogReviewDescriptionController.setEditable(false);
                                String loginName = entry.getLoginName();
                                String[] split = loginName.split("-");
                                dialogReviewDescriptionController
                                        .setLockedByUser(split[0]);

                                // show controller
                                activeReviewDescriptionController = Integer.parseInt(
                                        entry.getFileCount()) - 1;
                                activeReviewDescriptionControllerShown = true;
                                myController.showStage(REVIEW_DESCRIPTION_STAGE);

                            } else {
                                dialogReviewDescriptionController
                                        .setShortDescriptionField(reviewShortDescriptionCol);
                                dialogReviewDescriptionController
                                        .setTableEntry(entry, changedLineNr);
                                dialogReviewDescriptionController.setEditable(true);
                                String loginName = entry.getLoginName();
                                String[] split = loginName.split("-");
                                dialogReviewDescriptionController
                                        .setLockedByUser(split[0]);

                                // show controller
                                activeReviewDescriptionController = Integer.parseInt(
                                        entry.getFileCount()) - 1;
                                activeReviewDescriptionControllerShown = false;
                                myController.showStage(REVIEW_DESCRIPTION_STAGE);
                            }
                        }
                    }      
                }
            });
            reviewShortDescriptionCol.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent keyEvent) {
                    if (keyEvent.getCode() == KeyCode.ENTER) {
                        // select line
                        tableField.getSelectionModel().select(tableNr);

                        XmlHandling xml = lxrDifferenceTableController
                                .getXmlHandling();
                        LockType locked = xml.isLocked(Integer.parseInt(
                                entry.getFileCount())-1);
                        FXMLLoader loader;
                        DialogReviewDescriptionController dialogReviewDescriptionController;

                        loader = myController.getLoader(REVIEW_DESCRIPTION_STAGE);
                        dialogReviewDescriptionController = loader.
                                <DialogReviewDescriptionController>getController();
                        if (locked == LockType.locked) {
                            dialogReviewDescriptionController
                                    .setShortDescriptionField(reviewShortDescriptionCol);
                            dialogReviewDescriptionController
                                    .setTableEntry(entry, changedLineNr);
                            dialogReviewDescriptionController.setEditable(false);
                            String loginName = entry.getLoginName();
                            String[] split = loginName.split("-");
                            dialogReviewDescriptionController.setLockedByUser(split[0]);

                            // show controller
                            activeReviewDescriptionController = Integer.parseInt(
                                    entry.getFileCount()) - 1;
                            activeReviewDescriptionControllerShown = true;
                            myController.showStage(REVIEW_DESCRIPTION_STAGE);

                        } else {
                            dialogReviewDescriptionController
                                    .setShortDescriptionField(reviewShortDescriptionCol);
                            dialogReviewDescriptionController
                                    .setTableEntry(entry, changedLineNr);
                            dialogReviewDescriptionController.setEditable(true);
                            String loginName = entry.getLoginName();
                            String[] split = loginName.split("-");
                            dialogReviewDescriptionController
                                    .setLockedByUser(split[0]);

                            // show controller
                            activeReviewDescriptionController = Integer.parseInt(
                                    entry.getFileCount()) - 1;
                            activeReviewDescriptionControllerShown = false;
                            myController.showStage(REVIEW_DESCRIPTION_STAGE);
                        }
                    }                    
                }
            });
            reviewShortDescriptionCol.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent t) {
                    XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
                    LockType locked = xml.isLocked(Integer.parseInt(entry
                            .getFileCount())-1);
                    if ((locked == LockType.locked) && 
                            (!reviewShortDescriptionColBooleanList.get(fileNr-1)
                                    .get(changedLineNr))) {

                        // Open event-handler by typing the return key!
                        FXMLLoader loader;
                        DialogReviewDescriptionController dialogReviewDescriptionController;

                        loader = myController.getLoader(REVIEW_DESCRIPTION_STAGE);
                        dialogReviewDescriptionController = loader.
                                <DialogReviewDescriptionController>getController();

                        String text = reviewShortDescriptionCol.getText();
                        String last = conv.convertDescriptionString(
                                entry.getReviewShortDescriptionLast(
                                changedLineNr));
                        if (!text.equals(last)) {
                            // Restore original state!
                            Platform.runLater(() -> {
                                // Note:
                                // An "IndexOutOfBoundsException" would occur in
                                // "ReadOnlyUnbackedObservableList.java", if the
                                // "runLater" is not used!
                                String reviewDescriptionLast = entry
                                        .getReviewDescriptionLast(changedLineNr);
                                reviewShortDescriptionCol.setText(
                                        reviewDescriptionLast);
                            });                    

                            // Lock is active. Access is denied. A message is
                            // populated to the user.
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_STAGE);
                            }
                        }
                    } else if (reviewShortDescriptionColBooleanList.get(fileNr-1)
                            .get(changedLineNr)) {
                        // do nothing, because the result is already written to
                        // the local table entry

                        // reset to default state
                        reviewShortDescriptionColBooleanList.get(fileNr-1)
                                .set(changedLineNr, false);
                    } else {
                        // Open event-handler by typing the return key!
                        FXMLLoader loader;
                        DialogReviewDescriptionController dialogReviewDescriptionController;

                        loader = myController.getLoader(REVIEW_DESCRIPTION_STAGE);
                        dialogReviewDescriptionController = loader.
                                <DialogReviewDescriptionController>getController();

                        String text = reviewShortDescriptionCol.getText();
                        String last = conv.convertDescriptionString(
                                entry.getReviewShortDescriptionLast(
                                changedLineNr));
                        if (!text.equals(last)) {
                            if (xml.writeReviewShortDescriptionLock(entry)) {
                                // Lock is gotten! We can proceed!
                                // save changes!
                                entry.putReviewShortDescription(changedLineNr, 
                                        text);

                                // although douples the last description HTML field 
                                // (to hold it "in-line"
                                entry.putReviewDescription(changedLineNr, 
                                        entry.getReviewDescriptionLast(changedLineNr));

                                String user = System.getProperty("user.name");
                                entry.putReviewDescAuthor(changedLineNr, user);
                                String date = ZonedDateTime.now().
                                        format(DateTimeFormatter.RFC_1123_DATE_TIME);
                                entry.putReviewDescDate(changedLineNr, date);

                                try {
                                    if (locked == LockType.notLocked) {
                                        xml.writeReviewShortDescription(changedLineNr, 
                                                entry);
                                    } else if (locked == LockType.ownLocked) {
                                        xml.writeReviewShortDescription(changedLineNr, 
                                                entry);
                                    }
                                } catch (IOException ex) {
                                    logger.info("LxrTestTableController: " + 
                                            "An exception occured during writing "
                                            + "the review description to a file");
                                }

                                dialogReviewDescriptionController
                                        .setShortDescriptionField(reviewShortDescriptionCol);
                                dialogReviewDescriptionController
                                        .setTableEntry(entry, changedLineNr);

                                // show controller
                                activeReviewDescriptionController = Integer.parseInt(
                                        entry.getFileCount()) - 1;
                                activeReviewDescriptionControllerShown = false;
                                myController.showStage(REVIEW_DESCRIPTION_STAGE);

                            } else {
                                // Lock is not gotten!
                                //
                                // Restore original state!
                                Platform.runLater(() -> {
                                    // Note:
                                    // An "IndexOutOfBoundsException" would occur in
                                    // "ReadOnlyUnbackedObservableList.java", if the
                                    // "runLater" is not used!
                                    String reviewShortDescriptionLast = 
                                            conv.convertDescriptionString(entry
                                            .getReviewShortDescriptionLast(
                                                    changedLineNr));
                                    reviewShortDescriptionCol.setText(
                                            reviewShortDescriptionLast);
                                    logger.info("LxrTestTableController: " + 
                                            "reviewShortDescriptionLast = " + 
                                            reviewShortDescriptionLast);
                                });                    

                                // Lock is active. Access is denied. A message is
                                // populated to the user.
                                if (xml.isReadOnly()) {
                                    myController.showStage(READ_ONLY_MODE_STAGE);
                                } else {
                                    myController.showStage(LOCKED_STAGE);
                                }
                            }
                        } else {
                            dialogReviewDescriptionController
                                    .setShortDescriptionField(reviewShortDescriptionCol);
                            dialogReviewDescriptionController
                                    .setTableEntry(entry, changedLineNr);

                            // show controller
                            activeReviewDescriptionController = Integer.parseInt(
                                    entry.getFileCount()) - 1;
                            activeReviewDescriptionControllerShown = false;
                            myController.showStage(REVIEW_DESCRIPTION_STAGE);
                        }
                    } 
                }
            });

            reviewShortDescriptionCol.focusedProperty().addListener(
                    new ChangeListener<Boolean>()
            {
                @Override
                public void changed(ObservableValue<? extends Boolean> arg0, 
                        Boolean oldPropertyValue, Boolean newPropertyValue)
                {
                    if (newPropertyValue)
                    {
                        // on focus
                    }
                    else
                    {
                        // off focus
                        XmlHandling xml = lxrDifferenceTableController
                                .getXmlHandling();
                        LockType locked = xml.isLocked(Integer.parseInt(
                                entry.getFileCount())-1);
                        if (locked == LockType.locked) {
                            // Open event-handler by typing the return key!
                            FXMLLoader loader;
                            DialogReviewDescriptionController dialogReviewDescriptionController;

                            loader = myController.getLoader(REVIEW_DESCRIPTION_STAGE);
                            dialogReviewDescriptionController = loader.
                                    <DialogReviewDescriptionController>getController();

                            String text = reviewShortDescriptionCol.getText();
                            String last = conv.convertDescriptionString(
                                    entry.getReviewShortDescriptionLast(
                                    changedLineNr));
                            if (!text.equals(last)) {
                                // Restore original state!
                                Platform.runLater(() -> {
                                    // Note:
                                    // An "IndexOutOfBoundsException" would occur in
                                    // "ReadOnlyUnbackedObservableList.java", if the
                                    // "runLater" is not used!
                                    String reviewShortDescriptionLast = 
                                            conv.convertDescriptionString(entry
                                            .getReviewShortDescriptionLast(changedLineNr));
                                    reviewShortDescriptionCol
                                            .setText(reviewShortDescriptionLast);
                                });                    

                                // Lock is active. Access is denied. A message is
                                // populated to the user.
                                if (xml.isReadOnly()) {
                                    myController.showStage(READ_ONLY_MODE_STAGE);
                                } else {
                                    myController.showStage(LOCKED_STAGE);
                                }
                            }
                        } else {

                            String text = reviewShortDescriptionCol.getText();
                            String last = conv.convertDescriptionString(
                                    entry.getReviewShortDescriptionLast(
                                    changedLineNr));
                            if (!text.equals(last)) {
                                if (xml.writeReviewShortDescriptionLock(entry)) {
                                    // Lock is gotten! We can proceed!
                                    // save changes!
                                    entry.putReviewShortDescription(changedLineNr, 
                                            text);

                                    // although douples the last description HTML 
                                    // field (to hold it "in-line"
                                    entry.putReviewDescription(changedLineNr, 
                                            entry.getReviewDescriptionLast(
                                                    changedLineNr));

                                    String user = System.getProperty("user.name");
                                    entry.putReviewDescAuthor(changedLineNr, user);
                                    String date = ZonedDateTime.now().
                                            format(DateTimeFormatter.RFC_1123_DATE_TIME);
                                    entry.putReviewDescDate(changedLineNr, date);

                                    try {
                                        if (locked == LockType.notLocked) {
                                            xml.writeReviewShortDescription(
                                                    changedLineNr, entry);
                                        } else if (locked == LockType.ownLocked) {
                                            xml.writeReviewShortDescription(
                                                    changedLineNr, entry);
                                        }
                                    } catch (IOException ex) {
                                        logger.info("LxrTestTableController: "
                                                + "An exception occured during "
                                                + "writing the review description "
                                                + "to a file");
                                    }
                                } else {
                                    // Lock is not gotten!
                                    //
                                    // Restore original state!
                                    Platform.runLater(() -> {
                                        // Note:
                                        // An "IndexOutOfBoundsException" would 
                                        // occur in 
                                        //"ReadOnlyUnbackedObservableList.java", 
                                        // if the "runLater" is not used!
                                        String reviewShortDescriptionLast = 
                                                conv.convertDescriptionString(entry
                                                .getReviewShortDescriptionLast(
                                                        changedLineNr));
                                        reviewShortDescriptionCol.setText(
                                                reviewShortDescriptionLast);
                                        logger.info("LxrTestTableController: " 
                                                + "reviewShortDescriptionLast = " 
                                                + reviewShortDescriptionLast);
                                    });                    

                                    // Lock is active. Access is denied. A message is
                                    // populated to the user.
                                    if (xml.isReadOnly()) {
                                        myController.showStage(READ_ONLY_MODE_STAGE);
                                    } else {
                                        myController.showStage(LOCKED_STAGE);
                                    }
                                }
                            }
                        }
                    }
                }
            });

            TextField testShortDescriptionCol = new TextField(testShortDescription);
            helperTestShortDescription(fileNr, changedLineNr,
                    testShortDescriptionCol);
            testShortDescriptionCol.setContextMenu(testDescriptionHistoryContextMenu);
            testShortDescriptionCol.setTooltip(tooltipLongDescription);
            testShortDescriptionCol.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getButton().equals(MouseButton.PRIMARY)){
                        if(event.getClickCount() == 2){ // on double-click
                            // select line
                            tableField.getSelectionModel().select(tableNr);

                            XmlHandling xml = lxrDifferenceTableController
                                    .getXmlHandling();
                            LockType locked = xml.isLocked(Integer.parseInt(
                                    entry.getFileCount())-1);
                            FXMLLoader loader;
                            DialogTestDescriptionController dialogTestDescriptionController;

                            loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
                            dialogTestDescriptionController = loader.
                                    <DialogTestDescriptionController>getController();
                            if (locked == LockType.locked) {
                                dialogTestDescriptionController
                                        .setShortDescriptionField(testShortDescriptionCol);
                                dialogTestDescriptionController
                                        .setTableEntry(entry, changedLineNr);
                                dialogTestDescriptionController.setEditable(false);
                                String loginName = entry.getLoginName();
                                String[] split = loginName.split("-");
                                dialogTestDescriptionController
                                        .setLockedByUser(split[0]);

                                // show controller
                                activeTestDescriptionController = Integer.parseInt(
                                        entry.getFileCount()) - 1;
                                activeTestDescriptionControllerShown = true;
                                myController.showStage(TEST_DESCRIPTION_STAGE);

                            } else {
                                dialogTestDescriptionController
                                        .setShortDescriptionField(testShortDescriptionCol);
                                dialogTestDescriptionController
                                        .setTableEntry(entry, changedLineNr);
                                dialogTestDescriptionController.setEditable(true);
                                String loginName = entry.getLoginName();
                                String[] split = loginName.split("-");
                                dialogTestDescriptionController
                                        .setLockedByUser(split[0]);

                                // show controller
                                activeTestDescriptionController = Integer.parseInt(
                                        entry.getFileCount()) - 1;
                                activeTestDescriptionControllerShown = false;
                                myController.showStage(TEST_DESCRIPTION_STAGE);
                            }
                        }
                    }
                }
            });
            testShortDescriptionCol.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent keyEvent) {
                    if (keyEvent.getCode() == KeyCode.ENTER) {
                        // select line
                        tableField.getSelectionModel().select(tableNr);

                        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
                        LockType locked = xml.isLocked(Integer.parseInt(
                                entry.getFileCount())-1);
                        FXMLLoader loader;
                        DialogTestDescriptionController dialogTestDescriptionController;

                        loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
                        dialogTestDescriptionController = loader.
                                <DialogTestDescriptionController>getController();
                        if (locked == LockType.locked) {
                            dialogTestDescriptionController
                                    .setShortDescriptionField(testShortDescriptionCol);
                            dialogTestDescriptionController
                                    .setTableEntry(entry, changedLineNr);
                            dialogTestDescriptionController.setEditable(false);
                            String loginName = entry.getLoginName();
                            String[] split = loginName.split("-");
                            dialogTestDescriptionController.setLockedByUser(split[0]);

                            // show controller
                            activeTestDescriptionController = Integer.parseInt(entry
                                    .getFileCount()) - 1;
                            activeTestDescriptionControllerShown = true;
                            myController.showStage(TEST_DESCRIPTION_STAGE);

                        } else {
                            dialogTestDescriptionController
                                    .setShortDescriptionField(testShortDescriptionCol);
                            dialogTestDescriptionController
                                    .setTableEntry(entry, changedLineNr);
                            dialogTestDescriptionController.setEditable(true);
                            String loginName = entry.getLoginName();
                            String[] split = loginName.split("-");
                            dialogTestDescriptionController.setLockedByUser(split[0]);

                            // show controller
                            activeTestDescriptionController = Integer.parseInt(
                                    entry.getFileCount()) - 1;
                            activeTestDescriptionControllerShown = false;
                            myController.showStage(TEST_DESCRIPTION_STAGE);
                        }
                    }
                }
            });
            testShortDescriptionCol.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent t) {
                    XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
                    LockType locked = xml.isLocked(Integer.parseInt(
                            entry.getFileCount())-1);
                    if ((locked == LockType.locked) && 
                            (!testShortDescriptionColBooleanList.get(fileNr-1)
                                    .get(changedLineNr))) {

                        // Open event-handler by typing the return key!
                        FXMLLoader loader;
                        DialogTestDescriptionController dialogTestDescriptionController;

                        loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
                        dialogTestDescriptionController = loader.
                                <DialogTestDescriptionController>getController();

                        String text = testShortDescriptionCol.getText();
                        String last = conv.convertDescriptionString(
                                entry.getTestShortDescriptionLast(changedLineNr));
                        if (!text.equals(last)) {
                            // Restore original state!
                            Platform.runLater(() -> {
                                // Note:
                                // An "IndexOutOfBoundsException" would occur in
                                // "ReadOnlyUnbackedObservableList.java", if the
                                // "runLater" is not used!
                                String testDescriptionLast = entry
                                        .getTestDescriptionLast(changedLineNr);
                                testShortDescriptionCol.setText(testDescriptionLast);
                            });                    

                            // Lock is active. Access is denied. A message is
                            // populated to the user.
                            if (xml.isReadOnly()) {
                                myController.showStage(READ_ONLY_MODE_STAGE);
                            } else {
                                myController.showStage(LOCKED_STAGE);
                            }
                        }
                    } else if (testShortDescriptionColBooleanList.get(fileNr-1)
                            .get(changedLineNr)) {
                        // do nothing, because the result is already written to
                        // the local table entry

                        // reset to default state
                        testShortDescriptionColBooleanList.get(fileNr-1).set(
                                changedLineNr, false);
                    } else {
                        // Open event-handler by typing the return key!
                        FXMLLoader loader;
                        DialogTestDescriptionController dialogTestDescriptionController;

                        loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
                        dialogTestDescriptionController = loader.
                                <DialogTestDescriptionController>getController();

                        String text = testShortDescriptionCol.getText();
                        String last = conv.convertDescriptionString(
                                entry.getTestShortDescriptionLast(changedLineNr));
                        if (!text.equals(last)) {
                            if (xml.writeTestShortDescriptionLock(entry)) {
                                // Lock is gotten! We can proceed!
                                // save changes!
                                entry.putTestShortDescription(changedLineNr, text);

                                // although douples the last description HTML field 
                                // (to hold it "in-line")
                                entry.putTestDescription(changedLineNr, 
                                        entry.getTestDescriptionLast(changedLineNr));

                                String user = System.getProperty("user.name");
                                entry.putTestDescAuthor(changedLineNr, user);
                                String date = ZonedDateTime.now().
                                        format(DateTimeFormatter.RFC_1123_DATE_TIME);
                                entry.putTestDescDate(changedLineNr, date);

                                try {
                                    if (locked == LockType.notLocked) {
                                        xml.writeTestShortDescription(changedLineNr, 
                                                entry);
                                    } else if (locked == LockType.ownLocked) {
                                        xml.writeTestShortDescription(changedLineNr, 
                                                entry);
                                    }
                                } catch (IOException ex) {
                                    logger.info("LxrTestTableController: " 
                                            + "An exception occured during "
                                            + "writing the test description to a file");
                                }

                                dialogTestDescriptionController
                                        .setShortDescriptionField(testShortDescriptionCol);
                                dialogTestDescriptionController
                                        .setTableEntry(entry, changedLineNr);

                                // show controller
                                activeTestDescriptionController = Integer.parseInt(
                                        entry.getFileCount()) - 1;
                                activeTestDescriptionControllerShown = false;
                                myController.showStage(TEST_DESCRIPTION_STAGE);

                            } else {
                                // Lock is not gotten!
                                //
                                // Restore original state!
                                Platform.runLater(() -> {
                                    // Note:
                                    // An "IndexOutOfBoundsException" would occur in
                                    // "ReadOnlyUnbackedObservableList.java", if the
                                    // "runLater" is not used!
                                    String testShortDescriptionLast = 
                                            conv.convertDescriptionString(entry
                                            .getTestShortDescriptionLast(
                                                    changedLineNr));
                                    testShortDescriptionCol.setText(
                                            testShortDescriptionLast);
                                    logger.info("LxrTestTableController: " + 
                                            "testShortDescriptionLast = " + 
                                            testShortDescriptionLast);
                                });                    

                                // Lock is active. Access is denied. A message is
                                // populated to the user.
                                if (xml.isReadOnly()) {
                                    myController.showStage(READ_ONLY_MODE_STAGE);
                                } else {
                                    myController.showStage(LOCKED_STAGE);
                                }
                            }
                        } else {
                            dialogTestDescriptionController
                                    .setShortDescriptionField(testShortDescriptionCol);
                            dialogTestDescriptionController
                                    .setTableEntry(entry, changedLineNr);

                            // show controller
                            activeTestDescriptionController = Integer.parseInt(
                                    entry.getFileCount()) - 1;
                            activeTestDescriptionControllerShown = false;
                            myController.showStage(TEST_DESCRIPTION_STAGE);
                        }
                    } 
                }
            });

            testShortDescriptionCol.focusedProperty().addListener(
                    new ChangeListener<Boolean>()
            {
                @Override
                public void changed(ObservableValue<? extends Boolean> arg0, 
                        Boolean oldPropertyValue, Boolean newPropertyValue)
                {
                    if (newPropertyValue)
                    {
                        // on focus
                    }
                    else
                    {
                        // off focus
                        XmlHandling xml = lxrDifferenceTableController
                                .getXmlHandling();
                        LockType locked = xml.isLocked(Integer.parseInt(
                                entry.getFileCount())-1);
                        if (locked == LockType.locked) {
                            // Open event-handler by typing the return key!
                            FXMLLoader loader;
                            DialogTestDescriptionController dialogTestDescriptionController;

                            loader = myController.getLoader(TEST_DESCRIPTION_STAGE);
                            dialogTestDescriptionController = loader.
                                    <DialogTestDescriptionController>getController();

                            String text = testShortDescriptionCol.getText();
                            String last = conv.convertDescriptionString(
                                    entry.getTestShortDescriptionLast(
                                    changedLineNr));
                            if (!text.equals(last)) {
                                // Restore original state!
                                Platform.runLater(() -> {
                                    // Note:
                                    // An "IndexOutOfBoundsException" would occur in
                                    // "ReadOnlyUnbackedObservableList.java", if the
                                    // "runLater" is not used!
                                    String testShortDescriptionLast = 
                                            conv.convertDescriptionString(entry
                                            .getTestShortDescriptionLast(
                                                    changedLineNr));
                                    testShortDescriptionCol.setText(
                                            testShortDescriptionLast);
                                });                    

                                // Lock is active. Access is denied. A message is
                                // populated to the user.
                                if (xml.isReadOnly()) {
                                    myController.showStage(READ_ONLY_MODE_STAGE);
                                } else {
                                    myController.showStage(LOCKED_STAGE);
                                }
                            }
                        } else {

                            String text = testShortDescriptionCol.getText();
                            String last = conv.convertDescriptionString(
                                    entry.getTestShortDescriptionLast(
                                    changedLineNr));
                            if (!text.equals(last)) {
                                if (xml.writeTestShortDescriptionLock(entry)) {
                                    // Lock is gotten! We can proceed!
                                    // save changes!
                                    entry.putTestShortDescription(changedLineNr, 
                                            text);

                                    // although douples the last description HTML 
                                    // field (to hold it "in-line")
                                    entry.putTestDescription(changedLineNr, 
                                            entry.getTestDescriptionLast(
                                                    changedLineNr));

                                    String user = System.getProperty("user.name");
                                    entry.putTestDescAuthor(changedLineNr, user);
                                    String date = ZonedDateTime.now().
                                            format(DateTimeFormatter.RFC_1123_DATE_TIME);
                                    entry.putTestDescDate(changedLineNr, date);

                                    try {
                                        if (locked == LockType.notLocked) {
                                            xml.writeTestShortDescription(
                                                    changedLineNr, entry);
                                        } else if (locked == LockType.ownLocked) {
                                            xml.writeTestShortDescription(
                                                    changedLineNr, entry);
                                        }
                                    } catch (IOException ex) {
                                        logger.info("LxrTestTableController: " + 
                                                "An exception occured during "
                                                + "writing the test description " 
                                                + "to a file");
                                    }
                                } else {
                                    // Lock is not gotten!
                                    //
                                    // Restore original state!
                                    Platform.runLater(() -> {
                                        // Note:
                                        // An "IndexOutOfBoundsException" would occur
                                        // in "ReadOnlyUnbackedObservableList.java", 
                                        // if the "runLater" is not used!
                                        String testShortDescriptionLast = 
                                                conv.convertDescriptionString(entry
                                                .getTestShortDescriptionLast(
                                                        changedLineNr));
                                        testShortDescriptionCol.setText(
                                                testShortDescriptionLast);
                                        logger.info("LxrTestTableController: " + 
                                                "testShortDescriptionLast = " + 
                                                testShortDescriptionLast);
                                    });                    

                                    // Lock is active. Access is denied. A message is
                                    // populated to the user.
                                    if (xml.isReadOnly()) {
                                        myController.showStage(READ_ONLY_MODE_STAGE);
                                    } else {
                                        myController.showStage(LOCKED_STAGE);
                                    }
                                }
                            }
                        }
                    }
                }
            });

            TestDataModel testDataModel = new TestDataModel();
            testDataModel.setFileCount(Integer.parseInt(fileCountCol));
            testDataModel.setHrefText(hrefTextCol);
            testDataModel.setHref(hrefCol);
            testDataModel.setLineNumber(lineNumber, locale);
            if (svn) {
                testDataModel.setSvnVersion(svnVersionCol);
                testDataModel.setSvnDesc(svnDescCol);
                testDataModel.setSvnAuthor(svnAuthorCol);
                testDataModel.setSvnDate(svnDateCol);
            } else {
                // no SVN!
            }
            testDataModel.setReviewState(reviewStateCol);
            testDataModel.setReviewShortDescription(reviewShortDescriptionCol, locale);
            testDataModel.setTestState(testStateCol);
            testDataModel.setTestShortDescription(testShortDescriptionCol, locale);

            // update with runLater to avoid corruption of javafx table!
            if (computeStatistics) {
                entry.putTableViewLineNumber(tableNr);      // save reference position
            }

            // enable copy/paste
            TableUtils tableUtils = new TableUtils(changedFileList);
            tableUtils.installCopyPasteHandler(tableField);
            
            if (computeStatistics) {
                // running as normal fx-application task
                tableField.getItems().add(testDataModel);
                tableField.setRowFactory(tf -> {
                    TableRow<TestDataModel> row = new TableRow<>();
                    row.itemProperty().addListener((obs, previousValue, 
                            currentValue) -> {
                        
                        // Context Menu
                        ContextMenu svnContextMenu = new ContextMenu();
                        MenuItem svnHistoryMenuItem = new MenuItem(ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("menuItemSvnHistory"));
                        svnHistoryMenuItem.disableProperty().bind(
                                Bindings.isEmpty(tableField.getSelectionModel().
                                        getSelectedItems()));
                        svnHistoryMenuItem.setOnAction((ActionEvent event) -> {
                            int index = row.getIndex();
                            int nr = obs.getValue().getFileCount();
                            dialogSvnHistory(index, changedFileList.get(nr-1));
                        });
                        MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("menuItemCopy"));
                        MenuItem markAllOnPageMenuItem = new MenuItem(ResourceBundle.
                                getBundle("lxrdifferencetable/Bundle", locale).
                                getString("menuItemMarkAllOnPage"));
                        copyMenuItem.setOnAction((ActionEvent event) -> {
                            tableUtils.copySelectionToClipboard(tableField);
                        });
                        markAllOnPageMenuItem.setOnAction((ActionEvent event) -> {
                            tableField.getSelectionModel().selectAll();
                        });
                        
                        if (svnAvailable) {
                            svnContextMenu.getItems().addAll(svnHistoryMenuItem,
                                    markAllOnPageMenuItem, copyMenuItem);
                        } else {
                            svnContextMenu.getItems().addAll(markAllOnPageMenuItem, 
                                    copyMenuItem);
                        }
                       // Set context menu on row, but use a binding to make 
                       // it only show for non-empty rows:  
                        row.contextMenuProperty().bind(  
                                Bindings.when(row.emptyProperty())  
                                .then((ContextMenu)null)  
                                .otherwise(svnContextMenu)  
                        );                         

                        // XML Handling
                        XmlHandling xml = lxrDifferenceTableController
                                .getXmlHandling();
                        if (row.getItem() != null) {
                            if (row.getItem().getFileCount() >= 0) {
                                int i = row.getItem().getFileCount() - 1;
                                if (i >= 0) {
                                    if ((changedFileList.get(i)
                                            .getXmlLockFileNumber() != -1) && 
                                            (xml.isLoginValid(changedFileList.get(i)
                                                    .getLoginName()))) {
                                        // Colorize the row to show the user that the
                                        // row is locked by another user.
                                        row.setStyle("-fx-background-color: yellow");
                                        
                                        ComboBox<State> reviewState1 = row.getItem().getReviewState();
                                        row.getItem().setReviewState(reviewState1);

                                        // Important!
                                        // Reset to default state
                                        int size = changedFileList.get(i)
                                                .getLineNumber().size();
                                        for (int j = 0; j < size; j++) {
                                            reviewStateColBooleanList.get(i).set(j, 
                                                    false);
                                            testStateColBooleanList.get(i).set(j, 
                                                    false);
                                            reviewShortDescriptionColBooleanList
                                                    .get(i).set(j, false);
                                            testShortDescriptionColBooleanList
                                                    .get(i).set(j, false);
                                        }
                                    } else {
                                        // Important: Do not remove the following
                                        // line!
                                        // If the following line is removed, then
                                        // the colorizing of table view rows did not
                                        // function in the expected way. 
                                        row.setStyle("");

                                        // If locked, than de-lock stage!
                                        if (activeReviewDescriptionController == i) {
                                            // The stage "REVIEW_DESCRIPTION_STAGE" 
                                            // is active and locked! 
                                            // Do a transition from non-editable to 
                                            // editable, because the line is not 
                                            // locked by another user anymore.
                                            FXMLLoader loader;
                                            DialogReviewDescriptionController 
                                                    dialogReviewDescriptionController;

                                            loader = myController.getLoader(
                                                    REVIEW_DESCRIPTION_STAGE);
                                            dialogReviewDescriptionController = 
                                                    loader.
                                                    <DialogReviewDescriptionController>getController();

                                            dialogReviewDescriptionController
                                                    .setEditable(true);
                                            activeReviewDescriptionControllerShown = false;
                                        }
                                        // If locked, than de-lock stage!
                                        if (activeTestDescriptionController == i) {
                                            // The stage "TEST_DESCRIPTION_STAGE" is 
                                            // active and locked! 
                                            // Do a transition from non-editable to 
                                            // editable, because the line is not 
                                            // locked by another useranymore.
                                            FXMLLoader loader;
                                            DialogTestDescriptionController 
                                                    dialogTestDescriptionController;

                                            loader = myController.getLoader(
                                                    TEST_DESCRIPTION_STAGE);
                                            dialogTestDescriptionController = 
                                                    loader.
                                                    <DialogTestDescriptionController>getController();

                                            dialogTestDescriptionController
                                                    .setEditable(true);
                                            activeTestDescriptionControllerShown = false;
                                        }
                                    }
                                }
                            } else {
                                // do nothing!
                                logger.warning("LxrTestTableController: " + 
                                        "An Error occured during " + 
                                        "updating of row!");
                            }
                        }
                    });
                    return row;
                });
            } else {
                // running as thread
                Platform.runLater(() -> {
                    tableField.getItems().add(testDataModel);
                    tableField.setRowFactory(tf -> {

                        TableRow<TestDataModel> row = new TableRow<>();
                        row.itemProperty().addListener((obs, previousValue, 
                                currentValue) -> {

                            // Context Menu
                            ContextMenu svnContextMenu = new ContextMenu();
                            MenuItem svnHistoryMenuItem = new MenuItem(ResourceBundle.
                                    getBundle("lxrdifferencetable/Bundle", locale).
                                    getString("menuItemSvnHistory"));
                            svnHistoryMenuItem.disableProperty().bind(
                                    Bindings.isEmpty(tableField.getSelectionModel().
                                            getSelectedItems()));
                            svnHistoryMenuItem.setOnAction((ActionEvent event) -> {
                                int index = row.getIndex();
                                int nr = obs.getValue().getFileCount();
                                dialogSvnHistory(index, changedFileList.get(nr-1));
                            });
                            
                            MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                                    getBundle("lxrdifferencetable/Bundle", locale).
                                    getString("menuItemCopy"));
                            MenuItem markAllOnPageMenuItem = new MenuItem(ResourceBundle.
                                    getBundle("lxrdifferencetable/Bundle", locale).
                                    getString("menuItemMarkAllOnPage"));
                            copyMenuItem.setOnAction((ActionEvent event) -> {
                                tableUtils.copySelectionToClipboard(tableField);
                            });
                            markAllOnPageMenuItem.setOnAction((ActionEvent event) -> {
                                tableField.getSelectionModel().selectAll();
                            });
                            if (svnAvailable) {
                                svnContextMenu.getItems().addAll(svnHistoryMenuItem,
                                        markAllOnPageMenuItem, copyMenuItem);
                            } else {
                                svnContextMenu.getItems().addAll(markAllOnPageMenuItem, 
                                        copyMenuItem);
                            }
                            
                           // Set context menu on row, but use a binding to make 
                           // it only show for non-empty rows:  
                            row.contextMenuProperty().bind(  
                                    Bindings.when(row.emptyProperty())  
                                    .then((ContextMenu)null)  
                                    .otherwise(svnContextMenu)  
                            );                         

                            // XML Handling
                            XmlHandling xml = lxrDifferenceTableController
                                    .getXmlHandling();
                            if (row.getItem() != null) {
                                if (row.getItem().getFileCount() >= 0) {
                                    int i = row.getItem().getFileCount() - 1;
                                    if (i >= 0) {
                                        if ((changedFileList.get(i)
                                                .getXmlLockFileNumber() != -1) && 
                                                (xml.isLoginValid(changedFileList
                                                        .get(i).getLoginName()))) {
                                            // Colorize the row to show the user 
                                            // that the row is locked by another 
                                            // user.
                                            row.setStyle("-fx-background-color: yellow");

                                            // Important!
                                            // Reset to default state
                                            int size = changedFileList.get(i)
                                                    .getLineNumber().size();
                                            for (int j = 0; j < size; j++) {
                                                reviewStateColBooleanList
                                                        .get(i).set(j, false);
                                                testStateColBooleanList
                                                        .get(i).set(j, false);
                                                reviewShortDescriptionColBooleanList
                                                        .get(i).set(j, false);
                                                testShortDescriptionColBooleanList
                                                        .get(i).set(j, false);
                                            }
                                        } else {
                                            // Important: Do not remove the 
                                            // following line!!!
                                            // 
                                            // If the following line is removed, 
                                            // then the  colorizing of table view 
                                            // rows did not funktion in the 
                                            // expected way. 
                                            row.setStyle("");

                                            // If locked, than de-lock stage!
                                            if (activeReviewDescriptionController == i) {
                                                // The stage "REVIEW_DESCRIPTION_STAGE" 
                                                // is active and locked! 
                                                // Do a transition from non-editable 
                                                // to editable, because the line is
                                                //  not locked by another user 
                                                // anymore.
                                                FXMLLoader loader;
                                                DialogReviewDescriptionController 
                                                        dialogReviewDescriptionController;

                                                loader = myController.getLoader(
                                                        REVIEW_DESCRIPTION_STAGE);
                                                dialogReviewDescriptionController = 
                                                        loader.
                                                        <DialogReviewDescriptionController>getController();

                                                dialogReviewDescriptionController
                                                        .setEditable(true);
                                                activeReviewDescriptionControllerShown = false;
                                            }
                                            // If locked, than de-lock stage!
                                            if (activeTestDescriptionController == i) {
                                                // The stage "TEST_DESCRIPTION_STAGE" 
                                                // is active and locked! 
                                                // Do a transition from non-editable 
                                                // to editable, because the line is 
                                                // not locked by another user anymore.
                                                FXMLLoader loader;
                                                DialogTestDescriptionController 
                                                        dialogTestDescriptionController;

                                                loader = myController.getLoader(
                                                        TEST_DESCRIPTION_STAGE);
                                                dialogTestDescriptionController = 
                                                        loader.
                                                        <DialogTestDescriptionController>getController();

                                                dialogTestDescriptionController
                                                        .setEditable(true);
                                                activeTestDescriptionControllerShown = false;
                                            }
                                        }
                                    }
                                } else {
                                    // do nothing!
                                    logger.warning("LxrTestTableController: " + 
                                            "An Error occured during updating of row!");
                                }
                            }
                        });
                        return row;
                    });
                });
            }

            // enable multi-selection
            tableField.getSelectionModel().setCellSelectionEnabled(true);
            tableField.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            tableField.setEditable(true);

        } else {
            helperReviewState(fileNr, changedLineNr, null);
            helperTestState(fileNr, changedLineNr, null);

            helperReviewShortDescription(fileNr, changedLineNr, null);
            helperTestShortDescription(fileNr, changedLineNr, null);
        }
    }
    
    private void addNewLineInTableDummy(TableView<TestDataModel> tableField, 
            TableEntry entry, 
            int tableNr,
            int fileNr,
            int changedLineNr,
            boolean computeStatistics,
            String fileCount,
            String href, String hrefText,
            String lineNumber, String svnVersion, String svnDesc,
            String svnAuthor, String svnDate, boolean svn,
            State reviewState,
            String reviewShortDescription,
            State testState,
            String testShortDescription)
    {
        if (!isRowHidden(entry, changedLineNr)) {
            // compute statistical data
            if (computeStatistics) {
                statistics.incrementByOneNumberOfEntries();
                int fileCountInt = Integer.parseInt(fileCount); 
                try {
                    Integer.parseInt(lineNumber);
                    statistics.incrementByOneNumberOfChanges();
                    if (lastFileNr != fileCountInt) {
                        // It's a changed file and it's the first change.
                        statistics.incrementByOneNumberOfChangedFiles();
                    }
                } catch (Exception ex) {
                    switch (lineNumber) {
                        case "fileDeleted":
                            statistics.incrementByOneNumberOfDeletedFiles();
                            break;
                        case "directoryDeleted":
                            statistics.incrementByOneNumberOfDeletedFiles();
                            break;
                        case "newDirectory":
                            statistics.incrementByOneNumberOfNewFiles();
                            break;
                        case "newFile":
                            statistics.incrementByOneNumberOfNewFiles();
                            break;
                        case "notChanged":
                            statistics.incrementByOneNumberOfUnmodifiedFiles();
                            break;
                        case "dublicatedFile":
                            statistics.incrementByOneNumberOfDublicatedFiles();
                            break;
                        default:
                            break;
                    }
                }
                switch (reviewState) {
                    case PASSED:
                        statistics.incrementByOneReviewPassed();
                        break;
                    case FAILED:
                        statistics.incrementByOneReviewFailed();
                        break;
                    case COMMENT:
                        statistics.incrementByOneReviewComment();
                        break;
                    case INCONCLUSIVE:
                        statistics.incrementByOneReviewInconclusive();
                        break;
                    case OPEN:
                        statistics.incrementByOneReviewOpen();
                        break;
                    default:
                        statistics.incrementByOneReviewEmpty();
                        break;
                }
                switch (testState) {
                    case PASSED:
                        statistics.incrementByOneTestPassed();
                        break;
                    case FAILED:
                        statistics.incrementByOneTestFailed();
                        break;
                    case COMMENT:
                        statistics.incrementByOneTestComment();
                        break;
                    case INCONCLUSIVE:
                        statistics.incrementByOneTestInconclusive();
                        break;
                    case OPEN:
                        statistics.incrementByOneTestOpen();
                        break;
                    default:
                        statistics.incrementByOneTestEmpty();
                        break;
                }
                lastFileNr = fileCountInt;                        // update value
            }
            helperReviewState(fileNr, changedLineNr, null);
            helperTestState(fileNr, changedLineNr, null);

            helperReviewShortDescription(fileNr, changedLineNr, null);
            helperTestShortDescription(fileNr, changedLineNr, null);
        } else {
            helperReviewState(fileNr, changedLineNr, null);
            helperTestState(fileNr, changedLineNr, null);

            helperReviewShortDescription(fileNr, changedLineNr, null);
            helperTestShortDescription(fileNr, changedLineNr, null);
        }
    }

    private void addNewLineInSvnTable(String user, int numberOfCommits) {
        StatisticsDataModel svnDataModel = new StatisticsDataModel();
        svnDataModel.setData(user);
        svnDataModel.setCount(Integer.toString(numberOfCommits));
        svnStatisticsField.getItems().add(svnDataModel);
        svnStatisticsField.setEditable(true);
    }
    
    private void writeSvnTable() {
        Set<String> keySet = userMap.keySet();
        labelNrOfAuthors.setText("" + keySet.size());
        int count = 0;
        for (String svnUser : keySet) {
            Integer numberOfCommits = commitMap.get(userMap.get(svnUser));
            addNewLineInSvnTable(svnUser, numberOfCommits);
            count = count + numberOfCommits;
        }
        labelNrOfCommits.setText("" + count);
        
    }
    
    private void writeTestTable(TableView<TestDataModel> tableField, 
            int tableNumber,
            boolean computeStatistics,
            ProgressController progressController,
            SetProgress setProgress) {

        int fileCounter;
        int tableCounter = -1;
        for (TableEntry entry : partedFileList.get(tableNumber)) {
            fileCounter = Integer.parseInt(entry.getFileCount());
            if (computeStatistics) {
                statistics.incrementByOneNumberOfFiles();
            } 
            if (setProgress != null) {
                setProgress.setProgressFifthPass(fileCounter, 
                        changedFileList.size());
            }

            if ((progressController != null) && (progressController
                    .getAbortState())) {
                break;
            }

            int sizeHrefEntry = entry.getHref().size();
            int i = 0;
            for (int pos = 0; pos < sizeHrefEntry; pos++) {
                tableCounter++;
                if ((pos == 0) && (svnAvailable)) {
                    // print with SVN
                    String svnVersion = "";
                    String svnDescription = "";
                    String svnAuthor = "";
                    String svnDate = "";
                    int j = 0;
                    for (String version : entry.getSvnVersion()) {
                        if (j==0) {
                            svnVersion = version;
                            svnAuthor = entry.getSvnAuthor(j);
                            svnDate = entry.getSvnDate(j);
                            svnDescription = entry.getSvnDesc(j);
                        } else {
                            svnVersion = svnVersion + "\n" + version;
                            svnAuthor = svnAuthor + "\n" + entry.getSvnAuthor(j);
                            svnDate = svnDate + "\n" + entry.getSvnDate(j);
                            svnDescription = svnDescription + "\n" +
                                    entry.getSvnDesc(j);
                        }
                        j++;
                    }
                    addNewLineInTable(tableField, 
                            entry, tableCounter, fileCounter, pos, 
                            computeStatistics,
                            entry.getFileCount(),
                            entry.getHrefLast(pos), 
                            entry.getHrefTextLast(pos),
                            entry.getLineNumberLast(pos), 
                            svnVersion, svnDescription, svnAuthor, svnDate, 
                            svnAvailable,
                            entry.getReviewStateLast(pos),
                            conv.convertDescriptionString(entry.getReviewShortDescriptionLast(pos)),
                            entry.getTestStateLast(pos),
                            conv.convertDescriptionString(entry.getTestShortDescriptionLast(pos)));
                    if (entry.getDublicateEntry().size() > 0) {
                        break;
                    }
                } else {
                    // print without SVN
                    addNewLineInTable(tableField, 
                            entry, tableCounter, fileCounter, pos, 
                            computeStatistics, 
                            entry.getFileCount(), 
                            entry.getHrefLast(pos), 
                            entry.getHrefTextLast(pos),
                            entry.getLineNumberLast(pos),
                            "", "", "", "", svnAvailable,
                            entry.getReviewStateLast(pos),
                            conv.convertDescriptionString(entry.getReviewShortDescriptionLast(pos)),
                            entry.getTestStateLast(pos),
                            conv.convertDescriptionString(entry.getTestShortDescriptionLast(pos)));
                    if (entry.getDublicateEntry().size() > 0) {
                        break;
                    }
                }
            }
        }
    }

    private void writeTestTableDummy(TableView<TestDataModel> tableField, 
            int tableNumber,
            boolean computeStatistics,
            ProgressController progressController,
            SetProgress setProgress) {

        int fileCounter;
        int tableCounter = -1;
        for (TableEntry entry : partedFileList.get(tableNumber)) {
            fileCounter = Integer.parseInt(entry.getFileCount());
            if (computeStatistics) {
                statistics.incrementByOneNumberOfFiles();
            } 
            if (setProgress != null) {
                setProgress.setProgressFifthPass(fileCounter, 
                        changedFileList.size());
            }

            if ((progressController != null) && (progressController
                    .getAbortState())) {
                break;
            }

            int sizeHrefEntry = entry.getHref().size();
            int i = 0;
            for (int pos = 0; pos < sizeHrefEntry; pos++) {
                tableCounter++;
                addNewLineInTableDummy(tableField, 
                        entry, tableCounter, fileCounter, pos, 
                        computeStatistics,
                        entry.getFileCount(),
                        entry.getHrefLast(pos), 
                        entry.getHrefTextLast(pos),
                        entry.getLineNumberLast(pos), 
                        "", "", "", "", false,
                        entry.getReviewStateLast(pos),
                        conv.convertDescriptionString(entry.getReviewShortDescriptionLast(pos)),
                        entry.getTestStateLast(pos),
                        conv.convertDescriptionString(entry.getTestShortDescriptionLast(pos)));
                if (entry.getDublicateEntry().size() > 0) {
                    break;
                }
            }
        }
    }

    private void initSvnStatisticsTable() {
        //Create a customer cell factory so that cells can support editing.
        Callback<TableColumn<StatisticsDataModel, String>, 
                TableCell<StatisticsDataModel, String>> cellFactory;
        cellFactory = new Callback<TableColumn<StatisticsDataModel, String>, 
                TableCell<StatisticsDataModel, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                StatisticsEditingCell cell = new StatisticsEditingCell();
                cell.widthProperty()
                        .addListener((ObservableValue<? extends Number> observableValue, 
                                Number oldSceneWidth, Number newSceneWidth) -> {
                    Text text = new Text();
                    cell.setGraphic(text);
                    text.wrappingWidthProperty().bind(cell.widthProperty());
                    text.textProperty().bind(cell.itemProperty());
                });
                return cell;
            }
        };
        // statistics.reset();
        svnData.clear();
        userMap.clear();
        labelNrOfAuthors.setText("" + 0);
        labelNrOfCommits.setText("" + 0);
        
        // clear old values
        if (!svnStatisticsField.getColumns().isEmpty()) {
            svnStatisticsField.getItems().clear();        // clear content
            svnStatisticsField.getColumns().clear();      // clear rows
        }

        // Do only in case of first initialization.
        columnWithSvnUsers = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("svnUsers"));
        columnWithSvnUsers.setCellValueFactory(
                new PropertyValueFactory<>("data"));
        columnWithSvnUsers.setCellFactory(cellFactory);
        columnWithSvnCommits = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("svnCommits"));
        columnWithSvnCommits.setCellValueFactory(
                new PropertyValueFactory<>("count"));
        columnWithSvnCommits.setComparator(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                // Note:
                // This column has the following values:
                //          0000 .. 9999 .. 
                //          deleted
                //          new
                //          unchanged
                int i1 = 0;
                boolean i1Value;
                try {
                    i1 = Integer.parseInt(o1);
                    i1Value = true;
                } catch (Exception ex) {
                    i1Value = false;
                }
                int i2 = 0;
                boolean i2Value;
                try {
                    i2 = Integer.parseInt(o2);
                    i2Value = true;
                } catch (Exception ex) {
                    i2Value = false;
                }
                if (i1Value && i2Value) {
                    if(i1<i2)return -11;
                    if(i1>i2)return 1;
                    return 0;                
                } else {
                    if (i1Value) {
                        return 1;
                    } else if (i2Value) {
                        return -1;
                    } else {
                        return o1.compareTo(o2);
                    }
                }
            }
        });
        svnStatisticsField.getColumns().addAll(columnWithSvnUsers, 
                columnWithSvnCommits);     
        columnWithSvnUsers.setCellFactory(cellFactory);
	// enable multi-selection
	svnStatisticsField.getSelectionModel().setCellSelectionEnabled(true);
	svnStatisticsField.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // enable copy/paste
        lxrdifferencetable.tools.TableUtils tableUtils;
        tableUtils = new lxrdifferencetable.tools.TableUtils();
        tableUtils.installCopyPasteHandler(svnStatisticsField);
        
        // set context menu for copy/paste
        ContextMenu contextMenu = new ContextMenu();
        MenuItem copyMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem.setOnAction((ActionEvent event) -> {
            tableUtils.copySelectionToClipboard(svnStatisticsField);
        });
        markAllMenuItem.setOnAction((ActionEvent event) -> {
            svnStatisticsField.getSelectionModel().selectAll();
        });
        contextMenu.getItems().addAll(copyMenuItem, markAllMenuItem);
        svnStatisticsField.setContextMenu(contextMenu);
    }

    private void initStatiticsTable() {
        //Create a customer cell factory so that cells can support editing.
        Callback<TableColumn<StatisticsDataModel, String>, 
                TableCell<StatisticsDataModel, String>> cellFactory;
        cellFactory = new Callback<TableColumn<StatisticsDataModel, String>, 
                TableCell<StatisticsDataModel, String>>() {
            @Override
            public TableCell call(TableColumn p) {
                StatisticsEditingCell cell = new StatisticsEditingCell();
                cell.widthProperty()
                        .addListener((ObservableValue<? extends Number> observableValue, 
                                Number oldSceneWidth, Number newSceneWidth) -> {
                    Text text = new Text();
                    cell.setGraphic(text);
                    text.wrappingWidthProperty().bind(cell.widthProperty());
                    text.textProperty().bind(cell.itemProperty());
                });
                return cell;
            }
        };
        
        // statistics.reset();
        statistics.clear();
        
        // clear old values
        if (!tableStatisticsField.getColumns().isEmpty()) {
            tableStatisticsField.getItems().clear();        // clear content
            tableStatisticsField.getColumns().clear();      // clear rows
        }
        if (!reviewStatisticsField.getColumns().isEmpty()) {
            reviewStatisticsField.getItems().clear();       // clear content
            reviewStatisticsField.getColumns().clear();     // clear rows
        }
        if (!testStatisticsField.getColumns().isEmpty()) {
            testStatisticsField.getItems().clear();         // clear content
            testStatisticsField.getColumns().clear();       // clear rows
        }

        // Do only in case of first initialization.
        columnWithStatisticsTableData = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statTable"));
        columnWithStatisticsTableData.setCellValueFactory(
                new PropertyValueFactory<>("data"));
        columnWithStatisticsTableData.setCellFactory(cellFactory);
        columnWithStatisticsTableCount = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statCount"));
        columnWithStatisticsTableCount.setCellValueFactory(
                new PropertyValueFactory<>("count"));
        columnWithStatisticsTableCount.setCellFactory(cellFactory);
        columnWithStatisticsTableCount.setComparator(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                // Note:
                // This column has the following values:
                //          0000 .. 9999 .. 
                //          deleted
                //          new
                //          unchanged
                int i1 = 0;
                boolean i1Value;
                try {
                    i1 = Integer.parseInt(o1);
                    i1Value = true;
                } catch (Exception ex) {
                    i1Value = false;
                }
                int i2 = 0;
                boolean i2Value;
                try {
                    i2 = Integer.parseInt(o2);
                    i2Value = true;
                } catch (Exception ex) {
                    i2Value = false;
                }
                if (i1Value && i2Value) {
                    if(i1<i2)return -11;
                    if(i1>i2)return 1;
                    return 0;                
                } else {
                    if (i1Value) {
                        return 1;
                    } else if (i2Value) {
                        return -1;
                    } else {
                        return o1.compareTo(o2);
                    }
                }
            }
        });
        tableStatisticsField.getColumns().addAll(columnWithStatisticsTableData, 
                columnWithStatisticsTableCount);        
	// enable multi-selection
	tableStatisticsField.getSelectionModel().setCellSelectionEnabled(true);
	tableStatisticsField.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // enable copy/paste
        lxrdifferencetable.tools.TableUtils tableUtils1;
        tableUtils1 = new lxrdifferencetable.tools.TableUtils();
        tableUtils1.installCopyPasteHandler(tableStatisticsField);
        // set context menu for copy/paste
        ContextMenu contextMenu1 = new ContextMenu();
        MenuItem copyMenuItem1 = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem1 = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem1.setOnAction((ActionEvent event) -> {
            tableUtils1.copySelectionToClipboard(tableStatisticsField);
        });
        markAllMenuItem1.setOnAction((ActionEvent event) -> {
            tableStatisticsField.getSelectionModel().selectAll();
        });
        contextMenu1.getItems().addAll(copyMenuItem1, markAllMenuItem1);
        tableStatisticsField.setContextMenu(contextMenu1);

        columnWithStatisticsReviewData = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statReviewed"));
        columnWithStatisticsReviewData.setCellValueFactory(
                new PropertyValueFactory<>("data"));
        columnWithStatisticsReviewData.setCellFactory(cellFactory);
        columnWithStatisticsReviewCount = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statCount"));
        columnWithStatisticsReviewCount.setCellValueFactory(
                new PropertyValueFactory<>("count"));
        columnWithStatisticsReviewCount.setCellFactory(cellFactory);
        columnWithStatisticsReviewCount.setComparator(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                // Note:
                // This column has the following values:
                //          0000 .. 9999 .. 
                //          deleted
                //          new
                //          unchanged
                int i1 = 0;
                boolean i1Value;
                try {
                    i1 = Integer.parseInt(o1);
                    i1Value = true;
                } catch (Exception ex) {
                    i1Value = false;
                }
                int i2 = 0;
                boolean i2Value;
                try {
                    i2 = Integer.parseInt(o2);
                    i2Value = true;
                } catch (Exception ex) {
                    i2Value = false;
                }
                if (i1Value && i2Value) {
                    if(i1<i2)return -11;
                    if(i1>i2)return 1;
                    return 0;                
                } else {
                    if (i1Value) {
                        return 1;
                    } else if (i2Value) {
                        return -1;
                    } else {
                        return o1.compareTo(o2);
                    }
                }
            }
        });
        reviewStatisticsField.getColumns().addAll(columnWithStatisticsReviewData, 
                columnWithStatisticsReviewCount);        
	// enable multi-selection
	reviewStatisticsField.getSelectionModel().setCellSelectionEnabled(true);
	reviewStatisticsField.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // enable copy/paste
        lxrdifferencetable.tools.TableUtils tableUtils2;
        tableUtils2 = new lxrdifferencetable.tools.TableUtils();
        tableUtils2.installCopyPasteHandler(reviewStatisticsField);
        // set context menu for copy/paste
        ContextMenu contextMenu2 = new ContextMenu();
        MenuItem copyMenuItem2 = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem2 = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem2.setOnAction((ActionEvent event) -> {
            tableUtils2.copySelectionToClipboard(reviewStatisticsField);
        });
        markAllMenuItem2.setOnAction((ActionEvent event) -> {
            reviewStatisticsField.getSelectionModel().selectAll();
        });
        contextMenu2.getItems().addAll(copyMenuItem2, markAllMenuItem2);
        reviewStatisticsField.setContextMenu(contextMenu2);
        
        columnWithStatisticsTestData = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statTested"));
        columnWithStatisticsTestData.setCellValueFactory(
                new PropertyValueFactory<>("data"));
        columnWithStatisticsTestData.setCellFactory(cellFactory);
        columnWithStatisticsTestCount = new TableColumn<>(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statCount"));
        columnWithStatisticsTestCount.setCellValueFactory(
                new PropertyValueFactory<>("count"));
        columnWithStatisticsTestCount.setCellFactory(cellFactory);
        columnWithStatisticsTestCount.setComparator(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                // Note:
                // This column has the following values:
                //          0000 .. 9999 .. 
                //          deleted
                //          new
                //          unchanged
                int i1 = 0;
                boolean i1Value;
                try {
                    i1 = Integer.parseInt(o1);
                    i1Value = true;
                } catch (Exception ex) {
                    i1Value = false;
                }
                int i2 = 0;
                boolean i2Value;
                try {
                    i2 = Integer.parseInt(o2);
                    i2Value = true;
                } catch (Exception ex) {
                    i2Value = false;
                }
                if (i1Value && i2Value) {
                    if(i1<i2)return -11;
                    if(i1>i2)return 1;
                    return 0;                
                } else {
                    if (i1Value) {
                        return 1;
                    } else if (i2Value) {
                        return -1;
                    } else {
                        return o1.compareTo(o2);
                    }
                }
            }
        });
        testStatisticsField.getColumns().addAll(columnWithStatisticsTestData, 
                columnWithStatisticsTestCount);        
	// enable multi-selection
	testStatisticsField.getSelectionModel().setCellSelectionEnabled(true);
	testStatisticsField.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        // enable copy/paste
        lxrdifferencetable.tools.TableUtils tableUtils3;
        tableUtils3 = new lxrdifferencetable.tools.TableUtils();
        tableUtils3.installCopyPasteHandler(testStatisticsField);
        
        // set context menu for copy/paste
        ContextMenu contextMenu3 = new ContextMenu();
        MenuItem copyMenuItem3 = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemCopy"));
        MenuItem markAllMenuItem3 = new MenuItem(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("menuItemMarkAll"));
        copyMenuItem3.setOnAction((ActionEvent event) -> {
            tableUtils3.copySelectionToClipboard(testStatisticsField);
        });
        markAllMenuItem3.setOnAction((ActionEvent event) -> {
            testStatisticsField.getSelectionModel().selectAll();
        });
        contextMenu3.getItems().addAll(copyMenuItem3, markAllMenuItem3);
        testStatisticsField.setContextMenu(contextMenu3);
    }
    
    private void writeStatisticsTable() {
        writeTableStatistisField();     // statistics for table
        writeReviewStatisticsField();    // review statistics
        writeTestStatisticsField();     // test statistics
    }
    
    private void writeTableStatistisField() {
        tableStatisticsField.getItems().clear();
        StatisticsDataModel s0 = new StatisticsDataModel();
        s0.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statNumberOfPages"));
        s0.setCount(Integer.toString(statistics.getNumberOfPages()));
        tableStatisticsField.getItems().add(s0);

        StatisticsDataModel s1 = new StatisticsDataModel();
        s1.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statNumberOfEntries"));
        s1.setCount(Integer.toString(statistics.getNumberOfEntries()));
        tableStatisticsField.getItems().add(s1);

        StatisticsDataModel s2 = new StatisticsDataModel();
        s2.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statNumberOfFiles"));
        s2.setCount(Integer.toString(statistics.getNumberOfFiles()));
        tableStatisticsField.getItems().add(s2);

        StatisticsDataModel s3 = new StatisticsDataModel();
        s3.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statNumberOfChangedFiles"));
        s3.setCount(Integer.toString(statistics.getNumberOfChangedFiles()));
        tableStatisticsField.getItems().add(s3);

        if (statistics.getNumberOfUnmodifiedFiles() != 0) {
            StatisticsDataModel s4 = new StatisticsDataModel();
            s4.setData(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("statNumberOfUnmodifiedFiles"));
            s4.setCount(Integer.toString(statistics.getNumberOfUnmodifiedFiles()));
            tableStatisticsField.getItems().add(s4);
        }

        if (statistics.getNumberOfDublicatedFiles() != 0) {
            StatisticsDataModel s5 = new StatisticsDataModel();
            s5.setData(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("statNumberOfDublicatedFiles"));
            s5.setCount(Integer.toString(statistics.getNumberOfDublicatedFiles()));
            tableStatisticsField.getItems().add(s5);        
        }
        
        if (statistics.getNumberOfDeletedFiles() != 0) {
            StatisticsDataModel s6 = new StatisticsDataModel();
            s6.setData(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("statNumberOfDeletedFiles"));
            s6.setCount(Integer.toString(statistics.getNumberOfDeletedFiles()));
            tableStatisticsField.getItems().add(s6);
        }
        
        if (statistics.getNumberOfNewFiles() != 0) {
            StatisticsDataModel s7 = new StatisticsDataModel();
            s7.setData(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("statNumberOfNewFiles"));
            s7.setCount(Integer.toString(statistics.getNumberOfNewFiles()));
            tableStatisticsField.getItems().add(s7);
        }
        
        StatisticsDataModel s8 = new StatisticsDataModel();
        s8.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statNumberOfChanges"));
        s8.setCount(Integer.toString(statistics.getNumberOfChanges()));
        tableStatisticsField.getItems().add(s8);

        if (statistics.getNumberOfHiddenLines() != 0) {
            StatisticsDataModel s9 = new StatisticsDataModel();
            s9.setData(ResourceBundle.
                    getBundle("lxrdifferencetable/Bundle", locale).
                    getString("statNumberOfHiddenLines"));
            s9.setCount(Integer.toString(statistics.getNumberOfHiddenLines()));
            tableStatisticsField.getItems().add(s9);
        }
    }
    
    private void writeReviewStatisticsField() {
        reviewStatisticsField.getItems().clear();
        StatisticsDataModel s1 = new StatisticsDataModel();
        s1.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statReviewPASSED"));
        s1.setCount(Integer.toString(statistics.getReviewPassed()));
        reviewStatisticsField.getItems().add(s1);
        
        StatisticsDataModel s2 = new StatisticsDataModel();
        s2.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statReviewFAILED"));
        s2.setCount(Integer.toString(statistics.getReviewFailed()));
        reviewStatisticsField.getItems().add(s2);
        
        StatisticsDataModel s3 = new StatisticsDataModel();
        s3.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statReviewCOMMENT"));
        s3.setCount(Integer.toString(statistics.getReviewComment()));
        reviewStatisticsField.getItems().add(s3);
        
        StatisticsDataModel s4 = new StatisticsDataModel();
        s4.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statReviewINCONCLUSIVE"));
        s4.setCount(Integer.toString(statistics.getReviewInconclusive()));
        reviewStatisticsField.getItems().add(s4);
        
        StatisticsDataModel s5 = new StatisticsDataModel();
        s5.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statReviewOPEN"));
        s5.setCount(Integer.toString(statistics.getReviewOpen()));
        reviewStatisticsField.getItems().add(s5);
        
        StatisticsDataModel s6 = new StatisticsDataModel();
        s6.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statReviewEmpty"));
        s6.setCount(Integer.toString(statistics.getReviewEmpty()));
        reviewStatisticsField.getItems().add(s6);

        StatisticsDataModel s7 = new StatisticsDataModel();
        s7.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statReviewNotApplicable"));
        s7.setCount(Integer.toString(statistics.getReviewNotApplicable()));
        reviewStatisticsField.getItems().add(s7);
    }
    
    private void writeTestStatisticsField () {
        testStatisticsField.getItems().clear();
        StatisticsDataModel s1 = new StatisticsDataModel();
        s1.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statTestPASSED"));
        s1.setCount(Integer.toString(statistics.getTestPassed()));
        testStatisticsField.getItems().add(s1);
        
        StatisticsDataModel s2 = new StatisticsDataModel();
        s2.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statTestFAILED"));
        s2.setCount(Integer.toString(statistics.getTestFailed()));
        testStatisticsField.getItems().add(s2);
        
        StatisticsDataModel s3 = new StatisticsDataModel();
        s3.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statTestCOMMENT"));
        s3.setCount(Integer.toString(statistics.getTestComment()));
        testStatisticsField.getItems().add(s3);
        
        StatisticsDataModel s4 = new StatisticsDataModel();
        s4.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statTestINCONCLUSIVE"));
        s4.setCount(Integer.toString(statistics.getTestInconclusive()));
        testStatisticsField.getItems().add(s4);
        
        StatisticsDataModel s5 = new StatisticsDataModel();
        s5.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statTestOPEN"));
        s5.setCount(Integer.toString(statistics.getTestOpen()));
        testStatisticsField.getItems().add(s5);
        
        StatisticsDataModel s6 = new StatisticsDataModel();
        s6.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statTestEmpty"));
        s6.setCount(Integer.toString(statistics.getTestEmpty()));
        testStatisticsField.getItems().add(s6);

        StatisticsDataModel s7 = new StatisticsDataModel();
        s7.setData(ResourceBundle.
                getBundle("lxrdifferencetable/Bundle", locale).
                getString("statTestNotApplicable"));
        s7.setCount(Integer.toString(statistics.getTestNotApplicable()));
        testStatisticsField.getItems().add(s7);
    }
    
    public void setActiveReviewDescriptionController (int pos) {
        activeReviewDescriptionController = pos;
    }

    public int getActiveReviewDescriptionController () {
        return activeReviewDescriptionController;
    }

    public void setActiveTestDescriptionController (int pos) {
        activeTestDescriptionController = pos;
    }

    public int getActiveTestDescriptionController () {
        return activeTestDescriptionController;
    }
    
    private String evaluateTitle(File file) {
        String tmp = file.getPath();
        String rep = tmp.replace("\\", "/");
        String[] split = rep.split("/");
        String title = split[0];
        return title;
    }
    
    private void helperReviewState(int fileNr, int pos, ComboBox reviewStateCol) {
        while (reviewStateColList.size() < fileNr) {
            reviewStateColList.add(new ArrayList<>());
            reviewStateColBooleanList.add(new ArrayList<>());
        }
        if (reviewStateColList.get(fileNr-1).size() <= pos) {
            while (reviewStateColList.get(fileNr-1).size() <= pos) {
                reviewStateColList.get(fileNr-1).add(reviewStateCol);
                reviewStateColBooleanList.get(fileNr-1).add(false);
            }
        } else {
            reviewStateColList.get(fileNr-1).set(pos, reviewStateCol);
            reviewStateColBooleanList.get(fileNr-1).set(pos, false);
        }
    }
    
    private void helperTestState(int fileNr, int pos, ComboBox testStateCol) {
        while (testStateColList.size() < fileNr) {
            testStateColList.add(new ArrayList<>());
            testStateColBooleanList.add(new ArrayList<>());
        }
        if (testStateColList.get(fileNr-1).size() <= pos) {
            while (testStateColList.get(fileNr-1).size() <= pos) {
                testStateColList.get(fileNr-1).add(testStateCol);
                testStateColBooleanList.get(fileNr-1).add(false);
            }
        } else {
            testStateColList.get(fileNr-1).set(pos, testStateCol);
            testStateColBooleanList.get(fileNr-1).set(pos, false);
        }
    }
    
    private void helperReviewShortDescription(int fileNr, int pos, 
            TextField reviewShortDescriptionCol) {
        while (reviewShortDescriptionColList.size() < fileNr) {
            reviewShortDescriptionColList.add(new ArrayList<>());
            reviewShortDescriptionColBooleanList.add(new ArrayList<>());
        }
        if (reviewShortDescriptionColList.get(fileNr-1).size() <= pos) {
            while (reviewShortDescriptionColList.get(fileNr-1).size() <= pos) {
                reviewShortDescriptionColList.get(fileNr-1).add(reviewShortDescriptionCol);
                reviewShortDescriptionColBooleanList.get(fileNr-1).add(false);
            }
        } else {
            reviewShortDescriptionColList.get(fileNr-1).set(pos, reviewShortDescriptionCol);
            reviewShortDescriptionColBooleanList.get(fileNr-1).set(pos, false);
        }
    }
    
    private void helperTestShortDescription(int fileNr, int pos, 
            TextField testShortDescriptionCol) {
        while (testShortDescriptionColList.size() < fileNr) {
            testShortDescriptionColList.add(new ArrayList<>());
            testShortDescriptionColBooleanList.add(new ArrayList<>());
        }
        if (testShortDescriptionColList.get(fileNr-1).size() <= pos) {
            while (testShortDescriptionColList.get(fileNr-1).size() <= pos) {
                testShortDescriptionColList.get(fileNr-1).add(testShortDescriptionCol);
                testShortDescriptionColBooleanList.get(fileNr-1).add(false);
            }
        } else {
            testShortDescriptionColList.get(fileNr-1).set(pos, testShortDescriptionCol);
            testShortDescriptionColBooleanList.get(fileNr-1).set(pos, false);
        }
    }
    
    private String truncateLine (String line, int number) {
            String result = "";
            String[] lines = line.split("\n");
            if (lines.length > number) {
                int i = 0;
                for (String str : lines) {
                    result = result + str + "\n";
                    if (i == number) {
                        break;
                    }
                    i++;
                }
                result = result + "\n\n[" + 
                        ResourceBundle.getBundle("lxrdifferencetable/Bundle", locale).
                        getString("truncateSvnLine") + "]";
            } else {
                result = line;
            }
        return result;
    }
    
    private static class StateListCell extends ListCell<State> {
        
        private final ImageView view;
        StateListCell() {
            view = new ImageView();
        }

        @Override
        protected void updateItem(State item, boolean empty) {
            super.updateItem(item, empty);

            if (item == null || empty) {
                setGraphic(null);
                setText(null);
            } else {
                view.setImage(item.getIcon());
                setGraphic(view);
                setText(item.getName());
            }
        }
    }

    public class PartedTableTask extends Task<Integer> {
        private final int tableNumber;
        private final boolean computeStatistics;

        public PartedTableTask(int tableNumber, boolean computeStatistics) {
            this.tableNumber = tableNumber;
            this.computeStatistics = computeStatistics;
        }

        @Override
        protected Integer call() throws InterruptedException {
            try {
                switch (tableNumber) {
                    case 0:
                        tableField.getItems().clear();      // clear content
                        break;
                    case 1:
                        tableField1.getItems().clear();     // clear content
                        break;
                    case 2:
                        tableField2.getItems().clear();     // clear content
                        break;
                    case 3:
                        tableField3.getItems().clear();     // clear content
                        break;
                    case 4:
                        tableField4.getItems().clear();     // clear content
                        break;
                    default:
                        break;
                }

                int begin;
                int end =  0;
                // compute number of skiped files
                for (int i = 0; i < activeRegion + tableNumber; i++) {
                    end = end + partedFileList.get(i).size();
                }
                int region = activeRegion + tableNumber;
                begin = end + 1;
                end = begin + partedFileList.get(region).size()-1;

                String title;
                // title = begin + " ... " + end;  // e.g. "40 ... 44"
                title = partedFileList.get(region).get(0).getFileCount() + 
                        " ... " + 
                        partedFileList.get(region).get(partedFileList.get(region)
                                .size()-1).getFileCount();
                
                titleField[tableNumber] = title;
                String tabTooltip;
                switch (tableNumber) {
                    case 0:
                        writeTestTable(tableField, region, computeStatistics,
                                null, null);
                        finishedRunLater = false;
                        Platform.runLater(() -> {
                            tabTest0.setText(title);
                            tableField.refresh();
                            finishedRunLater = true;
                        });
                        
                        while (!finishedRunLater) {
                            Thread.sleep(100);
                        }

                        tabTooltip = getTabTooltip(partedFileList
                                .get(region), end - begin + 1,
                                tableField.getItems().size());
                        tabTestTooltip0.setText(tabTooltip);
                        break;
                    case 1:
                        writeTestTable(tableField1, region, computeStatistics,
                                null, null);
                        finishedRunLater = false;
                        Platform.runLater(() -> {
                            tabTest1.setText(title);
                            tableField1.refresh();
                            finishedRunLater = true;
                        });

                        while (!finishedRunLater) {
                            Thread.sleep(100);
                        }

                        tabTooltip = getTabTooltip(partedFileList
                                .get(region), end - begin + 1,
                                tableField1.getItems().size());
                        tabTestTooltip1.setText(tabTooltip);
                        break;
                    case 2:
                        writeTestTable(tableField2, region, computeStatistics,
                                null, null);
                        finishedRunLater = false;
                        Platform.runLater(() -> {
                            tabTest2.setText(title);
                            tableField2.refresh();
                            finishedRunLater = true;
                        });

                        while (!finishedRunLater) {
                            Thread.sleep(100);
                        }

                        tabTooltip = getTabTooltip(partedFileList
                                .get(region), end - begin + 1,
                                tableField2.getItems().size());
                        tabTestTooltip2.setText(tabTooltip);
                        break;
                    case 3:
                        writeTestTable(tableField3, region, computeStatistics,
                                null, null);
                        finishedRunLater = false;
                        Platform.runLater(() -> {
                            tabTest3.setText(title);
                            tableField3.refresh();
                            finishedRunLater = true;
                        });

                        while (!finishedRunLater) {
                            Thread.sleep(100);
                        }

                        tabTooltip = getTabTooltip(partedFileList
                                .get(region), end - begin + 1,
                                tableField3.getItems().size());
                        tabTestTooltip3.setText(tabTooltip);
                        break;
                    case 4:
                        writeTestTable(tableField4, region, computeStatistics,
                                null, null);
                        finishedRunLater = false;
                        Platform.runLater(() -> {
                            tabTest4.setText(title);
                            tableField4.refresh();
                            finishedRunLater = true;
                        });

                        while (!finishedRunLater) {
                            Thread.sleep(100);
                        }

                        tabTooltip = getTabTooltip(partedFileList
                                .get(region), end - begin + 1,
                                tableField4.getItems().size());
                        tabTestTooltip4.setText(tabTooltip);
                        break;
                    default:
                        break;
                }

            } catch (Exception ex) {
                // do nothing
            }

            // clean up task an and end
            taskField[tableNumber] = true;
            
            if (isJump) {
                jump(jumpFileCount, jumpLineNr);
                isJump = false;
            }
            return 0;
        }
    }


    private String getTabTooltip (List<TableEntry> partedFileList,
            int numberOfFiles, int numberOfTableEntries) {
        HashMap<String, Integer> map = new HashMap<>();
        List<String> dirList = new ArrayList<>();
        int i = 0;
        for (TableEntry e : partedFileList) {
            String hrefTextLast = e.getHrefTextLast(0);
            String fileString = hrefTextLast.replace("\\", File.separator);
            File file = new File (fileString);
            String parent = file.getParent();
            
            if (map.get(parent) == null) {
                map.put(parent, i);
                dirList.add(parent);
            }
            i++;
        }

        String text = ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("numberOfFiles") + " = " 
                + numberOfFiles + "\n";
        text = text + ResourceBundle.
                            getBundle("lxrdifferencetable/Bundle", locale).
                            getString("numberOfTableEntries") + " = " 
                + numberOfTableEntries + "\n";
        for (String str : dirList) {
            text = text + str + "\n";
        }
        
        return text;
    }    
    
    private String getTooltipMenuItems (int from, int to, 
            List<List<TableEntry>> partedFileList) {
        HashMap<String, Integer> map = new HashMap<>();
        List<String> dirList = new ArrayList<>();
        for (int i = from; i < to; i++) {
            List<TableEntry> list = partedFileList.get(i);

            int j = 0;
            for (TableEntry e : list) {
                String hrefTextLast = e.getHrefTextLast(0);
                String fileString = hrefTextLast.replace("\\", File.separator);
                File file = new File (fileString);
                String parent = file.getParent();

                if (map.get(parent) == null) {
                    map.put(parent, j);
                    dirList.add(parent);
                }
                j++;
            }
        }

        String text = "";
        int count = 0;
        for (String str : dirList) {
            if (count >= 29) {
                text = text + "       " + "..." + "\n";
                break;
            }
            text = text + "       " + str + "\n";
            count++;
        }
        
        return text;
    }    
    
    public boolean isMaximizedScreen() {
        return maximizedScreen;
    }


    /**
     * Private member function to handle the caption dialog.
     */
    private void dialogCaption () {
        FXMLLoader loader;
        loader = myController.getLoader(CAPTION_RENAME_STAGE);
        dialogRenameCaptionController = loader.
                <DialogRenameCaptionController>getController();

        XmlHandling xml = lxrDifferenceTableController.getXmlHandling();
        if (xml != null) {
            LockType captionLocked = xml.isCaptionLocked();
            if (captionLocked.equals(LockType.locked)) {
                labelCaption.setStyle("-fx-background-color: yellow");
                labelCaption.setText(configEntry.getCaptionLast());

                dialogRenameCaptionController.setCaptionName(labelCaption
                        .getText());
                dialogRenameCaptionController.setTableEntry(configEntry);
                dialogRenameCaptionController.setLockedByUser(
                        configEntry.getCaptionAuthorLast());
                dialogRenameCaptionController.setEditable(false);
            } else {
                labelCaption.setStyle("");
                labelCaption.setText(configEntry.getCaptionLast());

                dialogRenameCaptionController.setCaptionName(labelCaption
                        .getText());
                dialogRenameCaptionController.setTableEntry(configEntry);
                dialogRenameCaptionController.setLockedByUser(
                        configEntry.getCaptionAuthorLast());
                dialogRenameCaptionController.setEditable(true);
            }
        } else {
            dialogRenameCaptionController.setCaptionName(labelCaption
                    .getText());
            dialogRenameCaptionController.setTableEntry(configEntry);
            dialogRenameCaptionController.setLockedByUser(
                    configEntry.getCaptionAuthorLast());
            dialogRenameCaptionController.setEditable(true);
        }
        myController.showStage(CAPTION_RENAME_STAGE);
    }

        /**
     * Private member function to handle the svn history dialog.
     */

    private void dialogSvnHistory (int tableNr, TableEntry entry) {
        tableField.getSelectionModel().select(tableNr);

        FXMLLoader loader;
        DialogSvnHistoryController dialogSvnHistoryController;

        loader = myController.getLoader(SVN_HISTORY_STAGE);
        dialogSvnHistoryController = loader.
                <DialogSvnHistoryController>getController();
        dialogSvnHistoryController.setTable(entry);
        myController.showStage(SVN_HISTORY_STAGE);
    }
    
    public List<List<TableEntry>> getPartList() {
        List<TableEntry> subList = new ArrayList<>();
        partedFileList.clear();
        Integer count = 0;      // counter
        Integer i = 0;          // from sublist position (inclusive!)
        Integer j = 0;          // to sublist position (exclusive!)
        try {
            for (TableEntry entry : changedFileList) {
                boolean completlyHidden = true;
                for (int k = 0; k < entry.getHref().size(); k++) {
                    if (!isRowHidden(entry, k)) {
                        count++;
                        completlyHidden = false;
                    } else {
                        statistics.incrementNumberOfHiddenLines();
                    }
                }
                if (!completlyHidden) {
                    subList.add(entry);
                } else {
                    // do nothing
                }
                if (count <= MAX) {
                    // count = count + entry.getHref().size();
                } else {
                    partedFileList.add(subList);
                    subList = new ArrayList<>();
                    count = 0;
                    i = j + 1;
                }
                j++;
            }
            if (count != 0) {
                partedFileList.add(subList);
            }
        } catch (Exception ex) {
            // do nothing!
        }
        return partedFileList;
    }

    public void setPartList(List<List<TableEntry>> partList) {
        this.partedFileList = partList;
    }

    public void setHideDublicatedFiles(boolean hideDublicatedFiles) {
        this.hideDublicatedFiles = hideDublicatedFiles;
    }
    
    public void setHideDeletedFiles(boolean hideDeletedFiles) {
        this.hideDeletedFiles = hideDeletedFiles;
    }

    public void setHideNewFiles(boolean hideNewFiles) {
        this.hideNewFiles = hideNewFiles;
    }
    
    public void setSelectedTab() {
        if (tabTest0.isSelected()) {
            selectedTab = SelectedTab.tab0;
        }
        if (tabTest1.isSelected()) {
            selectedTab = SelectedTab.tab1;
        }
        if (tabTest2.isSelected()) {
            selectedTab = SelectedTab.tab2;
        }
        if (tabTest3.isSelected()) {
            selectedTab = SelectedTab.tab3;
        }
        if (tabTest4.isSelected()) {
            selectedTab = SelectedTab.tab4;
        }
        if (tabStatistics.isSelected()) {
            selectedTab = SelectedTab.tabStatistics;
        }
    }
    
    public class TableUtils {
        
        List<TableEntry> changedFileList;
        
        public TableUtils(List<TableEntry> changedFileList) {
            this.numberFormatter = NumberFormat.getNumberInstance();
            this.changedFileList = changedFileList;
        }
        
	private final NumberFormat numberFormatter;


	/**
	 * Install the keyboard handler:
	 *   + CTRL + C = copy to clipboard
	 *   + CTRL + V = paste from clipboard
	 * @param table
	 */
	public void installCopyPasteHandler(TableView<?> table) {

	    // install copy/paste keyboard handler
	    table.setOnKeyPressed(new TableKeyEventHandler());

	}

	/**
	 * Copy/Paste keyboard event handler.
	 * The handler uses the keyEvent's source for the clipboard data. 
         * The source must be of type TableView.
	 */
	public  class TableKeyEventHandler implements EventHandler<KeyEvent> {

	    KeyCodeCombination copyKeyCodeCompination = 
                    new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
	    KeyCodeCombination pasteKeyCodeCompination = 
                    new KeyCodeCombination(KeyCode.V, KeyCombination.CONTROL_ANY);

            @Override
	    public void handle(final KeyEvent keyEvent) {
		if (copyKeyCodeCompination.match(keyEvent)) {
		    if( keyEvent.getSource() instanceof TableView) {
			// copy to clipboard
			copySelectionToClipboard( (TableView<?>) keyEvent.getSource());
			// event is handled, consume it
			keyEvent.consume();
		    }
		} 
		else if (pasteKeyCodeCompination.match(keyEvent)) {
		    if( keyEvent.getSource() instanceof TableView) {
			// copy to clipboard
			pasteFromClipboard( (TableView<?>) keyEvent.getSource());
			// event is handled, consume it
			keyEvent.consume();
		    }
		} 
	    }
	}

	/**
	 * Get table selection and copy it to the clipboard.
	 * @param table
	 */
	public void copySelectionToClipboard(TableView<?> table) {
	    StringBuilder clipboardString = new StringBuilder();
	    ObservableList<TablePosition> positionList = 
                    table.getSelectionModel().getSelectedCells();

            int prevRow = -1;
	    for (TablePosition position : positionList) {
		int row = position.getRow();
		int col = position.getColumn();

		// determine whether we advance in a row (tab) or a column
		// (newline).
		if (prevRow == row) {
		    clipboardString.append('\t');
		} else if (prevRow != -1) {
		    clipboardString.append('\n');
		}

                if (col == -1) {
                    col = 0;
                }
		// create string from cell
		String text = "";
		Object observableValue = 
                        (Object) table.getColumns().get(col).getCellObservableValue( row);

		// null-check: provide empty string for nulls
		if (observableValue == null) {
		    text = "";
		}
		else if( observableValue instanceof DoubleProperty) {
		    text = numberFormatter.format( ((DoubleProperty) observableValue).get());
		}
		else if( observableValue instanceof IntegerProperty) { 
		    text = numberFormatter.format( ((IntegerProperty) observableValue).get());
		}			    	
		else if( observableValue instanceof StringProperty) { 
		    text = ((StringProperty) observableValue).get();
		}
		else {
                    // normal text
                    if (col == 1) {
                        String fileString;
                        int nr;
                        fileString = table.getColumns().get(0).getCellData(row)
                                .toString();
                        nr = Integer.parseInt(fileString);
                        String lineNumber;
                        lineNumber = table.getColumns().get(2).getCellData(row)
                                .toString();
                        nr--;
                        List<List<String>> href = changedFileList.get(nr).getHref();
                        
                        int i = 0;
                        boolean empty = false;
                        String lb;
                        while (!empty) {
                            lb = changedFileList.get(nr).getLineNumber(i).get(0);
                            if (lb.isEmpty()) {
                                empty = true;
                            } else if (lb.equals(lineNumber)) {
                                text = changedFileList.get(nr).getHref(i).get(0);
                                break;
                            } else if (lb.equals("dublicatedFile")) {
                                text = changedFileList.get(nr).getHref(i).get(0);
                                break;
                            } else if (lb.equals("fileDeleted")) {
                                text = changedFileList.get(nr).getHref(i).get(0);
                                break;
                            } else if (lb.equals("newFile")) {
                                text = changedFileList.get(nr).getHref(i).get(0);
                                break;
                            } else if (lb.equals("notChanged")) {
                                text = changedFileList.get(nr).getHref(i).get(0);
                                break;
                            }
                            i++;
                        }
                    } else {
                        Class<?> aClass = table.getColumns().get(col).getCellData(row).getClass();
                        if (aClass.equals( (new ComboBox()).getClass())) {
                            // get text from state-combobox
                            ComboBox<State> c = (ComboBox<State>) (table.getColumns().get(col).getCellData(row));
                            int index = c.getSelectionModel().getSelectedIndex();
                            text = c.getItems().get(index).toString();
                        } else if (aClass.equals( (new TextField()).getClass())) {
                            // get text from state-combobox
                            TextField c = (TextField) (table.getColumns().get(col).getCellData(row));
                            text = c.getText();
                        } else {
                            // normal text
                            text = table.getColumns().get(col).getCellData(row).toString();
                        }
                    }
                    if (text.contains("\n")) {
                        // surround text by double quotes
                        // text = text.replace("\n", "</br>");
                        text = "\"" + text + "\"";
                    }
		}
		// add new item to clipboard
		clipboardString.append(text);
		// remember previous
		prevRow = row;
	    }
	    // create clipboard content
	    final ClipboardContent clipboardContent = new ClipboardContent();
	    clipboardContent.putString(clipboardString.toString());
	    // set clipboard content
	    Clipboard.getSystemClipboard().setContent(clipboardContent);
	}

	public void pasteFromClipboard( TableView<?> table) {
	    // abort if there's not cell selected to start with
	    if( table.getSelectionModel().getSelectedCells().isEmpty()) {
		return;
	    }
	    // get the cell position to start with
	    TablePosition pasteCellPosition = table.getSelectionModel()
                    .getSelectedCells().get(0);
	    String pasteString = Clipboard.getSystemClipboard().getString();
	    int rowClipboard = -1;
	    StringTokenizer rowTokenizer = 
                    new StringTokenizer( pasteString, "\n");
	    while( rowTokenizer.hasMoreTokens()) {
		rowClipboard++;
		String rowString = rowTokenizer.nextToken();
		StringTokenizer columnTokenizer = 
                        new StringTokenizer( rowString, "\t");
		int colClipboard = -1;
		while( columnTokenizer.hasMoreTokens()) {
		    colClipboard++;
		    // get next cell data from clipboard
		    String clipboardCellContent = columnTokenizer.nextToken();
		    // calculate the position in the table cell
		    int rowTable = pasteCellPosition.getRow() + rowClipboard;
		    int colTable = pasteCellPosition.getColumn() + colClipboard;

		    // skip if we reached the end of the table
		    if( rowTable >= table.getItems().size()) {
			continue;
		    }
		    if( colTable >= table.getColumns().size()) {
			continue;
		    }

		    // get cell
		    TableColumn tableColumn = table.getColumns().get(colTable);
		    ObservableValue observableValue = tableColumn
                            .getCellObservableValue(rowTable);

		    // TODO: handle boolean, etc
		    if( observableValue instanceof DoubleProperty) { 
			try {
			    double value = numberFormatter
                                    .parse(clipboardCellContent).doubleValue();
			    ((DoubleProperty) observableValue).set(value);
			} catch (ParseException e) {
                            logger.info("LxrTestTableController: " + 
                                    "TODO: Should be removed!");
			}
		    }
		    else if( observableValue instanceof IntegerProperty) { 
			try {
			    int value = NumberFormat.getInstance()
                                    .parse(clipboardCellContent).intValue();
			    ((IntegerProperty) observableValue).set(value);
			} catch (ParseException e) {
                            logger.info("LxrTestTableController: " + 
                                    "TODO: Should be removed!");
			}
		    }			    	
		    else if( observableValue instanceof StringProperty) { 
			((StringProperty) observableValue).set(clipboardCellContent);
		    } else {
			System.out.println("Unsupported observable value: " + 
                                observableValue);
		    }
		    System.out.println(rowTable + "/" + colTable);
		}
	    }
	}
    }
}
