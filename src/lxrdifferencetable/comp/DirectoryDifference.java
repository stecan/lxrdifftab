/*
 * Copyright (C) 2015 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.comp;

import java.io.BufferedReader;
import lxrdifferencetable.data.DirEntry;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import lxrdifferencetable.data.LogEntry;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.Utils;

/**
 * This class compares two directories and generates a
 * list of all changed files.
 * 
 * @author Stefan Canali
 */
public class DirectoryDifference {
    
    private final Utils utils;
    private final List<DirEntry> dirList;   // list of changed files
    private int iterator;
    private final Properties props;
    
    private final List<DirEntry> repoList;        // list of all repositories
    private final List<String> repoExtensions;    // e.g. "cvs", "svn", "git"
    private final List<LogEntry> repoLog;         // collection of all log messages
    
    public DirectoryDifference() {
        props = new Properties();
        this.utils = new Utils();
        this.dirList = new ArrayList<>();
        iterator = 0;
        
        repoList = new ArrayList<>();
        repoExtensions = new ArrayList<>();
        repoLog = new ArrayList<>();
        
        String list = props.getProperty(props.REPOLIST);
        String[] data = list.split(",");
        repoExtensions.addAll(Arrays.asList(data));
    }
    
    /**
     * This method returns the next element in the generated list
     * of changed files.
     * 
     * @return  next element
     */
    public DirEntry next() {
        DirEntry entry = dirList.get(iterator);
        iterator++;
        return entry;
    }
    
    /**
     * This method resets the list to the first changed file.
     * 
     */
    public void setToFirstElement() {
        iterator = 0;
    }
    
    /**
     * This method returns the number of changed files.
     * 
     * @return number of changed files
     */
    public int size () {
        return dirList.size();
    }
            
    /**
     * Methode: createFileList
     * 
     * Description: 
     * This method recursively compares two directories (or files) 
     * on file level (not on content level).
     * 
     * @param file1
     * @param file2
     */
    public void createFileList (File file1, File file2) {

        File[] listOfFiles1, listOfFiles2;
        
        try {
            // Handle the two files
            // (There are 4 cases that has to be precessed)
            if ((file1 == null) && (file2 == null)) {               // (case 1)
                // do nothing! 
            } else if ((file1 == null) && (file2 != null)) {        // (case 2)
                if (!utils.blacklist(file2)) {
                    if (file2.isFile()) {
                        dirList.add(new DirEntry(file1, file2));
                    } else {
                        listOfFiles2 = resort(file2.listFiles());
                        for (File file : listOfFiles2) {
                            createFileList(null, file);
                        }
                    }
                } 
            } else if ((file1 != null) && (file2 == null)) {        // (case 3)
                if (!utils.blacklist(file1)) {
                    if (file1.isFile()) {
                        dirList.add(new DirEntry(file1, file2));
                    } else {
                        listOfFiles1 = resort(file1.listFiles());
                        for (File file : listOfFiles1) {
                            createFileList(file, null);
                        }
                    }
                } 
            } else if ((file1 != null) && (file2 != null)) {        // (case 4)
                // scan for repo
                String name = file1.getName();
                for (String ext : repoExtensions) {
                    if (name.equals("." + ext)) {
                        if (file2.getName().equals("." + ext)) {
                            DirEntry dirEntry = new DirEntry(file1, file2);
                            repoList.add(dirEntry);
                        }
                    }
                }
                if ((!utils.blacklist(file1)) || (!utils.blacklist(file2))) {
                    if ((file1.isFile()) && (file2.isFile())) {
                        dirList.add(new DirEntry(file1, file2));
                    } else if ((file1.isFile()) && (file2.isDirectory())) {
                        // there is someting wrong!
                    } else if ((file1.isDirectory()) && (file2.isFile())) {
                        // there is someting wrong!
                    } else if ((file1.isDirectory()) && (file2.isDirectory())) {
                        listOfFiles1 = resort(file1.listFiles());
                        listOfFiles2 = resort(file2.listFiles());

                        // handle dirctories
                        Hashtable<String, File> allFiles1 = new Hashtable<>();
                        Hashtable<String, File> allFiles2 = new Hashtable<>();

                        for (File file : listOfFiles1) {
                            if (file != null) {
                                allFiles1.put(file.getName(), file);
                            }
                        }
                        for (File file : listOfFiles2) {
                            if (file != null) {
                                allFiles2.put(file.getName(), file);
                            }
                        }

                        // first loop
                        for (File file : listOfFiles1) {
                            if (file != null) {
                                String filename = file.getName();
                                if (allFiles2.containsKey(filename)) {
                                    createFileList(allFiles1.get(filename), 
                                            allFiles2.get(filename));
                                } else {
                                    createFileList(allFiles1.get(filename), null);
                                }
                            }
                        }
                        // second loop (do for all other files)
                        for (File file : listOfFiles2) {
                            if (file != null) {
                                String filename = file.getName();
                                if (allFiles1.containsKey(filename)) {
                                    // do nothing!
                                } else {
                                    createFileList(null, allFiles2.get(filename));
                                }
                            }
                        }
                    }
                } else {
                    // blacklisted file: Do nothing!
                }
            }
        } catch (Exception ex) {
            // there is something wrong!
            LogEntry logEntry = new LogEntry("log.fileName", 
                    ex.getLocalizedMessage(), "", "", "");
            repoLog.add(logEntry);
            
        }
    }
    
    /**
     * Methode: fileToLines
     * 
     * Description: 
     * This method reads the contents of a text file and writes 
     * it into a list.
     * 
     * @param file
     * @return          content of the file as a list
     */
    public List<String> fileToLines(File file) {
        List<String> lines;
        String line;
        BufferedReader br;
        
        lines = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
               lines.add(line);
            }
        } catch (IOException ex) {
            // there is something wrong!
            lines.add("");
            LogEntry logEntry = new LogEntry("log.fileName", 
                    ex.getLocalizedMessage(), "", "", "");
            repoLog.add(logEntry);
        }

        return lines;
    }
    
    /**
     * This method returns the list of all available repositories.
     * 
     * @return  list of repositories
     */
    public List<DirEntry> getRepoList () {
        return repoList;
    }
    
    /**
     * This method returns all log messages collected during a repo scan.
     * 
     * @return  repoLog     all log messages collected during repo scan
     */
    public List<LogEntry> getRepoLog() {
        return repoLog;
    }
    
    /*
     * Private method to resort the "listOfFiles". 
     * 
     * The "listOfFiles" is resorted by
     *    1. files (alphabetically sorted)
     *    2. directories (alphabetically sorted)
     * 
    */
    private File[] resort(File[] listOfFiles) {
        List<File> files = new ArrayList<>();   // normal files
        List<File> dirs = new ArrayList<>();    // directories
        
        // get files
        for (File entry : listOfFiles) {
            if (entry.isFile()) {
                files.add(entry);
            }
        }
        // get directories
        for (File entry : listOfFiles) {
            if (entry.isDirectory()) {
                dirs.add(entry);
            }
        }
        // sort files and directories
        files.sort((Object f1, Object f2) -> {
            String fileName1 = ((File) f1).getName();
            String fileName2 = ((File) f2).getName();
            
            return fileName1.compareToIgnoreCase(fileName2);
        });
        dirs.sort((Object f1, Object f2) -> {
            String fileName1 = ((File) f1).getName();
            String fileName2 = ((File) f2).getName();
            
            return fileName1.compareToIgnoreCase(fileName2);
        });

        File[] resortedList= new File[listOfFiles.length];
        int i = 0;
        for (File f : files) {
            resortedList[i] = f;
            i++;
        }
        for (File d : dirs) {
            resortedList[i] = d;
            i++;
        }
        return resortedList;
    }
}
