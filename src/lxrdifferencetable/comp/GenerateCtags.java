/*
 * Copyright (C) 2021 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.comp;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import lxrdifferencetable.LxrDifferenceTableController;
import lxrdifferencetable.ProgressController;
import lxrdifferencetable.StagesController;
import lxrdifferencetable.comp.Generate.State;
import lxrdifferencetable.tools.Properties;
import lxrdifferencetable.tools.SetProgress;
import lxrdifferencetable.tools.WorkingDirectory;
import org.apache.commons.io.FileUtils;

/**
 * This class generates static content from the web addresses given 
 * in "outList" (variable "href").
 * 
 * @author Stefan Canali
 */
public class GenerateCtags {
   
    static final Logger logger = Logger.getLogger(
            LxrDifferenceTableController.class.getName());
    
    private StagesController myController;
    private final Properties props;
   
    private final SetProgress setProgress;      // sets progress bar
    
    // controller for updating the progress bar
    private final ProgressController progressController;
    
    private final File workingDir;
    private String ctags;
    private boolean localContent;
    private boolean ctagsAvailable;
    
    File reviewedSources1;
    File reviewedSources2;
    
    /**
     * Constructor
     * 
     * @param ctrl
     * @param outList
     */
    public GenerateCtags (ProgressController ctrl, StagesController stageParent){
        myController = stageParent;
        setProgress = new SetProgress(ctrl);
        progressController = ctrl;
        this.props = new Properties();
        String str = props.getProperty(props.EXTERNALTOOL_CTAGS);
        if ("".equals(str)) {
            // path to ctags not set
            ctags = "ctags";
        } else {
            ctags = str;
        }
        str = props.getProperty(props.LOCAL_GENERATED_VIEW);
        if ("".equals(str)) {
            // path to ctags not set
            localContent = false;
        } else if (str.equalsIgnoreCase("false")) {
            localContent = false;
        } else if (str.equalsIgnoreCase("true")) {
            localContent = true;
        } else {
            localContent = false;
        }
        
        try {
            Runtime.getRuntime().exec(ctags);
            ctagsAvailable = true;
        } catch (IOException | NumberFormatException ex) {
            ctagsAvailable = false;
        }
        
        WorkingDirectory dir = new WorkingDirectory(myController);
        workingDir = dir.getWorkingDir();
    }
    
    /**
     * This method scans "outList" with "ctags" to get the static content given
     * by "href".
     * 
     * @param progressController
     * @return
     */
    public State generateCtagsFile (File dir1, File dir2) {
        
        State state = State.PASSED;
        String command;
        

        if (!ctagsAvailable) {
            // ctags is not available. Break loop!
            state = State.ABORTED_NO_REPO;
        } else if (!localContent) {
            // static content should not be created.df  Break loop!
            state = State.ABORTED_NO_REPO;
        } else {

        }
        
        if (progressController.getAbortState()) {
            state = State.ABORTED;
        }
        setProgress.setProgressFourthPass(1,4); // update progress bar
        copyReviewedSources(workingDir, dir1, dir2);
        setProgress.setProgressFourthPass(2,4); // update progress bar
       
        String[] extensions = new String[] { "c", "cpp", "h", "hpp", "cs", 
            "java", "js", "lua", "pl", "php", "tcl", "tk", "asm", "awk", "bas",
            "cob", "bat", "eif", "flex", "erl", "html", "lisp", "el", "mak", 
            "pas", "py", "sh", "tex", "vhdl", "yac", "yacc" 
        };

        boolean error = false;
        List<File> files1 = (List<File>) FileUtils.listFiles(reviewedSources1, extensions, true);
        for (File file : files1) {
            if (progressController.getAbortState()) {
                state = State.ABORTED;
                break;
            }
            command = ctags  + " -a --excmd=n -f " + workingDir + 
                    File.separator + "ReviewedSources" + File.separator + 
                    "tags1 " + file.getPath();
            try {
                Process exec = Runtime.getRuntime().exec(command, null, 
                        workingDir);
                boolean ok = exec.waitFor(100, TimeUnit.MILLISECONDS);
                if (!ok) {
                    logger.warning(
                            "Ctags: Timeout occured on file: " + file.getPath());
                    state = State.TIMEOUT;
                }
                if (exec.exitValue() != 0) {
                    Platform.runLater(() -> {
                        Alert alert = new Alert(AlertType.ERROR);
                        alert.setTitle("Ctags");
                        alert.setHeaderText("Something went wrong on exectution of ctags");
                        alert.setContentText("Do you use ctags-excuberant?");

                        alert.showAndWait();                
                    }); 
                    error = true;
                    state = State.CTAGS_ERROR;
                    break;
                }
            } catch (IOException ex) {
                logger.warning(
                        "IOException on ctags!");
            } catch (InterruptedException ex) {
                logger.warning(
                        "InterruptedException on ctags!");
            }            
        }
        setProgress.setProgressFourthPass(3,4); // update progress bar

        if (!error) {
            List<File> files2 = (List<File>) FileUtils.listFiles(reviewedSources2, extensions, true);
            for (File file : files2) {
                if (progressController.getAbortState()) {
                    state = State.ABORTED;
                    break;
                }
                command = ctags  + " -a --excmd=n -f " + workingDir + 
                        File.separator + "ReviewedSources" + File.separator + 
                        "tags2 " + file.getPath();
                try {
                    Process exec = Runtime.getRuntime().exec(command, null, 
                            workingDir);
                    boolean ok = exec.waitFor(100, TimeUnit.MILLISECONDS);
                    if (!ok) {
                        logger.warning(
                                "Ctags: Timeout occured on file: " + file.getPath());
                        state = State.TIMEOUT;
                    }
                } catch (IOException ex) {
                    logger.warning(
                            "IOException on ctags!");
                } catch (InterruptedException ex) {
                    logger.warning(
                            "InterruptedException on ctags!");
                }            
            }
        }
        setProgress.setProgressFourthPass(4,4); // update progress bar
        
        return state;
    }
    
    /**
     * Copies file1 and file2 to ReviewedSources in workingDir
     * 
     * @param workingDir    working directory
     * @param file1         sources directory 1
     * @param file2         sources directory 2
     */
    private void copyReviewedSources(File workingDir, File file1, File file2) {
        // copy sources to directory "ReviewedSources"
        if ((file1.exists()) && (file2.exists())) {
            File reviewedSources = new File(workingDir + File.separator + 
                    "ReviewedSources");
            if (reviewedSources.exists()) {
                // purge old sources
                try {
                    FileUtils.deleteDirectory(reviewedSources);
                } catch (IOException ex) {
                    logger.warning("Directory " + reviewedSources + 
                            "could not be deleted!");
                }
            }
            reviewedSources.mkdir();    // create source directory
            reviewedSources1 = new File(workingDir + File.separator + 
                    "ReviewedSources" + File.separator + file1.getName());
            reviewedSources2 = new File(workingDir + File.separator + 
                    "ReviewedSources" + File.separator + file2.getName());
            try {
                copyDirectory(file1.getPath(), reviewedSources1.getPath());
            } catch (IOException ex) {
                logger.warning("Directory " + file1.getPath() + 
                        "could not be copied to " + 
                        reviewedSources1.getPath());
            }
            try {
                copyDirectory(file2.getPath(), reviewedSources2.getPath());
            } catch (IOException ex) {
                logger.warning("Directory " + file2.getPath() + 
                        "could not be copied to " + 
                        reviewedSources2.getPath());
            }
        } else {
            // do nothing (because a minimum of one directory is null)
        }
    }
    
    private void copyDirectory(String sourceDirectoryLocation, String destinationDirectoryLocation) 
      throws IOException {
        Files.walk(Paths.get(sourceDirectoryLocation))
          .forEach(source -> {
              Path destination = Paths.get(destinationDirectoryLocation, source.toString()
                .substring(sourceDirectoryLocation.length()));
              try {
                  Files.copy(source, destination);
              } catch (IOException e) {
                    logger.warning("Directory " + sourceDirectoryLocation + 
                            "could not be copied to " + 
                            destinationDirectoryLocation);
              }
          });
    }     
    
}
