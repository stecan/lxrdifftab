/*
 * Copyright (C) 2016 Stefan Canali
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package lxrdifferencetable.comp;

import lxrdifferencetable.tools.Properties;
import difflib.Delta;
import difflib.DiffUtils;
import difflib.Patch;
import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import lxrdifferencetable.ProgressController;
import lxrdifferencetable.data.DirEntry;
import lxrdifferencetable.data.LogEntry;
import lxrdifferencetable.data.TableEntry;
import lxrdifferencetable.normaldialogs.DialogSvnAuthenticationController;
import lxrdifferencetable.tools.GitEntry;
import lxrdifferencetable.tools.SetProgress;
import lxrdifferencetable.tools.SvnEntry;
import lxrdifferencetable.tools.Utils;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author Stefan Canali
 */
public class Generate {
    
    public enum State 
        {ABORTED,                               // user break, exit
        ABORTED_DURING_REPO_SCANNING,           // user break, do not exit 
        ABORTED_NO_REPO,                        // user break, do not exit, 
                                                // show warning message
        TIMEOUT,
        NO_LXR_SERVER,                          // LXR webserver not available
        CTAGS_ERROR,                            // wrong ctags program
        PASSED,                                 // it's all right
        CONTINUING};                            // after correct authentication,
                                                // try it again

    private State state;
    private final Utils utils;
    private final SetProgress setProgress;      // sets progress bar
    private List<TableEntry> outList;           // list of changed files
    private int diffSize;
    private List<DirEntry> repoList;            // list of all repositories
    private ListIterator<DirEntry> listIterator;
    private List<LogEntry> repoLog;
    private HashMap<String, Integer> map;

    boolean lineNumberAvailable;                // Are line numbers available?
    boolean review;                             // Is a review in progress?
    boolean continuing;
    
    private final Properties props;
    
    // controller for updating the progress bar
    private final ProgressController progressController;
    
    private DirectoryDifference dirDiff;
    
    private int repoCount;
    private String repoLast;
    
    // helper member variables for scanning of dublicated files
    private final HashMap<String, Integer> changedFilesMap;
    private final List<List<Integer>> refs;
    private final List<List<Integer>> comps;

    private GitEntry gitEntry;
    private SvnEntry svnEntry;
    
    /**
     * Constructor
     * 
     * @param ctrl      Controller for updating the progress shown to the user
     */
    public Generate(ProgressController ctrl) {
        this.state = State.PASSED;
        repoCount = 0;
        repoLast = "";
        props = new Properties();
        utils = new Utils();
        outList = new ArrayList<>();
        repoList = new ArrayList<>();
        repoLog = new ArrayList<>();
        listIterator = null;
        progressController = ctrl;
        diffSize = 0;
        lineNumberAvailable = false;
        setProgress = new SetProgress(ctrl);
        review = false;
        continuing = false;

        changedFilesMap = new HashMap<>();
        refs = new ArrayList<>();
        comps = new ArrayList<>();
        map = null;
    }
    
    /**
     * Initializes the list of changed files
     * 
     * Note:
     * This method computes the costs of the operation
     * of comparing two different directories "file1" and
     * "file2". 
     * 
     * @param file1
     * @param file2
     */
    public void init (File file1, File file2) {
        dirDiff = new DirectoryDifference();
        dirDiff.createFileList(file1, file2);   // create list of changed files
        repoList = dirDiff.getRepoList();       // get repos
        diffSize = dirDiff.size();
        repoLog = dirDiff.getRepoLog();         // initialize with logs of dirDiff
        review = Boolean.valueOf(props.getProperty(props.REVIEW));
    }
    
    
    /**
     * Method:  firstPass
     * 
     * This method creates a list of all changed files and writes it to the
     * outList.
     * 
     * Prerequisites:
     * The methode "init" should be called first!
     * 
     * Internal Use:
     * outList
     * 
     * @return state    - PASSED    -> it's gone ok
     *                  - ABORTED   -> interrupted by user
     */
    public State firstPass() {
        DirEntry dirEntry;
        File file1;
        File file2;
        state = State.PASSED;     // set default state
        
        List<String> LinesOfFile1;         // original file as list
        List<String> LinesOfFile2;         // comparable file as list
        List<Delta> deltaList;          // deltas
        int number = 0;
        
        for (int i = 0; i < dirDiff.size(); i++) {

            if (progressController.getAbortState()) {
                state = State.ABORTED;
                break;
            }
            setProgress.setProgressFirstPass(i,diffSize); // update progress bar

            dirEntry = dirDiff.next();                    // get next element
            file1 = dirEntry.Dir1();
            file2 = dirEntry.Dir2();
            
            if ((file1 == null) && (file2 == null)) {           // case 1
                // something is wrong!
            } else if ((file1 != null) && (file2 == null)) {    // case 2
                if (!utils.blacklist(file1)) {
                    if (file1.isDirectory()) {
                        TableEntry entry = new TableEntry();
                        number++;
                        entry.putFileCount("" + number);
                        entry.putFile(file1.toString());
                        // no line number available, print message instead.
                        lineNumberAvailable = true;
                        entry.putLineNumber("directoryDeleted");
                        entry.putDiff(Boolean.toString(false));
                        outList.add(entry);
                    } else {
                        TableEntry entry = new TableEntry();
                        number++;
                        entry.putFileCount("" + number);
                        entry.putFile(file1.toString());
                        // no line number available, print message instead.
                        lineNumberAvailable = true;
                        entry.putLineNumber("fileDeleted");
                        entry.putDiff(Boolean.toString(false));
                        outList.add(entry);
                    }
                }
            } else if ((file1 == null) && (file2 != null)) {    // case 3
                if (!utils.blacklist(file2)) {
                    if (file2.isDirectory()) {
                        TableEntry entry = new TableEntry();
                        number++;
                        entry.putFileCount("" + number);
                        entry.putFile(file2.toString());
                        // no line number available, print message instead.
                        lineNumberAvailable = true;
                        entry.putLineNumber("newDirectory");
                        entry.putDiff(Boolean.toString(false));
                        outList.add(entry);
                    } else {
                        TableEntry entry = new TableEntry();
                        number++;
                        entry.putFileCount("" + number);
                        entry.putFile(file2.toString());
                        // no line number available, print message instead.
                        lineNumberAvailable = true;
                        entry.putLineNumber("newFile");
                        entry.putDiff(Boolean.toString(false));
                        outList.add(entry);
                    }
                }
            } else if ((file1 != null) && (file2 != null)) {    // case 4
                if ((file1.isDirectory()) && (file2.isDirectory())) {   // case 4.1
                    // do nothing (directories are not printed!)
                } else if ((file1.isDirectory()) && (file2.isFile())) { // case 4.2
                    // something is wrong! (do not print!)
                } else if ((file1.isFile()) && (file2.isDirectory())) { // case 4.3
                    // something is wrong! (do not print!)                    
                } else if ((file1.isFile()) && (file2.isFile())) {      // case 4.4
                    if (!utils.blacklist(file1)) {
                        LinesOfFile1 = dirDiff.fileToLines(file1);
                        LinesOfFile2 = dirDiff.fileToLines(file2);

                        // Compute diff. Get the Patch object. 
                        // Patch is the container for computed deltas.
                        //
                        // Note: 
                        // The routine "diff" is long running (e.g. many seconds) 
                        // in case of big files with many modifications!
                        Patch patch = DiffUtils.diff(LinesOfFile1, LinesOfFile2);

                        deltaList = patch.getDeltas();
                        if (deltaList.isEmpty()) {
                            // The files are identical!
                            if (review) {
                                // if an review is in progress, then the
                                // actual file is printed.
                                number++;
                                TableEntry entry = new TableEntry();
                                entry.putFileCount("" + number);
                                entry.putDiff(Boolean.toString(false));
                                entry.putFile(file2.toString());
                                outList.add(entry);
                            }
                        } else {
                            number++;
                            TableEntry entry = new TableEntry();
                            entry.putFileCount("" + number);
                            entry.putDiff(Boolean.toString(true));
                            int j = 0;
                            for (Delta delta: patch.getDeltas()) {
                                if (progressController.getAbortState()) {
                                    state = State.ABORTED;
                                    break;
                                }
                                entry.putFile(file2.toString());
                                lineNumberAvailable = true;
                                entry.putLineNumber(j, 
                                        deltaToPosition(delta,
                                                LinesOfFile1.size(), 
                                                LinesOfFile2.size()));
                                if (isComment(delta, file1)) {
                                    entry.putReviewState(j, 
                                            TableEntry.State.COMMENT);
                                    entry.putReviewResAuthor(j, 
                                            System.getProperty("user.name"));
                                    entry.putReviewResDate(j, 
                                            ZonedDateTime.now().
                                            format(DateTimeFormatter
                                                    .RFC_1123_DATE_TIME));
                                    entry.putReviewShortDescription(j, 
                                            "automaticComment");
                                    entry.putReviewDescription(j, 
                                            "");
                                    entry.putReviewDescAuthor(j, 
                                            System.getProperty("user.name"));
                                    entry.putReviewDescDate(j, 
                                            ZonedDateTime.now().
                                            format(DateTimeFormatter
                                                    .RFC_1123_DATE_TIME));
                                    entry.putTestState(j, 
                                            TableEntry.State.COMMENT);
                                    entry.putTestResAuthor(j, 
                                            System.getProperty("user.name"));
                                    entry.putTestResDate(j, 
                                            ZonedDateTime.now().
                                            format(DateTimeFormatter
                                                    .RFC_1123_DATE_TIME));
                                    entry.putTestShortDescription(j, 
                                            "automaticComment");
                                    entry.putTestDescription(j, 
                                            "");
                                    entry.putTestDescAuthor(j, 
                                            System.getProperty("user.name"));
                                    entry.putTestDescDate(j, 
                                            ZonedDateTime.now().
                                            format(DateTimeFormatter
                                                    .RFC_1123_DATE_TIME));
                                }
                                j++;
                            }
                            outList.add(entry);
                            
                            // scanning for dublicated files
                            String name = file1.getName();
                            Integer value = changedFilesMap.get(name);
                            if (value == null) {
                                changedFilesMap.put(name, changedFilesMap
                                        .size());
                                
                                List<Integer> e = new ArrayList<>();
                                e.add(outList.size() - 1);
                                refs.add(e);    // pointer to outList

                                List<Integer> e1 = new ArrayList<>();
                                e1.add(outList.size() - 1);
                                comps.add(e1);   // equals to itself
                            } else {
                                // add duplicte file to list
                                int lastOutList = outList.size() - 1;
                                refs.get(value).add(lastOutList);
                                equality(value, 
                                        new File (outList.get(lastOutList)
                                                .getFile()));
                            }
                        }
                    }
                }
            }
        }
        
        // write dublicates to outlist
        for (int i = 0; i < refs.size(); i++) {
            List<Integer> r = refs.get(i);
            // refs[0]  -> 0 = 1962     comp[0] -> 0 = 1962 ==> base / no other files!
            //                                  -> write nothing
            //          -> 1 = 1987             -> 1 = 1974 ==> other files available!
            //                                  -> write!
            //          -> 2 = 1974             -> 2 = 1974 ==> base
            //                                  -> write nothing!
            if (r.size() > 1) {
                for (int j = 1; j < r.size();j++) {
                    Integer num1 = r.get(j);
                    Integer num2 = comps.get(i).get(j);
                    if (num1.equals(num2)) {
                        // do nothing
                    } else {
                        outList.get(num1).putDublicateEntry(num2);
                        // - clear line numbering!
                        // - mark line as "dublicatedFile"
                        // - clear eventually set review and test state
                        //   (it makes no sense to mark a line as dublicate 
                        //    and set the whole file as COMMENT!)
                        outList.get(num1).clearLineNumber();
                        outList.get(num1).clearReviewResAuthor();
                        outList.get(num1).clearReviewResDate();
                        outList.get(num1).clearReviewState();

                        outList.get(num1).clearReviewDescAuthor();
                        outList.get(num1).clearReviewDescDate();
                        outList.get(num1).clearReviewDescription();
                        outList.get(num1).clearReviewShortDescription();

                        outList.get(num1).clearTestResAuthor();
                        outList.get(num1).clearTestResDate();
                        outList.get(num1).clearTestState();

                        outList.get(num1).clearTestDescAuthor();
                        outList.get(num1).clearTestDescDate();
                        outList.get(num1).clearTestDescription();
                        outList.get(num1).clearTestShortDescription();

                        outList.get(num1).putLineNumber("dublicatedFile");

                        ///////
                        
                        outList.get(num1).putReviewState(0, 
                                TableEntry.State.NOTAPPLICABLE);
                        outList.get(num1).putReviewResAuthor(0, 
                                System.getProperty("user.name"));
                        outList.get(num1).putReviewResDate(0, 
                                ZonedDateTime.now().
                                format(DateTimeFormatter.RFC_1123_DATE_TIME));
                        outList.get(num1).putReviewShortDescription(0, 
                                "automaticDublicate");
                        outList.get(num1).putReviewDescription(0, 
                                "");
                        outList.get(num1).putReviewDescAuthor(0, 
                                System.getProperty("user.name"));
                        outList.get(num1).putReviewDescDate(0, 
                                ZonedDateTime.now().
                                format(DateTimeFormatter.RFC_1123_DATE_TIME));
                        outList.get(num1).putTestState(0, 
                                TableEntry.State.NOTAPPLICABLE);
                        outList.get(num1).putTestResAuthor(0, 
                                System.getProperty("user.name"));
                        outList.get(num1).putTestResDate(0, 
                                ZonedDateTime.now().
                                format(DateTimeFormatter.RFC_1123_DATE_TIME));
                        outList.get(num1).putTestShortDescription(0, 
                                "automaticDublicate");
                        outList.get(num1).putTestDescription(0, 
                                "");
                        outList.get(num1).putTestDescAuthor(0, 
                                System.getProperty("user.name"));
                        outList.get(num1).putTestDescDate(0, 
                                ZonedDateTime.now().
                                format(DateTimeFormatter.RFC_1123_DATE_TIME));
                    }
                }
            }
        }
        // show blacklisted file extensions and files to repolog
        String[] activeBlacklist = utils.getActiveBlacklist();
        if (activeBlacklist.length == 0) {
            LogEntry log = new LogEntry();
            log.setDescription("log.blacklist");
            log.setMessage1(" - ");    // no blacklisted file extensions
            repoLog.add(log);
        } else {
            for (String b : activeBlacklist) {
                LogEntry log = new LogEntry();
                log.setDescription("log.blacklist");
                log.setMessage1(b);
                repoLog.add(log);
            }
        }
        String[] activeBlacklistFiles = utils.getActiveBlacklistFiles();
        if (activeBlacklistFiles.length == 0) {
            LogEntry log = new LogEntry();
            log.setDescription("log.blacklistFiles");
            log.setMessage1(" - ");    // no blacklisted files
            repoLog.add(log);
        } else {
            for (String b : activeBlacklistFiles) {
                LogEntry log = new LogEntry();
                log.setDescription("log.blacklistFiles");
                log.setMessage1(b);
                repoLog.add(log);
            }
        }
        
        return state;
    }

    /**
     * Method:  secondPass
     * 
     * This routine parses the outList of the firstPass and 
     * adds all relevant information (e.g. hyperlink, ...)
     * 
     * @return State
     */
    public State secondPass() {
        state = State.PASSED;     // set default state
        
        File file;          // e.g. /home/mike/compare/V1.2/test.cpp
        String linkFile;    // e.g. V1.2/test.cpp
        String lxrLink;     // LXR Link Property
        String lxrDiff;     // LXR Diff Property
        String lxrLine;     // lxr line number Proberty
        String formatVersion; 
        String formatDiffVal;
        boolean diff;
        String link;
        String lineNumber;
        String version;
        
        lxrLink = props.getProperty(props.LXRLINK);
        lxrDiff = props.getProperty(props.LXRDIFF);
        lxrLine = props.getProperty(props.FORMATLINENR);
        formatVersion = props.getProperty(props.FORMATVERSION);
        formatDiffVal = props.getProperty(props.FORMATDIFFVAL);
        
        int count = 0;
        for (TableEntry entry : outList) {
            count++;
            if (progressController.getAbortState()) {
                state = State.ABORTED;
                break;
            }
            
            // update progress bar
            setProgress.setProgressSecondPass(count,outList.size());   
            
            // do for all changed files
            file =  new File(entry.getFile());
            
            diff = Boolean.valueOf(entry.getDiff());
            version = utils.getVersion(file);
            
            if ((review) && (entry.getLineNumber().isEmpty())) {
                entry.putLineNumber("notChanged");
            }
            for (int i = 0; i < entry.getLineNumber().size(); i++) {
                linkFile = utils.leaf(file).toString();
                lineNumber = entry.getLineNumberLast(i);

                if (lxrLink.endsWith("/")) {
                    // do nothing
                } else {
                    lxrLink = lxrLink + "/";
                }
                if (diff) {
                    link = lxrLink + "diff/" +  
                            linkFile.replace("\\", "/") + 
                            lxrDiff + lxrLine + lineNumber;
                } else {
                    if (entry.getLineNumberLast(0).equals("fileDeleted")) {
                        link = lxrLink + "source/" + 
                                linkFile.replace("\\", "/") + 
                                "?" + formatVersion + version;
                    } else if (entry.getLineNumberLast(0).equals("newFile")) {
                        link = lxrLink + "source/" + 
                                linkFile.replace("\\", "/") + 
                                "?" + formatVersion + version;
                    } else {
                        link = lxrLink + "source/" + 
                                linkFile.replace("\\", "/") + 
                                "?" + formatDiffVal + version;
                    }
                }
                entry.putHref(link);
                entry.putHrefText(utils.leaf(file).toString());
            }
        }
        return state;
    }
    
    /**
     * This routine scans the given repositories in "repoList".
     * 
     * For authenication purposes, the routine is exited. After authentication,
     * the routine is called again. Though, some variables are hold globally
     * in the inner of the class.
     * 
     * @param account       user account for svn repository
     * @param password      user password
     * @param dialogSvnAuthenticationController     authenitcation dialog
     * @return              State.CONTINUING)
     *                      State.ABORTED
     *                      State.ABORTED_DURING_REPO_SCANNING
     *                      State.PASSED
     *                      State.ABORTED_NO_REPO
     * 
     * @throws IOException  
     * @throws GitAPIException
     */
    public State repoPass(String account, String password,
            DialogSvnAuthenticationController dialogSvnAuthenticationController) 
            throws IOException {
        
        if (state.equals(State.CONTINUING)) {
            continuing = true;
            // try last repo again!
            state = svnEntry.init(account, password, progressController,
                    dialogSvnAuthenticationController);
            if (state == State.ABORTED) {
                // The scanning of the git repository is aborted, but 
                // the difference table is OK. Though, go on to display 
                // the difference table.
                progressController.setAbortState(false);
                state = State.ABORTED_DURING_REPO_SCANNING;
            } 
        } else {
            listIterator = repoList.listIterator();
            repoCount = 0;
            repoLast = "";
            state = State.PASSED;
            continuing = false;
        }
        int i = 0;
        while (listIterator.hasNext() && ((state.equals(State.PASSED)) || 
                (state.equals(State.ABORTED_NO_REPO)))) {
            i++;
            setProgress.setProgressThirdPass(i,repoList.size());
            DirEntry next;
            String name;
            if (continuing) {
                repoCount--;
                next = listIterator.previous();
                name = next.Dir1().getName();
                continuing = false;
            } else {
                repoCount++;
                next = listIterator.next();
                name = next.Dir1().getName();
            }
            switch (name) {
                case ".svn":
                    LogEntry logEntry;
                    svnEntry = new SvnEntry(next.Dir1(), next.Dir2(), outList, map);
                    map = svnEntry.getMap();
                    String tmpRepo = svnEntry.getRepo();
                    String tmpRepo2 = svnEntry.getRepo2();
                    if (!continuing) {
                        if (!tmpRepo.contains(tmpRepo2)) {
                            // An error occured. The two repos are different!
                            logEntry = new LogEntry("log.repositoryMismatch", 
                                    "Dir 1 = " + next.Dir1().toString(),
                                    "Dir 2 = " + next.Dir2().toString(),
                                    "Repo 1 = " + tmpRepo, 
                                    "Repo 2 = " + tmpRepo2);
                            repoLog.add(logEntry);
                        } else {
                            logEntry = new LogEntry("log.repositoryName", 
                                    "Dir 1 = " + next.Dir1().toString(), 
                                    "Dir 2 = " + next.Dir2().toString(), 
                                    "Repo 1 = " + tmpRepo,
                                    "Repo 2 = " + tmpRepo2);
                            repoLog.add(logEntry);
                        }
                        logEntry = new LogEntry("log.repositoryRevisions", 
                                "Revision 1 = " + svnEntry.getRevision1(),
                                "Revision 2 = " + svnEntry.getRevision2(),
                                "", "");
                        repoLog.add(logEntry);
                    }
                    if (repoLast.equals(tmpRepo)) {
                        // do nothing
                    } else {
                        repoLast = tmpRepo;
                        state = svnEntry.init(account, password, 
                                progressController,
                                dialogSvnAuthenticationController);
                        if (state == State.ABORTED) {
                            // The scanning of the git repository is aborted,
                            // but the difference table is OK. Though, go on 
                            // to display the difference table.
                            progressController.setAbortState(false);
                            state = State.ABORTED_DURING_REPO_SCANNING;
                            logEntry = new LogEntry("log.scanAborted", "", "", "", "");
                            repoLog.add(logEntry);
                        } 
                    }
                    break;
                case ".git":
                    logEntry = new LogEntry("log.repositoryName", 
                            "Dir 1 = " + next.Dir1().toString(), 
                            "Dir 2 = " + next.Dir2().toString(), 
                            "",
                            "");
                    repoLog.add(logEntry);
                    gitEntry = new GitEntry(next.Dir1(), next.Dir2(), outList);
                    state = gitEntry.init(progressController);
                    if (state == State.ABORTED) {
                        // The scanning of the git repository is aborted, but 
                        // the difference table is OK. Though, go on to display 
                        // the difference table.
                        progressController.setAbortState(false);
                        state = State.ABORTED_DURING_REPO_SCANNING;
                        logEntry = new LogEntry("log.scanAborted", "", "", "", "");
                        repoLog.add(logEntry);
                    } else if (state == State.ABORTED_NO_REPO) {
                        logEntry = new LogEntry("log.noGit", "", "", "", "");
                        repoLog.add(logEntry);
                    }
                    break;
                default:
                    break;
            }
        }
        return state;
    }
    
    
    /**
     * This method generates a LXR line number, that is composed of 
     * four digits.
     * 
     * Example
     *      delta           = "... position: 3..."
     *      returned string = "0003"
     * 
     * 
     * @param   delta
     * @return
     */
    private String deltaToPosition (Delta delta, int size1, int size2) {
        // adjust line numbering scheme to LXR  ("+1")
        int position;
        int position1 = delta.getOriginal().getPosition() + 1;
        if (position1 > size1) {
            position1 = size1 + 1;      // restrict to last line number
        }
        int position2 = delta.getRevised().getPosition() + 1;
        if (position2 > size2) {
            position2 = size2 + 1;      // restrict to last line number
        }
        
        position = position1;       // new style positioning according local 
                                    // generated view
        
        // Old LXR-style positioning
//        if (position1 < position2) {
//            position = position2;
//        } else {
//            position = position1;
//        }
        String lineNumber = "" + position;
        
        if (props.getProperty(props.FORMATLINENR).equals("#")) {
            // normal lxr-mode
            switch (lineNumber.length()) {
                case 0:
                    lineNumber = "0000";
                    break;
                case 1:
                    lineNumber = "000" + lineNumber;
                    break;
                case 2:
                    lineNumber = "00" + lineNumber;
                    break;
                case 3:
                    lineNumber = "0" + lineNumber;
                    break;
                default:
                    // The format is correct. Nothing has to be done!
                    break;
            }
        } else {
            // http://lxr.free-electrons.com mode
        }
        
        return lineNumber;
    }
    
    public List<TableEntry> getOutList() {
        return outList;
    }

    public void setOutList(List<TableEntry> list) {
        outList = list;
    }
    
    public boolean getLineNumberAvailable () {
        return lineNumberAvailable;
    }

    public int numberOfChangedFiles () {
        return changedFilesMap.size();
    }
    
    private void equality (int value, File file) {
        for (int i : refs.get(value)) {
            List<String> lines1 = dirDiff.fileToLines(new File (outList.get(i)
                    .getFile()));
            List<String> lines2 = dirDiff.fileToLines(file);
            // Compute diff. Get the Patch object. 
            // Patch is the container for computed deltas.
            //
            // Note: 
            // The routine "diff" is long running (e.g. many seconds) 
            // in case of big files with many modifications!
            Patch patch = DiffUtils.diff(lines1, lines2);

            List<Delta> deltas = patch.getDeltas();
            if (deltas.isEmpty()) {
                comps.get(value).add(i);
                break;
            }
        }
    }
    
    private boolean isComment (Delta delta, File file) {
        boolean comment = true;
        
        String extension = FilenameUtils.getExtension("" + file);
        switch (extension) {
            case "c": case "h": case "cpp": case "hpp": case "java":
                List<String> lines1 = (List<String>) delta.getOriginal()
                        .getLines();
                for (String line : lines1) {
                    if (line.matches("^(\\s)*//.*")) {            // e.g "  //..."
                        // it's ok
                    } else if (line.matches("^(\\s)*(\\*).*")) {  // e.g "  *... "
                        // it's ok
                    } else if (line.matches("^(\\s)*/(\\*).*")) { // e.g " /*..."
                        // it's ok
                    } else {
                        comment = false;
                        break;
                    }
                }
                if (comment) {
                    List<String> lines2 = (List<String>) delta.getRevised()
                            .getLines();
                    for (String line : lines2) {
                        if (line.matches("^(\\s)*//.*")) {            // e.g "  //..."
                            // it's ok
                        } else if (line.matches("^(\\s)*(\\*).*")) {  // e.g "  *... "
                            // it's ok
                        } else if (line.matches("^(\\s)*/(\\*).*")) { // e.g " /*..."
                            // it's ok
                        } else {
                            comment = false;
                            break;
                        }
                    }
                }
                break;
            case "sh": case "bash": 
                List<String> lines3 = (List<String>) delta.getOriginal()
                        .getLines();
                for (String line : lines3) {
                    if (line.matches("^(\\s)*#.*")) {              // e.g "  #..."
                        // it's ok
                    } else {
                        comment = false;
                        break;
                    }
                }
                if (comment) {
                    List<String> lines4 = (List<String>) delta.getRevised()
                            .getLines();
                        for (String line : lines4) {
                        if (line.matches("^(\\s)*#.*")) {         // e.g "  #..."
                            // it's ok
                        } else {
                            comment = false;
                            break;
                        }
                    }
                }
                break;
            default:
                comment = false;
                break;
        }
        return comment;
    }
    
    /**
     * This method returns all log messages collected during a repo scan.
     * 
     * @return  repoLog     all log messages collected during repo scan
     */
    public List<LogEntry> getRepoLog() {
        return repoLog;
    }
    
}